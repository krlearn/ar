﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

using System.Collections;

[InitializeOnLoad]
public class VoidARSetupScript {
	static VoidARSetupScript()
	{
        //Debug.Log("Up and running");
        //GraphicsDeviceType[] apis_win = { GraphicsDeviceType.OpenGLCore, GraphicsDeviceType.Direct3D11, GraphicsDeviceType.Direct3D9 };
        //PlayerSettings.SetGraphicsAPIs ( BuildTarget.StandaloneWindows, apis_win );
        //PlayerSettings.SetUseDefaultGraphicsAPIs (BuildTarget.StandaloneWindows, false);

        if (EditorBuildSettings.scenes.Length == 0 )
        {
            EditorBuildSettings.scenes = new EditorBuildSettingsScene[]
            { new EditorBuildSettingsScene("Assets/VoidAR/Scene/AllDemo.unity", true),
              new EditorBuildSettingsScene("Assets/VoidAR/Scene/ImageDemo.unity", true),
              new EditorBuildSettingsScene("Assets/VoidAR/Scene/ShapeDemo.unity", true),
              new EditorBuildSettingsScene("Assets/VoidAR/Scene/VideoDemo.unity", true),
              new EditorBuildSettingsScene("Assets/VoidAR/Scene/CloudDemo.unity", true),
              new EditorBuildSettingsScene("Assets/VoidAR/Scene/DynamicLoadDemo.unity", true)};//场景

        }




        GraphicsDeviceType[] apis_ios = { GraphicsDeviceType.OpenGLES2, GraphicsDeviceType.Metal };
		PlayerSettings.SetGraphicsAPIs ( BuildTarget.iOS, apis_ios );
		PlayerSettings.SetUseDefaultGraphicsAPIs (BuildTarget.iOS, false);

		GraphicsDeviceType[] apis_android = { GraphicsDeviceType.OpenGLES2 };
		PlayerSettings.SetGraphicsAPIs ( BuildTarget.Android, apis_android );
		PlayerSettings.SetUseDefaultGraphicsAPIs (BuildTarget.Android, false);

		//GraphicsDeviceType[] apis_osx = { GraphicsDeviceType.OpenGLCore };
		//PlayerSettings.SetGraphicsAPIs ( BuildTarget.StandaloneOSXUniversal, apis_osx );
		//PlayerSettings.SetUseDefaultGraphicsAPIs (BuildTarget.StandaloneOSXUniversal, false);

	}
}

