using UnityEngine;
using System.Collections;
using UnityEditor;

public class BundleCreater : Editor
{
	
	[MenuItem("Custom Editor/Create AssetBundles")]
	static void CreateAssetBunldesMain ()
	{
		Object[] SelectedAsset = Selection.GetFiltered (typeof(Object), SelectionMode.DeepAssets);
 
		foreach (Object obj in SelectedAsset) 
		{
			string targetWinPath = Application.dataPath + "/StreamingAssets/windows/" + obj.name + ".assetbundle";
            if (BuildPipeline.BuildAssetBundle(obj, null, targetWinPath, BuildAssetBundleOptions.CollectDependencies))
            {
                Debug.Log(obj.name + "资源Windows制作成功");
			} 
			else 
			{
                Debug.Log(obj.name + "资源Windows制作失败");
			}


            string targetIosPath = Application.dataPath + "/StreamingAssets/ios/" + obj.name + ".assetbundle";
            if (BuildPipeline.BuildAssetBundle(obj, null, targetIosPath, BuildAssetBundleOptions.CollectDependencies, BuildTarget.iOS))
            {
                Debug.Log(obj.name + "资源Ios制作成功");
            }
            else
            {
                Debug.Log(obj.name + "资源Ios制作失败");
            }

            string targetAndroidPath = Application.dataPath + "/StreamingAssets/android/" + obj.name + ".assetbundle";
            if (BuildPipeline.BuildAssetBundle(obj, null, targetAndroidPath, BuildAssetBundleOptions.CollectDependencies, BuildTarget.Android))
            {
                Debug.Log(obj.name + "资源Android制作成功");
            }
            else
            {
                Debug.Log(obj.name + "资源Android制作失败");
            }


            string targetOsxPath = Application.dataPath + "/StreamingAssets/osx/" + obj.name + ".assetbundle";
            if (BuildPipeline.BuildAssetBundle(obj, null, targetOsxPath, BuildAssetBundleOptions.CollectDependencies, BuildTarget.StandaloneOSXUniversal))
            {
                Debug.Log(obj.name + "资源osx制作成功");
            }
            else
            {
                Debug.Log(obj.name + "资源osx制作失败");
            }


		}
		AssetDatabase.Refresh ();	
		
	}
}
