﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;


[CustomEditor(typeof(VoidARMain))]
[InitializeOnLoad]
public class VoidARMainEditor : Editor
{

#if UNITY_IPHONE
    const string dllName = "__Internal";
#elif UNITY_ANDROID
    const string dllName = "VoidAR-Plugin";
#else
    const string dllName = "VoidAR-Plugin";
#endif
    [DllImport(dllName)]
    private static extern void _getCameraList(byte[] names);

    // Use this for initialization
    VoidARMainEditor() {
    }

    public override void OnInspectorGUI()
    {
        VoidARMain obj = target as VoidARMain;

#if UNITY_IPHONE || UNITY_ANDROID
        string[] deviceNames = new string[2];
        deviceNames[0] = "后置摄像头";
        deviceNames[1] = "前置摄像头";
        obj.CameraIndex = EditorGUILayout.Popup("Camera", obj.CameraIndex, deviceNames);
        EditorUtility.SetDirty(target);
#else
        byte[] names = new byte[1024];
        _getCameraList(names);
        string result = System.Text.Encoding.UTF8.GetString(names);
        char[] delimiterChars = { ',' };
        string[] deviceNames = result.Split(delimiterChars);
        obj.CameraIndex = EditorGUILayout.Popup("Camera Device", obj.CameraIndex, deviceNames);
        EditorUtility.SetDirty(target);
#endif

        obj.ARCamera = (Camera)EditorGUILayout.ObjectField("AR Camera", obj.ARCamera, typeof(Camera));
        string[] markerTypes = { "Shape", "Image" };
        obj.markerType = (VoidARMain.EMarkerType)EditorGUILayout.Popup("MarkerType", (int)(obj.markerType), markerTypes );
        if ( obj.markerType == VoidARMain.EMarkerType.Shape )
        {
            obj.shapeMatchAccuracy = EditorGUILayout.IntField("Match Accuracy", obj.shapeMatchAccuracy);
            obj.tracking = EditorGUILayout.Toggle("Is Tracking", obj.tracking);
            obj.is_use_cloud = false;
        }
        else
        {
            obj.tracking = false;
            obj.shapeMatchAccuracy = 5;

            obj.is_use_cloud = EditorGUILayout.Toggle("Use Cloud", obj.is_use_cloud);
            if (obj.is_use_cloud)
            {
                obj.accessKey = EditorGUILayout.TextField("Access Key", obj.accessKey);
                obj.secretKey = EditorGUILayout.TextField("Secret Key", obj.secretKey);
                obj.UseExtensionTracking = false;
            }
            else
            {
                obj.UseExtensionTracking = EditorGUILayout.Toggle("Extension Tracking", obj.UseExtensionTracking);
            }
        }
        
    }

}
