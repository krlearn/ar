﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System;


[CustomEditor(typeof(Marker))] 
[InitializeOnLoad]
public class MarkerEditor : Editor {
	// Use this for initialization

	static Dictionary< string, Material > matMap = new Dictionary< string , Material >();
	static Shader shader = Shader.Find( "Unlit/Transparent" );

	//private static string currentScene;

	static MarkerEditor()
	{
		//currentScene = EditorApplication.currentScene;
		//EditorApplication.hierarchyWindowChanged += hierarchyWindowChanged;
		EditorApplication.update += update;
	}

	private static void update()
	{

		if (EditorApplication.isPlaying)
			return;

		GameObject[] gameObjects = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];
		foreach (GameObject obj in gameObjects)
		{
			Marker marker = obj.GetComponent<Marker>();
			if (marker != null )
			{
				Transform tran = obj.transform.FindChild ("MarkerQuad");
				if (tran) {
					GameObject quad = tran.gameObject; 
					quad.hideFlags = HideFlags.HideInHierarchy;
					quad.SetActive (true);
					Material mat = LoadPNG (Application.streamingAssetsPath + "/" + marker.imageFilePath);
					if (mat) {

                        float asp = 240.0f / 320.0f;

                        float ratio = (float)mat.mainTexture.height / (float)mat.mainTexture.width;

                        if ( ratio > 1.0f )
                        {
                            quad.transform.localScale = new Vector3(1.0f / ratio * asp, 1 * asp, 1);
                        }
                        else
                        {
                            quad.transform.localScale = new Vector3( 1, ratio, 1);
                        }
						Renderer render = quad.GetComponent<Renderer> ();
						render.sharedMaterial = mat;
						
					} else {
						quad.GetComponent<Renderer> ().sharedMaterial = null;
					}
				}
			}
		}

		/*foreach (GameObject obj in gameObjects) 
		{
			Transform tran = obj.transform.FindChild ("MarkerQuad");
			if (tran) 
			{
				for (int i = 0; i < obj.transform.childCount; i++) {
					Transform childTransform = obj.transform.GetChild (i);
					VideoBehaviour vt = childTransform.gameObject.GetComponent<VideoBehaviour> ();
					if (vt) {
						childTransform.localScale = tran.localScale;
					}
				}
			}
		}*/

	}
	private static void hierarchyWindowChanged()
	{
		/*if (currentScene != EditorApplication.currentScene)
		{
			//a scene change has happened
			Debug.Log("Last Scene: " + currentScene);
			currentScene = EditorApplication.currentScene;

			GameObject[] gameObjects = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];
			foreach (GameObject obj in gameObjects)
			{
				Marker marker = obj.GetComponent<Marker>();
				if (marker != null )
				{
					Transform tran = obj.transform.FindChild ("Quad");
					if (tran) {
						GameObject quad = tran.gameObject; 
						quad.SetActive (true);
						Material mat = LoadPNG (Application.streamingAssetsPath + "/" + marker.imageFilePath);
						if (mat) {
							float ratio = (float)mat.mainTexture.height / (float)mat.mainTexture.width;
							Renderer render = quad.GetComponent<Renderer> ();
							render.sharedMaterial = mat;
							quad.transform.localScale = new Vector3 (1, ratio, 1);
						} else {
							quad.GetComponent<Renderer> ().sharedMaterial = null;
						}
					}
				}
			}

		}*/
	}

	/*public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}*/

	public static Material LoadPNG(string filePath) {
		
		Material mat = null;
		byte[] fileData;

		if ( matMap.ContainsKey(filePath)) {
			return matMap[filePath];
		} else {
			if (File.Exists(filePath))     {
				fileData = File.ReadAllBytes(filePath);
				Texture2D tex = new Texture2D(2, 2);

				tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
				mat = new Material( shader );
				mat.mainTexture = tex;
				mat.hideFlags = HideFlags.DontSave;
				tex.hideFlags = HideFlags.DontSave;
				matMap[filePath] = mat;
			}
			return mat;
		}
	}

}
