﻿Shader "Unlit/YUV420"
{
	Properties
	{
		Yp ("Y channel texture", 2D) = "white" {}
		CbCr("CbCr channel texture", 2D) = "black" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D Yp;
			float4 Yp_ST;
			sampler2D CbCr;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, Yp);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{	
				float4 color ;

				float3 yuv = float3(  
		        1.1643 * (	tex2D(Yp, i.uv).r - 0.0625),  
				tex2D(CbCr, i.uv).a - 0.5,  
				tex2D(CbCr, i.uv).r - 0.5  
		    	);  


				float3x3 yuv2rgb = { 
								1, 0, 1.2802,  
							    1, -0.214821, -0.380589,  
							    1, 2.127982, 0  };


				float3 rgb = mul(yuv2rgb , yuv); 
    			color = float4(rgb, 1); 
				return color;
			}
			ENDCG
		}
		
		
		
	}
}
