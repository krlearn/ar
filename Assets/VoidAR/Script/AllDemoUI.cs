﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using UnityEngine.SceneManagement;

public class AllDemoUI : MonoBehaviour 
{
    GameObject imageDemo;
    GameObject shapeDemo;
    GameObject videoDemo;
    GameObject cloudDemo;
    GameObject dynamicLoadDemo;
    void Start() {

        imageDemo = GameObject.Find("ImageDemo");
        Button btn = imageDemo.GetComponent<Button>();
        btn.onClick.AddListener(delegate ()
        {
            this.OnClick(imageDemo);
        });

       shapeDemo = GameObject.Find("ShapeDemo");
        Button btn1 = shapeDemo.GetComponent<Button>();
        btn1.onClick.AddListener(delegate ()
        {
            this.OnClick(shapeDemo);
        });



        videoDemo = GameObject.Find("VideoDemo");
        Button btn2 = videoDemo.GetComponent<Button>();
        btn2.onClick.AddListener(delegate ()
        {
            this.OnClick(videoDemo);
        });


        cloudDemo = GameObject.Find("CloudDemo");
        Button btn3 = cloudDemo.GetComponent<Button>();
        btn3.onClick.AddListener(delegate ()
        {
            this.OnClick(cloudDemo);
        });

        dynamicLoadDemo = GameObject.Find("DynamicLoadDemo");
        Button btn4 = dynamicLoadDemo.GetComponent<Button>();
        btn4.onClick.AddListener(delegate ()
        {
            this.OnClick(dynamicLoadDemo);
        });

    }

    public void OnClick(GameObject sender)
    {
        if (sender == imageDemo )
        {
            Application.LoadLevel("ImageDemo");
        }

        if (sender == shapeDemo)
        {
            Application.LoadLevel("ShapeDemo");
        }

        if (sender == videoDemo)
        {
            Application.LoadLevel("VideoDemo");          
        }

        if (sender == cloudDemo)
        {
            Application.LoadLevel("CloudDemo");
        }

        if (sender == dynamicLoadDemo)
        {
            Application.LoadLevel("DynamicLoadDemo");
        }
    }
    
    // Update is called once per frame
    void Update () {
       
    }
}
