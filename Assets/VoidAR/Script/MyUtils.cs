﻿using UnityEngine;
using System.Collections;

public static class MyUtils{

    public static Quaternion GetRotation(this Matrix4x4 matrix)
    {
        Vector3 forward;
        forward.x = matrix.m02;
        forward.y = matrix.m12;
        forward.z = matrix.m22;

        Vector3 upwards;
        upwards.x = matrix.m01;
        upwards.y = matrix.m11;
        upwards.z = matrix.m21;

        return Quaternion.LookRotation(forward, upwards);
      }

    public static Vector3 GetPosition(this Matrix4x4 matrix)
    {
        var x = matrix.m03;
        var y = matrix.m13;
        var z = matrix.m23;

        return new Vector3(x, y, z);
    }

    public static Vector3 GetScale(this Matrix4x4 matrix)
    {
        Vector3 scale;
        scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
        scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
        scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
        return scale;
    }

    /// <summary>
    /// Gets the lookAt matrix.
    /// </summary>
    /// <returns>The look at matrix.</returns>
    /// <param name="pos">Position.</param>
    /// <param name="target">Target.</param>
    /// <param name="up">Up.</param>
    public static Matrix4x4 getLookAtMatrix(Vector3 pos, Vector3 target, Vector3 up)
    {

        Vector3 z = Vector3.Normalize(target - pos);
        Vector3 x = Vector3.Normalize(Vector3.Cross(up, z));
        Vector3 y = Vector3.Normalize(Vector3.Cross(z, x));

        Matrix4x4 result = new Matrix4x4();
        result.SetRow(0, new Vector4(x.x, y.x, z.x, -(Vector3.Dot(pos, x))));
        result.SetRow(1, new Vector4(x.y, y.y, z.y, -(Vector3.Dot(pos, y))));
        result.SetRow(2, new Vector4(x.z, y.z, z.z, -(Vector3.Dot(pos, z))));
        result.SetRow(3, new Vector4(0, 0, 0, 1));

        return result;
    }
}
