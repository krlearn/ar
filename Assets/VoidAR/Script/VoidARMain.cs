﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class VoidARMain : MonoBehaviour {
    
    public Camera ARCamera;
    public enum EMarkerType { Shape = 0, Image = 1 }
    public EMarkerType markerType;

    public int shapeMatchAccuracy = 5;

    public bool is_use_cloud = false;
    public string accessKey = "";
    public string secretKey = "";
    public bool UseExtensionTracking = false;
    public bool tracking = true;

    [HideInInspector]
    public int CameraIndex = 0;

    //当程序唤醒时
    void Awake()
    {
#if UNITY_EDITOR
        String currentPath = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Process);
        String dllPath = Application.dataPath + Path.DirectorySeparatorChar + "Plugins\\x86";
        if (currentPath.Contains(dllPath) == false)
        {
            Environment.SetEnvironmentVariable("PATH", currentPath + Path.PathSeparator + dllPath, EnvironmentVariableTarget.Process);
        }
#elif UNITY_STANDALONE_WIN
        String currentPath = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Process);
        String dllPath = Application.dataPath + Path.DirectorySeparatorChar + "Plugins";
        if (currentPath.Contains(dllPath) == false)
        {
            Environment.SetEnvironmentVariable("PATH", currentPath + Path.PathSeparator + dllPath, EnvironmentVariableTarget.Process);
        }
#endif
    }

    void Start () {
        VoidAR.GetInstance().init( CameraIndex, (int)markerType + 1, UseExtensionTracking );
        StartCoroutine(AddTargets());

        if ((int)markerType == 0)
        {
            VoidAR.GetInstance().setShapeMatchLevel((int)shapeMatchAccuracy);
        }
        
        
        VoidAR.GetInstance().match();
        if ((int)markerType == 1 && is_use_cloud)
        {

            Debug.Log("use_cloud");

            if (accessKey.Equals("") || secretKey.Equals(""))
            {
                Debug.Log("key error");
            }
            else
            {
                VoidAR.GetInstance().setCloudUser(accessKey, secretKey, is_use_cloud);
            }
        }

       
	}

    // Update is called once per frame
    void Update()
    {
        VoidAR.GetInstance().update(ARCamera);

        if (is_use_cloud)
        {

            string url = "";
            string name = "";
            string metadata = "";
            int cloudCode = VoidAR.GetInstance().getCloudInfo(ref url, ref name, ref metadata);

            if (cloudCode > -1)
            {
                Debug.Log(url + " ," + name);
                Debug.Log("cloudCode  :" + cloudCode);
            }



            if (url != "" && !url.Equals("") && name != "" && !name.Equals(""))
            {
                //加载远程的assetBundle 
                Debug.Log("metadata  :" + metadata);
                StartCoroutine(LoadMainGameObject(url, name));
            }
            else
            {
                //加载本地imageTarget 实现动态远程视频
                /*
                 * 用户在云端无需配置assetBundel
                 * meta数据用户可以自定义 可根据自定义的数据格式来解析。
                 *测试代码 目前的videoURL 后端配置格式为 http://cloudar.ionhammer.cn:8080/develop/starwars.mp4|640|303
                 *用竖线分割了URL|markerWidth|markerHeight 用这个marker的高宽来适应视频大小
                 *这个数据用户可以根据自己的后端配置来解析所需数据
                 */

                if (metadata != "")
                {
                    url = "file://" + Application.streamingAssetsPath + "/windows/VideoPrefab.assetbundle";
#if UNITY_IOS
		        url = "file://" +  Application.streamingAssetsPath + "/ios/VideoPrefab.assetbundle";
#elif UNITY_ANDROID
                    url = Application.streamingAssetsPath + "/android/VideoPrefab.assetbundle";
#endif
                   
                    //加载本地的AssetsBundle VideoPrefab
                    StartCoroutine(LoadMainGameObjectTest(url, name, metadata));
                }


            }


        }
      

	}

    void OnDestroy()
    {
        VoidAR.GetInstance().unloadResource();
    }

    void OnApplicationPause(bool pause )
    {
        if (pause)
            VoidAR.GetInstance().stopCapture();
        else
        {
            int opened = 0;
            VoidAR.GetInstance().startCapture(CameraIndex,ref opened );
        }
            
    }

    void OnApplicationQuit()
    {
        
    }

    private IEnumerator LoadMainGameObject(string path,string name)
    {

        WWW bundle = new WWW(path);
        yield return bundle;

        if (bundle.error != null)
        {
            Debug.Log("Can not load " + path );
        }
        else
        {
            if (!VoidAR.GetInstance().isMarkerExist(name))
            {
                GameObject myBundle = (GameObject)Instantiate(bundle.assetBundle.mainAsset);
                Marker m = myBundle.GetComponent<Marker>();
                m.imageFilePath = name;
                yield return myBundle;
                bundle.assetBundle.Unload(false);
                VoidAR.GetInstance().addCloudTarget(m, myBundle);
            }
        }
    }

    private IEnumerator LoadMainGameObjectTest(string path, string name, string videoInfoStr)
    {
        WWW bundle = new WWW(path);
        yield return bundle;

        if (bundle.error != null)
        {
            Debug.Log("Can not load " + path);
        }
        else
        {
            if (!VoidAR.GetInstance().isMarkerExist(name))
            {
                GameObject myBundle = (GameObject)Instantiate(bundle.assetBundle.mainAsset);
                Marker m = myBundle.GetComponent<Marker>();
                m.imageFilePath = name;


                //设置视频的url地址
                VideoBehaviour vb = myBundle.transform.FindChild("Video").GetComponent<VideoBehaviour>();

                int markerWidth = 640;
                int markerHeight = 480;
                //如果直接用DEMO运行 后台的mate数据配置成 http://cloudar.ionhammer.cn:8080/develop/starwars.mp4|640|303的格式
                //url|markerWidth|markerHeight
                //也可自定义后台数据,请根据自己的后台数据来做解析
                string[] urlInfos = videoInfoStr.Split('|');
                if (urlInfos.Length >= 3)
                {
                    markerWidth =Int32.Parse(urlInfos[1]);
                    markerHeight = Int32.Parse(urlInfos[2]);
                }



                vb.path = urlInfos[0];
                myBundle.transform.FindChild("Video").gameObject.transform.localEulerAngles = new Vector3(0.0f, 180.0f, 0.0f);

                float asp = 0.75f;
                float ratio = (float)markerHeight / (float)markerWidth;

                Transform tran = myBundle.transform.FindChild("Video").gameObject.transform;
                if (ratio > 1.0f)
                {
                    tran.localScale = new Vector3(0.1f / ratio * asp, 0.1f, 0.1f * asp);
                }
                else
                {
                    tran.localScale = new Vector3(0.1f, 0.1f, 0.1f * ratio);
                }

                yield return myBundle;


                bundle.assetBundle.Unload(false);
                VoidAR.GetInstance().addCloudTarget(m, myBundle);
            }
        }
    }


    private IEnumerator AddTargets()
    {
        GameObject[] gameObjects = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];
        List<VoidAR.Image2ImageTarget> image2ImageTargetes = new List<VoidAR.Image2ImageTarget>();

        for (int i = 0; i < gameObjects.Length; i++)
        {
            Marker marker = gameObjects[i].GetComponent<Marker>();
            if (marker != null)
            {
                VoidAR.Image2ImageTarget obj = new VoidAR.Image2ImageTarget();
                obj.isMarkerLocal = true;
                obj.ImageTarget = gameObjects[i];
#if UNITY_ANDROID
                obj.imageUrl = Application.streamingAssetsPath + "/" + marker.imageFilePath;
#else
                obj.imageUrl = "file://" + Application.streamingAssetsPath + "/" + marker.imageFilePath;
#endif
                
                WWW file = new WWW(obj.imageUrl);
                yield return file;
                if (file.error != null)
                {
                    Debug.Log(obj.imageUrl + " error  !!!");
                }   
                else
                {
                    obj.imagedata = file.bytes;
                    image2ImageTargetes.Add(obj);
                    
                }
            }
        }
		VoidAR.GetInstance().addTargets(image2ImageTargetes);
    }
}
