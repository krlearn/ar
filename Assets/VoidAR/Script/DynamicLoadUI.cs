using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System;

class JPGUrl2ImageTargetUrl
{
    public bool isLocalImage;
    public string JpgNameURL;
    public string ImageTargetURL;
}


public class DynamicLoadUI : MonoBehaviour
{

    GameObject addMarker;
    GameObject exit;
    GameObject delMarker;

    List<JPGUrl2ImageTargetUrl> jpgUrl2targetUrl = new List<JPGUrl2ImageTargetUrl>();
    void initConfig()
    {
        //JPG在StreamingAsset， 模型在本地
        JPGUrl2ImageTargetUrl obj1 = new JPGUrl2ImageTargetUrl();

#if UNITY_IOS
        obj1.JpgNameURL = "file://" + Application.streamingAssetsPath + "/1yuan.jpg";
		obj1.ImageTargetURL = "file://" +  Application.streamingAssetsPath + "/ios/cubeTest.assetbundle";
#elif UNITY_ANDROID
        obj1.ImageTargetURL = Application.streamingAssetsPath + "/android/cubeTest.assetbundle";
        obj1.JpgNameURL = Application.streamingAssetsPath + "/1yuan.jpg";
#else
        obj1.JpgNameURL = "file://" + Application.streamingAssetsPath + "/1yuan.jpg";
        obj1.ImageTargetURL = "file://" +  Application.streamingAssetsPath + "/windows/cubeTest.assetbundle";
#endif
        obj1.isLocalImage = true;
        jpgUrl2targetUrl.Add(obj1);


/*
        //JPG在StreamingAsset, 模型在HTTP
        JPGUrl2ImageTargetUrl obj2 = new JPGUrl2ImageTargetUrl();

#if UNITY_IOS
        obj2.JpgNameURL = "file://" + Application.streamingAssetsPath + "/10yuan_back.jpg";
        obj2.ImageTargetURL = "http://YOUR_ASSET_SERVER/YOUR_ASSETBUNDLE";
#elif UNITY_ANDROID
        obj2.JpgNameURL = Application.streamingAssetsPath + "/10yuan_back.jpg";
        obj2.ImageTargetURL = "http://YOUR_ASSET_SERVER/YOUR_ASSETBUNDLE";
#else
        obj2.JpgNameURL = "file://" + Application.streamingAssetsPath + "/10yuan_back.jpg";
        obj2.ImageTargetURL = "http://YOUR_ASSET_SERVER/YOUR_ASSETBUNDLE";
#endif
        obj2.isLocalImage = true;
        jpgUrl2targetUrl.Add(obj2);
        //JPG在HTTP， 模型在本地
        JPGUrl2ImageTargetUrl obj3 = new JPGUrl2ImageTargetUrl();
        obj3.JpgNameURL = "http://YOUR_ASSET_SERVER/IMAGE_NAME.jpg"; ;
#if UNITY_IOS
		obj3.ImageTargetURL = "file://" +  Application.streamingAssetsPath + "/ios/ImageTarget.assetbundle";
#elif UNITY_ANDROID
        obj3.ImageTargetURL =  Application.streamingAssetsPath + "/android/1yuan_target.assetbundle";
#else
		obj3.ImageTargetURL = "file://" + Application.streamingAssetsPath + "/windows/1yuan_target.assetbundle";
#endif
        obj3.isLocalImage = false;
        jpgUrl2targetUrl.Add(obj3);

        //JPG和模型都在HTTP
        JPGUrl2ImageTargetUrl obj4 = new JPGUrl2ImageTargetUrl();
        obj4.JpgNameURL = "http://YOUR_ASSET_SERVER/IMAGE_NAME.jpg";
#if UNITY_IOS
        obj4.ImageTargetURL = "http://YOUR_ASSET_SERVER/YOUR_ASSETBUNDLE";
#elif UNITY_ANDROID
        obj4.ImageTargetURL = "http://YOUR_ASSET_SERVER/YOUR_ASSETBUNDLE";
#else
        obj4.ImageTargetURL = "http://YOUR_ASSET_SERVER/YOUR_ASSETBUNDLE";
#endif
        obj4.isLocalImage = false;
        jpgUrl2targetUrl.Add(obj4);
    */
    }


    void Start()
    {
       initConfig();

       addMarker = GameObject.Find("addMarker");

        Button btn = addMarker.GetComponent<Button>();

        btn.onClick.AddListener(delegate()
        {
            this.OnClick(addMarker);
        });

        delMarker = GameObject.Find("delMarker");
        Button delBtn = delMarker.GetComponent<Button>();

        delBtn.onClick.AddListener(delegate()
        {
            this.OnClick(delMarker);
        });




        exit = GameObject.Find("Exit");

        Button btn1 = exit.GetComponent<Button>();

        btn1.onClick.AddListener(delegate ()
        {
            this.OnClick(exit);
        });

    }
    void OnGUI()
    {
    }

    public void OnClick(GameObject sender)
    {
        if (sender == addMarker)
        {
            Debug.Log(" add marker");
            StartCoroutine(LoadGameObjects());
        }
        else if ( sender == exit )
        {
            Application.LoadLevel("AllDemo");
        }
        else if (sender == delMarker)
        {
            //删除1元的marker
            if (VoidAR.GetInstance().isMarkerExist(jpgUrl2targetUrl[0].JpgNameURL))
            {
                VoidAR.GetInstance().removeTarget(jpgUrl2targetUrl[0].JpgNameURL);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnPreRender()
    {
    }

    void OnPostRender()
    {

    }


    private IEnumerator LoadGameObjects()
    {
        List<VoidAR.Image2ImageTarget> image2ImageTargetes = new List<VoidAR.Image2ImageTarget>();
        foreach( JPGUrl2ImageTargetUrl url in jpgUrl2targetUrl )
        {
            if (!VoidAR.GetInstance().isMarkerExist(url.JpgNameURL))
            {
                WWW file = new WWW(url.JpgNameURL);
                yield return file;

                if (file.error != null)
                {
                    Debug.Log("Can not Load " + url.JpgNameURL);
                }
                else
                {
                    WWW bundle = new WWW(url.ImageTargetURL);
                    yield return bundle;
                    if (bundle.error != null)
                    {
                        Debug.Log("Can not Load " + url.ImageTargetURL);
                    }
                    else
                    {
                        VoidAR.Image2ImageTarget obj = new VoidAR.Image2ImageTarget();
                        obj.imageUrl = url.JpgNameURL;
                        obj.isMarkerLocal = url.isLocalImage;
                        obj.ImageTarget = (GameObject)Instantiate(bundle.assetBundle.mainAsset);
                        obj.imagedata = file.bytes;
                        image2ImageTargetes.Add(obj);
                        bundle.assetBundle.Unload(false);

                    }

                }
            }
        }
		VoidAR.GetInstance().addTargets(image2ImageTargetes);
    }
}
