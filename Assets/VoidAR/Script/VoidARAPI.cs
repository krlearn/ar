﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;
using System.IO;
using UnityEngine;
using System.Text;

public class VoidAR
{

    public class Image2ImageTarget
    {
        public bool isMarkerLocal;
        public string imageUrl;
        public GameObject ImageTarget;
        public byte[] imagedata;
    }

    public class MarkerMap
    {
        public GameObject gameObject;
        public Marker marker;
    }



    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MyDelegate(string str);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MatchingResultDelegate(int result);

    static void CallBackFunction(string str)
    {
        Debug.Log("VoidAR-Plugin:" + str);
    }

    static void ShapeMatchingCallBack(int success)
    {

        if (success == 1)
        {
            Debug.Log("添加目标 识别成功");
        }
        else
        {
            Debug.Log("添加目标 识别失败");
        }
    }

    public struct PowerAREvent
    {
        public int TargetID;
    }

    private static VoidAR instance;

#if UNITY_IPHONE
    const string dllName = "__Internal";
#elif UNITY_ANDROID
    const string dllName = "VoidAR-Plugin";
#else
    const string dllName = "VoidAR-Plugin";
#endif
    [DllImport(dllName)]
    private static extern void _setDebugLog(IntPtr fp);
    [DllImport(dllName)]
    private static extern void _setMatchingResult(IntPtr fp);
    [DllImport(dllName)]
    private static extern void _initBGTexture(ref int textureId, ref int width, ref int height);
    [DllImport(dllName)]
    private static extern void _initAndroidBGTexutre(ref int t1, ref int t2, ref int width, ref int height);
    [DllImport(dllName)]
    private static extern void _setScreenSize(ref int width, ref int height);
    [DllImport(dllName)]
    private static extern void _initLibrary( int type );
    [DllImport(dllName)]
    private static extern void _buildGLProjectionMatrix(int screenWidth, int sceenHeight, double[] matrix44);
    [DllImport(dllName)]
    private static extern void _startCapture(int cameraIndex, ref int opened);
    [DllImport(dllName)]
    private static extern void _stopCapture();
    [DllImport(dllName)]
    private static extern void _getMatchResult(IntPtr frame, ref int width, ref int height, ref int flip, double[] matrix, byte[] markers, ref int mainMarker, int[] scores);
    [DllImport(dllName)]
    private static extern void _unloadResource();
    [DllImport(dllName)]
    private static extern void _match();
    [DllImport(dllName)]
    private static extern void _getTrackingTime(double[] times);
    [DllImport(dllName)]
    private static extern void _reset();
    [DllImport(dllName)]
    private static extern void _getTrackStatus(int[] status);
    [DllImport(dllName)]
    private static extern void _setDebugDraw(int debugDraw);
    [DllImport(dllName)]
    private static extern void _analyzing(string path, IntPtr fp);
    [DllImport(dllName)]
    private static extern void _addCurrentShapeImageTarget(ref int success, byte[] name, int tracking);
    [DllImport(dllName)]
    private static extern void _getNewCloudTarget(ref int newTarget,byte[] urls, byte[] names ,byte[] matadata);
    [DllImport(dllName)]
    private static extern void _setCloudUser(string accessKey , string secretKey , int useCloud);
    [DllImport(dllName)]
    private static extern void _setShapeMatchLevel(int lvl);
	[DllImport(dllName)]
	private static extern bool _checkNet();
    [DllImport(dllName)]
    private static extern bool _useExtensionTracking( bool use);
    [DllImport(dllName)]
    private static extern bool _addTarget(string path, byte[] data, int size );
    [DllImport(dllName)]
    private static extern bool _finishAddTarget();
    [DllImport(dllName)]
    private static extern bool _removeImageTarget(string targetName);


    public enum EState { E_Init, E_Tracking, E_Lost, E_CameraNotOpened, E_Error }
    public EState state;
    // Use this for initialization
    Matrix4x4 projMatrix = new Matrix4x4();
    Dictionary<string, MarkerMap> mMarkers = new Dictionary<string, MarkerMap>();


    Marker currentTrackingMark;
    double[] vmMatrixData;
    GameObject backGround;
    Texture2D bgTexture1;

    Texture2D bgTexture2;

    byte[] byteFrame;
    Color32[] bgFrame;

    GCHandle bgFrameHandle;
    //GameObject webcam;
    //WebcamTextureToMat wtm;

    int screen_width = 352;
    int screen_height = 288;

    IntPtr onAnalyzingPtr;
    IntPtr p1;
    IntPtr p2;
    IntPtr p3;

    Vector2[] bguvs;
    
    float orthoSize1 = 0;
    float orthoSize2 = 0;
    //float scale;
    float screenH;
    float aspectRatio;
    int cameaWidth = 0;
    int cameraHeight = 0;
    struct ExternalTextureDesc
    {
        public System.IntPtr tex1;
        public System.IntPtr tex2;
        public int width;
        public int height;
    };

    ExternalTextureDesc externalTexture;

    

    private VoidAR()
    {
    }

    public static VoidAR GetInstance()
    {
        if (instance == null)
        {
            instance = new VoidAR();
        }
        return instance;
    }

    public void setShapeMatchLevel(int lvl)
    {
        _setShapeMatchLevel(lvl);
    }

    public void init(int cameraIndex, int markerType,bool extensionTracking )
    {
        setScreenSize(ref cameaWidth, ref cameraHeight);
        useExentsionTracking(extensionTracking);

        float max = Math.Max(Screen.width, Screen.height);
        float min = Math.Min(Screen.width, Screen.height);
        float coef = (float)max / (float)cameaWidth;
        aspectRatio = max / min;
        float h = min / coef;
        screenH = h;
        // Debug.Log(" sceenH " + screenH);
        //orthoSize1 = cameraHeight / 2;
        orthoSize1 = h / 2;

        orthoSize2 = orthoSize1 * aspectRatio;

        //scale = h / ( float ) cameraHeight;
        //setScreenSize(cameaWidth, (int)h);

        
        _initLibrary( markerType );

        if (initShapeLibrary("shape", markerType) == false)
        {
            state = EState.E_Error;
            return;
        }
        else
        {
            state = EState.E_Tracking;
            Camera.main.orthographicSize = orthoSize1;
            //Camera.main.orthographicSize = cameraHeight / 2;
            Camera.main.transform.eulerAngles = new Vector3(90, 0, 0);

            buildGLProjectionMatrix(cameaWidth, cameraHeight);

            vmMatrixData = new double[160];

            int opened = 0;
            startCapture(cameraIndex, ref opened);
            /*
            if (opened == 0)
            {
                state = EState.E_CameraNotOpened;
                Debug.Log("没有检测到摄像头！");
            }
            else
            {
                state = EState.E_Tracking;
            }
            */
            state = EState.E_Tracking;
        }

        if (markerType == 1)
        {
#if UNITY_IOS
#else
            MatchingResultDelegate callback_delegate = new MatchingResultDelegate(ShapeMatchingCallBack);
            IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);
            _setMatchingResult(intptr_delegate);
#endif
        }
    }


    public void initBGTexture()
    {
        #if UNITY_ANDROID
             int textureid1 = 0;
             int textureid2 = 0;
             _initAndroidBGTexutre(ref textureid1,ref textureid2, ref externalTexture.width, ref externalTexture.height);
             externalTexture.tex1  =(IntPtr)textureid1;
             externalTexture.tex2  =(IntPtr)textureid2;

             Debug.Log("Unity textureid1 " + textureid1);
             Debug.Log("Unity textureid2 " + textureid2);
             Debug.Log("Unity externalTexture.width " + externalTexture.width);
             Debug.Log("Unity externalTexture.height " + externalTexture.height);

            bgTexture1 = Texture2D.CreateExternalTexture (externalTexture.width, externalTexture.height, TextureFormat.RGBA32, false, false, externalTexture.tex1);
		    bgTexture1.filterMode = FilterMode.Bilinear;
		    bgTexture1.wrapMode = TextureWrapMode.Clamp;

            bgTexture2 = Texture2D.CreateExternalTexture (externalTexture.width /2 , externalTexture.height / 2, TextureFormat.Alpha8, false, false, externalTexture.tex2);
		    bgTexture2.filterMode = FilterMode.Bilinear;
		    bgTexture2.wrapMode = TextureWrapMode.Clamp;

           //backGround.GetComponent<Renderer>().material.mainTexture = bgTexture1;
            //Material mat = backGround.GetComponent<Renderer>().material;
            backGround.GetComponent<Renderer>().material.SetTexture("Yp", bgTexture1);
            backGround.GetComponent<Renderer>().material.SetTexture("CbCr", bgTexture2);
            //mat.SetTexture("Yp", bgTexture1);
           // mat.SetTexture("CbCr", bgTexture2);


            Debug.Log("textureid1" + textureid1);
            Debug.Log("textureid2" + textureid2);
#else
            int textureid = 0;
            _initBGTexture(ref textureid, ref externalTexture.width, ref externalTexture.height);
            externalTexture.tex1 = (IntPtr)textureid;

            bgTexture1 = Texture2D.CreateExternalTexture(externalTexture.width, externalTexture.height, TextureFormat.RGBA32, false, false, externalTexture.tex1);
            bgTexture1.filterMode = FilterMode.Bilinear;
            bgTexture1.wrapMode = TextureWrapMode.Clamp;

            backGround.GetComponent<Renderer>().material.mainTexture = bgTexture1;
#endif
    }
    public void setScreenSize(ref int width, ref int height)
    {
        _setScreenSize(ref width, ref height);
        screen_width = width;
        screen_height = height;
    }

    public bool isMarkerExist(string name)
    {
        return mMarkers.ContainsKey(name);
    }

    public void addCloudTarget(Marker  m , GameObject obj)
    {
       MarkerMap mm = new MarkerMap();
       mm.gameObject = obj;
       mm.marker = m;
       if (!mMarkers.ContainsKey(m.imageFilePath))
       {
           mMarkers.Add(m.imageFilePath, mm);
       }
    }

    bool initShapeLibrary(string path, int markerType)
    {
#if UNITY_EDITOR
        MyDelegate callback_delegate = new MyDelegate(CallBackFunction);
        IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);
        _setDebugLog(intptr_delegate);
#endif

        backGround = Camera.main.transform.GetChild(0).gameObject;
        backGround.GetComponent<Renderer>().enabled = true;
        backGround.transform.localScale = new Vector3(screen_width, -screen_height, 1);


#if UNITY_ANDROID
		//backGround.GetComponent<Renderer>().material =  new Material (Shader.Find("Unlit/QuadShader"));
		initBGTexture ();
#elif UNITY_IOS
		backGround.GetComponent<Renderer>().material = (Material)Resources.Load("quad_material_tran", typeof(Material));
		initBGTexture ();
#else
        backGround.GetComponent<Renderer>().material = (Material)Resources.Load("quad_material_tran", typeof(Material));
		bgTexture1 = new Texture2D(640, 480, TextureFormat.RGBA32, false);
		bgTexture1.filterMode = FilterMode.Bilinear;
		bgTexture1.wrapMode = TextureWrapMode.Clamp;
#endif
        return true;

    }



    void buildGLProjectionMatrix(int screenWidth, int sceenHeight)
    {
        double[] data = new double[16];
        _buildGLProjectionMatrix(screenWidth, sceenHeight, data);
        for (int i = 0; i < 1; i++)
            projMatrix[i] = (float)data[i];
    }

    public void startCapture(int cameraIndex, ref int opened)
    {
        _startCapture(cameraIndex, ref opened);
// #if UNITY_IPHONE
// 		    _startCapture(cameraIndex, ref opened );
// #elif UNITY_ANDROID
//        // string _error = "";
//         using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
//         {
//            // _error = "UnityPlayer getted " + (unityPlayer == null) + "\n";
//             using (AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
//             {
//                // _error = "currentActivity getted " + (activity == null) + "\n";
//                 using (AndroidJavaClass jc = new AndroidJavaClass("cn.voidar.engine.NativeCamera"))
//                 {
//                    // _error = "AndroidJavaClass getted " + (jc == null) + "\n";
//                     jc.CallStatic("initialize", activity);
//                 }
//             }
//         }
//         opened = 1;
// #else
//         _startCapture(cameraIndex, ref opened);
// #endif
    }



    public bool checkNet()
    {
        #if UNITY_ANDROID
         return _checkNet();
        #elif UNITY_IPHONE
		 return _checkNet();
        #else
        return true;
        #endif
    }





    public void stopCapture()
    {
        _stopCapture();
// #if UNITY_IPHONE
// 		    _stopCapture();
// #elif UNITY_ANDROID
//        // string _error = "";
// 
//         using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
//         {
//             //_error = "UnityPlayer getted " + (unityPlayer == null) + "\n";
//             //Debug.Log("====Test Debug str = " + _error);
//             using (AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
//             {
//               //  _error = "currentActivity getted " + (activity == null) + "\n";
//                 activity.Call("cameraStop");
//                // Debug.Log("====Test Debug str = " + _error);
//             }
//         }
// #else
//         _stopCapture();
// #endif
    }

    void getMatchResult(IntPtr frame, ref int width, ref int height, ref int flip, double[] matrix, byte[] markers, ref int mainMarker, int[] scores)
    {
        _getMatchResult(frame, ref width, ref height, ref flip, matrix, markers, ref mainMarker, scores);
    }

    public void unloadResource()
    {
        _unloadResource();
		mMarkers.Clear ();
    }

    public int update(Camera arCamera)
    {
        switch (state)
        {
            case EState.E_CameraNotOpened:
                break;
            case EState.E_Init:
                break;
            case EState.E_Tracking:
                updateTracking(arCamera);
                break;
            case EState.E_Lost:
                break;
        }
        return 0;
    }

    public void match()
    {
        _match();
    }

    void getTrackingTime(double[] times)
    {
        _getTrackingTime(times);
    }

    public void reset()
    {
        _reset();
    }

    public int getTrackStatus()
    {
        int[] status = new int[1];
        _getTrackStatus(status);
        return status[0];
    }

    public void addCurrentShapeImageTarget(ref int success  ,byte[] name , int tracking)
    {
        _addCurrentShapeImageTarget(ref  success  ,  name , tracking);
    }

    void setDebugDraw(int debugDraw)
    {
        _setDebugDraw(debugDraw);
    }
    static Matrix4x4 PerspectiveOffCenter(float fx, float fy, float cx, float cy, int screenWidth, int screenHeight)
    {

        float nearPlane = 0.01f;  // Near clipping distance
        float farPlane = 100.0f;  // Far clipping distance

        var matrix44 = new Matrix4x4();
        // Camera parameters
        matrix44.m00 = 2.0f * fx / screenWidth;
        matrix44.m01 = 0.0f;
        matrix44.m02 = 0.0f;
        matrix44.m03 = 0.0f;

        matrix44.m10 = 0.0f;
        matrix44.m11 = 2.0f * fy / screenHeight;
        matrix44.m12 = 0.0f;
        matrix44.m13 = 0.0f;

        matrix44.m20 = (2.0f * cx / screenWidth - 1.0f);
        matrix44.m21 = (1.0f - 2.0f * cy / screenHeight);
        matrix44.m22 = -(farPlane + nearPlane) / (farPlane - nearPlane);
        matrix44.m23 = -1.0f;

        matrix44.m30 = 0.0f;
        matrix44.m31 = 0.0f;
        matrix44.m32 = -2.0f * farPlane * nearPlane / (farPlane - nearPlane);
        matrix44.m33 = 0.0f;

        return matrix44.transpose;
    }

    bool getMatrix(int index, double[] inMatrix, ref Matrix4x4 matrix)
    {
        bool zero = true;
        int start = index * 16;
        for (int i = start; i < start + 16; i++)
        {
            if (inMatrix[i] != 0)
            {
                zero = false;
                break;
            }
        }

        if (!zero)
        {
            matrix.m00 = (float)inMatrix[start + 0]; matrix.m01 = (float)inMatrix[start + 1]; matrix.m02 = (float)inMatrix[start + 2]; matrix.m03 = (float)inMatrix[start + 12];
            matrix.m10 = (float)inMatrix[start + 4]; matrix.m11 = (float)inMatrix[start + 5]; matrix.m12 = (float)inMatrix[start + 6]; matrix.m13 = (float)inMatrix[start + 13];
            matrix.m20 = (float)inMatrix[start + 8]; matrix.m21 = (float)inMatrix[start + 9]; matrix.m22 = (float)inMatrix[start + 10]; matrix.m23 = (float)inMatrix[start + 14];
            matrix.m30 = (float)inMatrix[start + 3]; matrix.m31 = (float)inMatrix[start + 7]; matrix.m32 = (float)inMatrix[start + 11]; matrix.m33 = (float)inMatrix[start + 15];
            return true;
        }

        return false;

    }

    void updateTracking(Camera arCamera)
    {
        arCamera.fieldOfView = projMatrix[0];
       
        int mainmarker = -1;

        int[] scores = new int[10];
        int rotation = -1;
        int _width = 640;
        int _height = 480;

        byte[] markerids = new byte[1024];


#if UNITY_IOS 
        getMatchResult((IntPtr)0, ref _width, ref _height, ref rotation, vmMatrixData, markerids, ref mainmarker, scores);
#elif UNITY_ANDROID
        getMatchResult((IntPtr)0, ref _width, ref _height, ref rotation, vmMatrixData, markerids, ref mainmarker, scores);
#else
        Color32[] colors = new Color32[640 * 480];
        GCHandle colorsHandle = GCHandle.Alloc(colors, GCHandleType.Pinned);

        getMatchResult(colorsHandle.AddrOfPinnedObject(), ref _width, ref _height, ref rotation, vmMatrixData, markerids, ref mainmarker, scores);

        bgTexture1.SetPixels32(colors);
        bgTexture1.Apply(false);
        backGround.GetComponent<Renderer>().material.mainTexture = bgTexture1;
        colorsHandle.Free();
#endif
        string result = System.Text.Encoding.UTF8.GetString(markerids);

       
        char[] delimiterChars = { ',' };
        string[] markerStr = result.Split(delimiterChars);

 
         if (rotation == -1)
         {
             backGround.GetComponent<Renderer>().enabled = false;
         }
         else
        {
            backGround.GetComponent<Renderer>().enabled = true;
        }


        Matrix4x4 rot1 = Matrix4x4.identity;
        if (rotation == 0 || rotation == 2)
        {
            if (rotation == 0)
            {
                backGround.transform.localScale = new Vector3(screen_width, -screen_height, 1);
            }
            else
            {
                backGround.transform.localScale = new Vector3(-screen_width, screen_height, 1);
            }
            backGround.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -90));
            Camera.main.orthographicSize = orthoSize2;

            Quaternion q1 = new Quaternion();
            q1.eulerAngles = new Vector3(0, 0, -90);
            rot1 = Matrix4x4.TRS(new Vector3(0, 0, 0), q1, new Vector3(1, 1, 1));
        }
        else
        {
            if (rotation == 1)
            {
                backGround.transform.localScale = new Vector3(screen_width, -screen_height, 1);
            }
            else
            {
                backGround.transform.localScale = new Vector3(-screen_width, screen_height, 1);
            }
            backGround.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
            Camera.main.orthographicSize = orthoSize1;
        }


        Matrix4x4 m1 = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1, -1, 1));
        Matrix4x4 rot = new Matrix4x4();
        Quaternion q = new Quaternion();
        q.eulerAngles = new Vector3(-90, 0, 0);
        rot = Matrix4x4.TRS(new Vector3(0, 0, 1), q, new Vector3(1, 1, -1));

       
        
        foreach (var item in mMarkers)
        {
            bool find = false;
            int idx = 0;
            
            for (int i = 0; i < markerStr.Length; i++)
            {
                bool ret = string.Equals(item.Key.Trim(), markerStr[i].Trim());
                
                if (ret)
                {   
                    find = true;
                    
                    Matrix4x4 mv = new Matrix4x4();
                    if (item.Value.gameObject != null && getMatrix(idx, vmMatrixData, ref mv))
                    {
                       
                        Vector3 pos = MyUtils.GetPosition(mv);

                        float scale = cameraHeight / screenH;
                      
                        mv = rot1 * m1 * mv * rot;
                        pos = MyUtils.GetPosition(mv);
                        if (rotation == 0 || rotation == 2)
                            pos.z *= aspectRatio;

                        //Debug.Log("( "  + pos.z + " )");
                        pos.z /= scale;

                        item.Value.gameObject.transform.position = pos;
                        item.Value.gameObject.transform.rotation = MyUtils.GetRotation(mv);
                        item.Value.gameObject.transform.localScale = mv.GetScale();
                    }
                    break;
                }
                idx++;
            }
            if (item.Value.marker != null )
                item.Value.marker.UpdateState(find);
        }
    }

    public int getCloudInfo(ref string url, ref  string name , ref string matadata)
    {
        char[] delimiterChars = { '\0' };
        byte[] markerurl = new byte[512];
        byte[] markername = new byte[512];
        byte[] mdata = new byte[1024];

        int cloudCode = 0;
        url  = "";
        name = "";
        matadata = "";
        _getNewCloudTarget(ref cloudCode, markerurl, markername, mdata);


        if (cloudCode == 1)
        {
            url = System.Text.Encoding.UTF8.GetString(markerurl);
            name = System.Text.Encoding.UTF8.GetString(markername);
            matadata = System.Text.Encoding.UTF8.GetString(mdata);

            string[] urlStrs = url.Split(delimiterChars);
            string[] nameStrs = name.Split(delimiterChars);
            string[] mataDatas = matadata.Split(delimiterChars);
            url  = urlStrs[0];
            name = nameStrs[0];
            matadata = mataDatas[0];
        }

        return cloudCode;
    }

    public void setCloudUser(string accessKey,string sercetKey, bool useCloud)
    {
        if (useCloud)
        {
            _setCloudUser(accessKey, sercetKey, 1);
        }
        else
        {
            _setCloudUser(accessKey, sercetKey, 0);
        }
       
    }

     void useExentsionTracking( bool use)
    {
        _useExtensionTracking(use);
    }
    
     void addTarget(Image2ImageTarget image2ImageTarget)
    {
        Marker marker = image2ImageTarget.ImageTarget.GetComponent<Marker>();
        if ( marker )
        {
            if (mMarkers.ContainsKey(image2ImageTarget.imageUrl))
            {
                Debug.Log("marker is exist!!!" + image2ImageTarget.imageUrl);
            }
            else
            {
                if (image2ImageTarget.imagedata.Length > 0)
                {

                    //Debug.Log("Application.persistentDataPath : " + Application.persistentDataPath);

                    _addTarget(image2ImageTarget.imageUrl, image2ImageTarget.imagedata, image2ImageTarget.imagedata.Length);
                    MarkerMap mm = new MarkerMap();
                    mm.gameObject = image2ImageTarget.ImageTarget;
                    mm.marker = marker;
                    mMarkers.Add(image2ImageTarget.imageUrl, mm);
                }
            }
        }
    }

    public void addTargets(List<Image2ImageTarget> image2ImageTarget )
    {
        foreach( Image2ImageTarget target in image2ImageTarget )
        {
            addTarget(target);
        }

        _finishAddTarget();
    }

    public void removeTarget(string targetName)
    {
        GameObject.DestroyImmediate(mMarkers[targetName].gameObject);
        mMarkers.Remove(targetName);
        _removeImageTarget(targetName);
    }

   
}
