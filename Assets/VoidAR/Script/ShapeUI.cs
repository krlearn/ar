﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System;



public class ShapeUI : MonoBehaviour
{

    GameObject add;
    GameObject clear;
    GameObject exit;
    GameObject image;

    private int isTracking = 1;

    void Start()
    {
        //PowerAR.GetInstance().init(width, height);

        image = GameObject.Find("Image");

        add = GameObject.Find("add");

        Button btn = add.GetComponent<Button>();

        btn.onClick.AddListener(delegate()
        {
            this.OnClick(add);
        });


        clear = GameObject.Find("clear");
        Button btn1 = clear.GetComponent<Button>();

        btn1.onClick.AddListener(delegate()
        {
            this.OnClick(clear);
        });

        exit = GameObject.Find("Exit");
        Button btn2 = exit.GetComponent<Button>();

        btn2.onClick.AddListener(delegate ()
        {
            this.OnClick(exit);
        });
        isTracking = GameObject.Find("MainCamera").GetComponent<VoidARMain>().tracking == true ? 1 : 0;
    }
    void OnGUI()
    {
        Image img = image.GetComponent<Image>();
        int status = VoidAR.GetInstance().getTrackStatus();
        if (status == 0)
            img.color = new Color(1, 0, 0, 1);
        else if (status == 1)
            img.color = new Color(0, 0, 1, 1);
        else
            img.color = new Color(0, 1, 0, 1);
    }

    public void OnClick(GameObject sender)
    {
        if (sender == add)
        {
            int success = 0;
            byte[] name = new byte[512];
            VoidAR.GetInstance().addCurrentShapeImageTarget(ref success, name, isTracking);
            string markName = System.Text.Encoding.UTF8.GetString(name);
            Debug.Log("success  :" + success + "name : " + markName);

        }
        else if (sender == clear)
        {
            VoidAR.GetInstance().reset();
        }
        else if ( sender == exit )
        {
            Application.LoadLevel("AllDemo");
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnPreRender()
    {
    }

    void OnPostRender()
    {

    }
}
