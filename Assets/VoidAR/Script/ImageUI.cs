﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageUI : MonoBehaviour {

    GameObject exit;
    // Use this for initialization
    void Start()
    {
        exit = GameObject.Find("Exit");

        Button btn = exit.GetComponent<Button>();

        btn.onClick.AddListener(delegate ()
        {
            this.OnClick(exit);
        });
    }

    

    public void OnClick(GameObject sender)
    {
        if (sender == exit)
        {
            Application.LoadLevel("AllDemo");
        }
    }
}
