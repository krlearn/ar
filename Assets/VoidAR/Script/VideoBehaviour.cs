﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif


[ExecuteInEditMode]
public class VideoBehaviour : MonoBehaviour,FinderInterface {
	public string path = null;
    public Boolean loop = false;

    public bool autoScale = true;
    private string lastPath = "";

#if UNITY_ANDROID
    private AndroidJavaObject javaVideoPlayer = null;
#elif UNITY_IPHONE
	private IntPtr videoPlayerPtr = IntPtr.Zero;

	[DllImport("__Internal")] 
	private static extern void setVideoURL(IntPtr videoPlayerPtr, string videoURL);
	[DllImport("__Internal")] 
	private static extern int updateVideoTexture(IntPtr videoPlayerPtr,int textureID);

	[DllImport("__Internal")] 
	private static extern void play(IntPtr videoPlayerPtr);
	[DllImport("__Internal")] 
	private static extern void pauseVideo(IntPtr videoPlayerPtr);
	[DllImport("__Internal")] 
	private static extern void onExit(IntPtr videoPlayerPtr);
	[DllImport("__Internal")] 
	private static extern int seekTo(IntPtr videoPlayerPtr,float position);

	[DllImport("__Internal")] 
	private static extern IntPtr initVideoPlayer();

	[DllImport("__Internal")] 
	private static extern int getState(IntPtr videoPlayerPtr);
	[DllImport("__Internal")] 
	private static extern int getWidth(IntPtr videoPlayerPtr);
	[DllImport("__Internal")] 
	private static extern int getHeight(IntPtr videoPlayerPtr);
#endif

#if UNITY_STANDALONE
	public MovieTexture mVideoTexture;
#else
    private Texture2D mVideoTexture = null;
	private int nativeTextureID = -1;
#endif
    //是否设置了Video Path
	bool isLoaded = false;
    public enum ScaleMode
    {
        FILL,
        FIT,
        FIT_WIDTH,
        FIT_HEIGHT
    }

    public ScaleMode scaleMode;

    private enum PlayerState
    {
        NOT_READY = 0 , //默认状态
        READY = 1,  //准备好状态
        PLAYING = 2, //播放中
        PAUSED = 3, //暂停中
        END = 4,    //播放结束
        STOPPED = 5, //停止播放
        ERROR = 6  //错误状态
    }
    void Start () {

	}

	void updateShaderProperties(int texWidth, int texHeight)
	{
		float goWidth = gameObject.transform.localScale.x;
		float goHeight = gameObject.transform.localScale.z;
		
		if (scaleMode == ScaleMode.FILL)
		{
			gameObject.GetComponent<Renderer>().material.SetInt("_FillMode", 0);
		}
		else if (scaleMode == ScaleMode.FIT)
		{
			gameObject.GetComponent<Renderer>().material.SetInt("_FillMode", 1);
			float xScale = texWidth / (texHeight * goWidth / goHeight);
			float yScale = texHeight / (texWidth * goHeight / goWidth);
			if (xScale < 1.0f)
			{
				gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleX", xScale);
				gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleY", 1.0f);
			}
			else
			{
				gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleX", 1.0f);
				gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleY", yScale);
			}
		}
		else if (scaleMode == ScaleMode.FIT_WIDTH)
		{
			gameObject.GetComponent<Renderer>().material.SetInt("_FillMode", 2);
			float yScale = texHeight / (texWidth * goHeight / goWidth);
			gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleX", 1.0f);
			gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleY", yScale);
		}
		else if (scaleMode == ScaleMode.FIT_HEIGHT)
		{
			gameObject.GetComponent<Renderer>().material.SetInt("_FillMode", 3);
			float xScale = texWidth / (texHeight * goWidth / goHeight);
			gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleX", xScale);
			gameObject.GetComponent<Renderer>().material.SetFloat("_ScaleY", 1.0f);
		}
	}


void updateVideoPath()
{
    if (!Application.isPlaying)
        return;
        if (lastPath == path)
            return;

        lastPath = path;

		if (path != null && path.Length > 0) {
#if UNITY_ANDROID
			setVideoURL(path);
            createTexture(0, 0);
            gameObject.GetComponent<Renderer>().enabled = false;
#elif UNITY_IPHONE
			setVideoURL (getVideoPlayer(),path);
			Vector3 scale = gameObject.transform.localScale;
			scale.z *= -1;
			gameObject.transform.localScale = scale;
			createTexture(0,0);
#elif UNITY_STANDALONE
            int texWidth = mVideoTexture.width;
            int texHeight = mVideoTexture.height;
            gameObject.GetComponent< Renderer > ().material.mainTexture = mVideoTexture;
			updateShaderProperties(texWidth, texHeight);
#endif
        }
    }

    void OnDisable()
	{
		//Debug.Log ("OnDisable");
	}
#if UNITY_STANDALONE
	void setVideoURL(string filePath)
	{
	}

	IEnumerator DownloadMovie()  
	{//WWW www = new WWW("file:///D://Project//Movie/Data/Movie//movie.ogv");  
		
		string file =  Application.streamingAssetsPath + "/" + path; 
		WWW www = new WWW("file:///" + file);  
		mVideoTexture = (MovieTexture)www.movie;  
		
		while(!mVideoTexture.isReadyToPlay)  
			yield return www;  
		
		gameObject.GetComponent< Renderer > ().material.mainTexture = mVideoTexture;
		//renderer.material.mainTexture = movieTexture;//视频纹理  
		//audio.clip = movieTexture.audioClip;//音频  
	}

#endif

#if UNITY_ANDROID
	void setVideoURL(string filePath)
    {
        //Debug.Log("Call_Load filePath = " + filePath);

        using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            //Debug.Log("-------- activity = " + activity);
            if (javaVideoPlayer == null)
            {
                javaVideoPlayer = new AndroidJavaObject("cn.voidar.engine.NativeVideoPlayer");
            }
            javaVideoPlayer.Call("setActivity", activity);
            javaVideoPlayer.Call("setVideoURL", filePath);
        }
    }
#endif

#if !UNITY_STANDALONE
    private void createTexture(int videoWidth, int videoHeight)
    {
        //Debug.Log("----------------videoWidth = " + videoWidth + " videoHeight = " + videoHeight);
        mVideoTexture = new Texture2D(videoWidth, videoHeight, TextureFormat.RGBA32, false);
        mVideoTexture.filterMode = FilterMode.Bilinear;
        mVideoTexture.wrapMode = TextureWrapMode.Clamp;
        nativeTextureID = mVideoTexture.GetNativeTexturePtr().ToInt32();
        //Debug.Log("videoTextureId = " + nativeTextureID);
        GetComponent<Renderer>().material.mainTexture = mVideoTexture;
    }
#endif

#if UNITY_IPHONE
    private PlayerState mCurrState = PlayerState.NOT_READY;
    private bool isReady = false;
    void OnRenderObject(){
        updateVideoPath();
		if(nativeTextureID != -1){
			int result = updateVideoTexture(getVideoPlayer(),nativeTextureID);
			//Debug.Log("Unity OnRenderObject result = "+result);
			if(result != 0){
				if(!isReady){
					isReady = true;
					int videoWidth = getWidth(getVideoPlayer());
					int videoHeight = getHeight(getVideoPlayer());
					Debug.Log("----------------videoWidth = " + videoWidth + " videoHeight = " + videoHeight);
					updateShaderProperties(videoWidth, videoHeight);
				}

				if(result == 4 && loop){
					Debug.Log("Unity Relpay");
					seekTo(getVideoPlayer(),0);
					play(getVideoPlayer());
				}
				GL.InvalidateState();
			}
		}
    }

	IntPtr getVideoPlayer(){
		if(videoPlayerPtr == IntPtr.Zero){
			videoPlayerPtr = initVideoPlayer();
		}
		return videoPlayerPtr;
	}
#endif

#if UNITY_ANDROID
    private PlayerState mCurrState = PlayerState.NOT_READY;
    private bool isReady = false;

    void OnRenderObject()
    {
        if (nativeTextureID == -1)
        {
            updateVideoPath();
            return;
        }

        if (javaVideoPlayer != null)
        {
            mCurrState = (PlayerState)javaVideoPlayer.Call<int>("updateVideoTexture", nativeTextureID);
            //Debug.Log("######## android  size = " + mVideoTexture.width +", "+ mVideoTexture.height);
            if (!isReady && (mCurrState == PlayerState.READY || mCurrState == PlayerState.PLAYING))
            {
                int videoWidth = javaVideoPlayer.Call<int>("getVideoWidth");
                int videoHeight = javaVideoPlayer.Call<int>("getVideoHeight");
                updateShaderProperties(videoWidth, videoHeight);
                isReady = true;

                if (mCurrState == PlayerState.READY)
                {
#if UNITY_ANDROID
                        javaVideoPlayer.Call("play");
#endif
                }
            }

            if (mCurrState == PlayerState.PLAYING) {
                GetComponent<Renderer>().enabled = true;
                GL.InvalidateState();
            }
            else if (mCurrState == PlayerState.END)
            {
                if (loop)
                {
                    //Debug.Log("-----------LOOP--------");
                    javaVideoPlayer.Call("seekTo", 0f);
                    javaVideoPlayer.Call("play");
                }
            }
            else if (mCurrState == PlayerState.ERROR)
            {
                //Debug.Log("-----------Android MeidaPlayer Error--------");
            }
            
        }

    }

#endif
    void Update()
    {
        if ( !Application.isPlaying )
        {
            if (autoScale)
            {
                Transform quad = transform.parent.FindChild("MarkerQuad");
                if (quad != null)
                {
                    transform.localScale = new Vector3(quad.localScale.x / 10.0f, 0.1f, quad.localScale.y / 10.0f);
                }
            }
        }


        #if UNITY_STANDALONE
         updateVideoPath();
        #endif
    }

    void OnDestroy()
    {
        if (!Application.isPlaying)
        {
            return;
        }
#if UNITY_ANDROID
        if (javaVideoPlayer != null)
        {
            javaVideoPlayer.Call("onExit");
        }
#elif UNITY_IPHONE
		onExit(getVideoPlayer());
#endif
    }

    void OnApplicationPause(bool pause) {
        if (pause) { 
#if UNITY_ANDROID
            if (javaVideoPlayer != null)
            {
                javaVideoPlayer.Call("pause");
            }
#elif UNITY_IPHONE
			pauseVideo(getVideoPlayer());
#elif UNITY_STANDALONE
			mVideoTexture.Pause();
#endif
        }
    }

	public void OnFind(){
		//Debug.Log("Video on Find");
        LostOther();
#if UNITY_ANDROID
        javaVideoPlayer.Call("play");
#elif UNITY_IPHONE
        play(getVideoPlayer());
#elif UNITY_STANDALONE
		mVideoTexture.Play();
#endif
    }

    public void OnLost(){
        //Debug.Log("Video on Lost");
#if UNITY_ANDROID
        javaVideoPlayer.Call("pause");
#elif UNITY_IPHONE
		pauseVideo(getVideoPlayer());
#elif UNITY_STANDALONE
		mVideoTexture.Pause();
#endif
    }

    private void LostOther()
    {
        Marker[] allMarkers = (Marker[])GameObject.FindObjectsOfType(typeof(Marker));
        //Debug.Log("allMarkers len =  = = " + allMarkers.Length);
        foreach (Marker item in allMarkers)
        {
            if (item.model.activeSelf == true)
            {
                //Debug.Log("marker model show");
                VideoBehaviour video = item.model.GetComponent<VideoBehaviour>();
                if (video && video != this)
                {
                    //Debug.Log("video onLost  path = " + video.path);
                    item.OnLost();
                }
            }
        }
    }
}
