﻿using UnityEngine;
using System.Collections;
using System;

public class Marker : MonoBehaviour
{
    enum EMarkerState { INIT, FIND, LOST }
    EMarkerState markerState;
   
    public GameObject model;
    //[HideInInspector]
    public string imageFilePath;
	// Use this for initialization
	void Start () {
        if ( model )
            model.SetActive(false);
		GameObject quad = transform.FindChild ("MarkerQuad").gameObject; 
		quad.SetActive (false);
        markerState = EMarkerState.INIT;
	}
	
	// Update is called once per frame
	public void UpdateState ( bool find ) {
        if ( markerState == EMarkerState.FIND )
        {
            if ( !find)
            {
                OnLost();
            }
        }
        else
        {
            if ( find )
            {
                OnFind();
            }
        }
	}

    public void OnFind()
    {
        markerState = EMarkerState.FIND;
        if (model)
        {
            model.SetActive(true);
			var finders = model.GetComponents(typeof(FinderInterface));
			if(finders == null){
				return;
			}
			foreach(FinderInterface item in finders){
				item.OnFind();
			}
        }
        
    }

    public void OnLost()
    {
        if (model)
        {
            model.SetActive(false);
			var finders = model.GetComponents(typeof(FinderInterface));
			if(finders == null){
				return;
			}
			foreach(FinderInterface item in finders){
				item.OnLost();
			}
        }

        markerState = EMarkerState.LOST;


    }
    public void OnTracking()
    {
        Debug.Log("[Marker]=====OnTracking");
    }


    public void setMatrix(Matrix4x4 worldMatrix)
    {
        if(model){
            model.transform.localScale = worldMatrix.GetScale();
            model.transform.localRotation = worldMatrix.GetRotation();
            model.transform.localPosition = worldMatrix.GetPosition();
        }
    }

}
