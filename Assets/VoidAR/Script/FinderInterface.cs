﻿using System;

public interface FinderInterface  {
		void OnFind();
		void OnLost();
}
