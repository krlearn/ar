﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3575456220;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "AssemblyU2DCSharp_VideoBehaviour_ScaleMode2614717841.h"
#include "AssemblyU2DCSharp_VideoBehaviour_PlayerState2629781050.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoBehaviour
struct  VideoBehaviour_t999888626  : public MonoBehaviour_t774292115
{
public:
	// System.String VideoBehaviour::path
	String_t* ___path_2;
	// System.Boolean VideoBehaviour::loop
	bool ___loop_3;
	// System.Boolean VideoBehaviour::autoScale
	bool ___autoScale_4;
	// System.String VideoBehaviour::lastPath
	String_t* ___lastPath_5;
	// System.IntPtr VideoBehaviour::videoPlayerPtr
	IntPtr_t ___videoPlayerPtr_6;
	// UnityEngine.Texture2D VideoBehaviour::mVideoTexture
	Texture2D_t3575456220 * ___mVideoTexture_7;
	// System.Int32 VideoBehaviour::nativeTextureID
	int32_t ___nativeTextureID_8;
	// System.Boolean VideoBehaviour::isLoaded
	bool ___isLoaded_9;
	// VideoBehaviour/ScaleMode VideoBehaviour::scaleMode
	int32_t ___scaleMode_10;
	// VideoBehaviour/PlayerState VideoBehaviour::mCurrState
	int32_t ___mCurrState_11;
	// System.Boolean VideoBehaviour::isReady
	bool ___isReady_12;

public:
	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier(&___path_2, value);
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}

	inline static int32_t get_offset_of_autoScale_4() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___autoScale_4)); }
	inline bool get_autoScale_4() const { return ___autoScale_4; }
	inline bool* get_address_of_autoScale_4() { return &___autoScale_4; }
	inline void set_autoScale_4(bool value)
	{
		___autoScale_4 = value;
	}

	inline static int32_t get_offset_of_lastPath_5() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___lastPath_5)); }
	inline String_t* get_lastPath_5() const { return ___lastPath_5; }
	inline String_t** get_address_of_lastPath_5() { return &___lastPath_5; }
	inline void set_lastPath_5(String_t* value)
	{
		___lastPath_5 = value;
		Il2CppCodeGenWriteBarrier(&___lastPath_5, value);
	}

	inline static int32_t get_offset_of_videoPlayerPtr_6() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___videoPlayerPtr_6)); }
	inline IntPtr_t get_videoPlayerPtr_6() const { return ___videoPlayerPtr_6; }
	inline IntPtr_t* get_address_of_videoPlayerPtr_6() { return &___videoPlayerPtr_6; }
	inline void set_videoPlayerPtr_6(IntPtr_t value)
	{
		___videoPlayerPtr_6 = value;
	}

	inline static int32_t get_offset_of_mVideoTexture_7() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___mVideoTexture_7)); }
	inline Texture2D_t3575456220 * get_mVideoTexture_7() const { return ___mVideoTexture_7; }
	inline Texture2D_t3575456220 ** get_address_of_mVideoTexture_7() { return &___mVideoTexture_7; }
	inline void set_mVideoTexture_7(Texture2D_t3575456220 * value)
	{
		___mVideoTexture_7 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoTexture_7, value);
	}

	inline static int32_t get_offset_of_nativeTextureID_8() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___nativeTextureID_8)); }
	inline int32_t get_nativeTextureID_8() const { return ___nativeTextureID_8; }
	inline int32_t* get_address_of_nativeTextureID_8() { return &___nativeTextureID_8; }
	inline void set_nativeTextureID_8(int32_t value)
	{
		___nativeTextureID_8 = value;
	}

	inline static int32_t get_offset_of_isLoaded_9() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___isLoaded_9)); }
	inline bool get_isLoaded_9() const { return ___isLoaded_9; }
	inline bool* get_address_of_isLoaded_9() { return &___isLoaded_9; }
	inline void set_isLoaded_9(bool value)
	{
		___isLoaded_9 = value;
	}

	inline static int32_t get_offset_of_scaleMode_10() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___scaleMode_10)); }
	inline int32_t get_scaleMode_10() const { return ___scaleMode_10; }
	inline int32_t* get_address_of_scaleMode_10() { return &___scaleMode_10; }
	inline void set_scaleMode_10(int32_t value)
	{
		___scaleMode_10 = value;
	}

	inline static int32_t get_offset_of_mCurrState_11() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___mCurrState_11)); }
	inline int32_t get_mCurrState_11() const { return ___mCurrState_11; }
	inline int32_t* get_address_of_mCurrState_11() { return &___mCurrState_11; }
	inline void set_mCurrState_11(int32_t value)
	{
		___mCurrState_11 = value;
	}

	inline static int32_t get_offset_of_isReady_12() { return static_cast<int32_t>(offsetof(VideoBehaviour_t999888626, ___isReady_12)); }
	inline bool get_isReady_12() const { return ___isReady_12; }
	inline bool* get_address_of_isReady_12() { return &___isReady_12; }
	inline void set_isReady_12(bool value)
	{
		___isReady_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
