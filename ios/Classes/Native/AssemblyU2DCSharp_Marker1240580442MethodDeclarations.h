﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Marker
struct Marker_t1240580442;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x41261955742.h"

// System.Void Marker::.ctor()
extern "C"  void Marker__ctor_m1191903673 (Marker_t1240580442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker::Start()
extern "C"  void Marker_Start_m2923530521 (Marker_t1240580442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker::UpdateState(System.Boolean)
extern "C"  void Marker_UpdateState_m1195485018 (Marker_t1240580442 * __this, bool ___find0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker::OnFind()
extern "C"  void Marker_OnFind_m3478600613 (Marker_t1240580442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker::OnLost()
extern "C"  void Marker_OnLost_m1057848938 (Marker_t1240580442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker::OnTracking()
extern "C"  void Marker_OnTracking_m3358094995 (Marker_t1240580442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Marker::setMatrix(UnityEngine.Matrix4x4)
extern "C"  void Marker_setMatrix_m327639248 (Marker_t1240580442 * __this, Matrix4x4_t1261955742  ___worldMatrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
