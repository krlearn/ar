﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;
// System.Collections.Generic.List`1<JPGUrl2ImageTargetUrl>
struct List_1_t2159143318;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicLoadUI
struct  DynamicLoadUI_t1834292937  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GameObject DynamicLoadUI::addMarker
	GameObject_t1366199518 * ___addMarker_2;
	// UnityEngine.GameObject DynamicLoadUI::exit
	GameObject_t1366199518 * ___exit_3;
	// UnityEngine.GameObject DynamicLoadUI::delMarker
	GameObject_t1366199518 * ___delMarker_4;
	// System.Collections.Generic.List`1<JPGUrl2ImageTargetUrl> DynamicLoadUI::jpgUrl2targetUrl
	List_1_t2159143318 * ___jpgUrl2targetUrl_5;

public:
	inline static int32_t get_offset_of_addMarker_2() { return static_cast<int32_t>(offsetof(DynamicLoadUI_t1834292937, ___addMarker_2)); }
	inline GameObject_t1366199518 * get_addMarker_2() const { return ___addMarker_2; }
	inline GameObject_t1366199518 ** get_address_of_addMarker_2() { return &___addMarker_2; }
	inline void set_addMarker_2(GameObject_t1366199518 * value)
	{
		___addMarker_2 = value;
		Il2CppCodeGenWriteBarrier(&___addMarker_2, value);
	}

	inline static int32_t get_offset_of_exit_3() { return static_cast<int32_t>(offsetof(DynamicLoadUI_t1834292937, ___exit_3)); }
	inline GameObject_t1366199518 * get_exit_3() const { return ___exit_3; }
	inline GameObject_t1366199518 ** get_address_of_exit_3() { return &___exit_3; }
	inline void set_exit_3(GameObject_t1366199518 * value)
	{
		___exit_3 = value;
		Il2CppCodeGenWriteBarrier(&___exit_3, value);
	}

	inline static int32_t get_offset_of_delMarker_4() { return static_cast<int32_t>(offsetof(DynamicLoadUI_t1834292937, ___delMarker_4)); }
	inline GameObject_t1366199518 * get_delMarker_4() const { return ___delMarker_4; }
	inline GameObject_t1366199518 ** get_address_of_delMarker_4() { return &___delMarker_4; }
	inline void set_delMarker_4(GameObject_t1366199518 * value)
	{
		___delMarker_4 = value;
		Il2CppCodeGenWriteBarrier(&___delMarker_4, value);
	}

	inline static int32_t get_offset_of_jpgUrl2targetUrl_5() { return static_cast<int32_t>(offsetof(DynamicLoadUI_t1834292937, ___jpgUrl2targetUrl_5)); }
	inline List_1_t2159143318 * get_jpgUrl2targetUrl_5() const { return ___jpgUrl2targetUrl_5; }
	inline List_1_t2159143318 ** get_address_of_jpgUrl2targetUrl_5() { return &___jpgUrl2targetUrl_5; }
	inline void set_jpgUrl2targetUrl_5(List_1_t2159143318 * value)
	{
		___jpgUrl2targetUrl_5 = value;
		Il2CppCodeGenWriteBarrier(&___jpgUrl2targetUrl_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
