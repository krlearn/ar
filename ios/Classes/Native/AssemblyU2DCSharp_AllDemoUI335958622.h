﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AllDemoUI
struct  AllDemoUI_t335958622  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GameObject AllDemoUI::imageDemo
	GameObject_t1366199518 * ___imageDemo_2;
	// UnityEngine.GameObject AllDemoUI::shapeDemo
	GameObject_t1366199518 * ___shapeDemo_3;
	// UnityEngine.GameObject AllDemoUI::videoDemo
	GameObject_t1366199518 * ___videoDemo_4;
	// UnityEngine.GameObject AllDemoUI::cloudDemo
	GameObject_t1366199518 * ___cloudDemo_5;
	// UnityEngine.GameObject AllDemoUI::dynamicLoadDemo
	GameObject_t1366199518 * ___dynamicLoadDemo_6;

public:
	inline static int32_t get_offset_of_imageDemo_2() { return static_cast<int32_t>(offsetof(AllDemoUI_t335958622, ___imageDemo_2)); }
	inline GameObject_t1366199518 * get_imageDemo_2() const { return ___imageDemo_2; }
	inline GameObject_t1366199518 ** get_address_of_imageDemo_2() { return &___imageDemo_2; }
	inline void set_imageDemo_2(GameObject_t1366199518 * value)
	{
		___imageDemo_2 = value;
		Il2CppCodeGenWriteBarrier(&___imageDemo_2, value);
	}

	inline static int32_t get_offset_of_shapeDemo_3() { return static_cast<int32_t>(offsetof(AllDemoUI_t335958622, ___shapeDemo_3)); }
	inline GameObject_t1366199518 * get_shapeDemo_3() const { return ___shapeDemo_3; }
	inline GameObject_t1366199518 ** get_address_of_shapeDemo_3() { return &___shapeDemo_3; }
	inline void set_shapeDemo_3(GameObject_t1366199518 * value)
	{
		___shapeDemo_3 = value;
		Il2CppCodeGenWriteBarrier(&___shapeDemo_3, value);
	}

	inline static int32_t get_offset_of_videoDemo_4() { return static_cast<int32_t>(offsetof(AllDemoUI_t335958622, ___videoDemo_4)); }
	inline GameObject_t1366199518 * get_videoDemo_4() const { return ___videoDemo_4; }
	inline GameObject_t1366199518 ** get_address_of_videoDemo_4() { return &___videoDemo_4; }
	inline void set_videoDemo_4(GameObject_t1366199518 * value)
	{
		___videoDemo_4 = value;
		Il2CppCodeGenWriteBarrier(&___videoDemo_4, value);
	}

	inline static int32_t get_offset_of_cloudDemo_5() { return static_cast<int32_t>(offsetof(AllDemoUI_t335958622, ___cloudDemo_5)); }
	inline GameObject_t1366199518 * get_cloudDemo_5() const { return ___cloudDemo_5; }
	inline GameObject_t1366199518 ** get_address_of_cloudDemo_5() { return &___cloudDemo_5; }
	inline void set_cloudDemo_5(GameObject_t1366199518 * value)
	{
		___cloudDemo_5 = value;
		Il2CppCodeGenWriteBarrier(&___cloudDemo_5, value);
	}

	inline static int32_t get_offset_of_dynamicLoadDemo_6() { return static_cast<int32_t>(offsetof(AllDemoUI_t335958622, ___dynamicLoadDemo_6)); }
	inline GameObject_t1366199518 * get_dynamicLoadDemo_6() const { return ___dynamicLoadDemo_6; }
	inline GameObject_t1366199518 ** get_address_of_dynamicLoadDemo_6() { return &___dynamicLoadDemo_6; }
	inline void set_dynamicLoadDemo_6(GameObject_t1366199518 * value)
	{
		___dynamicLoadDemo_6 = value;
		Il2CppCodeGenWriteBarrier(&___dynamicLoadDemo_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
