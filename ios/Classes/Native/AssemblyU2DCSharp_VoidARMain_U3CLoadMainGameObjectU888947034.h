﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3146501818;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// Marker
struct Marker_t1240580442;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidARMain/<LoadMainGameObject>c__Iterator1
struct  U3CLoadMainGameObjectU3Ec__Iterator1_t888947034  : public Il2CppObject
{
public:
	// System.String VoidARMain/<LoadMainGameObject>c__Iterator1::path
	String_t* ___path_0;
	// UnityEngine.WWW VoidARMain/<LoadMainGameObject>c__Iterator1::<bundle>__0
	WWW_t3146501818 * ___U3CbundleU3E__0_1;
	// System.String VoidARMain/<LoadMainGameObject>c__Iterator1::name
	String_t* ___name_2;
	// UnityEngine.GameObject VoidARMain/<LoadMainGameObject>c__Iterator1::<myBundle>__1
	GameObject_t1366199518 * ___U3CmyBundleU3E__1_3;
	// Marker VoidARMain/<LoadMainGameObject>c__Iterator1::<m>__2
	Marker_t1240580442 * ___U3CmU3E__2_4;
	// System.Int32 VoidARMain/<LoadMainGameObject>c__Iterator1::$PC
	int32_t ___U24PC_5;
	// System.Object VoidARMain/<LoadMainGameObject>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.String VoidARMain/<LoadMainGameObject>c__Iterator1::<$>path
	String_t* ___U3CU24U3Epath_7;
	// System.String VoidARMain/<LoadMainGameObject>c__Iterator1::<$>name
	String_t* ___U3CU24U3Ename_8;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_U3CbundleU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___U3CbundleU3E__0_1)); }
	inline WWW_t3146501818 * get_U3CbundleU3E__0_1() const { return ___U3CbundleU3E__0_1; }
	inline WWW_t3146501818 ** get_address_of_U3CbundleU3E__0_1() { return &___U3CbundleU3E__0_1; }
	inline void set_U3CbundleU3E__0_1(WWW_t3146501818 * value)
	{
		___U3CbundleU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbundleU3E__0_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_U3CmyBundleU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___U3CmyBundleU3E__1_3)); }
	inline GameObject_t1366199518 * get_U3CmyBundleU3E__1_3() const { return ___U3CmyBundleU3E__1_3; }
	inline GameObject_t1366199518 ** get_address_of_U3CmyBundleU3E__1_3() { return &___U3CmyBundleU3E__1_3; }
	inline void set_U3CmyBundleU3E__1_3(GameObject_t1366199518 * value)
	{
		___U3CmyBundleU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyBundleU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CmU3E__2_4() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___U3CmU3E__2_4)); }
	inline Marker_t1240580442 * get_U3CmU3E__2_4() const { return ___U3CmU3E__2_4; }
	inline Marker_t1240580442 ** get_address_of_U3CmU3E__2_4() { return &___U3CmU3E__2_4; }
	inline void set_U3CmU3E__2_4(Marker_t1240580442 * value)
	{
		___U3CmU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Epath_7() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___U3CU24U3Epath_7)); }
	inline String_t* get_U3CU24U3Epath_7() const { return ___U3CU24U3Epath_7; }
	inline String_t** get_address_of_U3CU24U3Epath_7() { return &___U3CU24U3Epath_7; }
	inline void set_U3CU24U3Epath_7(String_t* value)
	{
		___U3CU24U3Epath_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Epath_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ename_8() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034, ___U3CU24U3Ename_8)); }
	inline String_t* get_U3CU24U3Ename_8() const { return ___U3CU24U3Ename_8; }
	inline String_t** get_address_of_U3CU24U3Ename_8() { return &___U3CU24U3Ename_8; }
	inline void set_U3CU24U3Ename_8(String_t* value)
	{
		___U3CU24U3Ename_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ename_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
