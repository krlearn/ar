﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidARMain/<AddTargets>c__Iterator3
struct U3CAddTargetsU3Ec__Iterator3_t660142821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VoidARMain/<AddTargets>c__Iterator3::.ctor()
extern "C"  void U3CAddTargetsU3Ec__Iterator3__ctor_m4182318543 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoidARMain/<AddTargets>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAddTargetsU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2910634357 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoidARMain/<AddTargets>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAddTargetsU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3680384733 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidARMain/<AddTargets>c__Iterator3::MoveNext()
extern "C"  bool U3CAddTargetsU3Ec__Iterator3_MoveNext_m2505530933 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain/<AddTargets>c__Iterator3::Dispose()
extern "C"  void U3CAddTargetsU3Ec__Iterator3_Dispose_m964754966 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain/<AddTargets>c__Iterator3::Reset()
extern "C"  void U3CAddTargetsU3Ec__Iterator3_Reset_m2343917316 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
