﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoidAR
struct VoidAR_t451713137;
// System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>
struct Dictionary_2_t3496208296;
// Marker
struct Marker_t1240580442;
// System.Double[]
struct DoubleU5BU5D_t2839599125;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// UnityEngine.Texture2D
struct Texture2D_t3575456220;
// System.Byte[]
struct ByteU5BU5D_t3835026402;
// UnityEngine.Color32[]
struct Color32U5BU5D_t269272674;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1383993251;

#include "mscorlib_System_Object707969140.h"
#include "AssemblyU2DCSharp_VoidAR_EState127356804.h"
#include "UnityEngine_UnityEngine_Matrix4x41261955742.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle1044382764.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "AssemblyU2DCSharp_VoidAR_ExternalTextureDesc3964848077.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidAR
struct  VoidAR_t451713137  : public Il2CppObject
{
public:
	// VoidAR/EState VoidAR::state
	int32_t ___state_2;
	// UnityEngine.Matrix4x4 VoidAR::projMatrix
	Matrix4x4_t1261955742  ___projMatrix_3;
	// System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap> VoidAR::mMarkers
	Dictionary_2_t3496208296 * ___mMarkers_4;
	// Marker VoidAR::currentTrackingMark
	Marker_t1240580442 * ___currentTrackingMark_5;
	// System.Double[] VoidAR::vmMatrixData
	DoubleU5BU5D_t2839599125* ___vmMatrixData_6;
	// UnityEngine.GameObject VoidAR::backGround
	GameObject_t1366199518 * ___backGround_7;
	// UnityEngine.Texture2D VoidAR::bgTexture1
	Texture2D_t3575456220 * ___bgTexture1_8;
	// UnityEngine.Texture2D VoidAR::bgTexture2
	Texture2D_t3575456220 * ___bgTexture2_9;
	// System.Byte[] VoidAR::byteFrame
	ByteU5BU5D_t3835026402* ___byteFrame_10;
	// UnityEngine.Color32[] VoidAR::bgFrame
	Color32U5BU5D_t269272674* ___bgFrame_11;
	// System.Runtime.InteropServices.GCHandle VoidAR::bgFrameHandle
	GCHandle_t1044382764  ___bgFrameHandle_12;
	// System.Int32 VoidAR::screen_width
	int32_t ___screen_width_13;
	// System.Int32 VoidAR::screen_height
	int32_t ___screen_height_14;
	// System.IntPtr VoidAR::onAnalyzingPtr
	IntPtr_t ___onAnalyzingPtr_15;
	// System.IntPtr VoidAR::p1
	IntPtr_t ___p1_16;
	// System.IntPtr VoidAR::p2
	IntPtr_t ___p2_17;
	// System.IntPtr VoidAR::p3
	IntPtr_t ___p3_18;
	// UnityEngine.Vector2[] VoidAR::bguvs
	Vector2U5BU5D_t1383993251* ___bguvs_19;
	// System.Single VoidAR::orthoSize1
	float ___orthoSize1_20;
	// System.Single VoidAR::orthoSize2
	float ___orthoSize2_21;
	// System.Single VoidAR::screenH
	float ___screenH_22;
	// System.Single VoidAR::aspectRatio
	float ___aspectRatio_23;
	// System.Int32 VoidAR::cameaWidth
	int32_t ___cameaWidth_24;
	// System.Int32 VoidAR::cameraHeight
	int32_t ___cameraHeight_25;
	// VoidAR/ExternalTextureDesc VoidAR::externalTexture
	ExternalTextureDesc_t3964848077  ___externalTexture_26;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_projMatrix_3() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___projMatrix_3)); }
	inline Matrix4x4_t1261955742  get_projMatrix_3() const { return ___projMatrix_3; }
	inline Matrix4x4_t1261955742 * get_address_of_projMatrix_3() { return &___projMatrix_3; }
	inline void set_projMatrix_3(Matrix4x4_t1261955742  value)
	{
		___projMatrix_3 = value;
	}

	inline static int32_t get_offset_of_mMarkers_4() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___mMarkers_4)); }
	inline Dictionary_2_t3496208296 * get_mMarkers_4() const { return ___mMarkers_4; }
	inline Dictionary_2_t3496208296 ** get_address_of_mMarkers_4() { return &___mMarkers_4; }
	inline void set_mMarkers_4(Dictionary_2_t3496208296 * value)
	{
		___mMarkers_4 = value;
		Il2CppCodeGenWriteBarrier(&___mMarkers_4, value);
	}

	inline static int32_t get_offset_of_currentTrackingMark_5() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___currentTrackingMark_5)); }
	inline Marker_t1240580442 * get_currentTrackingMark_5() const { return ___currentTrackingMark_5; }
	inline Marker_t1240580442 ** get_address_of_currentTrackingMark_5() { return &___currentTrackingMark_5; }
	inline void set_currentTrackingMark_5(Marker_t1240580442 * value)
	{
		___currentTrackingMark_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentTrackingMark_5, value);
	}

	inline static int32_t get_offset_of_vmMatrixData_6() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___vmMatrixData_6)); }
	inline DoubleU5BU5D_t2839599125* get_vmMatrixData_6() const { return ___vmMatrixData_6; }
	inline DoubleU5BU5D_t2839599125** get_address_of_vmMatrixData_6() { return &___vmMatrixData_6; }
	inline void set_vmMatrixData_6(DoubleU5BU5D_t2839599125* value)
	{
		___vmMatrixData_6 = value;
		Il2CppCodeGenWriteBarrier(&___vmMatrixData_6, value);
	}

	inline static int32_t get_offset_of_backGround_7() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___backGround_7)); }
	inline GameObject_t1366199518 * get_backGround_7() const { return ___backGround_7; }
	inline GameObject_t1366199518 ** get_address_of_backGround_7() { return &___backGround_7; }
	inline void set_backGround_7(GameObject_t1366199518 * value)
	{
		___backGround_7 = value;
		Il2CppCodeGenWriteBarrier(&___backGround_7, value);
	}

	inline static int32_t get_offset_of_bgTexture1_8() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___bgTexture1_8)); }
	inline Texture2D_t3575456220 * get_bgTexture1_8() const { return ___bgTexture1_8; }
	inline Texture2D_t3575456220 ** get_address_of_bgTexture1_8() { return &___bgTexture1_8; }
	inline void set_bgTexture1_8(Texture2D_t3575456220 * value)
	{
		___bgTexture1_8 = value;
		Il2CppCodeGenWriteBarrier(&___bgTexture1_8, value);
	}

	inline static int32_t get_offset_of_bgTexture2_9() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___bgTexture2_9)); }
	inline Texture2D_t3575456220 * get_bgTexture2_9() const { return ___bgTexture2_9; }
	inline Texture2D_t3575456220 ** get_address_of_bgTexture2_9() { return &___bgTexture2_9; }
	inline void set_bgTexture2_9(Texture2D_t3575456220 * value)
	{
		___bgTexture2_9 = value;
		Il2CppCodeGenWriteBarrier(&___bgTexture2_9, value);
	}

	inline static int32_t get_offset_of_byteFrame_10() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___byteFrame_10)); }
	inline ByteU5BU5D_t3835026402* get_byteFrame_10() const { return ___byteFrame_10; }
	inline ByteU5BU5D_t3835026402** get_address_of_byteFrame_10() { return &___byteFrame_10; }
	inline void set_byteFrame_10(ByteU5BU5D_t3835026402* value)
	{
		___byteFrame_10 = value;
		Il2CppCodeGenWriteBarrier(&___byteFrame_10, value);
	}

	inline static int32_t get_offset_of_bgFrame_11() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___bgFrame_11)); }
	inline Color32U5BU5D_t269272674* get_bgFrame_11() const { return ___bgFrame_11; }
	inline Color32U5BU5D_t269272674** get_address_of_bgFrame_11() { return &___bgFrame_11; }
	inline void set_bgFrame_11(Color32U5BU5D_t269272674* value)
	{
		___bgFrame_11 = value;
		Il2CppCodeGenWriteBarrier(&___bgFrame_11, value);
	}

	inline static int32_t get_offset_of_bgFrameHandle_12() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___bgFrameHandle_12)); }
	inline GCHandle_t1044382764  get_bgFrameHandle_12() const { return ___bgFrameHandle_12; }
	inline GCHandle_t1044382764 * get_address_of_bgFrameHandle_12() { return &___bgFrameHandle_12; }
	inline void set_bgFrameHandle_12(GCHandle_t1044382764  value)
	{
		___bgFrameHandle_12 = value;
	}

	inline static int32_t get_offset_of_screen_width_13() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___screen_width_13)); }
	inline int32_t get_screen_width_13() const { return ___screen_width_13; }
	inline int32_t* get_address_of_screen_width_13() { return &___screen_width_13; }
	inline void set_screen_width_13(int32_t value)
	{
		___screen_width_13 = value;
	}

	inline static int32_t get_offset_of_screen_height_14() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___screen_height_14)); }
	inline int32_t get_screen_height_14() const { return ___screen_height_14; }
	inline int32_t* get_address_of_screen_height_14() { return &___screen_height_14; }
	inline void set_screen_height_14(int32_t value)
	{
		___screen_height_14 = value;
	}

	inline static int32_t get_offset_of_onAnalyzingPtr_15() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___onAnalyzingPtr_15)); }
	inline IntPtr_t get_onAnalyzingPtr_15() const { return ___onAnalyzingPtr_15; }
	inline IntPtr_t* get_address_of_onAnalyzingPtr_15() { return &___onAnalyzingPtr_15; }
	inline void set_onAnalyzingPtr_15(IntPtr_t value)
	{
		___onAnalyzingPtr_15 = value;
	}

	inline static int32_t get_offset_of_p1_16() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___p1_16)); }
	inline IntPtr_t get_p1_16() const { return ___p1_16; }
	inline IntPtr_t* get_address_of_p1_16() { return &___p1_16; }
	inline void set_p1_16(IntPtr_t value)
	{
		___p1_16 = value;
	}

	inline static int32_t get_offset_of_p2_17() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___p2_17)); }
	inline IntPtr_t get_p2_17() const { return ___p2_17; }
	inline IntPtr_t* get_address_of_p2_17() { return &___p2_17; }
	inline void set_p2_17(IntPtr_t value)
	{
		___p2_17 = value;
	}

	inline static int32_t get_offset_of_p3_18() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___p3_18)); }
	inline IntPtr_t get_p3_18() const { return ___p3_18; }
	inline IntPtr_t* get_address_of_p3_18() { return &___p3_18; }
	inline void set_p3_18(IntPtr_t value)
	{
		___p3_18 = value;
	}

	inline static int32_t get_offset_of_bguvs_19() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___bguvs_19)); }
	inline Vector2U5BU5D_t1383993251* get_bguvs_19() const { return ___bguvs_19; }
	inline Vector2U5BU5D_t1383993251** get_address_of_bguvs_19() { return &___bguvs_19; }
	inline void set_bguvs_19(Vector2U5BU5D_t1383993251* value)
	{
		___bguvs_19 = value;
		Il2CppCodeGenWriteBarrier(&___bguvs_19, value);
	}

	inline static int32_t get_offset_of_orthoSize1_20() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___orthoSize1_20)); }
	inline float get_orthoSize1_20() const { return ___orthoSize1_20; }
	inline float* get_address_of_orthoSize1_20() { return &___orthoSize1_20; }
	inline void set_orthoSize1_20(float value)
	{
		___orthoSize1_20 = value;
	}

	inline static int32_t get_offset_of_orthoSize2_21() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___orthoSize2_21)); }
	inline float get_orthoSize2_21() const { return ___orthoSize2_21; }
	inline float* get_address_of_orthoSize2_21() { return &___orthoSize2_21; }
	inline void set_orthoSize2_21(float value)
	{
		___orthoSize2_21 = value;
	}

	inline static int32_t get_offset_of_screenH_22() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___screenH_22)); }
	inline float get_screenH_22() const { return ___screenH_22; }
	inline float* get_address_of_screenH_22() { return &___screenH_22; }
	inline void set_screenH_22(float value)
	{
		___screenH_22 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_23() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___aspectRatio_23)); }
	inline float get_aspectRatio_23() const { return ___aspectRatio_23; }
	inline float* get_address_of_aspectRatio_23() { return &___aspectRatio_23; }
	inline void set_aspectRatio_23(float value)
	{
		___aspectRatio_23 = value;
	}

	inline static int32_t get_offset_of_cameaWidth_24() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___cameaWidth_24)); }
	inline int32_t get_cameaWidth_24() const { return ___cameaWidth_24; }
	inline int32_t* get_address_of_cameaWidth_24() { return &___cameaWidth_24; }
	inline void set_cameaWidth_24(int32_t value)
	{
		___cameaWidth_24 = value;
	}

	inline static int32_t get_offset_of_cameraHeight_25() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___cameraHeight_25)); }
	inline int32_t get_cameraHeight_25() const { return ___cameraHeight_25; }
	inline int32_t* get_address_of_cameraHeight_25() { return &___cameraHeight_25; }
	inline void set_cameraHeight_25(int32_t value)
	{
		___cameraHeight_25 = value;
	}

	inline static int32_t get_offset_of_externalTexture_26() { return static_cast<int32_t>(offsetof(VoidAR_t451713137, ___externalTexture_26)); }
	inline ExternalTextureDesc_t3964848077  get_externalTexture_26() const { return ___externalTexture_26; }
	inline ExternalTextureDesc_t3964848077 * get_address_of_externalTexture_26() { return &___externalTexture_26; }
	inline void set_externalTexture_26(ExternalTextureDesc_t3964848077  value)
	{
		___externalTexture_26 = value;
	}
};

struct VoidAR_t451713137_StaticFields
{
public:
	// VoidAR VoidAR::instance
	VoidAR_t451713137 * ___instance_1;

public:
	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(VoidAR_t451713137_StaticFields, ___instance_1)); }
	inline VoidAR_t451713137 * get_instance_1() const { return ___instance_1; }
	inline VoidAR_t451713137 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(VoidAR_t451713137 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier(&___instance_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
