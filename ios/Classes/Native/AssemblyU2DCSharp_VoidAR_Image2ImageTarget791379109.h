﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// System.Byte[]
struct ByteU5BU5D_t3835026402;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidAR/Image2ImageTarget
struct  Image2ImageTarget_t791379109  : public Il2CppObject
{
public:
	// System.Boolean VoidAR/Image2ImageTarget::isMarkerLocal
	bool ___isMarkerLocal_0;
	// System.String VoidAR/Image2ImageTarget::imageUrl
	String_t* ___imageUrl_1;
	// UnityEngine.GameObject VoidAR/Image2ImageTarget::ImageTarget
	GameObject_t1366199518 * ___ImageTarget_2;
	// System.Byte[] VoidAR/Image2ImageTarget::imagedata
	ByteU5BU5D_t3835026402* ___imagedata_3;

public:
	inline static int32_t get_offset_of_isMarkerLocal_0() { return static_cast<int32_t>(offsetof(Image2ImageTarget_t791379109, ___isMarkerLocal_0)); }
	inline bool get_isMarkerLocal_0() const { return ___isMarkerLocal_0; }
	inline bool* get_address_of_isMarkerLocal_0() { return &___isMarkerLocal_0; }
	inline void set_isMarkerLocal_0(bool value)
	{
		___isMarkerLocal_0 = value;
	}

	inline static int32_t get_offset_of_imageUrl_1() { return static_cast<int32_t>(offsetof(Image2ImageTarget_t791379109, ___imageUrl_1)); }
	inline String_t* get_imageUrl_1() const { return ___imageUrl_1; }
	inline String_t** get_address_of_imageUrl_1() { return &___imageUrl_1; }
	inline void set_imageUrl_1(String_t* value)
	{
		___imageUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&___imageUrl_1, value);
	}

	inline static int32_t get_offset_of_ImageTarget_2() { return static_cast<int32_t>(offsetof(Image2ImageTarget_t791379109, ___ImageTarget_2)); }
	inline GameObject_t1366199518 * get_ImageTarget_2() const { return ___ImageTarget_2; }
	inline GameObject_t1366199518 ** get_address_of_ImageTarget_2() { return &___ImageTarget_2; }
	inline void set_ImageTarget_2(GameObject_t1366199518 * value)
	{
		___ImageTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___ImageTarget_2, value);
	}

	inline static int32_t get_offset_of_imagedata_3() { return static_cast<int32_t>(offsetof(Image2ImageTarget_t791379109, ___imagedata_3)); }
	inline ByteU5BU5D_t3835026402* get_imagedata_3() const { return ___imagedata_3; }
	inline ByteU5BU5D_t3835026402** get_address_of_imagedata_3() { return &___imagedata_3; }
	inline void set_imagedata_3(ByteU5BU5D_t3835026402* value)
	{
		___imagedata_3 = value;
		Il2CppCodeGenWriteBarrier(&___imagedata_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
