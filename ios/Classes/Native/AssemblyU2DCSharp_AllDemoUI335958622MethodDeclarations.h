﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AllDemoUI
struct AllDemoUI_t335958622;
// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"

// System.Void AllDemoUI::.ctor()
extern "C"  void AllDemoUI__ctor_m3237967449 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::Start()
extern "C"  void AllDemoUI_Start_m2376172665 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::OnClick(UnityEngine.GameObject)
extern "C"  void AllDemoUI_OnClick_m3572893740 (AllDemoUI_t335958622 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::Update()
extern "C"  void AllDemoUI_Update_m2760020508 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::<Start>m__0()
extern "C"  void AllDemoUI_U3CStartU3Em__0_m4276879520 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::<Start>m__1()
extern "C"  void AllDemoUI_U3CStartU3Em__1_m2730998555 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::<Start>m__2()
extern "C"  void AllDemoUI_U3CStartU3Em__2_m3994554518 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::<Start>m__3()
extern "C"  void AllDemoUI_U3CStartU3Em__3_m2448673553 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AllDemoUI::<Start>m__4()
extern "C"  void AllDemoUI_U3CStartU3Em__4_m546562228 (AllDemoUI_t335958622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
