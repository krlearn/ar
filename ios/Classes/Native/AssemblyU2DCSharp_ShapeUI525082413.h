﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShapeUI
struct  ShapeUI_t525082413  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GameObject ShapeUI::add
	GameObject_t1366199518 * ___add_2;
	// UnityEngine.GameObject ShapeUI::clear
	GameObject_t1366199518 * ___clear_3;
	// UnityEngine.GameObject ShapeUI::exit
	GameObject_t1366199518 * ___exit_4;
	// UnityEngine.GameObject ShapeUI::image
	GameObject_t1366199518 * ___image_5;
	// System.Int32 ShapeUI::isTracking
	int32_t ___isTracking_6;

public:
	inline static int32_t get_offset_of_add_2() { return static_cast<int32_t>(offsetof(ShapeUI_t525082413, ___add_2)); }
	inline GameObject_t1366199518 * get_add_2() const { return ___add_2; }
	inline GameObject_t1366199518 ** get_address_of_add_2() { return &___add_2; }
	inline void set_add_2(GameObject_t1366199518 * value)
	{
		___add_2 = value;
		Il2CppCodeGenWriteBarrier(&___add_2, value);
	}

	inline static int32_t get_offset_of_clear_3() { return static_cast<int32_t>(offsetof(ShapeUI_t525082413, ___clear_3)); }
	inline GameObject_t1366199518 * get_clear_3() const { return ___clear_3; }
	inline GameObject_t1366199518 ** get_address_of_clear_3() { return &___clear_3; }
	inline void set_clear_3(GameObject_t1366199518 * value)
	{
		___clear_3 = value;
		Il2CppCodeGenWriteBarrier(&___clear_3, value);
	}

	inline static int32_t get_offset_of_exit_4() { return static_cast<int32_t>(offsetof(ShapeUI_t525082413, ___exit_4)); }
	inline GameObject_t1366199518 * get_exit_4() const { return ___exit_4; }
	inline GameObject_t1366199518 ** get_address_of_exit_4() { return &___exit_4; }
	inline void set_exit_4(GameObject_t1366199518 * value)
	{
		___exit_4 = value;
		Il2CppCodeGenWriteBarrier(&___exit_4, value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ShapeUI_t525082413, ___image_5)); }
	inline GameObject_t1366199518 * get_image_5() const { return ___image_5; }
	inline GameObject_t1366199518 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(GameObject_t1366199518 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier(&___image_5, value);
	}

	inline static int32_t get_offset_of_isTracking_6() { return static_cast<int32_t>(offsetof(ShapeUI_t525082413, ___isTracking_6)); }
	inline int32_t get_isTracking_6() const { return ___isTracking_6; }
	inline int32_t* get_address_of_isTracking_6() { return &___isTracking_6; }
	inline void set_isTracking_6(int32_t value)
	{
		___isTracking_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
