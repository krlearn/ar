﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra29268282MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,VoidAR/MarkerMap,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m548624469(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1172544762 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m4161450529_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,VoidAR/MarkerMap,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1311365057(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1445113794  (*) (Transform_1_t1172544762 *, String_t*, MarkerMap_t684226712 *, const MethodInfo*))Transform_1_Invoke_m2770612589_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,VoidAR/MarkerMap,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2676469034(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1172544762 *, String_t*, MarkerMap_t684226712 *, AsyncCallback_t889871978 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3014766640_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,VoidAR/MarkerMap,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m349778335(__this, ___result0, method) ((  DictionaryEntry_t1445113794  (*) (Transform_1_t1172544762 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m803975703_gshared)(__this, ___result0, method)
