﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AllDemoUI
struct AllDemoUI_t335958622;
// UnityEngine.UI.Button
struct Button_t2491204935;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// CloudUI
struct CloudUI_t2626580091;
// DragObject
struct DragObject_t4073257439;
// DynamicLoadUI
struct DynamicLoadUI_t1834292937;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;
// DynamicLoadUI/<LoadGameObjects>c__Iterator0
struct U3CLoadGameObjectsU3Ec__Iterator0_t4032670743;
// ImageUI
struct ImageUI_t751656141;
// JPGUrl2ImageTargetUrl
struct JPGUrl2ImageTargetUrl_t3047771975;
// Marker
struct Marker_t1240580442;
// ShapeUI
struct ShapeUI_t525082413;
// VoidARMain
struct VoidARMain_t3733354248;
// UnityEngine.UI.Image
struct Image_t2058862956;
// VideoBehaviour
struct VideoBehaviour_t999888626;
// System.String
struct String_t;
// UnityEngine.Renderer
struct Renderer_t2715231144;
// VideoUI
struct VideoUI_t1842427015;
// VoidAR
struct VoidAR_t451713137;
// System.Double[]
struct DoubleU5BU5D_t2839599125;
// System.Byte[]
struct ByteU5BU5D_t3835026402;
// System.Int32[]
struct Int32U5BU5D_t3315407976;
// UnityEngine.Camera
struct Camera_t2805735124;
// VoidAR/Image2ImageTarget
struct Image2ImageTarget_t791379109;
// System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>
struct List_1_t4197717748;
// VoidAR/MarkerMap
struct MarkerMap_t684226712;
// VoidAR/MatchingResultDelegate
struct MatchingResultDelegate_t688824355;
// System.IAsyncResult
struct IAsyncResult_t1538479585;
// System.AsyncCallback
struct AsyncCallback_t889871978;
// VoidAR/MyDelegate
struct MyDelegate_t3224270133;
// VoidARMain/<AddTargets>c__Iterator3
struct U3CAddTargetsU3Ec__Iterator3_t660142821;
// VoidARMain/<LoadMainGameObject>c__Iterator1
struct U3CLoadMainGameObjectU3Ec__Iterator1_t888947034;
// VoidARMain/<LoadMainGameObjectTest>c__Iterator2
struct U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array4136897760.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AllDemoUI335958622.h"
#include "AssemblyU2DCSharp_AllDemoUI335958622MethodDeclarations.h"
#include "mscorlib_System_Void2799814243.h"
#include "UnityEngine_UnityEngine_MonoBehaviour774292115MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1366199518MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2491204935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction625099497MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent789719291MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2491204935.h"
#include "mscorlib_System_String1967731336.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked3753894607.h"
#include "UnityEngine_UnityEngine_Events_UnityAction625099497.h"
#include "mscorlib_System_Object707969140.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "UnityEngine_UnityEngine_Object1181371020MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application3439453659MethodDeclarations.h"
#include "mscorlib_System_Boolean3143194569.h"
#include "UnityEngine_UnityEngine_Object1181371020.h"
#include "AssemblyU2DCSharp_CloudUI2626580091.h"
#include "AssemblyU2DCSharp_CloudUI2626580091MethodDeclarations.h"
#include "AssemblyU2DCSharp_DragObject4073257439.h"
#include "AssemblyU2DCSharp_DragObject4073257439MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component1078601330MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform224878301MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane2472683311MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4173266137MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2805735124MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch4220330964MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2465617798MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray4121084637MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics1761133122MethodDeclarations.h"
#include "mscorlib_System_String1967731336MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane2472683311.h"
#include "UnityEngine_UnityEngine_Touch4220330964.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Int321448170597.h"
#include "UnityEngine_UnityEngine_Ray4121084637.h"
#include "mscorlib_System_Single1791520093.h"
#include "UnityEngine_UnityEngine_Vector3465617797.h"
#include "UnityEngine_UnityEngine_RaycastHit317570005.h"
#include "UnityEngine_UnityEngine_Transform224878301.h"
#include "UnityEngine_UnityEngine_Camera2805735124.h"
#include "UnityEngine_UnityEngine_Vector2465617798.h"
#include "UnityEngine_UnityEngine_TouchPhase3373375299.h"
#include "AssemblyU2DCSharp_DynamicLoadUI1834292937.h"
#include "AssemblyU2DCSharp_DynamicLoadUI1834292937MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2159143318MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2159143318.h"
#include "AssemblyU2DCSharp_JPGUrl2ImageTargetUrl3047771975MethodDeclarations.h"
#include "AssemblyU2DCSharp_JPGUrl2ImageTargetUrl3047771975.h"
#include "UnityEngine_UnityEngine_Debug12548584MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidAR451713137MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3261918659.h"
#include "AssemblyU2DCSharp_VoidAR451713137.h"
#include "AssemblyU2DCSharp_DynamicLoadUI_U3CLoadGameObjects4032670743MethodDeclarations.h"
#include "AssemblyU2DCSharp_DynamicLoadUI_U3CLoadGameObjects4032670743.h"
#include "mscorlib_System_Object707969140MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4197717748MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2420141438MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW3146501818MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidAR_Image2ImageTarget791379109MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundle945621937MethodDeclarations.h"
#include "mscorlib_System_UInt323922122178.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4197717748.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2420141438.h"
#include "UnityEngine_UnityEngine_WWW3146501818.h"
#include "AssemblyU2DCSharp_VoidAR_Image2ImageTarget791379109.h"
#include "UnityEngine_UnityEngine_AssetBundle945621937.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte77837043.h"
#include "mscorlib_System_NotSupportedException3178859535MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException3178859535.h"
#include "AssemblyU2DCSharp_ImageUI751656141.h"
#include "AssemblyU2DCSharp_ImageUI751656141MethodDeclarations.h"
#include "AssemblyU2DCSharp_Marker1240580442.h"
#include "AssemblyU2DCSharp_Marker1240580442MethodDeclarations.h"
#include "AssemblyU2DCSharp_Marker_EMarkerState3748166026.h"
#include "mscorlib_System_Type77837473MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component1078601330.h"
#include "mscorlib_System_Type77837473.h"
#include "mscorlib_System_RuntimeTypeHandle2078935215.h"
#include "UnityEngine_UnityEngine_Matrix4x41261955742.h"
#include "AssemblyU2DCSharp_MyUtils3616582885MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion83606849.h"
#include "AssemblyU2DCSharp_Marker_EMarkerState3748166026MethodDeclarations.h"
#include "AssemblyU2DCSharp_MyUtils3616582885.h"
#include "UnityEngine_UnityEngine_Quaternion83606849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3465617797MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4465617796MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4465617796.h"
#include "UnityEngine_UnityEngine_Matrix4x41261955742MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShapeUI525082413.h"
#include "AssemblyU2DCSharp_ShapeUI525082413MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidARMain3733354248.h"
#include "UnityEngine_UnityEngine_Color2250949164MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic4081158439MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2058862956.h"
#include "UnityEngine_UnityEngine_Color2250949164.h"
#include "mscorlib_System_Text_Encoding2125916575MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding2125916575.h"
#include "AssemblyU2DCSharp_VideoBehaviour999888626.h"
#include "AssemblyU2DCSharp_VideoBehaviour999888626MethodDeclarations.h"
#include "mscorlib_System_IntPtr3076297692MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer2715231144MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material2197338622MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoBehaviour_ScaleMode2614717841.h"
#include "UnityEngine_UnityEngine_Renderer2715231144.h"
#include "UnityEngine_UnityEngine_Material2197338622.h"
#include "UnityEngine_UnityEngine_Texture2D3575456220MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture465682066MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3575456220.h"
#include "UnityEngine_UnityEngine_TextureFormat3640027657.h"
#include "UnityEngine_UnityEngine_FilterMode1090826974.h"
#include "UnityEngine_UnityEngine_TextureWrapMode353616617.h"
#include "UnityEngine_UnityEngine_Texture465682066.h"
#include "UnityEngine_UnityEngine_GL1930963370MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_VideoBehaviour_PlayerState2629781050.h"
#include "AssemblyU2DCSharp_VideoBehaviour_PlayerState2629781050MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoBehaviour_ScaleMode2614717841MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoUI1842427015.h"
#include "AssemblyU2DCSharp_VideoUI1842427015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3496208296MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3496208296.h"
#include "mscorlib_System_Double609272444.h"
#include "UnityEngine_UnityEngine_Screen173559997MethodDeclarations.h"
#include "mscorlib_System_Math1031958459MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidAR_EState127356804.h"
#include "AssemblyU2DCSharp_VoidAR_ExternalTextureDesc3964848077.h"
#include "AssemblyU2DCSharp_VoidAR_MarkerMap684226712MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidAR_MarkerMap684226712.h"
#include "UnityEngine_UnityEngine_Resources3616872916MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En793537021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22053528608MethodDeclarations.h"
#include "mscorlib_System_Char3633234117.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22053528608.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En793537021.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat163748572MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat163748572.h"
#include "AssemblyU2DCSharp_VoidAR_EState127356804MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidAR_ExternalTextureDesc3964848077MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidAR_MatchingResultDelegate688824355.h"
#include "AssemblyU2DCSharp_VoidAR_MatchingResultDelegate688824355MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback889871978.h"
#include "AssemblyU2DCSharp_VoidAR_MyDelegate3224270133.h"
#include "AssemblyU2DCSharp_VoidAR_MyDelegate3224270133MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidAR_PowerAREvent1377968902.h"
#include "AssemblyU2DCSharp_VoidAR_PowerAREvent1377968902MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidARMain3733354248MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidARMain_EMarkerType3099013493.h"
#include "AssemblyU2DCSharp_VoidARMain_U3CLoadMainGameObjectU888947034MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidARMain_U3CLoadMainGameObjectU888947034.h"
#include "AssemblyU2DCSharp_VoidARMain_U3CLoadMainGameObjectT703256831MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidARMain_U3CLoadMainGameObjectT703256831.h"
#include "AssemblyU2DCSharp_VoidARMain_U3CAddTargetsU3Ec__Ite660142821MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidARMain_U3CAddTargetsU3Ec__Ite660142821.h"
#include "mscorlib_System_Int321448170597MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoidARMain_EMarkerType3099013493MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1366199518 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2491204935_m1008560876(__this, method) ((  Button_t2491204935 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VoidARMain>()
#define GameObject_GetComponent_TisVoidARMain_t3733354248_m2811090911(__this, method) ((  VoidARMain_t3733354248 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2058862956_m4162535761(__this, method) ((  Image_t2058862956 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(__this, method) ((  Renderer_t2715231144 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t1078601330 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2715231144_m772028041(__this, method) ((  Renderer_t2715231144 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<VideoBehaviour>()
#define GameObject_GetComponent_TisVideoBehaviour_t999888626_m2277997695(__this, method) ((  VideoBehaviour_t999888626 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Marker>()
#define GameObject_GetComponent_TisMarker_t1240580442_m452303857(__this, method) ((  Marker_t1240580442 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<VideoBehaviour>()
#define Component_GetComponent_TisVideoBehaviour_t999888626_m4069762147(__this, method) ((  VideoBehaviour_t999888626 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AllDemoUI::.ctor()
extern "C"  void AllDemoUI__ctor_m3237967449 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AllDemoUI::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var;
extern const MethodInfo* AllDemoUI_U3CStartU3Em__0_m4276879520_MethodInfo_var;
extern const MethodInfo* AllDemoUI_U3CStartU3Em__1_m2730998555_MethodInfo_var;
extern const MethodInfo* AllDemoUI_U3CStartU3Em__2_m3994554518_MethodInfo_var;
extern const MethodInfo* AllDemoUI_U3CStartU3Em__3_m2448673553_MethodInfo_var;
extern const MethodInfo* AllDemoUI_U3CStartU3Em__4_m546562228_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3098728128;
extern Il2CppCodeGenString* _stringLiteral1584119036;
extern Il2CppCodeGenString* _stringLiteral795689618;
extern Il2CppCodeGenString* _stringLiteral2173657872;
extern Il2CppCodeGenString* _stringLiteral2194622192;
extern const uint32_t AllDemoUI_Start_m2376172665_MetadataUsageId;
extern "C"  void AllDemoUI_Start_m2376172665 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AllDemoUI_Start_m2376172665_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	Button_t2491204935 * V_1 = NULL;
	Button_t2491204935 * V_2 = NULL;
	Button_t2491204935 * V_3 = NULL;
	Button_t2491204935 * V_4 = NULL;
	{
		GameObject_t1366199518 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3098728128, /*hidden argument*/NULL);
		__this->set_imageDemo_2(L_0);
		GameObject_t1366199518 * L_1 = __this->get_imageDemo_2();
		NullCheck(L_1);
		Button_t2491204935 * L_2 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_0 = L_2;
		Button_t2491204935 * L_3 = V_0;
		NullCheck(L_3);
		ButtonClickedEvent_t3753894607 * L_4 = Button_get_onClick_m1595880935(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)AllDemoUI_U3CStartU3Em__0_m4276879520_MethodInfo_var);
		UnityAction_t625099497 * L_6 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityEvent_AddListener_m1596810379(L_4, L_6, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_7 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1584119036, /*hidden argument*/NULL);
		__this->set_shapeDemo_3(L_7);
		GameObject_t1366199518 * L_8 = __this->get_shapeDemo_3();
		NullCheck(L_8);
		Button_t2491204935 * L_9 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_8, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_1 = L_9;
		Button_t2491204935 * L_10 = V_1;
		NullCheck(L_10);
		ButtonClickedEvent_t3753894607 * L_11 = Button_get_onClick_m1595880935(L_10, /*hidden argument*/NULL);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)AllDemoUI_U3CStartU3Em__1_m2730998555_MethodInfo_var);
		UnityAction_t625099497 * L_13 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_13, __this, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		UnityEvent_AddListener_m1596810379(L_11, L_13, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_14 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral795689618, /*hidden argument*/NULL);
		__this->set_videoDemo_4(L_14);
		GameObject_t1366199518 * L_15 = __this->get_videoDemo_4();
		NullCheck(L_15);
		Button_t2491204935 * L_16 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_15, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_2 = L_16;
		Button_t2491204935 * L_17 = V_2;
		NullCheck(L_17);
		ButtonClickedEvent_t3753894607 * L_18 = Button_get_onClick_m1595880935(L_17, /*hidden argument*/NULL);
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)AllDemoUI_U3CStartU3Em__2_m3994554518_MethodInfo_var);
		UnityAction_t625099497 * L_20 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_20, __this, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		UnityEvent_AddListener_m1596810379(L_18, L_20, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_21 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2173657872, /*hidden argument*/NULL);
		__this->set_cloudDemo_5(L_21);
		GameObject_t1366199518 * L_22 = __this->get_cloudDemo_5();
		NullCheck(L_22);
		Button_t2491204935 * L_23 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_22, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_3 = L_23;
		Button_t2491204935 * L_24 = V_3;
		NullCheck(L_24);
		ButtonClickedEvent_t3753894607 * L_25 = Button_get_onClick_m1595880935(L_24, /*hidden argument*/NULL);
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)AllDemoUI_U3CStartU3Em__3_m2448673553_MethodInfo_var);
		UnityAction_t625099497 * L_27 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_27, __this, L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		UnityEvent_AddListener_m1596810379(L_25, L_27, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_28 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2194622192, /*hidden argument*/NULL);
		__this->set_dynamicLoadDemo_6(L_28);
		GameObject_t1366199518 * L_29 = __this->get_dynamicLoadDemo_6();
		NullCheck(L_29);
		Button_t2491204935 * L_30 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_29, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_4 = L_30;
		Button_t2491204935 * L_31 = V_4;
		NullCheck(L_31);
		ButtonClickedEvent_t3753894607 * L_32 = Button_get_onClick_m1595880935(L_31, /*hidden argument*/NULL);
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)AllDemoUI_U3CStartU3Em__4_m546562228_MethodInfo_var);
		UnityAction_t625099497 * L_34 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_34, __this, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		UnityEvent_AddListener_m1596810379(L_32, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AllDemoUI::OnClick(UnityEngine.GameObject)
extern Il2CppCodeGenString* _stringLiteral3098728128;
extern Il2CppCodeGenString* _stringLiteral1584119036;
extern Il2CppCodeGenString* _stringLiteral795689618;
extern Il2CppCodeGenString* _stringLiteral2173657872;
extern Il2CppCodeGenString* _stringLiteral2194622192;
extern const uint32_t AllDemoUI_OnClick_m3572893740_MetadataUsageId;
extern "C"  void AllDemoUI_OnClick_m3572893740 (AllDemoUI_t335958622 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AllDemoUI_OnClick_m3572893740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = ___sender0;
		GameObject_t1366199518 * L_1 = __this->get_imageDemo_2();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral3098728128, /*hidden argument*/NULL);
	}

IL_001b:
	{
		GameObject_t1366199518 * L_3 = ___sender0;
		GameObject_t1366199518 * L_4 = __this->get_shapeDemo_3();
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral1584119036, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GameObject_t1366199518 * L_6 = ___sender0;
		GameObject_t1366199518 * L_7 = __this->get_videoDemo_4();
		bool L_8 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral795689618, /*hidden argument*/NULL);
	}

IL_0051:
	{
		GameObject_t1366199518 * L_9 = ___sender0;
		GameObject_t1366199518 * L_10 = __this->get_cloudDemo_5();
		bool L_11 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006c;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral2173657872, /*hidden argument*/NULL);
	}

IL_006c:
	{
		GameObject_t1366199518 * L_12 = ___sender0;
		GameObject_t1366199518 * L_13 = __this->get_dynamicLoadDemo_6();
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0087;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral2194622192, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return;
	}
}
// System.Void AllDemoUI::Update()
extern "C"  void AllDemoUI_Update_m2760020508 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AllDemoUI::<Start>m__0()
extern "C"  void AllDemoUI_U3CStartU3Em__0_m4276879520 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_imageDemo_2();
		AllDemoUI_OnClick_m3572893740(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AllDemoUI::<Start>m__1()
extern "C"  void AllDemoUI_U3CStartU3Em__1_m2730998555 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_shapeDemo_3();
		AllDemoUI_OnClick_m3572893740(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AllDemoUI::<Start>m__2()
extern "C"  void AllDemoUI_U3CStartU3Em__2_m3994554518 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_videoDemo_4();
		AllDemoUI_OnClick_m3572893740(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AllDemoUI::<Start>m__3()
extern "C"  void AllDemoUI_U3CStartU3Em__3_m2448673553 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_cloudDemo_5();
		AllDemoUI_OnClick_m3572893740(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AllDemoUI::<Start>m__4()
extern "C"  void AllDemoUI_U3CStartU3Em__4_m546562228 (AllDemoUI_t335958622 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_dynamicLoadDemo_6();
		AllDemoUI_OnClick_m3572893740(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CloudUI::.ctor()
extern "C"  void CloudUI__ctor_m3207867426 (CloudUI_t2626580091 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CloudUI::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var;
extern const MethodInfo* CloudUI_U3CStartU3Em__5_m3471406432_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern const uint32_t CloudUI_Start_m3010768470_MetadataUsageId;
extern "C"  void CloudUI_Start_m3010768470 (CloudUI_t2626580091 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CloudUI_Start_m3010768470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	{
		GameObject_t1366199518 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3558534422, /*hidden argument*/NULL);
		__this->set_exit_2(L_0);
		GameObject_t1366199518 * L_1 = __this->get_exit_2();
		NullCheck(L_1);
		Button_t2491204935 * L_2 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_0 = L_2;
		Button_t2491204935 * L_3 = V_0;
		NullCheck(L_3);
		ButtonClickedEvent_t3753894607 * L_4 = Button_get_onClick_m1595880935(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)CloudUI_U3CStartU3Em__5_m3471406432_MethodInfo_var);
		UnityAction_t625099497 * L_6 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityEvent_AddListener_m1596810379(L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CloudUI::OnClick(UnityEngine.GameObject)
extern Il2CppCodeGenString* _stringLiteral2245616130;
extern const uint32_t CloudUI_OnClick_m3960742617_MetadataUsageId;
extern "C"  void CloudUI_OnClick_m3960742617 (CloudUI_t2626580091 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CloudUI_OnClick_m3960742617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = ___sender0;
		GameObject_t1366199518 * L_1 = __this->get_exit_2();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral2245616130, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void CloudUI::<Start>m__5()
extern "C"  void CloudUI_U3CStartU3Em__5_m3471406432 (CloudUI_t2626580091 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_exit_2();
		CloudUI_OnClick_m3960742617(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragObject::.ctor()
extern "C"  void DragObject__ctor_m2177710912 (DragObject_t4073257439 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragObject::Start()
extern "C"  void DragObject_Start_m4294151148 (DragObject_t4073257439 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DragObject::Update()
extern Il2CppClass* Input_t4173266137_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastHit_t317570005_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral62618674;
extern Il2CppCodeGenString* _stringLiteral3143064304;
extern Il2CppCodeGenString* _stringLiteral3021629712;
extern Il2CppCodeGenString* _stringLiteral3326971234;
extern Il2CppCodeGenString* _stringLiteral1111077458;
extern Il2CppCodeGenString* _stringLiteral628790455;
extern Il2CppCodeGenString* _stringLiteral1502599955;
extern Il2CppCodeGenString* _stringLiteral2807976789;
extern Il2CppCodeGenString* _stringLiteral405905554;
extern Il2CppCodeGenString* _stringLiteral3719104155;
extern const uint32_t DragObject_Update_m1756000767_MetadataUsageId;
extern "C"  void DragObject_Update_m1756000767 (DragObject_t4073257439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DragObject_Update_m1756000767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Plane_t2472683311  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Touch_t4220330964  V_1;
	memset(&V_1, 0, sizeof(V_1));
	TouchU5BU5D_t2943471197* V_2 = NULL;
	int32_t V_3 = 0;
	Ray_t4121084637  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t465617797  V_6;
	memset(&V_6, 0, sizeof(V_6));
	RaycastHit_t317570005  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		Transform_t224878301 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t465617797  L_1 = Transform_get_up_m1603627763(L_0, /*hidden argument*/NULL);
		Transform_t224878301 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t465617797  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Plane__ctor_m3187718367((&V_0), L_1, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		TouchU5BU5D_t2943471197* L_4 = Input_get_touches_m388011594(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_4;
		V_3 = 0;
		goto IL_0185;
	}

IL_002a:
	{
		TouchU5BU5D_t2943471197* L_5 = V_2;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		V_1 = (*(Touch_t4220330964 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		Camera_t2805735124 * L_7 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t465617798  L_8 = Touch_get_position_m2079703643((&V_1), /*hidden argument*/NULL);
		Vector3_t465617797  L_9 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Ray_t4121084637  L_10 = Camera_ScreenPointToRay_m614889538(L_7, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		V_5 = (0.0f);
		Ray_t4121084637  L_11 = V_4;
		Plane_Raycast_m2870142810((&V_0), L_11, (&V_5), /*hidden argument*/NULL);
		float L_12 = V_5;
		Vector3_t465617797  L_13 = Ray_GetPoint_m1353702366((&V_4), L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		int32_t L_14 = Touch_get_phase_m196706494((&V_1), /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0181;
		}
	}
	{
		Initobj (RaycastHit_t317570005_il2cpp_TypeInfo_var, (&V_7));
		Ray_t4121084637  L_15 = V_4;
		bool L_16 = Physics_Raycast_m2308457076(NULL /*static, unused*/, L_15, (&V_7), (1000.0f), /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_017a;
		}
	}
	{
		GameObject_t1366199518 * L_17 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t224878301 * L_18 = GameObject_get_transform_m909382139(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m2079638459(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_19, _stringLiteral62618674, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c2;
		}
	}
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral3143064304, /*hidden argument*/NULL);
		goto IL_0175;
	}

IL_00c2:
	{
		GameObject_t1366199518 * L_21 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t224878301 * L_22 = GameObject_get_transform_m909382139(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = Object_get_name_m2079638459(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_23, _stringLiteral3021629712, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00f0;
		}
	}
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral3326971234, /*hidden argument*/NULL);
		goto IL_0175;
	}

IL_00f0:
	{
		GameObject_t1366199518 * L_25 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t224878301 * L_26 = GameObject_get_transform_m909382139(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = Object_get_name_m2079638459(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_28 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_27, _stringLiteral1111077458, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_011e;
		}
	}
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral628790455, /*hidden argument*/NULL);
		goto IL_0175;
	}

IL_011e:
	{
		GameObject_t1366199518 * L_29 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t224878301 * L_30 = GameObject_get_transform_m909382139(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = Object_get_name_m2079638459(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_31, _stringLiteral1502599955, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_014c;
		}
	}
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral2807976789, /*hidden argument*/NULL);
		goto IL_0175;
	}

IL_014c:
	{
		GameObject_t1366199518 * L_33 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_t224878301 * L_34 = GameObject_get_transform_m909382139(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_35 = Object_get_name_m2079638459(L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_35, _stringLiteral405905554, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0175;
		}
	}
	{
		Application_OpenURL_m3882634228(NULL /*static, unused*/, _stringLiteral3719104155, /*hidden argument*/NULL);
	}

IL_0175:
	{
		goto IL_0181;
	}

IL_017a:
	{
		__this->set_pickedObject_3((Transform_t224878301 *)NULL);
	}

IL_0181:
	{
		int32_t L_37 = V_3;
		V_3 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_0185:
	{
		int32_t L_38 = V_3;
		TouchU5BU5D_t2943471197* L_39 = V_2;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))))))
		{
			goto IL_002a;
		}
	}
	{
		return;
	}
}
// System.Void DynamicLoadUI::.ctor()
extern Il2CppClass* List_1_t2159143318_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1381601124_MethodInfo_var;
extern const uint32_t DynamicLoadUI__ctor_m1247451118_MetadataUsageId;
extern "C"  void DynamicLoadUI__ctor_m1247451118 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DynamicLoadUI__ctor_m1247451118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2159143318 * L_0 = (List_1_t2159143318 *)il2cpp_codegen_object_new(List_1_t2159143318_il2cpp_TypeInfo_var);
		List_1__ctor_m1381601124(L_0, /*hidden argument*/List_1__ctor_m1381601124_MethodInfo_var);
		__this->set_jpgUrl2targetUrl_5(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicLoadUI::initConfig()
extern Il2CppClass* JPGUrl2ImageTargetUrl_t3047771975_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2494949592;
extern Il2CppCodeGenString* _stringLiteral3897984470;
extern Il2CppCodeGenString* _stringLiteral1965113854;
extern const uint32_t DynamicLoadUI_initConfig_m503738412_MetadataUsageId;
extern "C"  void DynamicLoadUI_initConfig_m503738412 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DynamicLoadUI_initConfig_m503738412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JPGUrl2ImageTargetUrl_t3047771975 * V_0 = NULL;
	{
		JPGUrl2ImageTargetUrl_t3047771975 * L_0 = (JPGUrl2ImageTargetUrl_t3047771975 *)il2cpp_codegen_object_new(JPGUrl2ImageTargetUrl_t3047771975_il2cpp_TypeInfo_var);
		JPGUrl2ImageTargetUrl__ctor_m1425597358(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JPGUrl2ImageTargetUrl_t3047771975 * L_1 = V_0;
		String_t* L_2 = Application_get_streamingAssetsPath_m8890645(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2494949592, L_2, _stringLiteral3897984470, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_JpgNameURL_1(L_3);
		JPGUrl2ImageTargetUrl_t3047771975 * L_4 = V_0;
		String_t* L_5 = Application_get_streamingAssetsPath_m8890645(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2494949592, L_5, _stringLiteral1965113854, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_ImageTargetURL_2(L_6);
		JPGUrl2ImageTargetUrl_t3047771975 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_isLocalImage_0((bool)1);
		List_1_t2159143318 * L_8 = __this->get_jpgUrl2targetUrl_5();
		JPGUrl2ImageTargetUrl_t3047771975 * L_9 = V_0;
		NullCheck(L_8);
		VirtActionInvoker1< JPGUrl2ImageTargetUrl_t3047771975 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<JPGUrl2ImageTargetUrl>::Add(!0) */, L_8, L_9);
		return;
	}
}
// System.Void DynamicLoadUI::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var;
extern const MethodInfo* DynamicLoadUI_U3CStartU3Em__6_m3672923077_MethodInfo_var;
extern const MethodInfo* DynamicLoadUI_U3CStartU3Em__7_m3814085578_MethodInfo_var;
extern const MethodInfo* DynamicLoadUI_U3CStartU3Em__8_m1548837347_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2294030271;
extern Il2CppCodeGenString* _stringLiteral3626071671;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern const uint32_t DynamicLoadUI_Start_m439724174_MetadataUsageId;
extern "C"  void DynamicLoadUI_Start_m439724174 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DynamicLoadUI_Start_m439724174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	Button_t2491204935 * V_1 = NULL;
	Button_t2491204935 * V_2 = NULL;
	{
		DynamicLoadUI_initConfig_m503738412(__this, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2294030271, /*hidden argument*/NULL);
		__this->set_addMarker_2(L_0);
		GameObject_t1366199518 * L_1 = __this->get_addMarker_2();
		NullCheck(L_1);
		Button_t2491204935 * L_2 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_0 = L_2;
		Button_t2491204935 * L_3 = V_0;
		NullCheck(L_3);
		ButtonClickedEvent_t3753894607 * L_4 = Button_get_onClick_m1595880935(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)DynamicLoadUI_U3CStartU3Em__6_m3672923077_MethodInfo_var);
		UnityAction_t625099497 * L_6 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityEvent_AddListener_m1596810379(L_4, L_6, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_7 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3626071671, /*hidden argument*/NULL);
		__this->set_delMarker_4(L_7);
		GameObject_t1366199518 * L_8 = __this->get_delMarker_4();
		NullCheck(L_8);
		Button_t2491204935 * L_9 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_8, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_1 = L_9;
		Button_t2491204935 * L_10 = V_1;
		NullCheck(L_10);
		ButtonClickedEvent_t3753894607 * L_11 = Button_get_onClick_m1595880935(L_10, /*hidden argument*/NULL);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)DynamicLoadUI_U3CStartU3Em__7_m3814085578_MethodInfo_var);
		UnityAction_t625099497 * L_13 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_13, __this, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		UnityEvent_AddListener_m1596810379(L_11, L_13, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_14 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3558534422, /*hidden argument*/NULL);
		__this->set_exit_3(L_14);
		GameObject_t1366199518 * L_15 = __this->get_exit_3();
		NullCheck(L_15);
		Button_t2491204935 * L_16 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_15, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_2 = L_16;
		Button_t2491204935 * L_17 = V_2;
		NullCheck(L_17);
		ButtonClickedEvent_t3753894607 * L_18 = Button_get_onClick_m1595880935(L_17, /*hidden argument*/NULL);
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)DynamicLoadUI_U3CStartU3Em__8_m1548837347_MethodInfo_var);
		UnityAction_t625099497 * L_20 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_20, __this, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		UnityEvent_AddListener_m1596810379(L_18, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicLoadUI::OnGUI()
extern "C"  void DynamicLoadUI_OnGUI_m259973830 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DynamicLoadUI::OnClick(UnityEngine.GameObject)
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral357496571;
extern Il2CppCodeGenString* _stringLiteral2245616130;
extern const uint32_t DynamicLoadUI_OnClick_m1489096455_MetadataUsageId;
extern "C"  void DynamicLoadUI_OnClick_m1489096455 (DynamicLoadUI_t1834292937 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DynamicLoadUI_OnClick_m1489096455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = ___sender0;
		GameObject_t1366199518 * L_1 = __this->get_addMarker_2();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral357496571, /*hidden argument*/NULL);
		Il2CppObject * L_3 = DynamicLoadUI_LoadGameObjects_m1267458712(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_002d:
	{
		GameObject_t1366199518 * L_4 = ___sender0;
		GameObject_t1366199518 * L_5 = __this->get_exit_3();
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004d;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral2245616130, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_004d:
	{
		GameObject_t1366199518 * L_7 = ___sender0;
		GameObject_t1366199518 * L_8 = __this->get_delMarker_4();
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0099;
		}
	}
	{
		VoidAR_t451713137 * L_10 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t2159143318 * L_11 = __this->get_jpgUrl2targetUrl_5();
		NullCheck(L_11);
		JPGUrl2ImageTargetUrl_t3047771975 * L_12 = VirtFuncInvoker1< JPGUrl2ImageTargetUrl_t3047771975 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<JPGUrl2ImageTargetUrl>::get_Item(System.Int32) */, L_11, 0);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_JpgNameURL_1();
		NullCheck(L_10);
		bool L_14 = VoidAR_isMarkerExist_m1590497145(L_10, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0099;
		}
	}
	{
		VoidAR_t451713137 * L_15 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t2159143318 * L_16 = __this->get_jpgUrl2targetUrl_5();
		NullCheck(L_16);
		JPGUrl2ImageTargetUrl_t3047771975 * L_17 = VirtFuncInvoker1< JPGUrl2ImageTargetUrl_t3047771975 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<JPGUrl2ImageTargetUrl>::get_Item(System.Int32) */, L_16, 0);
		NullCheck(L_17);
		String_t* L_18 = L_17->get_JpgNameURL_1();
		NullCheck(L_15);
		VoidAR_removeTarget_m2605434269(L_15, L_18, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Void DynamicLoadUI::Update()
extern "C"  void DynamicLoadUI_Update_m2189177905 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DynamicLoadUI::OnPreRender()
extern "C"  void DynamicLoadUI_OnPreRender_m1813738144 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DynamicLoadUI::OnPostRender()
extern "C"  void DynamicLoadUI_OnPostRender_m2167730739 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator DynamicLoadUI::LoadGameObjects()
extern Il2CppClass* U3CLoadGameObjectsU3Ec__Iterator0_t4032670743_il2cpp_TypeInfo_var;
extern const uint32_t DynamicLoadUI_LoadGameObjects_m1267458712_MetadataUsageId;
extern "C"  Il2CppObject * DynamicLoadUI_LoadGameObjects_m1267458712 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DynamicLoadUI_LoadGameObjects_m1267458712_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * V_0 = NULL;
	{
		U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * L_0 = (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 *)il2cpp_codegen_object_new(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743_il2cpp_TypeInfo_var);
		U3CLoadGameObjectsU3Ec__Iterator0__ctor_m750538106(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_8(__this);
		U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DynamicLoadUI::<Start>m__6()
extern "C"  void DynamicLoadUI_U3CStartU3Em__6_m3672923077 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_addMarker_2();
		DynamicLoadUI_OnClick_m1489096455(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicLoadUI::<Start>m__7()
extern "C"  void DynamicLoadUI_U3CStartU3Em__7_m3814085578 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_delMarker_4();
		DynamicLoadUI_OnClick_m1489096455(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicLoadUI::<Start>m__8()
extern "C"  void DynamicLoadUI_U3CStartU3Em__8_m1548837347 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_exit_3();
		DynamicLoadUI_OnClick_m1489096455(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicLoadUI/<LoadGameObjects>c__Iterator0::.ctor()
extern "C"  void U3CLoadGameObjectsU3Ec__Iterator0__ctor_m750538106 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object DynamicLoadUI/<LoadGameObjects>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadGameObjectsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1857533900 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object DynamicLoadUI/<LoadGameObjects>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadGameObjectsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3175333924 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean DynamicLoadUI/<LoadGameObjects>c__Iterator0::MoveNext()
extern Il2CppClass* List_1_t4197717748_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3146501818_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppClass* Image2ImageTarget_t791379109_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1366199518_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2420141438_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1786818228_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3529032890_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4203604673_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1520675709_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m860455637_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1020042719;
extern const uint32_t U3CLoadGameObjectsU3Ec__Iterator0_MoveNext_m2895991842_MetadataUsageId;
extern "C"  bool U3CLoadGameObjectsU3Ec__Iterator0_MoveNext_m2895991842 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadGameObjectsU3Ec__Iterator0_MoveNext_m2895991842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1145979430 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1145979430 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_004b;
		}
		if (L_1 == 2)
		{
			goto IL_004b;
		}
	}
	{
		goto IL_021e;
	}

IL_0027:
	{
		List_1_t4197717748 * L_2 = (List_1_t4197717748 *)il2cpp_codegen_object_new(List_1_t4197717748_il2cpp_TypeInfo_var);
		List_1__ctor_m3529032890(L_2, /*hidden argument*/List_1__ctor_m3529032890_MethodInfo_var);
		__this->set_U3Cimage2ImageTargetesU3E__0_0(L_2);
		DynamicLoadUI_t1834292937 * L_3 = __this->get_U3CU3Ef__this_8();
		NullCheck(L_3);
		List_1_t2159143318 * L_4 = L_3->get_jpgUrl2targetUrl_5();
		NullCheck(L_4);
		Enumerator_t2420141438  L_5 = List_1_GetEnumerator_m4203604673(L_4, /*hidden argument*/List_1_GetEnumerator_m4203604673_MethodInfo_var);
		__this->set_U3CU24s_3U3E__1_1(L_5);
		V_0 = ((int32_t)-3);
	}

IL_004b:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_00bb;
			}
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 1)
			{
				goto IL_011a;
			}
		}

IL_005b:
		{
			goto IL_01dd;
		}

IL_0060:
		{
			Enumerator_t2420141438 * L_7 = __this->get_address_of_U3CU24s_3U3E__1_1();
			JPGUrl2ImageTargetUrl_t3047771975 * L_8 = Enumerator_get_Current_m1520675709(L_7, /*hidden argument*/Enumerator_get_Current_m1520675709_MethodInfo_var);
			__this->set_U3CurlU3E__2_2(L_8);
			VoidAR_t451713137 * L_9 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
			JPGUrl2ImageTargetUrl_t3047771975 * L_10 = __this->get_U3CurlU3E__2_2();
			NullCheck(L_10);
			String_t* L_11 = L_10->get_JpgNameURL_1();
			NullCheck(L_9);
			bool L_12 = VoidAR_isMarkerExist_m1590497145(L_9, L_11, /*hidden argument*/NULL);
			if (L_12)
			{
				goto IL_01dd;
			}
		}

IL_008b:
		{
			JPGUrl2ImageTargetUrl_t3047771975 * L_13 = __this->get_U3CurlU3E__2_2();
			NullCheck(L_13);
			String_t* L_14 = L_13->get_JpgNameURL_1();
			WWW_t3146501818 * L_15 = (WWW_t3146501818 *)il2cpp_codegen_object_new(WWW_t3146501818_il2cpp_TypeInfo_var);
			WWW__ctor_m2024029190(L_15, L_14, /*hidden argument*/NULL);
			__this->set_U3CfileU3E__3_3(L_15);
			WWW_t3146501818 * L_16 = __this->get_U3CfileU3E__3_3();
			__this->set_U24current_7(L_16);
			__this->set_U24PC_6(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x220, FINALLY_01f2);
		}

IL_00bb:
		{
			WWW_t3146501818 * L_17 = __this->get_U3CfileU3E__3_3();
			NullCheck(L_17);
			String_t* L_18 = WWW_get_error_m3092701216(L_17, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_00ea;
			}
		}

IL_00cb:
		{
			JPGUrl2ImageTargetUrl_t3047771975 * L_19 = __this->get_U3CurlU3E__2_2();
			NullCheck(L_19);
			String_t* L_20 = L_19->get_JpgNameURL_1();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_21 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1020042719, L_20, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			goto IL_01dd;
		}

IL_00ea:
		{
			JPGUrl2ImageTargetUrl_t3047771975 * L_22 = __this->get_U3CurlU3E__2_2();
			NullCheck(L_22);
			String_t* L_23 = L_22->get_ImageTargetURL_2();
			WWW_t3146501818 * L_24 = (WWW_t3146501818 *)il2cpp_codegen_object_new(WWW_t3146501818_il2cpp_TypeInfo_var);
			WWW__ctor_m2024029190(L_24, L_23, /*hidden argument*/NULL);
			__this->set_U3CbundleU3E__4_4(L_24);
			WWW_t3146501818 * L_25 = __this->get_U3CbundleU3E__4_4();
			__this->set_U24current_7(L_25);
			__this->set_U24PC_6(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x220, FINALLY_01f2);
		}

IL_011a:
		{
			WWW_t3146501818 * L_26 = __this->get_U3CbundleU3E__4_4();
			NullCheck(L_26);
			String_t* L_27 = WWW_get_error_m3092701216(L_26, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_0149;
			}
		}

IL_012a:
		{
			JPGUrl2ImageTargetUrl_t3047771975 * L_28 = __this->get_U3CurlU3E__2_2();
			NullCheck(L_28);
			String_t* L_29 = L_28->get_ImageTargetURL_2();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_30 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1020042719, L_29, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			goto IL_01dd;
		}

IL_0149:
		{
			Image2ImageTarget_t791379109 * L_31 = (Image2ImageTarget_t791379109 *)il2cpp_codegen_object_new(Image2ImageTarget_t791379109_il2cpp_TypeInfo_var);
			Image2ImageTarget__ctor_m3111905800(L_31, /*hidden argument*/NULL);
			__this->set_U3CobjU3E__5_5(L_31);
			Image2ImageTarget_t791379109 * L_32 = __this->get_U3CobjU3E__5_5();
			JPGUrl2ImageTargetUrl_t3047771975 * L_33 = __this->get_U3CurlU3E__2_2();
			NullCheck(L_33);
			String_t* L_34 = L_33->get_JpgNameURL_1();
			NullCheck(L_32);
			L_32->set_imageUrl_1(L_34);
			Image2ImageTarget_t791379109 * L_35 = __this->get_U3CobjU3E__5_5();
			JPGUrl2ImageTargetUrl_t3047771975 * L_36 = __this->get_U3CurlU3E__2_2();
			NullCheck(L_36);
			bool L_37 = L_36->get_isLocalImage_0();
			NullCheck(L_35);
			L_35->set_isMarkerLocal_0(L_37);
			Image2ImageTarget_t791379109 * L_38 = __this->get_U3CobjU3E__5_5();
			WWW_t3146501818 * L_39 = __this->get_U3CbundleU3E__4_4();
			NullCheck(L_39);
			AssetBundle_t945621937 * L_40 = WWW_get_assetBundle_m80031863(L_39, /*hidden argument*/NULL);
			NullCheck(L_40);
			Object_t1181371020 * L_41 = AssetBundle_get_mainAsset_m3897042316(L_40, /*hidden argument*/NULL);
			Object_t1181371020 * L_42 = Object_Instantiate_m2439155489(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
			NullCheck(L_38);
			L_38->set_ImageTarget_2(((GameObject_t1366199518 *)CastclassSealed(L_42, GameObject_t1366199518_il2cpp_TypeInfo_var)));
			Image2ImageTarget_t791379109 * L_43 = __this->get_U3CobjU3E__5_5();
			WWW_t3146501818 * L_44 = __this->get_U3CfileU3E__3_3();
			NullCheck(L_44);
			ByteU5BU5D_t3835026402* L_45 = WWW_get_bytes_m420718112(L_44, /*hidden argument*/NULL);
			NullCheck(L_43);
			L_43->set_imagedata_3(L_45);
			List_1_t4197717748 * L_46 = __this->get_U3Cimage2ImageTargetesU3E__0_0();
			Image2ImageTarget_t791379109 * L_47 = __this->get_U3CobjU3E__5_5();
			NullCheck(L_46);
			VirtActionInvoker1< Image2ImageTarget_t791379109 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Add(!0) */, L_46, L_47);
			WWW_t3146501818 * L_48 = __this->get_U3CbundleU3E__4_4();
			NullCheck(L_48);
			AssetBundle_t945621937 * L_49 = WWW_get_assetBundle_m80031863(L_48, /*hidden argument*/NULL);
			NullCheck(L_49);
			AssetBundle_Unload_m167529087(L_49, (bool)0, /*hidden argument*/NULL);
		}

IL_01dd:
		{
			Enumerator_t2420141438 * L_50 = __this->get_address_of_U3CU24s_3U3E__1_1();
			bool L_51 = Enumerator_MoveNext_m860455637(L_50, /*hidden argument*/Enumerator_MoveNext_m860455637_MethodInfo_var);
			if (L_51)
			{
				goto IL_0060;
			}
		}

IL_01ed:
		{
			IL2CPP_LEAVE(0x207, FINALLY_01f2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1145979430 *)e.ex;
		goto FINALLY_01f2;
	}

FINALLY_01f2:
	{ // begin finally (depth: 1)
		{
			bool L_52 = V_1;
			if (!L_52)
			{
				goto IL_01f6;
			}
		}

IL_01f5:
		{
			IL2CPP_END_FINALLY(498)
		}

IL_01f6:
		{
			Enumerator_t2420141438  L_53 = __this->get_U3CU24s_3U3E__1_1();
			Enumerator_t2420141438  L_54 = L_53;
			Il2CppObject * L_55 = Box(Enumerator_t2420141438_il2cpp_TypeInfo_var, &L_54);
			NullCheck((Il2CppObject *)L_55);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1786818228_il2cpp_TypeInfo_var, (Il2CppObject *)L_55);
			IL2CPP_END_FINALLY(498)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(498)
	{
		IL2CPP_JUMP_TBL(0x220, IL_0220)
		IL2CPP_JUMP_TBL(0x207, IL_0207)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1145979430 *)
	}

IL_0207:
	{
		VoidAR_t451713137 * L_56 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t4197717748 * L_57 = __this->get_U3Cimage2ImageTargetesU3E__0_0();
		NullCheck(L_56);
		VoidAR_addTargets_m1495199696(L_56, L_57, /*hidden argument*/NULL);
		__this->set_U24PC_6((-1));
	}

IL_021e:
	{
		return (bool)0;
	}

IL_0220:
	{
		return (bool)1;
	}
	// Dead block : IL_0222: ldloc.2
}
// System.Void DynamicLoadUI/<LoadGameObjects>c__Iterator0::Dispose()
extern Il2CppClass* Enumerator_t2420141438_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1786818228_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadGameObjectsU3Ec__Iterator0_Dispose_m3648127845_MetadataUsageId;
extern "C"  void U3CLoadGameObjectsU3Ec__Iterator0_Dispose_m3648127845 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadGameObjectsU3Ec__Iterator0_Dispose_m3648127845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1145979430 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1145979430 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_003b;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1145979430 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		Enumerator_t2420141438  L_2 = __this->get_U3CU24s_3U3E__1_1();
		Enumerator_t2420141438  L_3 = L_2;
		Il2CppObject * L_4 = Box(Enumerator_t2420141438_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1786818228_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(42)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1145979430 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void DynamicLoadUI/<LoadGameObjects>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadGameObjectsU3Ec__Iterator0_Reset_m2294091015_MetadataUsageId;
extern "C"  void U3CLoadGameObjectsU3Ec__Iterator0_Reset_m2294091015 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadGameObjectsU3Ec__Iterator0_Reset_m2294091015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ImageUI::.ctor()
extern "C"  void ImageUI__ctor_m4034870110 (ImageUI_t751656141 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageUI::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var;
extern const MethodInfo* ImageUI_U3CStartU3Em__9_m1005909124_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern const uint32_t ImageUI_Start_m2147152894_MetadataUsageId;
extern "C"  void ImageUI_Start_m2147152894 (ImageUI_t751656141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ImageUI_Start_m2147152894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	{
		GameObject_t1366199518 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3558534422, /*hidden argument*/NULL);
		__this->set_exit_2(L_0);
		GameObject_t1366199518 * L_1 = __this->get_exit_2();
		NullCheck(L_1);
		Button_t2491204935 * L_2 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_0 = L_2;
		Button_t2491204935 * L_3 = V_0;
		NullCheck(L_3);
		ButtonClickedEvent_t3753894607 * L_4 = Button_get_onClick_m1595880935(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)ImageUI_U3CStartU3Em__9_m1005909124_MethodInfo_var);
		UnityAction_t625099497 * L_6 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityEvent_AddListener_m1596810379(L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageUI::OnClick(UnityEngine.GameObject)
extern Il2CppCodeGenString* _stringLiteral2245616130;
extern const uint32_t ImageUI_OnClick_m884303051_MetadataUsageId;
extern "C"  void ImageUI_OnClick_m884303051 (ImageUI_t751656141 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ImageUI_OnClick_m884303051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = ___sender0;
		GameObject_t1366199518 * L_1 = __this->get_exit_2();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral2245616130, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void ImageUI::<Start>m__9()
extern "C"  void ImageUI_U3CStartU3Em__9_m1005909124 (ImageUI_t751656141 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_exit_2();
		ImageUI_OnClick_m884303051(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JPGUrl2ImageTargetUrl::.ctor()
extern "C"  void JPGUrl2ImageTargetUrl__ctor_m1425597358 (JPGUrl2ImageTargetUrl_t3047771975 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Marker::.ctor()
extern "C"  void Marker__ctor_m1191903673 (Marker_t1240580442 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Marker::Start()
extern Il2CppCodeGenString* _stringLiteral332379563;
extern const uint32_t Marker_Start_m2923530521_MetadataUsageId;
extern "C"  void Marker_Start_m2923530521 (Marker_t1240580442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marker_Start_m2923530521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1366199518 * V_0 = NULL;
	{
		GameObject_t1366199518 * L_0 = __this->get_model_3();
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t1366199518 * L_2 = __this->get_model_3();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Transform_t224878301 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t224878301 * L_4 = Transform_FindChild_m2677714886(L_3, _stringLiteral332379563, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1366199518 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t1366199518 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		__this->set_markerState_2(0);
		return;
	}
}
// System.Void Marker::UpdateState(System.Boolean)
extern "C"  void Marker_UpdateState_m1195485018 (Marker_t1240580442 * __this, bool ___find0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_markerState_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_001d;
		}
	}
	{
		bool L_1 = ___find0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		Marker_OnLost_m1057848938(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		goto IL_0029;
	}

IL_001d:
	{
		bool L_2 = ___find0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Marker_OnFind_m3478600613(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void Marker::OnFind()
extern const Il2CppType* FinderInterface_t3704800497_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FinderInterface_t3704800497_il2cpp_TypeInfo_var;
extern const uint32_t Marker_OnFind_m3478600613_MetadataUsageId;
extern "C"  void Marker_OnFind_m3478600613 (Marker_t1240580442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marker_OnFind_m3478600613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ComponentU5BU5D_t4011263879* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ComponentU5BU5D_t4011263879* V_2 = NULL;
	int32_t V_3 = 0;
	{
		__this->set_markerState_2(1);
		GameObject_t1366199518 * L_0 = __this->get_model_3();
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0065;
		}
	}
	{
		GameObject_t1366199518 * L_2 = __this->get_model_3();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_3 = __this->get_model_3();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(FinderInterface_t3704800497_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		ComponentU5BU5D_t4011263879* L_5 = GameObject_GetComponents_m297658252(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ComponentU5BU5D_t4011263879* L_6 = V_0;
		if (L_6)
		{
			goto IL_0040;
		}
	}
	{
		return;
	}

IL_0040:
	{
		ComponentU5BU5D_t4011263879* L_7 = V_0;
		V_2 = L_7;
		V_3 = 0;
		goto IL_005c;
	}

IL_0049:
	{
		ComponentU5BU5D_t4011263879* L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_1 = ((Il2CppObject *)Castclass(((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), FinderInterface_t3704800497_il2cpp_TypeInfo_var));
		Il2CppObject * L_11 = V_1;
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void FinderInterface::OnFind() */, FinderInterface_t3704800497_il2cpp_TypeInfo_var, L_11);
		int32_t L_12 = V_3;
		V_3 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_13 = V_3;
		ComponentU5BU5D_t4011263879* L_14 = V_2;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0049;
		}
	}

IL_0065:
	{
		return;
	}
}
// System.Void Marker::OnLost()
extern const Il2CppType* FinderInterface_t3704800497_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FinderInterface_t3704800497_il2cpp_TypeInfo_var;
extern const uint32_t Marker_OnLost_m1057848938_MetadataUsageId;
extern "C"  void Marker_OnLost_m1057848938 (Marker_t1240580442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marker_OnLost_m1057848938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ComponentU5BU5D_t4011263879* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ComponentU5BU5D_t4011263879* V_2 = NULL;
	int32_t V_3 = 0;
	{
		GameObject_t1366199518 * L_0 = __this->get_model_3();
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005e;
		}
	}
	{
		GameObject_t1366199518 * L_2 = __this->get_model_3();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_3 = __this->get_model_3();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(FinderInterface_t3704800497_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		ComponentU5BU5D_t4011263879* L_5 = GameObject_GetComponents_m297658252(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ComponentU5BU5D_t4011263879* L_6 = V_0;
		if (L_6)
		{
			goto IL_0039;
		}
	}
	{
		return;
	}

IL_0039:
	{
		ComponentU5BU5D_t4011263879* L_7 = V_0;
		V_2 = L_7;
		V_3 = 0;
		goto IL_0055;
	}

IL_0042:
	{
		ComponentU5BU5D_t4011263879* L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_1 = ((Il2CppObject *)Castclass(((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), FinderInterface_t3704800497_il2cpp_TypeInfo_var));
		Il2CppObject * L_11 = V_1;
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(1 /* System.Void FinderInterface::OnLost() */, FinderInterface_t3704800497_il2cpp_TypeInfo_var, L_11);
		int32_t L_12 = V_3;
		V_3 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_13 = V_3;
		ComponentU5BU5D_t4011263879* L_14 = V_2;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0042;
		}
	}

IL_005e:
	{
		__this->set_markerState_2(2);
		return;
	}
}
// System.Void Marker::OnTracking()
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1206997531;
extern const uint32_t Marker_OnTracking_m3358094995_MetadataUsageId;
extern "C"  void Marker_OnTracking_m3358094995 (Marker_t1240580442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Marker_OnTracking_m3358094995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1206997531, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Marker::setMatrix(UnityEngine.Matrix4x4)
extern "C"  void Marker_setMatrix_m327639248 (Marker_t1240580442 * __this, Matrix4x4_t1261955742  ___worldMatrix0, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_model_3();
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		GameObject_t1366199518 * L_2 = __this->get_model_3();
		NullCheck(L_2);
		Transform_t224878301 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		Matrix4x4_t1261955742  L_4 = ___worldMatrix0;
		Vector3_t465617797  L_5 = MyUtils_GetScale_m2960603332(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m2325460848(L_3, L_5, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_6 = __this->get_model_3();
		NullCheck(L_6);
		Transform_t224878301 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		Matrix4x4_t1261955742  L_8 = ___worldMatrix0;
		Quaternion_t83606849  L_9 = MyUtils_GetRotation_m2559326662(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localRotation_m2055111962(L_7, L_9, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_10 = __this->get_model_3();
		NullCheck(L_10);
		Transform_t224878301 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		Matrix4x4_t1261955742  L_12 = ___worldMatrix0;
		Vector3_t465617797  L_13 = MyUtils_GetPosition_m4264985309(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localPosition_m1026930133(L_11, L_13, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// UnityEngine.Quaternion MyUtils::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t83606849  MyUtils_GetRotation_m2559326662 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1261955742  ___matrix0, const MethodInfo* method)
{
	Vector3_t465617797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t465617797  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = (&___matrix0)->get_m02_8();
		(&V_0)->set_x_1(L_0);
		float L_1 = (&___matrix0)->get_m12_9();
		(&V_0)->set_y_2(L_1);
		float L_2 = (&___matrix0)->get_m22_10();
		(&V_0)->set_z_3(L_2);
		float L_3 = (&___matrix0)->get_m01_4();
		(&V_1)->set_x_1(L_3);
		float L_4 = (&___matrix0)->get_m11_5();
		(&V_1)->set_y_2(L_4);
		float L_5 = (&___matrix0)->get_m21_6();
		(&V_1)->set_z_3(L_5);
		Vector3_t465617797  L_6 = V_0;
		Vector3_t465617797  L_7 = V_1;
		Quaternion_t83606849  L_8 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector3 MyUtils::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t465617797  MyUtils_GetPosition_m4264985309 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1261955742  ___matrix0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = (&___matrix0)->get_m03_12();
		V_0 = L_0;
		float L_1 = (&___matrix0)->get_m13_13();
		V_1 = L_1;
		float L_2 = (&___matrix0)->get_m23_14();
		V_2 = L_2;
		float L_3 = V_0;
		float L_4 = V_1;
		float L_5 = V_2;
		Vector3_t465617797  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 MyUtils::GetScale(UnityEngine.Matrix4x4)
extern "C"  Vector3_t465617797  MyUtils_GetScale_m2960603332 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1261955742  ___matrix0, const MethodInfo* method)
{
	Vector3_t465617797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t465617796  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t465617796  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t465617796  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = (&___matrix0)->get_m00_0();
		float L_1 = (&___matrix0)->get_m10_1();
		float L_2 = (&___matrix0)->get_m20_2();
		float L_3 = (&___matrix0)->get_m30_3();
		Vector4__ctor_m1222289168((&V_1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		float L_4 = Vector4_get_magnitude_m4049434951((&V_1), /*hidden argument*/NULL);
		(&V_0)->set_x_1(L_4);
		float L_5 = (&___matrix0)->get_m01_4();
		float L_6 = (&___matrix0)->get_m11_5();
		float L_7 = (&___matrix0)->get_m21_6();
		float L_8 = (&___matrix0)->get_m31_7();
		Vector4__ctor_m1222289168((&V_2), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		float L_9 = Vector4_get_magnitude_m4049434951((&V_2), /*hidden argument*/NULL);
		(&V_0)->set_y_2(L_9);
		float L_10 = (&___matrix0)->get_m02_8();
		float L_11 = (&___matrix0)->get_m12_9();
		float L_12 = (&___matrix0)->get_m22_10();
		float L_13 = (&___matrix0)->get_m32_11();
		Vector4__ctor_m1222289168((&V_3), L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		float L_14 = Vector4_get_magnitude_m4049434951((&V_3), /*hidden argument*/NULL);
		(&V_0)->set_z_3(L_14);
		Vector3_t465617797  L_15 = V_0;
		return L_15;
	}
}
// UnityEngine.Matrix4x4 MyUtils::getLookAtMatrix(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Matrix4x4_t1261955742_il2cpp_TypeInfo_var;
extern const uint32_t MyUtils_getLookAtMatrix_m4143432437_MetadataUsageId;
extern "C"  Matrix4x4_t1261955742  MyUtils_getLookAtMatrix_m4143432437 (Il2CppObject * __this /* static, unused */, Vector3_t465617797  ___pos0, Vector3_t465617797  ___target1, Vector3_t465617797  ___up2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MyUtils_getLookAtMatrix_m4143432437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t465617797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t465617797  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t465617797  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Matrix4x4_t1261955742  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3_t465617797  L_0 = ___target1;
		Vector3_t465617797  L_1 = ___pos0;
		Vector3_t465617797  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t465617797  L_3 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t465617797  L_4 = ___up2;
		Vector3_t465617797  L_5 = V_0;
		Vector3_t465617797  L_6 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector3_t465617797  L_7 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t465617797  L_8 = V_0;
		Vector3_t465617797  L_9 = V_1;
		Vector3_t465617797  L_10 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t465617797  L_11 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Initobj (Matrix4x4_t1261955742_il2cpp_TypeInfo_var, (&V_3));
		float L_12 = (&V_1)->get_x_1();
		float L_13 = (&V_2)->get_x_1();
		float L_14 = (&V_0)->get_x_1();
		Vector3_t465617797  L_15 = ___pos0;
		Vector3_t465617797  L_16 = V_1;
		float L_17 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		Vector4_t465617796  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector4__ctor_m1222289168(&L_18, L_12, L_13, L_14, ((-L_17)), /*hidden argument*/NULL);
		Matrix4x4_SetRow_m117954573((&V_3), 0, L_18, /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_y_2();
		float L_20 = (&V_2)->get_y_2();
		float L_21 = (&V_0)->get_y_2();
		Vector3_t465617797  L_22 = ___pos0;
		Vector3_t465617797  L_23 = V_2;
		float L_24 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Vector4_t465617796  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector4__ctor_m1222289168(&L_25, L_19, L_20, L_21, ((-L_24)), /*hidden argument*/NULL);
		Matrix4x4_SetRow_m117954573((&V_3), 1, L_25, /*hidden argument*/NULL);
		float L_26 = (&V_1)->get_z_3();
		float L_27 = (&V_2)->get_z_3();
		float L_28 = (&V_0)->get_z_3();
		Vector3_t465617797  L_29 = ___pos0;
		Vector3_t465617797  L_30 = V_0;
		float L_31 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		Vector4_t465617796  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Vector4__ctor_m1222289168(&L_32, L_26, L_27, L_28, ((-L_31)), /*hidden argument*/NULL);
		Matrix4x4_SetRow_m117954573((&V_3), 2, L_32, /*hidden argument*/NULL);
		Vector4_t465617796  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector4__ctor_m1222289168(&L_33, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_SetRow_m117954573((&V_3), 3, L_33, /*hidden argument*/NULL);
		Matrix4x4_t1261955742  L_34 = V_3;
		return L_34;
	}
}
// System.Void ShapeUI::.ctor()
extern "C"  void ShapeUI__ctor_m946005802 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	{
		__this->set_isTracking_6(1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShapeUI::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var;
extern const MethodInfo* ShapeUI_U3CStartU3Em__A_m1976121500_MethodInfo_var;
extern const MethodInfo* ShapeUI_U3CStartU3Em__B_m147915533_MethodInfo_var;
extern const MethodInfo* ShapeUI_U3CStartU3Em__C_m1693796498_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVoidARMain_t3733354248_m2811090911_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1561429203;
extern Il2CppCodeGenString* _stringLiteral292744773;
extern Il2CppCodeGenString* _stringLiteral613662425;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern Il2CppCodeGenString* _stringLiteral2115673192;
extern const uint32_t ShapeUI_Start_m3839721906_MetadataUsageId;
extern "C"  void ShapeUI_Start_m3839721906 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShapeUI_Start_m3839721906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	Button_t2491204935 * V_1 = NULL;
	Button_t2491204935 * V_2 = NULL;
	ShapeUI_t525082413 * G_B2_0 = NULL;
	ShapeUI_t525082413 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ShapeUI_t525082413 * G_B3_1 = NULL;
	{
		GameObject_t1366199518 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1561429203, /*hidden argument*/NULL);
		__this->set_image_5(L_0);
		GameObject_t1366199518 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral292744773, /*hidden argument*/NULL);
		__this->set_add_2(L_1);
		GameObject_t1366199518 * L_2 = __this->get_add_2();
		NullCheck(L_2);
		Button_t2491204935 * L_3 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_2, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_0 = L_3;
		Button_t2491204935 * L_4 = V_0;
		NullCheck(L_4);
		ButtonClickedEvent_t3753894607 * L_5 = Button_get_onClick_m1595880935(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)ShapeUI_U3CStartU3Em__A_m1976121500_MethodInfo_var);
		UnityAction_t625099497 * L_7 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEvent_AddListener_m1596810379(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_8 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral613662425, /*hidden argument*/NULL);
		__this->set_clear_3(L_8);
		GameObject_t1366199518 * L_9 = __this->get_clear_3();
		NullCheck(L_9);
		Button_t2491204935 * L_10 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_9, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_1 = L_10;
		Button_t2491204935 * L_11 = V_1;
		NullCheck(L_11);
		ButtonClickedEvent_t3753894607 * L_12 = Button_get_onClick_m1595880935(L_11, /*hidden argument*/NULL);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)ShapeUI_U3CStartU3Em__B_m147915533_MethodInfo_var);
		UnityAction_t625099497 * L_14 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_14, __this, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityEvent_AddListener_m1596810379(L_12, L_14, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_15 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3558534422, /*hidden argument*/NULL);
		__this->set_exit_4(L_15);
		GameObject_t1366199518 * L_16 = __this->get_exit_4();
		NullCheck(L_16);
		Button_t2491204935 * L_17 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_16, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_2 = L_17;
		Button_t2491204935 * L_18 = V_2;
		NullCheck(L_18);
		ButtonClickedEvent_t3753894607 * L_19 = Button_get_onClick_m1595880935(L_18, /*hidden argument*/NULL);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)ShapeUI_U3CStartU3Em__C_m1693796498_MethodInfo_var);
		UnityAction_t625099497 * L_21 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_21, __this, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		UnityEvent_AddListener_m1596810379(L_19, L_21, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_22 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2115673192, /*hidden argument*/NULL);
		NullCheck(L_22);
		VoidARMain_t3733354248 * L_23 = GameObject_GetComponent_TisVoidARMain_t3733354248_m2811090911(L_22, /*hidden argument*/GameObject_GetComponent_TisVoidARMain_t3733354248_m2811090911_MethodInfo_var);
		NullCheck(L_23);
		bool L_24 = L_23->get_tracking_9();
		G_B1_0 = __this;
		if (!L_24)
		{
			G_B2_0 = __this;
			goto IL_00c9;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_00ca;
	}

IL_00c9:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_00ca:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_isTracking_6(G_B3_0);
		return;
	}
}
// System.Void ShapeUI::OnGUI()
extern const MethodInfo* GameObject_GetComponent_TisImage_t2058862956_m4162535761_MethodInfo_var;
extern const uint32_t ShapeUI_OnGUI_m176602914_MetadataUsageId;
extern "C"  void ShapeUI_OnGUI_m176602914 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShapeUI_OnGUI_m176602914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Image_t2058862956 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		GameObject_t1366199518 * L_0 = __this->get_image_5();
		NullCheck(L_0);
		Image_t2058862956 * L_1 = GameObject_GetComponent_TisImage_t2058862956_m4162535761(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t2058862956_m4162535761_MethodInfo_var);
		V_0 = L_1;
		VoidAR_t451713137 * L_2 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = VoidAR_getTrackStatus_m415318551(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		Image_t2058862956 * L_5 = V_0;
		Color_t2250949164  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m1909920690(&L_6, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Graphic_set_color_m646149893(L_5, L_6, /*hidden argument*/NULL);
		goto IL_008b;
	}

IL_0041:
	{
		int32_t L_7 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_006c;
		}
	}
	{
		Image_t2058862956 * L_8 = V_0;
		Color_t2250949164  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Graphic_set_color_m646149893(L_8, L_9, /*hidden argument*/NULL);
		goto IL_008b;
	}

IL_006c:
	{
		Image_t2058862956 * L_10 = V_0;
		Color_t2250949164  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m1909920690(&L_11, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		Graphic_set_color_m646149893(L_10, L_11, /*hidden argument*/NULL);
	}

IL_008b:
	{
		return;
	}
}
// System.Void ShapeUI::OnClick(UnityEngine.GameObject)
extern Il2CppClass* ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2125916575_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3632007997_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1448170597_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3438136805;
extern Il2CppCodeGenString* _stringLiteral2054819917;
extern Il2CppCodeGenString* _stringLiteral2245616130;
extern const uint32_t ShapeUI_OnClick_m2498337499_MetadataUsageId;
extern "C"  void ShapeUI_OnClick_m2498337499 (ShapeUI_t525082413 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShapeUI_OnClick_m2498337499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t3835026402* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		GameObject_t1366199518 * L_0 = ___sender0;
		GameObject_t1366199518 * L_1 = __this->get_add_2();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_006f;
		}
	}
	{
		V_0 = 0;
		V_1 = ((ByteU5BU5D_t3835026402*)SZArrayNew(ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var, (uint32_t)((int32_t)512)));
		VoidAR_t451713137 * L_3 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3835026402* L_4 = V_1;
		int32_t L_5 = __this->get_isTracking_6();
		NullCheck(L_3);
		VoidAR_addCurrentShapeImageTarget_m2500774288(L_3, (&V_0), L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2125916575_il2cpp_TypeInfo_var);
		Encoding_t2125916575 * L_6 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3835026402* L_7 = V_1;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3835026402* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_6, L_7);
		V_2 = L_8;
		ObjectU5BU5D_t3632007997* L_9 = ((ObjectU5BU5D_t3632007997*)SZArrayNew(ObjectU5BU5D_t3632007997_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral3438136805);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3438136805);
		ObjectU5BU5D_t3632007997* L_10 = L_9;
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_13);
		ObjectU5BU5D_t3632007997* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		ArrayElementTypeCheck (L_14, _stringLiteral2054819917);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2054819917);
		ObjectU5BU5D_t3632007997* L_15 = L_14;
		String_t* L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m3881798623(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_006f:
	{
		GameObject_t1366199518 * L_18 = ___sender0;
		GameObject_t1366199518 * L_19 = __this->get_clear_3();
		bool L_20 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_008f;
		}
	}
	{
		VoidAR_t451713137 * L_21 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		VoidAR_reset_m3948994529(L_21, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_008f:
	{
		GameObject_t1366199518 * L_22 = ___sender0;
		GameObject_t1366199518 * L_23 = __this->get_exit_4();
		bool L_24 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00aa;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral2245616130, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		return;
	}
}
// System.Void ShapeUI::Update()
extern "C"  void ShapeUI_Update_m2661239157 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ShapeUI::OnPreRender()
extern "C"  void ShapeUI_OnPreRender_m4062510828 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ShapeUI::OnPostRender()
extern "C"  void ShapeUI_OnPostRender_m3204645719 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ShapeUI::<Start>m__A()
extern "C"  void ShapeUI_U3CStartU3Em__A_m1976121500 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_add_2();
		ShapeUI_OnClick_m2498337499(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShapeUI::<Start>m__B()
extern "C"  void ShapeUI_U3CStartU3Em__B_m147915533 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_clear_3();
		ShapeUI_OnClick_m2498337499(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShapeUI::<Start>m__C()
extern "C"  void ShapeUI_U3CStartU3Em__C_m1693796498 (ShapeUI_t525082413 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_exit_4();
		ShapeUI_OnClick_m2498337499(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoBehaviour::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t VideoBehaviour__ctor_m1696050063_MetadataUsageId;
extern "C"  void VideoBehaviour__ctor_m1696050063 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour__ctor_m1696050063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_autoScale_4((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_lastPath_5(L_0);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_videoPlayerPtr_6(L_1);
		__this->set_nativeTextureID_8((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoBehaviour::setVideoURL(System.IntPtr,System.String)
extern "C" void DEFAULT_CALL setVideoURL(intptr_t, char*);
extern "C"  void VideoBehaviour_setVideoURL_m517507651 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, String_t* ___videoURL1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Marshaling of parameter '___videoURL1' to native representation
	char* ____videoURL1_marshaled = NULL;
	____videoURL1_marshaled = il2cpp_codegen_marshal_string(___videoURL1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setVideoURL)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()), ____videoURL1_marshaled);

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

	// Marshaling cleanup of parameter '___videoURL1' native representation
	il2cpp_codegen_marshal_free(____videoURL1_marshaled);
	____videoURL1_marshaled = NULL;

}
// System.Int32 VideoBehaviour::updateVideoTexture(System.IntPtr,System.Int32)
extern "C" int32_t DEFAULT_CALL updateVideoTexture(intptr_t, int32_t);
extern "C"  int32_t VideoBehaviour_updateVideoTexture_m2281003269 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, int32_t ___textureID1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Marshaling of parameter '___textureID1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(updateVideoTexture)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()), ___textureID1);

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

	// Marshaling cleanup of parameter '___textureID1' native representation

	return _return_value;
}
// System.Void VideoBehaviour::play(System.IntPtr)
extern "C" void DEFAULT_CALL play(intptr_t);
extern "C"  void VideoBehaviour_play_m3310994277 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(play)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()));

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

}
// System.Void VideoBehaviour::pauseVideo(System.IntPtr)
extern "C" void DEFAULT_CALL pauseVideo(intptr_t);
extern "C"  void VideoBehaviour_pauseVideo_m4095381734 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(pauseVideo)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()));

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

}
// System.Void VideoBehaviour::onExit(System.IntPtr)
extern "C" void DEFAULT_CALL onExit(intptr_t);
extern "C"  void VideoBehaviour_onExit_m2467534378 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(onExit)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()));

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

}
// System.Int32 VideoBehaviour::seekTo(System.IntPtr,System.Single)
extern "C" int32_t DEFAULT_CALL seekTo(intptr_t, float);
extern "C"  int32_t VideoBehaviour_seekTo_m3643336045 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, float ___position1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Marshaling of parameter '___position1' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(seekTo)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()), ___position1);

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

	// Marshaling cleanup of parameter '___position1' native representation

	return _return_value;
}
// System.IntPtr VideoBehaviour::initVideoPlayer()
extern "C" intptr_t DEFAULT_CALL initVideoPlayer();
extern "C"  IntPtr_t VideoBehaviour_initVideoPlayer_m916844338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = reinterpret_cast<PInvokeFunc>(initVideoPlayer)();
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	return __return_value_unmarshaled;
}
// System.Int32 VideoBehaviour::getState(System.IntPtr)
extern "C" int32_t DEFAULT_CALL getState(intptr_t);
extern "C"  int32_t VideoBehaviour_getState_m3474545224 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(getState)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()));

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

	return _return_value;
}
// System.Int32 VideoBehaviour::getWidth(System.IntPtr)
extern "C" int32_t DEFAULT_CALL getWidth(intptr_t);
extern "C"  int32_t VideoBehaviour_getWidth_m2218897931 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(getWidth)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()));

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

	return _return_value;
}
// System.Int32 VideoBehaviour::getHeight(System.IntPtr)
extern "C" int32_t DEFAULT_CALL getHeight(intptr_t);
extern "C"  int32_t VideoBehaviour_getHeight_m2717968170 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___videoPlayerPtr0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(getHeight)(reinterpret_cast<intptr_t>(___videoPlayerPtr0.get_m_value_0()));

	// Marshaling cleanup of parameter '___videoPlayerPtr0' native representation

	return _return_value;
}
// System.Void VideoBehaviour::Start()
extern "C"  void VideoBehaviour_Start_m3135072211 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VideoBehaviour::updateShaderProperties(System.Int32,System.Int32)
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3688796787;
extern Il2CppCodeGenString* _stringLiteral2751555271;
extern Il2CppCodeGenString* _stringLiteral2751555270;
extern const uint32_t VideoBehaviour_updateShaderProperties_m4109932260_MetadataUsageId;
extern "C"  void VideoBehaviour_updateShaderProperties_m4109932260 (VideoBehaviour_t999888626 * __this, int32_t ___texWidth0, int32_t ___texHeight1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour_updateShaderProperties_m4109932260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector3_t465617797  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t465617797  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		GameObject_t1366199518 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t224878301 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t465617797  L_2 = Transform_get_localScale_m3074381503(L_1, /*hidden argument*/NULL);
		V_6 = L_2;
		float L_3 = (&V_6)->get_x_1();
		V_0 = L_3;
		GameObject_t1366199518 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t224878301 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t465617797  L_6 = Transform_get_localScale_m3074381503(L_5, /*hidden argument*/NULL);
		V_7 = L_6;
		float L_7 = (&V_7)->get_z_3();
		V_1 = L_7;
		int32_t L_8 = __this->get_scaleMode_10();
		if (L_8)
		{
			goto IL_005f;
		}
	}
	{
		GameObject_t1366199518 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Renderer_t2715231144 * L_10 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_9, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_10);
		Material_t2197338622 * L_11 = Renderer_get_material_m2553789785(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetInt_m522302436(L_11, _stringLiteral3688796787, 0, /*hidden argument*/NULL);
		goto IL_0202;
	}

IL_005f:
	{
		int32_t L_12 = __this->get_scaleMode_10();
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_0123;
		}
	}
	{
		GameObject_t1366199518 * L_13 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Renderer_t2715231144 * L_14 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_13, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_14);
		Material_t2197338622 * L_15 = Renderer_get_material_m2553789785(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetInt_m522302436(L_15, _stringLiteral3688796787, 1, /*hidden argument*/NULL);
		int32_t L_16 = ___texWidth0;
		int32_t L_17 = ___texHeight1;
		float L_18 = V_0;
		float L_19 = V_1;
		V_2 = ((float)((float)(((float)((float)L_16)))/(float)((float)((float)((float)((float)(((float)((float)L_17)))*(float)L_18))/(float)L_19))));
		int32_t L_20 = ___texHeight1;
		int32_t L_21 = ___texWidth0;
		float L_22 = V_1;
		float L_23 = V_0;
		V_3 = ((float)((float)(((float)((float)L_20)))/(float)((float)((float)((float)((float)(((float)((float)L_21)))*(float)L_22))/(float)L_23))));
		float L_24 = V_2;
		if ((!(((float)L_24) < ((float)(1.0f)))))
		{
			goto IL_00e4;
		}
	}
	{
		GameObject_t1366199518 * L_25 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Renderer_t2715231144 * L_26 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_25, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_26);
		Material_t2197338622 * L_27 = Renderer_get_material_m2553789785(L_26, /*hidden argument*/NULL);
		float L_28 = V_2;
		NullCheck(L_27);
		Material_SetFloat_m1926275467(L_27, _stringLiteral2751555271, L_28, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_29 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Renderer_t2715231144 * L_30 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_29, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_30);
		Material_t2197338622 * L_31 = Renderer_get_material_m2553789785(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Material_SetFloat_m1926275467(L_31, _stringLiteral2751555270, (1.0f), /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_00e4:
	{
		GameObject_t1366199518 * L_32 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Renderer_t2715231144 * L_33 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_32, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_33);
		Material_t2197338622 * L_34 = Renderer_get_material_m2553789785(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Material_SetFloat_m1926275467(L_34, _stringLiteral2751555271, (1.0f), /*hidden argument*/NULL);
		GameObject_t1366199518 * L_35 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Renderer_t2715231144 * L_36 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_35, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_36);
		Material_t2197338622 * L_37 = Renderer_get_material_m2553789785(L_36, /*hidden argument*/NULL);
		float L_38 = V_3;
		NullCheck(L_37);
		Material_SetFloat_m1926275467(L_37, _stringLiteral2751555270, L_38, /*hidden argument*/NULL);
	}

IL_011e:
	{
		goto IL_0202;
	}

IL_0123:
	{
		int32_t L_39 = __this->get_scaleMode_10();
		if ((!(((uint32_t)L_39) == ((uint32_t)2))))
		{
			goto IL_0195;
		}
	}
	{
		GameObject_t1366199518 * L_40 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Renderer_t2715231144 * L_41 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_40, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_41);
		Material_t2197338622 * L_42 = Renderer_get_material_m2553789785(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Material_SetInt_m522302436(L_42, _stringLiteral3688796787, 2, /*hidden argument*/NULL);
		int32_t L_43 = ___texHeight1;
		int32_t L_44 = ___texWidth0;
		float L_45 = V_1;
		float L_46 = V_0;
		V_4 = ((float)((float)(((float)((float)L_43)))/(float)((float)((float)((float)((float)(((float)((float)L_44)))*(float)L_45))/(float)L_46))));
		GameObject_t1366199518 * L_47 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Renderer_t2715231144 * L_48 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_47, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_48);
		Material_t2197338622 * L_49 = Renderer_get_material_m2553789785(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		Material_SetFloat_m1926275467(L_49, _stringLiteral2751555271, (1.0f), /*hidden argument*/NULL);
		GameObject_t1366199518 * L_50 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_50);
		Renderer_t2715231144 * L_51 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_50, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_51);
		Material_t2197338622 * L_52 = Renderer_get_material_m2553789785(L_51, /*hidden argument*/NULL);
		float L_53 = V_4;
		NullCheck(L_52);
		Material_SetFloat_m1926275467(L_52, _stringLiteral2751555270, L_53, /*hidden argument*/NULL);
		goto IL_0202;
	}

IL_0195:
	{
		int32_t L_54 = __this->get_scaleMode_10();
		if ((!(((uint32_t)L_54) == ((uint32_t)3))))
		{
			goto IL_0202;
		}
	}
	{
		GameObject_t1366199518 * L_55 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Renderer_t2715231144 * L_56 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_55, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_56);
		Material_t2197338622 * L_57 = Renderer_get_material_m2553789785(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		Material_SetInt_m522302436(L_57, _stringLiteral3688796787, 3, /*hidden argument*/NULL);
		int32_t L_58 = ___texWidth0;
		int32_t L_59 = ___texHeight1;
		float L_60 = V_0;
		float L_61 = V_1;
		V_5 = ((float)((float)(((float)((float)L_58)))/(float)((float)((float)((float)((float)(((float)((float)L_59)))*(float)L_60))/(float)L_61))));
		GameObject_t1366199518 * L_62 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		Renderer_t2715231144 * L_63 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_62, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_63);
		Material_t2197338622 * L_64 = Renderer_get_material_m2553789785(L_63, /*hidden argument*/NULL);
		float L_65 = V_5;
		NullCheck(L_64);
		Material_SetFloat_m1926275467(L_64, _stringLiteral2751555271, L_65, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_66 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_66);
		Renderer_t2715231144 * L_67 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_66, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_67);
		Material_t2197338622 * L_68 = Renderer_get_material_m2553789785(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		Material_SetFloat_m1926275467(L_68, _stringLiteral2751555270, (1.0f), /*hidden argument*/NULL);
	}

IL_0202:
	{
		return;
	}
}
// System.Void VideoBehaviour::updateVideoPath()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t VideoBehaviour_updateVideoPath_m2549856422_MetadataUsageId;
extern "C"  void VideoBehaviour_updateVideoPath_m2549856422 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour_updateVideoPath_m2549856422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t465617797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_1 = __this->get_lastPath_5();
		String_t* L_2 = __this->get_path_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		String_t* L_4 = __this->get_path_2();
		__this->set_lastPath_5(L_4);
		String_t* L_5 = __this->get_path_2();
		if (!L_5)
		{
			goto IL_0098;
		}
	}
	{
		String_t* L_6 = __this->get_path_2();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_0098;
		}
	}
	{
		IntPtr_t L_8 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		String_t* L_9 = __this->get_path_2();
		VideoBehaviour_setVideoURL_m517507651(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t224878301 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t465617797  L_12 = Transform_get_localScale_m3074381503(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Vector3_t465617797 * L_13 = (&V_0);
		float L_14 = L_13->get_z_3();
		L_13->set_z_3(((float)((float)L_14*(float)(-1.0f))));
		GameObject_t1366199518 * L_15 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t224878301 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		Vector3_t465617797  L_17 = V_0;
		NullCheck(L_16);
		Transform_set_localScale_m2325460848(L_16, L_17, /*hidden argument*/NULL);
		VideoBehaviour_createTexture_m1175852098(__this, 0, 0, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Void VideoBehaviour::OnDisable()
extern "C"  void VideoBehaviour_OnDisable_m4130485718 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VideoBehaviour::createTexture(System.Int32,System.Int32)
extern Il2CppClass* Texture2D_t3575456220_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t2715231144_m772028041_MethodInfo_var;
extern const uint32_t VideoBehaviour_createTexture_m1175852098_MetadataUsageId;
extern "C"  void VideoBehaviour_createTexture_m1175852098 (VideoBehaviour_t999888626 * __this, int32_t ___videoWidth0, int32_t ___videoHeight1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour_createTexture_m1175852098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___videoWidth0;
		int32_t L_1 = ___videoHeight1;
		Texture2D_t3575456220 * L_2 = (Texture2D_t3575456220 *)il2cpp_codegen_object_new(Texture2D_t3575456220_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_2, L_0, L_1, 4, (bool)0, /*hidden argument*/NULL);
		__this->set_mVideoTexture_7(L_2);
		Texture2D_t3575456220 * L_3 = __this->get_mVideoTexture_7();
		NullCheck(L_3);
		Texture_set_filterMode_m3838996656(L_3, 1, /*hidden argument*/NULL);
		Texture2D_t3575456220 * L_4 = __this->get_mVideoTexture_7();
		NullCheck(L_4);
		Texture_set_wrapMode_m333956747(L_4, 1, /*hidden argument*/NULL);
		Texture2D_t3575456220 * L_5 = __this->get_mVideoTexture_7();
		NullCheck(L_5);
		IntPtr_t L_6 = Texture_GetNativeTexturePtr_m292373493(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		int32_t L_7 = IntPtr_ToInt32_m4084182445((&V_0), /*hidden argument*/NULL);
		__this->set_nativeTextureID_8(L_7);
		Renderer_t2715231144 * L_8 = Component_GetComponent_TisRenderer_t2715231144_m772028041(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2715231144_m772028041_MethodInfo_var);
		NullCheck(L_8);
		Material_t2197338622 * L_9 = Renderer_get_material_m2553789785(L_8, /*hidden argument*/NULL);
		Texture2D_t3575456220 * L_10 = __this->get_mVideoTexture_7();
		NullCheck(L_9);
		Material_set_mainTexture_m3584203343(L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoBehaviour::OnRenderObject()
extern Il2CppClass* ObjectU5BU5D_t3632007997_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1448170597_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3580049392;
extern Il2CppCodeGenString* _stringLiteral3914587647;
extern Il2CppCodeGenString* _stringLiteral2036519198;
extern const uint32_t VideoBehaviour_OnRenderObject_m170918469_MetadataUsageId;
extern "C"  void VideoBehaviour_OnRenderObject_m170918469 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour_OnRenderObject_m170918469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		VideoBehaviour_updateVideoPath_m2549856422(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_nativeTextureID_8();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_00cb;
		}
	}
	{
		IntPtr_t L_1 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_nativeTextureID_8();
		int32_t L_3 = VideoBehaviour_updateVideoTexture_m2281003269(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (!L_4)
		{
			goto IL_00cb;
		}
	}
	{
		bool L_5 = __this->get_isReady_12();
		if (L_5)
		{
			goto IL_008e;
		}
	}
	{
		__this->set_isReady_12((bool)1);
		IntPtr_t L_6 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		int32_t L_7 = VideoBehaviour_getWidth_m2218897931(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IntPtr_t L_8 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		int32_t L_9 = VideoBehaviour_getHeight_m2717968170(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		ObjectU5BU5D_t3632007997* L_10 = ((ObjectU5BU5D_t3632007997*)SZArrayNew(ObjectU5BU5D_t3632007997_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, _stringLiteral3580049392);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3580049392);
		ObjectU5BU5D_t3632007997* L_11 = L_10;
		int32_t L_12 = V_1;
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3632007997* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, _stringLiteral3914587647);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3914587647);
		ObjectU5BU5D_t3632007997* L_16 = L_15;
		int32_t L_17 = V_2;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m3881798623(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		int32_t L_21 = V_1;
		int32_t L_22 = V_2;
		VideoBehaviour_updateShaderProperties_m4109932260(__this, L_21, L_22, /*hidden argument*/NULL);
	}

IL_008e:
	{
		int32_t L_23 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)4))))
		{
			goto IL_00c6;
		}
	}
	{
		bool L_24 = __this->get_loop_3();
		if (!L_24)
		{
			goto IL_00c6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2036519198, /*hidden argument*/NULL);
		IntPtr_t L_25 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		VideoBehaviour_seekTo_m3643336045(NULL /*static, unused*/, L_25, (0.0f), /*hidden argument*/NULL);
		IntPtr_t L_26 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		VideoBehaviour_play_m3310994277(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		GL_InvalidateState_m3232431926(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		return;
	}
}
// System.IntPtr VideoBehaviour::getVideoPlayer()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t VideoBehaviour_getVideoPlayer_m2899693470_MetadataUsageId;
extern "C"  IntPtr_t VideoBehaviour_getVideoPlayer_m2899693470 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour_getVideoPlayer_m2899693470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_videoPlayerPtr_6();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IntPtr_t L_3 = VideoBehaviour_initVideoPlayer_m916844338(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_videoPlayerPtr_6(L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = __this->get_videoPlayerPtr_6();
		return L_4;
	}
}
// System.Void VideoBehaviour::Update()
extern Il2CppCodeGenString* _stringLiteral332379563;
extern const uint32_t VideoBehaviour_Update_m2848589696_MetadataUsageId;
extern "C"  void VideoBehaviour_Update_m2848589696 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour_Update_m2848589696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t224878301 * V_0 = NULL;
	Vector3_t465617797  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t465617797  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0074;
		}
	}
	{
		bool L_1 = __this->get_autoScale_4();
		if (!L_1)
		{
			goto IL_0074;
		}
	}
	{
		Transform_t224878301 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t224878301 * L_3 = Transform_get_parent_m147407266(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t224878301 * L_4 = Transform_FindChild_m2677714886(L_3, _stringLiteral332379563, /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t224878301 * L_5 = V_0;
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0074;
		}
	}
	{
		Transform_t224878301 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t224878301 * L_8 = V_0;
		NullCheck(L_8);
		Vector3_t465617797  L_9 = Transform_get_localScale_m3074381503(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_x_1();
		Transform_t224878301 * L_11 = V_0;
		NullCheck(L_11);
		Vector3_t465617797  L_12 = Transform_get_localScale_m3074381503(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = (&V_2)->get_y_2();
		Vector3_t465617797  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, ((float)((float)L_10/(float)(10.0f))), (0.1f), ((float)((float)L_13/(float)(10.0f))), /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_m2325460848(L_7, L_14, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void VideoBehaviour::OnDestroy()
extern "C"  void VideoBehaviour_OnDestroy_m41376480 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IntPtr_t L_1 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		VideoBehaviour_onExit_m2467534378(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoBehaviour::OnApplicationPause(System.Boolean)
extern "C"  void VideoBehaviour_OnApplicationPause_m2429793449 (VideoBehaviour_t999888626 * __this, bool ___pause0, const MethodInfo* method)
{
	{
		bool L_0 = ___pause0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IntPtr_t L_1 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		VideoBehaviour_pauseVideo_m4095381734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void VideoBehaviour::OnFind()
extern "C"  void VideoBehaviour_OnFind_m537416423 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	{
		VideoBehaviour_LostOther_m352837323(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		VideoBehaviour_play_m3310994277(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoBehaviour::OnLost()
extern "C"  void VideoBehaviour_OnLost_m1504587150 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = VideoBehaviour_getVideoPlayer_m2899693470(__this, /*hidden argument*/NULL);
		VideoBehaviour_pauseVideo_m4095381734(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoBehaviour::LostOther()
extern const Il2CppType* Marker_t1240580442_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MarkerU5BU5D_t3893320703_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisVideoBehaviour_t999888626_m2277997695_MethodInfo_var;
extern const uint32_t VideoBehaviour_LostOther_m352837323_MetadataUsageId;
extern "C"  void VideoBehaviour_LostOther_m352837323 (VideoBehaviour_t999888626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoBehaviour_LostOther_m352837323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MarkerU5BU5D_t3893320703* V_0 = NULL;
	Marker_t1240580442 * V_1 = NULL;
	MarkerU5BU5D_t3893320703* V_2 = NULL;
	int32_t V_3 = 0;
	VideoBehaviour_t999888626 * V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Marker_t1240580442_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t2631032261* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((MarkerU5BU5D_t3893320703*)Castclass(L_1, MarkerU5BU5D_t3893320703_il2cpp_TypeInfo_var));
		MarkerU5BU5D_t3893320703* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0062;
	}

IL_001e:
	{
		MarkerU5BU5D_t3893320703* L_3 = V_2;
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		Marker_t1240580442 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_t1366199518 * L_7 = L_6->get_model_3();
		NullCheck(L_7);
		bool L_8 = GameObject_get_activeSelf_m313590879(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005e;
		}
	}
	{
		Marker_t1240580442 * L_9 = V_1;
		NullCheck(L_9);
		GameObject_t1366199518 * L_10 = L_9->get_model_3();
		NullCheck(L_10);
		VideoBehaviour_t999888626 * L_11 = GameObject_GetComponent_TisVideoBehaviour_t999888626_m2277997695(L_10, /*hidden argument*/GameObject_GetComponent_TisVideoBehaviour_t999888626_m2277997695_MethodInfo_var);
		V_4 = L_11;
		VideoBehaviour_t999888626 * L_12 = V_4;
		bool L_13 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_005e;
		}
	}
	{
		VideoBehaviour_t999888626 * L_14 = V_4;
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, __this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_005e;
		}
	}
	{
		Marker_t1240580442 * L_16 = V_1;
		NullCheck(L_16);
		Marker_OnLost_m1057848938(L_16, /*hidden argument*/NULL);
	}

IL_005e:
	{
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_18 = V_3;
		MarkerU5BU5D_t3893320703* L_19 = V_2;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		return;
	}
}
// System.Void VideoUI::.ctor()
extern "C"  void VideoUI__ctor_m2018193328 (VideoUI_t1842427015 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoUI::Start()
extern Il2CppClass* UnityAction_t625099497_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var;
extern const MethodInfo* VideoUI_U3CStartU3Em__D_m1701572033_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3558534422;
extern const uint32_t VideoUI_Start_m1606344332_MetadataUsageId;
extern "C"  void VideoUI_Start_m1606344332 (VideoUI_t1842427015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoUI_Start_m1606344332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Button_t2491204935 * V_0 = NULL;
	{
		GameObject_t1366199518 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3558534422, /*hidden argument*/NULL);
		__this->set_exit_2(L_0);
		GameObject_t1366199518 * L_1 = __this->get_exit_2();
		NullCheck(L_1);
		Button_t2491204935 * L_2 = GameObject_GetComponent_TisButton_t2491204935_m1008560876(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t2491204935_m1008560876_MethodInfo_var);
		V_0 = L_2;
		Button_t2491204935 * L_3 = V_0;
		NullCheck(L_3);
		ButtonClickedEvent_t3753894607 * L_4 = Button_get_onClick_m1595880935(L_3, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)VideoUI_U3CStartU3Em__D_m1701572033_MethodInfo_var);
		UnityAction_t625099497 * L_6 = (UnityAction_t625099497 *)il2cpp_codegen_object_new(UnityAction_t625099497_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityEvent_AddListener_m1596810379(L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoUI::OnClick(UnityEngine.GameObject)
extern Il2CppCodeGenString* _stringLiteral2245616130;
extern const uint32_t VideoUI_OnClick_m4151328389_MetadataUsageId;
extern "C"  void VideoUI_OnClick_m4151328389 (VideoUI_t1842427015 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VideoUI_OnClick_m4151328389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = ___sender0;
		GameObject_t1366199518 * L_1 = __this->get_exit_2();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Application_LoadLevel_m393995325(NULL /*static, unused*/, _stringLiteral2245616130, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void VideoUI::<Start>m__D()
extern "C"  void VideoUI_U3CStartU3Em__D_m1701572033 (VideoUI_t1842427015 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = __this->get_exit_2();
		VideoUI_OnClick_m4151328389(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::.ctor()
extern Il2CppClass* Matrix4x4_t1261955742_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3496208296_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2408986778_MethodInfo_var;
extern const uint32_t VoidAR__ctor_m729901080_MetadataUsageId;
extern "C"  void VoidAR__ctor_m729901080 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR__ctor_m729901080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t1261955742  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t1261955742_il2cpp_TypeInfo_var, (&V_0));
		Matrix4x4_t1261955742  L_0 = V_0;
		__this->set_projMatrix_3(L_0);
		Dictionary_2_t3496208296 * L_1 = (Dictionary_2_t3496208296 *)il2cpp_codegen_object_new(Dictionary_2_t3496208296_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2408986778(L_1, /*hidden argument*/Dictionary_2__ctor_m2408986778_MethodInfo_var);
		__this->set_mMarkers_4(L_1);
		__this->set_screen_width_13(((int32_t)352));
		__this->set_screen_height_14(((int32_t)288));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::CallBackFunction(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral277392709;
extern const uint32_t VoidAR_CallBackFunction_m2677318649_MetadataUsageId;
extern "C"  void VoidAR_CallBackFunction_m2677318649 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_CallBackFunction_m2677318649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral277392709, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::ShapeMatchingCallBack(System.Int32)
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral993391560;
extern Il2CppCodeGenString* _stringLiteral1790591047;
extern const uint32_t VoidAR_ShapeMatchingCallBack_m2416343084_MetadataUsageId;
extern "C"  void VoidAR_ShapeMatchingCallBack_m2416343084 (Il2CppObject * __this /* static, unused */, int32_t ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_ShapeMatchingCallBack_m2416343084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___success0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral993391560, /*hidden argument*/NULL);
		goto IL_0020;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1790591047, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void VoidAR::_setDebugLog(System.IntPtr)
extern "C" void DEFAULT_CALL _setDebugLog(intptr_t);
extern "C"  void VoidAR__setDebugLog_m1237218656 (Il2CppObject * __this /* static, unused */, IntPtr_t ___fp0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___fp0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setDebugLog)(reinterpret_cast<intptr_t>(___fp0.get_m_value_0()));

	// Marshaling cleanup of parameter '___fp0' native representation

}
// System.Void VoidAR::_setMatchingResult(System.IntPtr)
extern "C" void DEFAULT_CALL _setMatchingResult(intptr_t);
extern "C"  void VoidAR__setMatchingResult_m3843032621 (Il2CppObject * __this /* static, unused */, IntPtr_t ___fp0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Marshaling of parameter '___fp0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setMatchingResult)(reinterpret_cast<intptr_t>(___fp0.get_m_value_0()));

	// Marshaling cleanup of parameter '___fp0' native representation

}
// System.Void VoidAR::_initBGTexture(System.Int32&,System.Int32&,System.Int32&)
extern "C" void DEFAULT_CALL _initBGTexture(int32_t*, int32_t*, int32_t*);
extern "C"  void VoidAR__initBGTexture_m585106992 (Il2CppObject * __this /* static, unused */, int32_t* ___textureId0, int32_t* ___width1, int32_t* ___height2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*, int32_t*, int32_t*);

	// Marshaling of parameter '___textureId0' to native representation
	int32_t* ____textureId0_marshaled = NULL;
	int32_t ____textureId0_marshaled_dereferenced = 0;
	____textureId0_marshaled_dereferenced = *___textureId0;
	____textureId0_marshaled = &____textureId0_marshaled_dereferenced;

	// Marshaling of parameter '___width1' to native representation
	int32_t* ____width1_marshaled = NULL;
	int32_t ____width1_marshaled_dereferenced = 0;
	____width1_marshaled_dereferenced = *___width1;
	____width1_marshaled = &____width1_marshaled_dereferenced;

	// Marshaling of parameter '___height2' to native representation
	int32_t* ____height2_marshaled = NULL;
	int32_t ____height2_marshaled_dereferenced = 0;
	____height2_marshaled_dereferenced = *___height2;
	____height2_marshaled = &____height2_marshaled_dereferenced;

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_initBGTexture)(____textureId0_marshaled, ____width1_marshaled, ____height2_marshaled);

	// Marshaling of parameter '___textureId0' back from native representation
	int32_t ____textureId0_result_dereferenced = 0;
	int32_t* ____textureId0_result = &____textureId0_result_dereferenced;
	*____textureId0_result = *____textureId0_marshaled;
	*___textureId0 = *____textureId0_result;

	// Marshaling cleanup of parameter '___textureId0' native representation

	// Marshaling of parameter '___width1' back from native representation
	int32_t ____width1_result_dereferenced = 0;
	int32_t* ____width1_result = &____width1_result_dereferenced;
	*____width1_result = *____width1_marshaled;
	*___width1 = *____width1_result;

	// Marshaling cleanup of parameter '___width1' native representation

	// Marshaling of parameter '___height2' back from native representation
	int32_t ____height2_result_dereferenced = 0;
	int32_t* ____height2_result = &____height2_result_dereferenced;
	*____height2_result = *____height2_marshaled;
	*___height2 = *____height2_result;

	// Marshaling cleanup of parameter '___height2' native representation

}
// System.Void VoidAR::_initAndroidBGTexutre(System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern "C" void DEFAULT_CALL _initAndroidBGTexutre(int32_t*, int32_t*, int32_t*, int32_t*);
extern "C"  void VoidAR__initAndroidBGTexutre_m108464840 (Il2CppObject * __this /* static, unused */, int32_t* ___t10, int32_t* ___t21, int32_t* ___width2, int32_t* ___height3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*, int32_t*, int32_t*, int32_t*);

	// Marshaling of parameter '___t10' to native representation
	int32_t* ____t10_marshaled = NULL;
	int32_t ____t10_marshaled_dereferenced = 0;
	____t10_marshaled_dereferenced = *___t10;
	____t10_marshaled = &____t10_marshaled_dereferenced;

	// Marshaling of parameter '___t21' to native representation
	int32_t* ____t21_marshaled = NULL;
	int32_t ____t21_marshaled_dereferenced = 0;
	____t21_marshaled_dereferenced = *___t21;
	____t21_marshaled = &____t21_marshaled_dereferenced;

	// Marshaling of parameter '___width2' to native representation
	int32_t* ____width2_marshaled = NULL;
	int32_t ____width2_marshaled_dereferenced = 0;
	____width2_marshaled_dereferenced = *___width2;
	____width2_marshaled = &____width2_marshaled_dereferenced;

	// Marshaling of parameter '___height3' to native representation
	int32_t* ____height3_marshaled = NULL;
	int32_t ____height3_marshaled_dereferenced = 0;
	____height3_marshaled_dereferenced = *___height3;
	____height3_marshaled = &____height3_marshaled_dereferenced;

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_initAndroidBGTexutre)(____t10_marshaled, ____t21_marshaled, ____width2_marshaled, ____height3_marshaled);

	// Marshaling of parameter '___t10' back from native representation
	int32_t ____t10_result_dereferenced = 0;
	int32_t* ____t10_result = &____t10_result_dereferenced;
	*____t10_result = *____t10_marshaled;
	*___t10 = *____t10_result;

	// Marshaling cleanup of parameter '___t10' native representation

	// Marshaling of parameter '___t21' back from native representation
	int32_t ____t21_result_dereferenced = 0;
	int32_t* ____t21_result = &____t21_result_dereferenced;
	*____t21_result = *____t21_marshaled;
	*___t21 = *____t21_result;

	// Marshaling cleanup of parameter '___t21' native representation

	// Marshaling of parameter '___width2' back from native representation
	int32_t ____width2_result_dereferenced = 0;
	int32_t* ____width2_result = &____width2_result_dereferenced;
	*____width2_result = *____width2_marshaled;
	*___width2 = *____width2_result;

	// Marshaling cleanup of parameter '___width2' native representation

	// Marshaling of parameter '___height3' back from native representation
	int32_t ____height3_result_dereferenced = 0;
	int32_t* ____height3_result = &____height3_result_dereferenced;
	*____height3_result = *____height3_marshaled;
	*___height3 = *____height3_result;

	// Marshaling cleanup of parameter '___height3' native representation

}
// System.Void VoidAR::_setScreenSize(System.Int32&,System.Int32&)
extern "C" void DEFAULT_CALL _setScreenSize(int32_t*, int32_t*);
extern "C"  void VoidAR__setScreenSize_m914176366 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*, int32_t*);

	// Marshaling of parameter '___width0' to native representation
	int32_t* ____width0_marshaled = NULL;
	int32_t ____width0_marshaled_dereferenced = 0;
	____width0_marshaled_dereferenced = *___width0;
	____width0_marshaled = &____width0_marshaled_dereferenced;

	// Marshaling of parameter '___height1' to native representation
	int32_t* ____height1_marshaled = NULL;
	int32_t ____height1_marshaled_dereferenced = 0;
	____height1_marshaled_dereferenced = *___height1;
	____height1_marshaled = &____height1_marshaled_dereferenced;

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setScreenSize)(____width0_marshaled, ____height1_marshaled);

	// Marshaling of parameter '___width0' back from native representation
	int32_t ____width0_result_dereferenced = 0;
	int32_t* ____width0_result = &____width0_result_dereferenced;
	*____width0_result = *____width0_marshaled;
	*___width0 = *____width0_result;

	// Marshaling cleanup of parameter '___width0' native representation

	// Marshaling of parameter '___height1' back from native representation
	int32_t ____height1_result_dereferenced = 0;
	int32_t* ____height1_result = &____height1_result_dereferenced;
	*____height1_result = *____height1_marshaled;
	*___height1 = *____height1_result;

	// Marshaling cleanup of parameter '___height1' native representation

}
// System.Void VoidAR::_initLibrary(System.Int32)
extern "C" void DEFAULT_CALL _initLibrary(int32_t);
extern "C"  void VoidAR__initLibrary_m4058222619 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___type0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_initLibrary)(___type0);

	// Marshaling cleanup of parameter '___type0' native representation

}
// System.Void VoidAR::_buildGLProjectionMatrix(System.Int32,System.Int32,System.Double[])
extern "C" void DEFAULT_CALL _buildGLProjectionMatrix(int32_t, int32_t, double*);
extern "C"  void VoidAR__buildGLProjectionMatrix_m2182184962 (Il2CppObject * __this /* static, unused */, int32_t ___screenWidth0, int32_t ___sceenHeight1, DoubleU5BU5D_t2839599125* ___matrix442, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, double*);

	// Marshaling of parameter '___screenWidth0' to native representation

	// Marshaling of parameter '___sceenHeight1' to native representation

	// Marshaling of parameter '___matrix442' to native representation
	double* ____matrix442_marshaled = NULL;
	____matrix442_marshaled = il2cpp_codegen_marshal_array<double>((Il2CppCodeGenArray*)___matrix442);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_buildGLProjectionMatrix)(___screenWidth0, ___sceenHeight1, ____matrix442_marshaled);

	// Marshaling cleanup of parameter '___screenWidth0' native representation

	// Marshaling cleanup of parameter '___sceenHeight1' native representation

	// Marshaling cleanup of parameter '___matrix442' native representation

}
// System.Void VoidAR::_startCapture(System.Int32,System.Int32&)
extern "C" void DEFAULT_CALL _startCapture(int32_t, int32_t*);
extern "C"  void VoidAR__startCapture_m3291993969 (Il2CppObject * __this /* static, unused */, int32_t ___cameraIndex0, int32_t* ___opened1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t*);

	// Marshaling of parameter '___cameraIndex0' to native representation

	// Marshaling of parameter '___opened1' to native representation
	int32_t* ____opened1_marshaled = NULL;
	int32_t ____opened1_marshaled_dereferenced = 0;
	____opened1_marshaled_dereferenced = *___opened1;
	____opened1_marshaled = &____opened1_marshaled_dereferenced;

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_startCapture)(___cameraIndex0, ____opened1_marshaled);

	// Marshaling cleanup of parameter '___cameraIndex0' native representation

	// Marshaling of parameter '___opened1' back from native representation
	int32_t ____opened1_result_dereferenced = 0;
	int32_t* ____opened1_result = &____opened1_result_dereferenced;
	*____opened1_result = *____opened1_marshaled;
	*___opened1 = *____opened1_result;

	// Marshaling cleanup of parameter '___opened1' native representation

}
// System.Void VoidAR::_stopCapture()
extern "C" void DEFAULT_CALL _stopCapture();
extern "C"  void VoidAR__stopCapture_m719693631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_stopCapture)();

}
// System.Void VoidAR::_getMatchResult(System.IntPtr,System.Int32&,System.Int32&,System.Int32&,System.Double[],System.Byte[],System.Int32&,System.Int32[])
extern "C" void DEFAULT_CALL _getMatchResult(intptr_t, int32_t*, int32_t*, int32_t*, double*, uint8_t*, int32_t*, int32_t*);
extern "C"  void VoidAR__getMatchResult_m819854597 (Il2CppObject * __this /* static, unused */, IntPtr_t ___frame0, int32_t* ___width1, int32_t* ___height2, int32_t* ___flip3, DoubleU5BU5D_t2839599125* ___matrix4, ByteU5BU5D_t3835026402* ___markers5, int32_t* ___mainMarker6, Int32U5BU5D_t3315407976* ___scores7, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t*, int32_t*, int32_t*, double*, uint8_t*, int32_t*, int32_t*);

	// Marshaling of parameter '___frame0' to native representation

	// Marshaling of parameter '___width1' to native representation
	int32_t* ____width1_marshaled = NULL;
	int32_t ____width1_marshaled_dereferenced = 0;
	____width1_marshaled_dereferenced = *___width1;
	____width1_marshaled = &____width1_marshaled_dereferenced;

	// Marshaling of parameter '___height2' to native representation
	int32_t* ____height2_marshaled = NULL;
	int32_t ____height2_marshaled_dereferenced = 0;
	____height2_marshaled_dereferenced = *___height2;
	____height2_marshaled = &____height2_marshaled_dereferenced;

	// Marshaling of parameter '___flip3' to native representation
	int32_t* ____flip3_marshaled = NULL;
	int32_t ____flip3_marshaled_dereferenced = 0;
	____flip3_marshaled_dereferenced = *___flip3;
	____flip3_marshaled = &____flip3_marshaled_dereferenced;

	// Marshaling of parameter '___matrix4' to native representation
	double* ____matrix4_marshaled = NULL;
	____matrix4_marshaled = il2cpp_codegen_marshal_array<double>((Il2CppCodeGenArray*)___matrix4);

	// Marshaling of parameter '___markers5' to native representation
	uint8_t* ____markers5_marshaled = NULL;
	____markers5_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___markers5);

	// Marshaling of parameter '___mainMarker6' to native representation
	int32_t* ____mainMarker6_marshaled = NULL;
	int32_t ____mainMarker6_marshaled_dereferenced = 0;
	____mainMarker6_marshaled_dereferenced = *___mainMarker6;
	____mainMarker6_marshaled = &____mainMarker6_marshaled_dereferenced;

	// Marshaling of parameter '___scores7' to native representation
	int32_t* ____scores7_marshaled = NULL;
	____scores7_marshaled = il2cpp_codegen_marshal_array<int32_t>((Il2CppCodeGenArray*)___scores7);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_getMatchResult)(reinterpret_cast<intptr_t>(___frame0.get_m_value_0()), ____width1_marshaled, ____height2_marshaled, ____flip3_marshaled, ____matrix4_marshaled, ____markers5_marshaled, ____mainMarker6_marshaled, ____scores7_marshaled);

	// Marshaling cleanup of parameter '___frame0' native representation

	// Marshaling of parameter '___width1' back from native representation
	int32_t ____width1_result_dereferenced = 0;
	int32_t* ____width1_result = &____width1_result_dereferenced;
	*____width1_result = *____width1_marshaled;
	*___width1 = *____width1_result;

	// Marshaling cleanup of parameter '___width1' native representation

	// Marshaling of parameter '___height2' back from native representation
	int32_t ____height2_result_dereferenced = 0;
	int32_t* ____height2_result = &____height2_result_dereferenced;
	*____height2_result = *____height2_marshaled;
	*___height2 = *____height2_result;

	// Marshaling cleanup of parameter '___height2' native representation

	// Marshaling of parameter '___flip3' back from native representation
	int32_t ____flip3_result_dereferenced = 0;
	int32_t* ____flip3_result = &____flip3_result_dereferenced;
	*____flip3_result = *____flip3_marshaled;
	*___flip3 = *____flip3_result;

	// Marshaling cleanup of parameter '___flip3' native representation

	// Marshaling cleanup of parameter '___matrix4' native representation

	// Marshaling cleanup of parameter '___markers5' native representation

	// Marshaling of parameter '___mainMarker6' back from native representation
	int32_t ____mainMarker6_result_dereferenced = 0;
	int32_t* ____mainMarker6_result = &____mainMarker6_result_dereferenced;
	*____mainMarker6_result = *____mainMarker6_marshaled;
	*___mainMarker6 = *____mainMarker6_result;

	// Marshaling cleanup of parameter '___mainMarker6' native representation

	// Marshaling cleanup of parameter '___scores7' native representation

}
// System.Void VoidAR::_unloadResource()
extern "C" void DEFAULT_CALL _unloadResource();
extern "C"  void VoidAR__unloadResource_m1465702158 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_unloadResource)();

}
// System.Void VoidAR::_match()
extern "C" void DEFAULT_CALL _match();
extern "C"  void VoidAR__match_m1170501128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_match)();

}
// System.Void VoidAR::_getTrackingTime(System.Double[])
extern "C" void DEFAULT_CALL _getTrackingTime(double*);
extern "C"  void VoidAR__getTrackingTime_m1283378439 (Il2CppObject * __this /* static, unused */, DoubleU5BU5D_t2839599125* ___times0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (double*);

	// Marshaling of parameter '___times0' to native representation
	double* ____times0_marshaled = NULL;
	____times0_marshaled = il2cpp_codegen_marshal_array<double>((Il2CppCodeGenArray*)___times0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_getTrackingTime)(____times0_marshaled);

	// Marshaling cleanup of parameter '___times0' native representation

}
// System.Void VoidAR::_reset()
extern "C" void DEFAULT_CALL _reset();
extern "C"  void VoidAR__reset_m2806132926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_reset)();

}
// System.Void VoidAR::_getTrackStatus(System.Int32[])
extern "C" void DEFAULT_CALL _getTrackStatus(int32_t*);
extern "C"  void VoidAR__getTrackStatus_m2897874513 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3315407976* ___status0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*);

	// Marshaling of parameter '___status0' to native representation
	int32_t* ____status0_marshaled = NULL;
	____status0_marshaled = il2cpp_codegen_marshal_array<int32_t>((Il2CppCodeGenArray*)___status0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_getTrackStatus)(____status0_marshaled);

	// Marshaling cleanup of parameter '___status0' native representation

}
// System.Void VoidAR::_setDebugDraw(System.Int32)
extern "C" void DEFAULT_CALL _setDebugDraw(int32_t);
extern "C"  void VoidAR__setDebugDraw_m1360007373 (Il2CppObject * __this /* static, unused */, int32_t ___debugDraw0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___debugDraw0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setDebugDraw)(___debugDraw0);

	// Marshaling cleanup of parameter '___debugDraw0' native representation

}
// System.Void VoidAR::_analyzing(System.String,System.IntPtr)
extern "C" void DEFAULT_CALL _analyzing(char*, intptr_t);
extern "C"  void VoidAR__analyzing_m2999421260 (Il2CppObject * __this /* static, unused */, String_t* ___path0, IntPtr_t ___fp1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, intptr_t);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Marshaling of parameter '___fp1' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_analyzing)(____path0_marshaled, reinterpret_cast<intptr_t>(___fp1.get_m_value_0()));

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	// Marshaling cleanup of parameter '___fp1' native representation

}
// System.Void VoidAR::_addCurrentShapeImageTarget(System.Int32&,System.Byte[],System.Int32)
extern "C" void DEFAULT_CALL _addCurrentShapeImageTarget(int32_t*, uint8_t*, int32_t);
extern "C"  void VoidAR__addCurrentShapeImageTarget_m144643407 (Il2CppObject * __this /* static, unused */, int32_t* ___success0, ByteU5BU5D_t3835026402* ___name1, int32_t ___tracking2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*, uint8_t*, int32_t);

	// Marshaling of parameter '___success0' to native representation
	int32_t* ____success0_marshaled = NULL;
	int32_t ____success0_marshaled_dereferenced = 0;
	____success0_marshaled_dereferenced = *___success0;
	____success0_marshaled = &____success0_marshaled_dereferenced;

	// Marshaling of parameter '___name1' to native representation
	uint8_t* ____name1_marshaled = NULL;
	____name1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___name1);

	// Marshaling of parameter '___tracking2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_addCurrentShapeImageTarget)(____success0_marshaled, ____name1_marshaled, ___tracking2);

	// Marshaling of parameter '___success0' back from native representation
	int32_t ____success0_result_dereferenced = 0;
	int32_t* ____success0_result = &____success0_result_dereferenced;
	*____success0_result = *____success0_marshaled;
	*___success0 = *____success0_result;

	// Marshaling cleanup of parameter '___success0' native representation

	// Marshaling cleanup of parameter '___name1' native representation

	// Marshaling cleanup of parameter '___tracking2' native representation

}
// System.Void VoidAR::_getNewCloudTarget(System.Int32&,System.Byte[],System.Byte[],System.Byte[])
extern "C" void DEFAULT_CALL _getNewCloudTarget(int32_t*, uint8_t*, uint8_t*, uint8_t*);
extern "C"  void VoidAR__getNewCloudTarget_m2546856289 (Il2CppObject * __this /* static, unused */, int32_t* ___newTarget0, ByteU5BU5D_t3835026402* ___urls1, ByteU5BU5D_t3835026402* ___names2, ByteU5BU5D_t3835026402* ___matadata3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*, uint8_t*, uint8_t*, uint8_t*);

	// Marshaling of parameter '___newTarget0' to native representation
	int32_t* ____newTarget0_marshaled = NULL;
	int32_t ____newTarget0_marshaled_dereferenced = 0;
	____newTarget0_marshaled_dereferenced = *___newTarget0;
	____newTarget0_marshaled = &____newTarget0_marshaled_dereferenced;

	// Marshaling of parameter '___urls1' to native representation
	uint8_t* ____urls1_marshaled = NULL;
	____urls1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___urls1);

	// Marshaling of parameter '___names2' to native representation
	uint8_t* ____names2_marshaled = NULL;
	____names2_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___names2);

	// Marshaling of parameter '___matadata3' to native representation
	uint8_t* ____matadata3_marshaled = NULL;
	____matadata3_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___matadata3);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_getNewCloudTarget)(____newTarget0_marshaled, ____urls1_marshaled, ____names2_marshaled, ____matadata3_marshaled);

	// Marshaling of parameter '___newTarget0' back from native representation
	int32_t ____newTarget0_result_dereferenced = 0;
	int32_t* ____newTarget0_result = &____newTarget0_result_dereferenced;
	*____newTarget0_result = *____newTarget0_marshaled;
	*___newTarget0 = *____newTarget0_result;

	// Marshaling cleanup of parameter '___newTarget0' native representation

	// Marshaling cleanup of parameter '___urls1' native representation

	// Marshaling cleanup of parameter '___names2' native representation

	// Marshaling cleanup of parameter '___matadata3' native representation

}
// System.Void VoidAR::_setCloudUser(System.String,System.String,System.Int32)
extern "C" void DEFAULT_CALL _setCloudUser(char*, char*, int32_t);
extern "C"  void VoidAR__setCloudUser_m456182358 (Il2CppObject * __this /* static, unused */, String_t* ___accessKey0, String_t* ___secretKey1, int32_t ___useCloud2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, int32_t);

	// Marshaling of parameter '___accessKey0' to native representation
	char* ____accessKey0_marshaled = NULL;
	____accessKey0_marshaled = il2cpp_codegen_marshal_string(___accessKey0);

	// Marshaling of parameter '___secretKey1' to native representation
	char* ____secretKey1_marshaled = NULL;
	____secretKey1_marshaled = il2cpp_codegen_marshal_string(___secretKey1);

	// Marshaling of parameter '___useCloud2' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setCloudUser)(____accessKey0_marshaled, ____secretKey1_marshaled, ___useCloud2);

	// Marshaling cleanup of parameter '___accessKey0' native representation
	il2cpp_codegen_marshal_free(____accessKey0_marshaled);
	____accessKey0_marshaled = NULL;

	// Marshaling cleanup of parameter '___secretKey1' native representation
	il2cpp_codegen_marshal_free(____secretKey1_marshaled);
	____secretKey1_marshaled = NULL;

	// Marshaling cleanup of parameter '___useCloud2' native representation

}
// System.Void VoidAR::_setShapeMatchLevel(System.Int32)
extern "C" void DEFAULT_CALL _setShapeMatchLevel(int32_t);
extern "C"  void VoidAR__setShapeMatchLevel_m1236546128 (Il2CppObject * __this /* static, unused */, int32_t ___lvl0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___lvl0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setShapeMatchLevel)(___lvl0);

	// Marshaling cleanup of parameter '___lvl0' native representation

}
// System.Boolean VoidAR::_checkNet()
extern "C" int32_t DEFAULT_CALL _checkNet();
extern "C"  bool VoidAR__checkNet_m2630442504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_checkNet)();

	return _return_value;
}
// System.Boolean VoidAR::_useExtensionTracking(System.Boolean)
extern "C" int32_t DEFAULT_CALL _useExtensionTracking(int32_t);
extern "C"  bool VoidAR__useExtensionTracking_m2889953485 (Il2CppObject * __this /* static, unused */, bool ___use0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___use0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_useExtensionTracking)(___use0);

	// Marshaling cleanup of parameter '___use0' native representation

	return _return_value;
}
// System.Boolean VoidAR::_addTarget(System.String,System.Byte[],System.Int32)
extern "C" int32_t DEFAULT_CALL _addTarget(char*, uint8_t*, int32_t);
extern "C"  bool VoidAR__addTarget_m1219800199 (Il2CppObject * __this /* static, unused */, String_t* ___path0, ByteU5BU5D_t3835026402* ___data1, int32_t ___size2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, uint8_t*, int32_t);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Marshaling of parameter '___data1' to native representation
	uint8_t* ____data1_marshaled = NULL;
	____data1_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data1);

	// Marshaling of parameter '___size2' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_addTarget)(____path0_marshaled, ____data1_marshaled, ___size2);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	// Marshaling cleanup of parameter '___data1' native representation

	// Marshaling cleanup of parameter '___size2' native representation

	return _return_value;
}
// System.Boolean VoidAR::_finishAddTarget()
extern "C" int32_t DEFAULT_CALL _finishAddTarget();
extern "C"  bool VoidAR__finishAddTarget_m3800497644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_finishAddTarget)();

	return _return_value;
}
// System.Boolean VoidAR::_removeImageTarget(System.String)
extern "C" int32_t DEFAULT_CALL _removeImageTarget(char*);
extern "C"  bool VoidAR__removeImageTarget_m3284403673 (Il2CppObject * __this /* static, unused */, String_t* ___targetName0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___targetName0' to native representation
	char* ____targetName0_marshaled = NULL;
	____targetName0_marshaled = il2cpp_codegen_marshal_string(___targetName0);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(_removeImageTarget)(____targetName0_marshaled);

	// Marshaling cleanup of parameter '___targetName0' native representation
	il2cpp_codegen_marshal_free(____targetName0_marshaled);
	____targetName0_marshaled = NULL;

	return _return_value;
}
// VoidAR VoidAR::GetInstance()
extern Il2CppClass* VoidAR_t451713137_il2cpp_TypeInfo_var;
extern const uint32_t VoidAR_GetInstance_m3573393179_MetadataUsageId;
extern "C"  VoidAR_t451713137 * VoidAR_GetInstance_m3573393179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_GetInstance_m3573393179_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		VoidAR_t451713137 * L_0 = ((VoidAR_t451713137_StaticFields*)VoidAR_t451713137_il2cpp_TypeInfo_var->static_fields)->get_instance_1();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		VoidAR_t451713137 * L_1 = (VoidAR_t451713137 *)il2cpp_codegen_object_new(VoidAR_t451713137_il2cpp_TypeInfo_var);
		VoidAR__ctor_m729901080(L_1, /*hidden argument*/NULL);
		((VoidAR_t451713137_StaticFields*)VoidAR_t451713137_il2cpp_TypeInfo_var->static_fields)->set_instance_1(L_1);
	}

IL_0014:
	{
		VoidAR_t451713137 * L_2 = ((VoidAR_t451713137_StaticFields*)VoidAR_t451713137_il2cpp_TypeInfo_var->static_fields)->get_instance_1();
		return L_2;
	}
}
// System.Void VoidAR::setShapeMatchLevel(System.Int32)
extern "C"  void VoidAR_setShapeMatchLevel_m764983593 (VoidAR_t451713137 * __this, int32_t ___lvl0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___lvl0;
		VoidAR__setShapeMatchLevel_m1236546128(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::init(System.Int32,System.Int32,System.Boolean)
extern Il2CppClass* DoubleU5BU5D_t2839599125_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2031953187;
extern const uint32_t VoidAR_init_m3437805449_MetadataUsageId;
extern "C"  void VoidAR_init_m3437805449 (VoidAR_t451713137 * __this, int32_t ___cameraIndex0, int32_t ___markerType1, bool ___extensionTracking2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_init_m3437805449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		int32_t* L_0 = __this->get_address_of_cameaWidth_24();
		int32_t* L_1 = __this->get_address_of_cameraHeight_25();
		VoidAR_setScreenSize_m2831642495(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = ___extensionTracking2;
		VoidAR_useExentsionTracking_m1015617692(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = (((float)((float)L_5)));
		int32_t L_6 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Math_Min_m4290821911(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_1 = (((float)((float)L_8)));
		float L_9 = V_0;
		int32_t L_10 = __this->get_cameaWidth_24();
		V_2 = ((float)((float)(((float)((float)L_9)))/(float)(((float)((float)L_10)))));
		float L_11 = V_0;
		float L_12 = V_1;
		__this->set_aspectRatio_23(((float)((float)L_11/(float)L_12)));
		float L_13 = V_1;
		float L_14 = V_2;
		V_3 = ((float)((float)L_13/(float)L_14));
		float L_15 = V_3;
		__this->set_screenH_22(L_15);
		float L_16 = V_3;
		__this->set_orthoSize1_20(((float)((float)L_16/(float)(2.0f))));
		float L_17 = __this->get_orthoSize1_20();
		float L_18 = __this->get_aspectRatio_23();
		__this->set_orthoSize2_21(((float)((float)L_17*(float)L_18)));
		int32_t L_19 = ___markerType1;
		VoidAR__initLibrary_m4058222619(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		int32_t L_20 = ___markerType1;
		bool L_21 = VoidAR_initShapeLibrary_m2925494533(__this, _stringLiteral2031953187, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_0099;
		}
	}
	{
		__this->set_state_2(4);
		return;
	}

IL_0099:
	{
		__this->set_state_2(1);
		Camera_t2805735124 * L_22 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = __this->get_orthoSize1_20();
		NullCheck(L_22);
		Camera_set_orthographicSize_m2708824189(L_22, L_23, /*hidden argument*/NULL);
		Camera_t2805735124 * L_24 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t224878301 * L_25 = Component_get_transform_m2697483695(L_24, /*hidden argument*/NULL);
		Vector3_t465617797  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector3__ctor_m2638739322(&L_26, (90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_eulerAngles_m2881310872(L_25, L_26, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_cameaWidth_24();
		int32_t L_28 = __this->get_cameraHeight_25();
		VoidAR_buildGLProjectionMatrix_m2599995543(__this, L_27, L_28, /*hidden argument*/NULL);
		__this->set_vmMatrixData_6(((DoubleU5BU5D_t2839599125*)SZArrayNew(DoubleU5BU5D_t2839599125_il2cpp_TypeInfo_var, (uint32_t)((int32_t)160))));
		V_4 = 0;
		int32_t L_29 = ___cameraIndex0;
		VoidAR_startCapture_m1392394096(__this, L_29, (&V_4), /*hidden argument*/NULL);
		__this->set_state_2(1);
		int32_t L_30 = ___markerType1;
		if ((!(((uint32_t)L_30) == ((uint32_t)1))))
		{
			goto IL_010f;
		}
	}

IL_010f:
	{
		return;
	}
}
// System.Void VoidAR::initBGTexture()
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var;
extern const uint32_t VoidAR_initBGTexture_m1773369504_MetadataUsageId;
extern "C"  void VoidAR_initBGTexture_m1773369504 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_initBGTexture_m1773369504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ExternalTextureDesc_t3964848077 * L_0 = __this->get_address_of_externalTexture_26();
		int32_t* L_1 = L_0->get_address_of_width_2();
		ExternalTextureDesc_t3964848077 * L_2 = __this->get_address_of_externalTexture_26();
		int32_t* L_3 = L_2->get_address_of_height_3();
		VoidAR__initBGTexture_m585106992(NULL /*static, unused*/, (&V_0), L_1, L_3, /*hidden argument*/NULL);
		ExternalTextureDesc_t3964848077 * L_4 = __this->get_address_of_externalTexture_26();
		int32_t L_5 = V_0;
		IntPtr_t L_6 = IntPtr_op_Explicit_m3896766622(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		L_4->set_tex1_0(L_6);
		ExternalTextureDesc_t3964848077 * L_7 = __this->get_address_of_externalTexture_26();
		int32_t L_8 = L_7->get_width_2();
		ExternalTextureDesc_t3964848077 * L_9 = __this->get_address_of_externalTexture_26();
		int32_t L_10 = L_9->get_height_3();
		ExternalTextureDesc_t3964848077 * L_11 = __this->get_address_of_externalTexture_26();
		IntPtr_t L_12 = L_11->get_tex1_0();
		Texture2D_t3575456220 * L_13 = Texture2D_CreateExternalTexture_m3402112250(NULL /*static, unused*/, L_8, L_10, 4, (bool)0, (bool)0, L_12, /*hidden argument*/NULL);
		__this->set_bgTexture1_8(L_13);
		Texture2D_t3575456220 * L_14 = __this->get_bgTexture1_8();
		NullCheck(L_14);
		Texture_set_filterMode_m3838996656(L_14, 1, /*hidden argument*/NULL);
		Texture2D_t3575456220 * L_15 = __this->get_bgTexture1_8();
		NullCheck(L_15);
		Texture_set_wrapMode_m333956747(L_15, 1, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_16 = __this->get_backGround_7();
		NullCheck(L_16);
		Renderer_t2715231144 * L_17 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_16, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_17);
		Material_t2197338622 * L_18 = Renderer_get_material_m2553789785(L_17, /*hidden argument*/NULL);
		Texture2D_t3575456220 * L_19 = __this->get_bgTexture1_8();
		NullCheck(L_18);
		Material_set_mainTexture_m3584203343(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::setScreenSize(System.Int32&,System.Int32&)
extern "C"  void VoidAR_setScreenSize_m2831642495 (VoidAR_t451713137 * __this, int32_t* ___width0, int32_t* ___height1, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___width0;
		int32_t* L_1 = ___height1;
		VoidAR__setScreenSize_m914176366(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___width0;
		__this->set_screen_width_13((*((int32_t*)L_2)));
		int32_t* L_3 = ___height1;
		__this->set_screen_height_14((*((int32_t*)L_3)));
		return;
	}
}
// System.Boolean VoidAR::isMarkerExist(System.String)
extern "C"  bool VoidAR_isMarkerExist_m1590497145 (VoidAR_t451713137 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Dictionary_2_t3496208296 * L_0 = __this->get_mMarkers_4();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void VoidAR::addCloudTarget(Marker,UnityEngine.GameObject)
extern Il2CppClass* MarkerMap_t684226712_il2cpp_TypeInfo_var;
extern const uint32_t VoidAR_addCloudTarget_m1660151627_MetadataUsageId;
extern "C"  void VoidAR_addCloudTarget_m1660151627 (VoidAR_t451713137 * __this, Marker_t1240580442 * ___m0, GameObject_t1366199518 * ___obj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_addCloudTarget_m1660151627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MarkerMap_t684226712 * V_0 = NULL;
	{
		MarkerMap_t684226712 * L_0 = (MarkerMap_t684226712 *)il2cpp_codegen_object_new(MarkerMap_t684226712_il2cpp_TypeInfo_var);
		MarkerMap__ctor_m3444299521(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		MarkerMap_t684226712 * L_1 = V_0;
		GameObject_t1366199518 * L_2 = ___obj1;
		NullCheck(L_1);
		L_1->set_gameObject_0(L_2);
		MarkerMap_t684226712 * L_3 = V_0;
		Marker_t1240580442 * L_4 = ___m0;
		NullCheck(L_3);
		L_3->set_marker_1(L_4);
		Dictionary_2_t3496208296 * L_5 = __this->get_mMarkers_4();
		Marker_t1240580442 * L_6 = ___m0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_imageFilePath_4();
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::ContainsKey(!0) */, L_5, L_7);
		if (L_8)
		{
			goto IL_003c;
		}
	}
	{
		Dictionary_2_t3496208296 * L_9 = __this->get_mMarkers_4();
		Marker_t1240580442 * L_10 = ___m0;
		NullCheck(L_10);
		String_t* L_11 = L_10->get_imageFilePath_4();
		MarkerMap_t684226712 * L_12 = V_0;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, MarkerMap_t684226712 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::Add(!0,!1) */, L_9, L_11, L_12);
	}

IL_003c:
	{
		return;
	}
}
// System.Boolean VoidAR::initShapeLibrary(System.String,System.Int32)
extern const Il2CppType* Material_t2197338622_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t2197338622_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3746601679;
extern const uint32_t VoidAR_initShapeLibrary_m2925494533_MetadataUsageId;
extern "C"  bool VoidAR_initShapeLibrary_m2925494533 (VoidAR_t451713137 * __this, String_t* ___path0, int32_t ___markerType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_initShapeLibrary_m2925494533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t2805735124 * L_0 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t224878301 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t224878301 * L_2 = Transform_GetChild_m3838588184(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1366199518 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		__this->set_backGround_7(L_3);
		GameObject_t1366199518 * L_4 = __this->get_backGround_7();
		NullCheck(L_4);
		Renderer_t2715231144 * L_5 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_5);
		Renderer_set_enabled_m142717579(L_5, (bool)1, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_6 = __this->get_backGround_7();
		NullCheck(L_6);
		Transform_t224878301 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_screen_width_13();
		int32_t L_9 = __this->get_screen_height_14();
		Vector3_t465617797  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, (((float)((float)L_8))), (((float)((float)((-L_9))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_m2325460848(L_7, L_10, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_11 = __this->get_backGround_7();
		NullCheck(L_11);
		Renderer_t2715231144 * L_12 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_11, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Material_t2197338622_0_0_0_var), /*hidden argument*/NULL);
		Object_t1181371020 * L_14 = Resources_Load_m243305716(NULL /*static, unused*/, _stringLiteral3746601679, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Renderer_set_material_m1053097112(L_12, ((Material_t2197338622 *)CastclassClass(L_14, Material_t2197338622_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		VoidAR_initBGTexture_m1773369504(__this, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void VoidAR::buildGLProjectionMatrix(System.Int32,System.Int32)
extern Il2CppClass* DoubleU5BU5D_t2839599125_il2cpp_TypeInfo_var;
extern const uint32_t VoidAR_buildGLProjectionMatrix_m2599995543_MetadataUsageId;
extern "C"  void VoidAR_buildGLProjectionMatrix_m2599995543 (VoidAR_t451713137 * __this, int32_t ___screenWidth0, int32_t ___sceenHeight1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_buildGLProjectionMatrix_m2599995543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DoubleU5BU5D_t2839599125* V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_0 = ((DoubleU5BU5D_t2839599125*)SZArrayNew(DoubleU5BU5D_t2839599125_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		int32_t L_0 = ___screenWidth0;
		int32_t L_1 = ___sceenHeight1;
		DoubleU5BU5D_t2839599125* L_2 = V_0;
		VoidAR__buildGLProjectionMatrix_m2182184962(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_002b;
	}

IL_0017:
	{
		Matrix4x4_t1261955742 * L_3 = __this->get_address_of_projMatrix_3();
		int32_t L_4 = V_1;
		DoubleU5BU5D_t2839599125* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Matrix4x4_set_Item_m870949794(L_3, L_4, (((float)((float)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) < ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}
}
// System.Void VoidAR::startCapture(System.Int32,System.Int32&)
extern "C"  void VoidAR_startCapture_m1392394096 (VoidAR_t451713137 * __this, int32_t ___cameraIndex0, int32_t* ___opened1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___cameraIndex0;
		int32_t* L_1 = ___opened1;
		VoidAR__startCapture_m3291993969(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoidAR::checkNet()
extern "C"  bool VoidAR_checkNet_m1936777097 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VoidAR__checkNet_m2630442504(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void VoidAR::stopCapture()
extern "C"  void VoidAR_stopCapture_m1505908938 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	{
		VoidAR__stopCapture_m719693631(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::getMatchResult(System.IntPtr,System.Int32&,System.Int32&,System.Int32&,System.Double[],System.Byte[],System.Int32&,System.Int32[])
extern "C"  void VoidAR_getMatchResult_m624574000 (VoidAR_t451713137 * __this, IntPtr_t ___frame0, int32_t* ___width1, int32_t* ___height2, int32_t* ___flip3, DoubleU5BU5D_t2839599125* ___matrix4, ByteU5BU5D_t3835026402* ___markers5, int32_t* ___mainMarker6, Int32U5BU5D_t3315407976* ___scores7, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___frame0;
		int32_t* L_1 = ___width1;
		int32_t* L_2 = ___height2;
		int32_t* L_3 = ___flip3;
		DoubleU5BU5D_t2839599125* L_4 = ___matrix4;
		ByteU5BU5D_t3835026402* L_5 = ___markers5;
		int32_t* L_6 = ___mainMarker6;
		Int32U5BU5D_t3315407976* L_7 = ___scores7;
		VoidAR__getMatchResult_m819854597(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::unloadResource()
extern "C"  void VoidAR_unloadResource_m606358455 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	{
		VoidAR__unloadResource_m1465702158(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t3496208296 * L_0 = __this->get_mMarkers_4();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::Clear() */, L_0);
		return;
	}
}
// System.Int32 VoidAR::update(UnityEngine.Camera)
extern "C"  int32_t VoidAR_update_m1138037781 (VoidAR_t451713137 * __this, Camera_t2805735124 * ___arCamera0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_state_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0027;
		}
		if (L_1 == 1)
		{
			goto IL_002c;
		}
		if (L_1 == 2)
		{
			goto IL_0038;
		}
		if (L_1 == 3)
		{
			goto IL_0022;
		}
	}
	{
		goto IL_003d;
	}

IL_0022:
	{
		goto IL_003d;
	}

IL_0027:
	{
		goto IL_003d;
	}

IL_002c:
	{
		Camera_t2805735124 * L_2 = ___arCamera0;
		VoidAR_updateTracking_m3752549890(__this, L_2, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0038:
	{
		goto IL_003d;
	}

IL_003d:
	{
		return 0;
	}
}
// System.Void VoidAR::match()
extern "C"  void VoidAR_match_m3342905039 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	{
		VoidAR__match_m1170501128(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::getTrackingTime(System.Double[])
extern "C"  void VoidAR_getTrackingTime_m1491785298 (VoidAR_t451713137 * __this, DoubleU5BU5D_t2839599125* ___times0, const MethodInfo* method)
{
	{
		DoubleU5BU5D_t2839599125* L_0 = ___times0;
		VoidAR__getTrackingTime_m1283378439(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::reset()
extern "C"  void VoidAR_reset_m3948994529 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	{
		VoidAR__reset_m2806132926(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 VoidAR::getTrackStatus()
extern Il2CppClass* Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var;
extern const uint32_t VoidAR_getTrackStatus_m415318551_MetadataUsageId;
extern "C"  int32_t VoidAR_getTrackStatus_m415318551 (VoidAR_t451713137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_getTrackStatus_m415318551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3315407976* V_0 = NULL;
	{
		V_0 = ((Int32U5BU5D_t3315407976*)SZArrayNew(Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var, (uint32_t)1));
		Int32U5BU5D_t3315407976* L_0 = V_0;
		VoidAR__getTrackStatus_m2897874513(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Int32U5BU5D_t3315407976* L_1 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		return ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void VoidAR::addCurrentShapeImageTarget(System.Int32&,System.Byte[],System.Int32)
extern "C"  void VoidAR_addCurrentShapeImageTarget_m2500774288 (VoidAR_t451713137 * __this, int32_t* ___success0, ByteU5BU5D_t3835026402* ___name1, int32_t ___tracking2, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___success0;
		ByteU5BU5D_t3835026402* L_1 = ___name1;
		int32_t L_2 = ___tracking2;
		VoidAR__addCurrentShapeImageTarget_m144643407(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::setDebugDraw(System.Int32)
extern "C"  void VoidAR_setDebugDraw_m1312727622 (VoidAR_t451713137 * __this, int32_t ___debugDraw0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___debugDraw0;
		VoidAR__setDebugDraw_m1360007373(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Matrix4x4 VoidAR::PerspectiveOffCenter(System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32)
extern Il2CppClass* Matrix4x4_t1261955742_il2cpp_TypeInfo_var;
extern const uint32_t VoidAR_PerspectiveOffCenter_m1977762971_MetadataUsageId;
extern "C"  Matrix4x4_t1261955742  VoidAR_PerspectiveOffCenter_m1977762971 (Il2CppObject * __this /* static, unused */, float ___fx0, float ___fy1, float ___cx2, float ___cy3, int32_t ___screenWidth4, int32_t ___screenHeight5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_PerspectiveOffCenter_m1977762971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Matrix4x4_t1261955742  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (0.01f);
		V_1 = (100.0f);
		Initobj (Matrix4x4_t1261955742_il2cpp_TypeInfo_var, (&V_2));
		float L_0 = ___fx0;
		int32_t L_1 = ___screenWidth4;
		(&V_2)->set_m00_0(((float)((float)((float)((float)(2.0f)*(float)L_0))/(float)(((float)((float)L_1))))));
		(&V_2)->set_m01_4((0.0f));
		(&V_2)->set_m02_8((0.0f));
		(&V_2)->set_m03_12((0.0f));
		(&V_2)->set_m10_1((0.0f));
		float L_2 = ___fy1;
		int32_t L_3 = ___screenHeight5;
		(&V_2)->set_m11_5(((float)((float)((float)((float)(2.0f)*(float)L_2))/(float)(((float)((float)L_3))))));
		(&V_2)->set_m12_9((0.0f));
		(&V_2)->set_m13_13((0.0f));
		float L_4 = ___cx2;
		int32_t L_5 = ___screenWidth4;
		(&V_2)->set_m20_2(((float)((float)((float)((float)((float)((float)(2.0f)*(float)L_4))/(float)(((float)((float)L_5)))))-(float)(1.0f))));
		float L_6 = ___cy3;
		int32_t L_7 = ___screenHeight5;
		(&V_2)->set_m21_6(((float)((float)(1.0f)-(float)((float)((float)((float)((float)(2.0f)*(float)L_6))/(float)(((float)((float)L_7))))))));
		float L_8 = V_1;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_0;
		(&V_2)->set_m22_10(((float)((float)((-((float)((float)L_8+(float)L_9))))/(float)((float)((float)L_10-(float)L_11)))));
		(&V_2)->set_m23_14((-1.0f));
		(&V_2)->set_m30_3((0.0f));
		(&V_2)->set_m31_7((0.0f));
		float L_12 = V_1;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_0;
		(&V_2)->set_m32_11(((float)((float)((float)((float)((float)((float)(-2.0f)*(float)L_12))*(float)L_13))/(float)((float)((float)L_14-(float)L_15)))));
		(&V_2)->set_m33_15((0.0f));
		Matrix4x4_t1261955742  L_16 = Matrix4x4_get_transpose_m3368305223((&V_2), /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean VoidAR::getMatrix(System.Int32,System.Double[],UnityEngine.Matrix4x4&)
extern "C"  bool VoidAR_getMatrix_m1862633888 (VoidAR_t451713137 * __this, int32_t ___index0, DoubleU5BU5D_t2839599125* ___inMatrix1, Matrix4x4_t1261955742 * ___matrix2, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (bool)1;
		int32_t L_0 = ___index0;
		V_1 = ((int32_t)((int32_t)L_0*(int32_t)((int32_t)16)));
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_002a;
	}

IL_000e:
	{
		DoubleU5BU5D_t2839599125* L_2 = ___inMatrix1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		if ((((double)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)))) == ((double)(0.0))))
		{
			goto IL_0026;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0034;
	}

IL_0026:
	{
		int32_t L_5 = V_2;
		V_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_6 = V_2;
		int32_t L_7 = V_1;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)((int32_t)L_7+(int32_t)((int32_t)16))))))
		{
			goto IL_000e;
		}
	}

IL_0034:
	{
		bool L_8 = V_0;
		if (L_8)
		{
			goto IL_0101;
		}
	}
	{
		Matrix4x4_t1261955742 * L_9 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_10 = ___inMatrix1;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		L_9->set_m00_0((((float)((float)((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)))))));
		Matrix4x4_t1261955742 * L_13 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_14 = ___inMatrix1;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_15+(int32_t)1)));
		int32_t L_16 = ((int32_t)((int32_t)L_15+(int32_t)1));
		L_13->set_m01_4((((float)((float)((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16)))))));
		Matrix4x4_t1261955742 * L_17 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_18 = ___inMatrix1;
		int32_t L_19 = V_1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)2)));
		int32_t L_20 = ((int32_t)((int32_t)L_19+(int32_t)2));
		L_17->set_m02_8((((float)((float)((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20)))))));
		Matrix4x4_t1261955742 * L_21 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_22 = ___inMatrix1;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)((int32_t)12))));
		int32_t L_24 = ((int32_t)((int32_t)L_23+(int32_t)((int32_t)12)));
		L_21->set_m03_12((((float)((float)((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24)))))));
		Matrix4x4_t1261955742 * L_25 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_26 = ___inMatrix1;
		int32_t L_27 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)((int32_t)L_27+(int32_t)4)));
		int32_t L_28 = ((int32_t)((int32_t)L_27+(int32_t)4));
		L_25->set_m10_1((((float)((float)((L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28)))))));
		Matrix4x4_t1261955742 * L_29 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_30 = ___inMatrix1;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)L_31+(int32_t)5)));
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)5));
		L_29->set_m11_5((((float)((float)((L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32)))))));
		Matrix4x4_t1261955742 * L_33 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_34 = ___inMatrix1;
		int32_t L_35 = V_1;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)((int32_t)L_35+(int32_t)6)));
		int32_t L_36 = ((int32_t)((int32_t)L_35+(int32_t)6));
		L_33->set_m12_9((((float)((float)((L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36)))))));
		Matrix4x4_t1261955742 * L_37 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_38 = ___inMatrix1;
		int32_t L_39 = V_1;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)((int32_t)L_39+(int32_t)((int32_t)13))));
		int32_t L_40 = ((int32_t)((int32_t)L_39+(int32_t)((int32_t)13)));
		L_37->set_m13_13((((float)((float)((L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40)))))));
		Matrix4x4_t1261955742 * L_41 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_42 = ___inMatrix1;
		int32_t L_43 = V_1;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)L_43+(int32_t)8)));
		int32_t L_44 = ((int32_t)((int32_t)L_43+(int32_t)8));
		L_41->set_m20_2((((float)((float)((L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44)))))));
		Matrix4x4_t1261955742 * L_45 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_46 = ___inMatrix1;
		int32_t L_47 = V_1;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)((int32_t)L_47+(int32_t)((int32_t)9))));
		int32_t L_48 = ((int32_t)((int32_t)L_47+(int32_t)((int32_t)9)));
		L_45->set_m21_6((((float)((float)((L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48)))))));
		Matrix4x4_t1261955742 * L_49 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_50 = ___inMatrix1;
		int32_t L_51 = V_1;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)((int32_t)L_51+(int32_t)((int32_t)10))));
		int32_t L_52 = ((int32_t)((int32_t)L_51+(int32_t)((int32_t)10)));
		L_49->set_m22_10((((float)((float)((L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52)))))));
		Matrix4x4_t1261955742 * L_53 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_54 = ___inMatrix1;
		int32_t L_55 = V_1;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)L_55+(int32_t)((int32_t)14))));
		int32_t L_56 = ((int32_t)((int32_t)L_55+(int32_t)((int32_t)14)));
		L_53->set_m23_14((((float)((float)((L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56)))))));
		Matrix4x4_t1261955742 * L_57 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_58 = ___inMatrix1;
		int32_t L_59 = V_1;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)((int32_t)L_59+(int32_t)3)));
		int32_t L_60 = ((int32_t)((int32_t)L_59+(int32_t)3));
		L_57->set_m30_3((((float)((float)((L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60)))))));
		Matrix4x4_t1261955742 * L_61 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_62 = ___inMatrix1;
		int32_t L_63 = V_1;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)((int32_t)L_63+(int32_t)7)));
		int32_t L_64 = ((int32_t)((int32_t)L_63+(int32_t)7));
		L_61->set_m31_7((((float)((float)((L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64)))))));
		Matrix4x4_t1261955742 * L_65 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_66 = ___inMatrix1;
		int32_t L_67 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)((int32_t)L_67+(int32_t)((int32_t)11))));
		int32_t L_68 = ((int32_t)((int32_t)L_67+(int32_t)((int32_t)11)));
		L_65->set_m32_11((((float)((float)((L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68)))))));
		Matrix4x4_t1261955742 * L_69 = ___matrix2;
		DoubleU5BU5D_t2839599125* L_70 = ___inMatrix1;
		int32_t L_71 = V_1;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)((int32_t)L_71+(int32_t)((int32_t)15))));
		int32_t L_72 = ((int32_t)((int32_t)L_71+(int32_t)((int32_t)15)));
		L_69->set_m33_15((((float)((float)((L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72)))))));
		return (bool)1;
	}

IL_0101:
	{
		return (bool)0;
	}
}
// System.Void VoidAR::updateTracking(UnityEngine.Camera)
extern Il2CppClass* Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2125916575_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1685951112_il2cpp_TypeInfo_var;
extern Il2CppClass* Quaternion_t83606849_il2cpp_TypeInfo_var;
extern Il2CppClass* Matrix4x4_t1261955742_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t793537021_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1786818228_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m4133290344_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1048306582_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3991964846_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m747115171_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3306365281_MethodInfo_var;
extern const uint32_t VoidAR_updateTracking_m3752549890_MetadataUsageId;
extern "C"  void VoidAR_updateTracking_m3752549890 (VoidAR_t451713137 * __this, Camera_t2805735124 * ___arCamera0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_updateTracking_m3752549890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3315407976* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ByteU5BU5D_t3835026402* V_5 = NULL;
	String_t* V_6 = NULL;
	CharU5BU5D_t1685951112* V_7 = NULL;
	StringU5BU5D_t3764931161* V_8 = NULL;
	Matrix4x4_t1261955742  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Quaternion_t83606849  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Matrix4x4_t1261955742  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Matrix4x4_t1261955742  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Quaternion_t83606849  V_13;
	memset(&V_13, 0, sizeof(V_13));
	KeyValuePair_2_t2053528608  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Enumerator_t793537021  V_15;
	memset(&V_15, 0, sizeof(V_15));
	bool V_16 = false;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	bool V_19 = false;
	Matrix4x4_t1261955742  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t465617797  V_21;
	memset(&V_21, 0, sizeof(V_21));
	float V_22 = 0.0f;
	Exception_t1145979430 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1145979430 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Camera_t2805735124 * L_0 = ___arCamera0;
		Matrix4x4_t1261955742 * L_1 = __this->get_address_of_projMatrix_3();
		float L_2 = Matrix4x4_get_Item_m3317262185(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_set_fieldOfView_m3974156396(L_0, L_2, /*hidden argument*/NULL);
		V_0 = (-1);
		V_1 = ((Int32U5BU5D_t3315407976*)SZArrayNew(Int32U5BU5D_t3315407976_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		V_2 = (-1);
		V_3 = ((int32_t)640);
		V_4 = ((int32_t)480);
		V_5 = ((ByteU5BU5D_t3835026402*)SZArrayNew(ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024)));
		IntPtr_t L_3 = IntPtr_op_Explicit_m3896766622(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		DoubleU5BU5D_t2839599125* L_4 = __this->get_vmMatrixData_6();
		ByteU5BU5D_t3835026402* L_5 = V_5;
		Int32U5BU5D_t3315407976* L_6 = V_1;
		VoidAR_getMatchResult_m624574000(__this, L_3, (&V_3), (&V_4), (&V_2), L_4, L_5, (&V_0), L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2125916575_il2cpp_TypeInfo_var);
		Encoding_t2125916575 * L_7 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3835026402* L_8 = V_5;
		NullCheck(L_7);
		String_t* L_9 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3835026402* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_7, L_8);
		V_6 = L_9;
		CharU5BU5D_t1685951112* L_10 = ((CharU5BU5D_t1685951112*)SZArrayNew(CharU5BU5D_t1685951112_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)44));
		V_7 = L_10;
		String_t* L_11 = V_6;
		CharU5BU5D_t1685951112* L_12 = V_7;
		NullCheck(L_11);
		StringU5BU5D_t3764931161* L_13 = String_Split_m3326265864(L_11, L_12, /*hidden argument*/NULL);
		V_8 = L_13;
		int32_t L_14 = V_2;
		if ((!(((uint32_t)L_14) == ((uint32_t)(-1)))))
		{
			goto IL_0097;
		}
	}
	{
		GameObject_t1366199518 * L_15 = __this->get_backGround_7();
		NullCheck(L_15);
		Renderer_t2715231144 * L_16 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_15, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_16);
		Renderer_set_enabled_m142717579(L_16, (bool)0, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_0097:
	{
		GameObject_t1366199518 * L_17 = __this->get_backGround_7();
		NullCheck(L_17);
		Renderer_t2715231144 * L_18 = GameObject_GetComponent_TisRenderer_t2715231144_m1312615893(L_17, /*hidden argument*/GameObject_GetComponent_TisRenderer_t2715231144_m1312615893_MethodInfo_var);
		NullCheck(L_18);
		Renderer_set_enabled_m142717579(L_18, (bool)1, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		Matrix4x4_t1261955742  L_19 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_19;
		int32_t L_20 = V_2;
		if (!L_20)
		{
			goto IL_00bc;
		}
	}
	{
		int32_t L_21 = V_2;
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_01ab;
		}
	}

IL_00bc:
	{
		int32_t L_22 = V_2;
		if (L_22)
		{
			goto IL_00f0;
		}
	}
	{
		GameObject_t1366199518 * L_23 = __this->get_backGround_7();
		NullCheck(L_23);
		Transform_t224878301 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		int32_t L_25 = __this->get_screen_width_13();
		int32_t L_26 = __this->get_screen_height_14();
		Vector3_t465617797  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2638739322(&L_27, (((float)((float)L_25))), (((float)((float)((-L_26))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localScale_m2325460848(L_24, L_27, /*hidden argument*/NULL);
		goto IL_0119;
	}

IL_00f0:
	{
		GameObject_t1366199518 * L_28 = __this->get_backGround_7();
		NullCheck(L_28);
		Transform_t224878301 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		int32_t L_30 = __this->get_screen_width_13();
		int32_t L_31 = __this->get_screen_height_14();
		Vector3_t465617797  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Vector3__ctor_m2638739322(&L_32, (((float)((float)((-L_30))))), (((float)((float)L_31))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_localScale_m2325460848(L_29, L_32, /*hidden argument*/NULL);
	}

IL_0119:
	{
		GameObject_t1366199518 * L_33 = __this->get_backGround_7();
		NullCheck(L_33);
		Transform_t224878301 * L_34 = GameObject_get_transform_m909382139(L_33, /*hidden argument*/NULL);
		Vector3_t465617797  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector3__ctor_m2638739322(&L_35, (0.0f), (0.0f), (-90.0f), /*hidden argument*/NULL);
		Quaternion_t83606849  L_36 = Quaternion_Euler_m3586339259(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_localRotation_m2055111962(L_34, L_36, /*hidden argument*/NULL);
		Camera_t2805735124 * L_37 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_38 = __this->get_orthoSize2_21();
		NullCheck(L_37);
		Camera_set_orthographicSize_m2708824189(L_37, L_38, /*hidden argument*/NULL);
		Initobj (Quaternion_t83606849_il2cpp_TypeInfo_var, (&V_10));
		Vector3_t465617797  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Vector3__ctor_m2638739322(&L_39, (0.0f), (0.0f), (-90.0f), /*hidden argument*/NULL);
		Quaternion_set_eulerAngles_m3695252804((&V_10), L_39, /*hidden argument*/NULL);
		Vector3_t465617797  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Vector3__ctor_m2638739322(&L_40, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t83606849  L_41 = V_10;
		Vector3_t465617797  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector3__ctor_m2638739322(&L_42, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t1261955742  L_43 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_40, L_41, L_42, /*hidden argument*/NULL);
		V_9 = L_43;
		goto IL_0242;
	}

IL_01ab:
	{
		int32_t L_44 = V_2;
		if ((!(((uint32_t)L_44) == ((uint32_t)1))))
		{
			goto IL_01e0;
		}
	}
	{
		GameObject_t1366199518 * L_45 = __this->get_backGround_7();
		NullCheck(L_45);
		Transform_t224878301 * L_46 = GameObject_get_transform_m909382139(L_45, /*hidden argument*/NULL);
		int32_t L_47 = __this->get_screen_width_13();
		int32_t L_48 = __this->get_screen_height_14();
		Vector3_t465617797  L_49;
		memset(&L_49, 0, sizeof(L_49));
		Vector3__ctor_m2638739322(&L_49, (((float)((float)L_47))), (((float)((float)((-L_48))))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_localScale_m2325460848(L_46, L_49, /*hidden argument*/NULL);
		goto IL_0209;
	}

IL_01e0:
	{
		GameObject_t1366199518 * L_50 = __this->get_backGround_7();
		NullCheck(L_50);
		Transform_t224878301 * L_51 = GameObject_get_transform_m909382139(L_50, /*hidden argument*/NULL);
		int32_t L_52 = __this->get_screen_width_13();
		int32_t L_53 = __this->get_screen_height_14();
		Vector3_t465617797  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Vector3__ctor_m2638739322(&L_54, (((float)((float)((-L_52))))), (((float)((float)L_53))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_set_localScale_m2325460848(L_51, L_54, /*hidden argument*/NULL);
	}

IL_0209:
	{
		GameObject_t1366199518 * L_55 = __this->get_backGround_7();
		NullCheck(L_55);
		Transform_t224878301 * L_56 = GameObject_get_transform_m909382139(L_55, /*hidden argument*/NULL);
		Vector3_t465617797  L_57;
		memset(&L_57, 0, sizeof(L_57));
		Vector3__ctor_m2638739322(&L_57, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t83606849  L_58 = Quaternion_Euler_m3586339259(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_localRotation_m2055111962(L_56, L_58, /*hidden argument*/NULL);
		Camera_t2805735124 * L_59 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_60 = __this->get_orthoSize1_20();
		NullCheck(L_59);
		Camera_set_orthographicSize_m2708824189(L_59, L_60, /*hidden argument*/NULL);
	}

IL_0242:
	{
		Vector3_t465617797  L_61 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t83606849  L_62 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t465617797  L_63;
		memset(&L_63, 0, sizeof(L_63));
		Vector3__ctor_m2638739322(&L_63, (1.0f), (-1.0f), (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t1261955742  L_64 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_61, L_62, L_63, /*hidden argument*/NULL);
		V_11 = L_64;
		Initobj (Matrix4x4_t1261955742_il2cpp_TypeInfo_var, (&V_12));
		Initobj (Quaternion_t83606849_il2cpp_TypeInfo_var, (&V_13));
		Vector3_t465617797  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Vector3__ctor_m2638739322(&L_65, (-90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_set_eulerAngles_m3695252804((&V_13), L_65, /*hidden argument*/NULL);
		Vector3_t465617797  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Vector3__ctor_m2638739322(&L_66, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Quaternion_t83606849  L_67 = V_13;
		Vector3_t465617797  L_68;
		memset(&L_68, 0, sizeof(L_68));
		Vector3__ctor_m2638739322(&L_68, (1.0f), (1.0f), (-1.0f), /*hidden argument*/NULL);
		Matrix4x4_t1261955742  L_69 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_66, L_67, L_68, /*hidden argument*/NULL);
		V_12 = L_69;
		Dictionary_2_t3496208296 * L_70 = __this->get_mMarkers_4();
		NullCheck(L_70);
		Enumerator_t793537021  L_71 = Dictionary_2_GetEnumerator_m4133290344(L_70, /*hidden argument*/Dictionary_2_GetEnumerator_m4133290344_MethodInfo_var);
		V_15 = L_71;
	}

IL_02d0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_044b;
		}

IL_02d5:
		{
			KeyValuePair_2_t2053528608  L_72 = Enumerator_get_Current_m1048306582((&V_15), /*hidden argument*/Enumerator_get_Current_m1048306582_MethodInfo_var);
			V_14 = L_72;
			V_16 = (bool)0;
			V_17 = 0;
			V_18 = 0;
			goto IL_0416;
		}

IL_02ec:
		{
			String_t* L_73 = KeyValuePair_2_get_Key_m3991964846((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m3991964846_MethodInfo_var);
			NullCheck(L_73);
			String_t* L_74 = String_Trim_m2668767713(L_73, /*hidden argument*/NULL);
			StringU5BU5D_t3764931161* L_75 = V_8;
			int32_t L_76 = V_18;
			NullCheck(L_75);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_75, L_76);
			int32_t L_77 = L_76;
			NullCheck(((L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77))));
			String_t* L_78 = String_Trim_m2668767713(((L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77))), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_79 = String_Equals_m3568148125(NULL /*static, unused*/, L_74, L_78, /*hidden argument*/NULL);
			V_19 = L_79;
			bool L_80 = V_19;
			if (!L_80)
			{
				goto IL_040a;
			}
		}

IL_0310:
		{
			V_16 = (bool)1;
			Initobj (Matrix4x4_t1261955742_il2cpp_TypeInfo_var, (&V_20));
			MarkerMap_t684226712 * L_81 = KeyValuePair_2_get_Value_m747115171((&V_14), /*hidden argument*/KeyValuePair_2_get_Value_m747115171_MethodInfo_var);
			NullCheck(L_81);
			GameObject_t1366199518 * L_82 = L_81->get_gameObject_0();
			bool L_83 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_82, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
			if (!L_83)
			{
				goto IL_0405;
			}
		}

IL_0332:
		{
			int32_t L_84 = V_17;
			DoubleU5BU5D_t2839599125* L_85 = __this->get_vmMatrixData_6();
			bool L_86 = VoidAR_getMatrix_m1862633888(__this, L_84, L_85, (&V_20), /*hidden argument*/NULL);
			if (!L_86)
			{
				goto IL_0405;
			}
		}

IL_0347:
		{
			Matrix4x4_t1261955742  L_87 = V_20;
			Vector3_t465617797  L_88 = MyUtils_GetPosition_m4264985309(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
			V_21 = L_88;
			int32_t L_89 = __this->get_cameraHeight_25();
			float L_90 = __this->get_screenH_22();
			V_22 = ((float)((float)(((float)((float)L_89)))/(float)L_90));
			Matrix4x4_t1261955742  L_91 = V_9;
			Matrix4x4_t1261955742  L_92 = V_11;
			Matrix4x4_t1261955742  L_93 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
			Matrix4x4_t1261955742  L_94 = V_20;
			Matrix4x4_t1261955742  L_95 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
			Matrix4x4_t1261955742  L_96 = V_12;
			Matrix4x4_t1261955742  L_97 = Matrix4x4_op_Multiply_m2352863493(NULL /*static, unused*/, L_95, L_96, /*hidden argument*/NULL);
			V_20 = L_97;
			Matrix4x4_t1261955742  L_98 = V_20;
			Vector3_t465617797  L_99 = MyUtils_GetPosition_m4264985309(NULL /*static, unused*/, L_98, /*hidden argument*/NULL);
			V_21 = L_99;
			int32_t L_100 = V_2;
			if (!L_100)
			{
				goto IL_038f;
			}
		}

IL_0388:
		{
			int32_t L_101 = V_2;
			if ((!(((uint32_t)L_101) == ((uint32_t)2))))
			{
				goto IL_03a3;
			}
		}

IL_038f:
		{
			Vector3_t465617797 * L_102 = (&V_21);
			float L_103 = L_102->get_z_3();
			float L_104 = __this->get_aspectRatio_23();
			L_102->set_z_3(((float)((float)L_103*(float)L_104)));
		}

IL_03a3:
		{
			Vector3_t465617797 * L_105 = (&V_21);
			float L_106 = L_105->get_z_3();
			float L_107 = V_22;
			L_105->set_z_3(((float)((float)L_106/(float)L_107)));
			MarkerMap_t684226712 * L_108 = KeyValuePair_2_get_Value_m747115171((&V_14), /*hidden argument*/KeyValuePair_2_get_Value_m747115171_MethodInfo_var);
			NullCheck(L_108);
			GameObject_t1366199518 * L_109 = L_108->get_gameObject_0();
			NullCheck(L_109);
			Transform_t224878301 * L_110 = GameObject_get_transform_m909382139(L_109, /*hidden argument*/NULL);
			Vector3_t465617797  L_111 = V_21;
			NullCheck(L_110);
			Transform_set_position_m2469242620(L_110, L_111, /*hidden argument*/NULL);
			MarkerMap_t684226712 * L_112 = KeyValuePair_2_get_Value_m747115171((&V_14), /*hidden argument*/KeyValuePair_2_get_Value_m747115171_MethodInfo_var);
			NullCheck(L_112);
			GameObject_t1366199518 * L_113 = L_112->get_gameObject_0();
			NullCheck(L_113);
			Transform_t224878301 * L_114 = GameObject_get_transform_m909382139(L_113, /*hidden argument*/NULL);
			Matrix4x4_t1261955742  L_115 = V_20;
			Quaternion_t83606849  L_116 = MyUtils_GetRotation_m2559326662(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
			NullCheck(L_114);
			Transform_set_rotation_m3411284563(L_114, L_116, /*hidden argument*/NULL);
			MarkerMap_t684226712 * L_117 = KeyValuePair_2_get_Value_m747115171((&V_14), /*hidden argument*/KeyValuePair_2_get_Value_m747115171_MethodInfo_var);
			NullCheck(L_117);
			GameObject_t1366199518 * L_118 = L_117->get_gameObject_0();
			NullCheck(L_118);
			Transform_t224878301 * L_119 = GameObject_get_transform_m909382139(L_118, /*hidden argument*/NULL);
			Matrix4x4_t1261955742  L_120 = V_20;
			Vector3_t465617797  L_121 = MyUtils_GetScale_m2960603332(NULL /*static, unused*/, L_120, /*hidden argument*/NULL);
			NullCheck(L_119);
			Transform_set_localScale_m2325460848(L_119, L_121, /*hidden argument*/NULL);
		}

IL_0405:
		{
			goto IL_0421;
		}

IL_040a:
		{
			int32_t L_122 = V_17;
			V_17 = ((int32_t)((int32_t)L_122+(int32_t)1));
			int32_t L_123 = V_18;
			V_18 = ((int32_t)((int32_t)L_123+(int32_t)1));
		}

IL_0416:
		{
			int32_t L_124 = V_18;
			StringU5BU5D_t3764931161* L_125 = V_8;
			NullCheck(L_125);
			if ((((int32_t)L_124) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_125)->max_length)))))))
			{
				goto IL_02ec;
			}
		}

IL_0421:
		{
			MarkerMap_t684226712 * L_126 = KeyValuePair_2_get_Value_m747115171((&V_14), /*hidden argument*/KeyValuePair_2_get_Value_m747115171_MethodInfo_var);
			NullCheck(L_126);
			Marker_t1240580442 * L_127 = L_126->get_marker_1();
			bool L_128 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_127, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
			if (!L_128)
			{
				goto IL_044b;
			}
		}

IL_0438:
		{
			MarkerMap_t684226712 * L_129 = KeyValuePair_2_get_Value_m747115171((&V_14), /*hidden argument*/KeyValuePair_2_get_Value_m747115171_MethodInfo_var);
			NullCheck(L_129);
			Marker_t1240580442 * L_130 = L_129->get_marker_1();
			bool L_131 = V_16;
			NullCheck(L_130);
			Marker_UpdateState_m1195485018(L_130, L_131, /*hidden argument*/NULL);
		}

IL_044b:
		{
			bool L_132 = Enumerator_MoveNext_m3306365281((&V_15), /*hidden argument*/Enumerator_MoveNext_m3306365281_MethodInfo_var);
			if (L_132)
			{
				goto IL_02d5;
			}
		}

IL_0457:
		{
			IL2CPP_LEAVE(0x469, FINALLY_045c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1145979430 *)e.ex;
		goto FINALLY_045c;
	}

FINALLY_045c:
	{ // begin finally (depth: 1)
		Enumerator_t793537021  L_133 = V_15;
		Enumerator_t793537021  L_134 = L_133;
		Il2CppObject * L_135 = Box(Enumerator_t793537021_il2cpp_TypeInfo_var, &L_134);
		NullCheck((Il2CppObject *)L_135);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1786818228_il2cpp_TypeInfo_var, (Il2CppObject *)L_135);
		IL2CPP_END_FINALLY(1116)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1116)
	{
		IL2CPP_JUMP_TBL(0x469, IL_0469)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1145979430 *)
	}

IL_0469:
	{
		return;
	}
}
// System.Int32 VoidAR::getCloudInfo(System.String&,System.String&,System.String&)
extern Il2CppClass* CharU5BU5D_t1685951112_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2125916575_il2cpp_TypeInfo_var;
extern const uint32_t VoidAR_getCloudInfo_m205723223_MetadataUsageId;
extern "C"  int32_t VoidAR_getCloudInfo_m205723223 (VoidAR_t451713137 * __this, String_t** ___url0, String_t** ___name1, String_t** ___matadata2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_getCloudInfo_m205723223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t1685951112* V_0 = NULL;
	ByteU5BU5D_t3835026402* V_1 = NULL;
	ByteU5BU5D_t3835026402* V_2 = NULL;
	ByteU5BU5D_t3835026402* V_3 = NULL;
	int32_t V_4 = 0;
	StringU5BU5D_t3764931161* V_5 = NULL;
	StringU5BU5D_t3764931161* V_6 = NULL;
	StringU5BU5D_t3764931161* V_7 = NULL;
	{
		V_0 = ((CharU5BU5D_t1685951112*)SZArrayNew(CharU5BU5D_t1685951112_il2cpp_TypeInfo_var, (uint32_t)1));
		V_1 = ((ByteU5BU5D_t3835026402*)SZArrayNew(ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var, (uint32_t)((int32_t)512)));
		V_2 = ((ByteU5BU5D_t3835026402*)SZArrayNew(ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var, (uint32_t)((int32_t)512)));
		V_3 = ((ByteU5BU5D_t3835026402*)SZArrayNew(ByteU5BU5D_t3835026402_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024)));
		V_4 = 0;
		String_t** L_0 = ___url0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_0)) = (Il2CppObject *)L_1;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_0), (Il2CppObject *)L_1);
		String_t** L_2 = ___name1;
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_2)) = (Il2CppObject *)L_3;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_2), (Il2CppObject *)L_3);
		String_t** L_4 = ___matadata2;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_4)) = (Il2CppObject *)L_5;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_4), (Il2CppObject *)L_5);
		ByteU5BU5D_t3835026402* L_6 = V_1;
		ByteU5BU5D_t3835026402* L_7 = V_2;
		ByteU5BU5D_t3835026402* L_8 = V_3;
		VoidAR__getNewCloudTarget_m2546856289(NULL /*static, unused*/, (&V_4), L_6, L_7, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_4;
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_00a9;
		}
	}
	{
		String_t** L_10 = ___url0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2125916575_il2cpp_TypeInfo_var);
		Encoding_t2125916575 * L_11 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3835026402* L_12 = V_1;
		NullCheck(L_11);
		String_t* L_13 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3835026402* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_11, L_12);
		*((Il2CppObject **)(L_10)) = (Il2CppObject *)L_13;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_10), (Il2CppObject *)L_13);
		String_t** L_14 = ___name1;
		Encoding_t2125916575 * L_15 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3835026402* L_16 = V_2;
		NullCheck(L_15);
		String_t* L_17 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3835026402* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_15, L_16);
		*((Il2CppObject **)(L_14)) = (Il2CppObject *)L_17;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_14), (Il2CppObject *)L_17);
		String_t** L_18 = ___matadata2;
		Encoding_t2125916575 * L_19 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3835026402* L_20 = V_3;
		NullCheck(L_19);
		String_t* L_21 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3835026402* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_19, L_20);
		*((Il2CppObject **)(L_18)) = (Il2CppObject *)L_21;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_18), (Il2CppObject *)L_21);
		String_t** L_22 = ___url0;
		CharU5BU5D_t1685951112* L_23 = V_0;
		NullCheck((*((String_t**)L_22)));
		StringU5BU5D_t3764931161* L_24 = String_Split_m3326265864((*((String_t**)L_22)), L_23, /*hidden argument*/NULL);
		V_5 = L_24;
		String_t** L_25 = ___name1;
		CharU5BU5D_t1685951112* L_26 = V_0;
		NullCheck((*((String_t**)L_25)));
		StringU5BU5D_t3764931161* L_27 = String_Split_m3326265864((*((String_t**)L_25)), L_26, /*hidden argument*/NULL);
		V_6 = L_27;
		String_t** L_28 = ___matadata2;
		CharU5BU5D_t1685951112* L_29 = V_0;
		NullCheck((*((String_t**)L_28)));
		StringU5BU5D_t3764931161* L_30 = String_Split_m3326265864((*((String_t**)L_28)), L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		String_t** L_31 = ___url0;
		StringU5BU5D_t3764931161* L_32 = V_5;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 0);
		int32_t L_33 = 0;
		*((Il2CppObject **)(L_31)) = (Il2CppObject *)((L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33)));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_31), (Il2CppObject *)((L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_33))));
		String_t** L_34 = ___name1;
		StringU5BU5D_t3764931161* L_35 = V_6;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 0);
		int32_t L_36 = 0;
		*((Il2CppObject **)(L_34)) = (Il2CppObject *)((L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_36)));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_34), (Il2CppObject *)((L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_36))));
		String_t** L_37 = ___matadata2;
		StringU5BU5D_t3764931161* L_38 = V_7;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		int32_t L_39 = 0;
		*((Il2CppObject **)(L_37)) = (Il2CppObject *)((L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39)));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_37), (Il2CppObject *)((L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_39))));
	}

IL_00a9:
	{
		int32_t L_40 = V_4;
		return L_40;
	}
}
// System.Void VoidAR::setCloudUser(System.String,System.String,System.Boolean)
extern "C"  void VoidAR_setCloudUser_m1766345473 (VoidAR_t451713137 * __this, String_t* ___accessKey0, String_t* ___sercetKey1, bool ___useCloud2, const MethodInfo* method)
{
	{
		bool L_0 = ___useCloud2;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_1 = ___accessKey0;
		String_t* L_2 = ___sercetKey1;
		VoidAR__setCloudUser_m456182358(NULL /*static, unused*/, L_1, L_2, 1, /*hidden argument*/NULL);
		goto IL_001b;
	}

IL_0013:
	{
		String_t* L_3 = ___accessKey0;
		String_t* L_4 = ___sercetKey1;
		VoidAR__setCloudUser_m456182358(NULL /*static, unused*/, L_3, L_4, 0, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void VoidAR::useExentsionTracking(System.Boolean)
extern "C"  void VoidAR_useExentsionTracking_m1015617692 (VoidAR_t451713137 * __this, bool ___use0, const MethodInfo* method)
{
	{
		bool L_0 = ___use0;
		VoidAR__useExtensionTracking_m2889953485(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::addTarget(VoidAR/Image2ImageTarget)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppClass* MarkerMap_t684226712_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral305759120;
extern const uint32_t VoidAR_addTarget_m1335584111_MetadataUsageId;
extern "C"  void VoidAR_addTarget_m1335584111 (VoidAR_t451713137 * __this, Image2ImageTarget_t791379109 * ___image2ImageTarget0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_addTarget_m1335584111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Marker_t1240580442 * V_0 = NULL;
	MarkerMap_t684226712 * V_1 = NULL;
	{
		Image2ImageTarget_t791379109 * L_0 = ___image2ImageTarget0;
		NullCheck(L_0);
		GameObject_t1366199518 * L_1 = L_0->get_ImageTarget_2();
		NullCheck(L_1);
		Marker_t1240580442 * L_2 = GameObject_GetComponent_TisMarker_t1240580442_m452303857(L_1, /*hidden argument*/GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var);
		V_0 = L_2;
		Marker_t1240580442 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_009a;
		}
	}
	{
		Dictionary_2_t3496208296 * L_5 = __this->get_mMarkers_4();
		Image2ImageTarget_t791379109 * L_6 = ___image2ImageTarget0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_imageUrl_1();
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::ContainsKey(!0) */, L_5, L_7);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		Image2ImageTarget_t791379109 * L_9 = ___image2ImageTarget0;
		NullCheck(L_9);
		String_t* L_10 = L_9->get_imageUrl_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral305759120, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0047:
	{
		Image2ImageTarget_t791379109 * L_12 = ___image2ImageTarget0;
		NullCheck(L_12);
		ByteU5BU5D_t3835026402* L_13 = L_12->get_imagedata_3();
		NullCheck(L_13);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_009a;
		}
	}
	{
		Image2ImageTarget_t791379109 * L_14 = ___image2ImageTarget0;
		NullCheck(L_14);
		String_t* L_15 = L_14->get_imageUrl_1();
		Image2ImageTarget_t791379109 * L_16 = ___image2ImageTarget0;
		NullCheck(L_16);
		ByteU5BU5D_t3835026402* L_17 = L_16->get_imagedata_3();
		Image2ImageTarget_t791379109 * L_18 = ___image2ImageTarget0;
		NullCheck(L_18);
		ByteU5BU5D_t3835026402* L_19 = L_18->get_imagedata_3();
		NullCheck(L_19);
		VoidAR__addTarget_m1219800199(NULL /*static, unused*/, L_15, L_17, (((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))), /*hidden argument*/NULL);
		MarkerMap_t684226712 * L_20 = (MarkerMap_t684226712 *)il2cpp_codegen_object_new(MarkerMap_t684226712_il2cpp_TypeInfo_var);
		MarkerMap__ctor_m3444299521(L_20, /*hidden argument*/NULL);
		V_1 = L_20;
		MarkerMap_t684226712 * L_21 = V_1;
		Image2ImageTarget_t791379109 * L_22 = ___image2ImageTarget0;
		NullCheck(L_22);
		GameObject_t1366199518 * L_23 = L_22->get_ImageTarget_2();
		NullCheck(L_21);
		L_21->set_gameObject_0(L_23);
		MarkerMap_t684226712 * L_24 = V_1;
		Marker_t1240580442 * L_25 = V_0;
		NullCheck(L_24);
		L_24->set_marker_1(L_25);
		Dictionary_2_t3496208296 * L_26 = __this->get_mMarkers_4();
		Image2ImageTarget_t791379109 * L_27 = ___image2ImageTarget0;
		NullCheck(L_27);
		String_t* L_28 = L_27->get_imageUrl_1();
		MarkerMap_t684226712 * L_29 = V_1;
		NullCheck(L_26);
		VirtActionInvoker2< String_t*, MarkerMap_t684226712 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::Add(!0,!1) */, L_26, L_28, L_29);
	}

IL_009a:
	{
		return;
	}
}
// System.Void VoidAR::addTargets(System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>)
extern Il2CppClass* Enumerator_t163748572_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1786818228_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m951101811_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4042599567_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2807011299_MethodInfo_var;
extern const uint32_t VoidAR_addTargets_m1495199696_MetadataUsageId;
extern "C"  void VoidAR_addTargets_m1495199696 (VoidAR_t451713137 * __this, List_1_t4197717748 * ___image2ImageTarget0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidAR_addTargets_m1495199696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Image2ImageTarget_t791379109 * V_0 = NULL;
	Enumerator_t163748572  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1145979430 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1145979430 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t4197717748 * L_0 = ___image2ImageTarget0;
		NullCheck(L_0);
		Enumerator_t163748572  L_1 = List_1_GetEnumerator_m951101811(L_0, /*hidden argument*/List_1_GetEnumerator_m951101811_MethodInfo_var);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001b;
		}

IL_000c:
		{
			Image2ImageTarget_t791379109 * L_2 = Enumerator_get_Current_m4042599567((&V_1), /*hidden argument*/Enumerator_get_Current_m4042599567_MethodInfo_var);
			V_0 = L_2;
			Image2ImageTarget_t791379109 * L_3 = V_0;
			VoidAR_addTarget_m1335584111(__this, L_3, /*hidden argument*/NULL);
		}

IL_001b:
		{
			bool L_4 = Enumerator_MoveNext_m2807011299((&V_1), /*hidden argument*/Enumerator_MoveNext_m2807011299_MethodInfo_var);
			if (L_4)
			{
				goto IL_000c;
			}
		}

IL_0027:
		{
			IL2CPP_LEAVE(0x38, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1145979430 *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Enumerator_t163748572  L_5 = V_1;
		Enumerator_t163748572  L_6 = L_5;
		Il2CppObject * L_7 = Box(Enumerator_t163748572_il2cpp_TypeInfo_var, &L_6);
		NullCheck((Il2CppObject *)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1786818228_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1145979430 *)
	}

IL_0038:
	{
		VoidAR__finishAddTarget_m3800497644(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR::removeTarget(System.String)
extern "C"  void VoidAR_removeTarget_m2605434269 (VoidAR_t451713137 * __this, String_t* ___targetName0, const MethodInfo* method)
{
	{
		Dictionary_2_t3496208296 * L_0 = __this->get_mMarkers_4();
		String_t* L_1 = ___targetName0;
		NullCheck(L_0);
		MarkerMap_t684226712 * L_2 = VirtFuncInvoker1< MarkerMap_t684226712 *, String_t* >::Invoke(24 /* !1 System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::get_Item(!0) */, L_0, L_1);
		NullCheck(L_2);
		GameObject_t1366199518 * L_3 = L_2->get_gameObject_0();
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Dictionary_2_t3496208296 * L_4 = __this->get_mMarkers_4();
		String_t* L_5 = ___targetName0;
		NullCheck(L_4);
		VirtFuncInvoker1< bool, String_t* >::Invoke(30 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,VoidAR/MarkerMap>::Remove(!0) */, L_4, L_5);
		String_t* L_6 = ___targetName0;
		VoidAR__removeImageTarget_m3284403673(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: VoidAR/ExternalTextureDesc
extern "C" void ExternalTextureDesc_t3964848077_marshal_pinvoke(const ExternalTextureDesc_t3964848077& unmarshaled, ExternalTextureDesc_t3964848077_marshaled_pinvoke& marshaled)
{
	marshaled.___tex1_0 = reinterpret_cast<intptr_t>((unmarshaled.get_tex1_0()).get_m_value_0());
	marshaled.___tex2_1 = reinterpret_cast<intptr_t>((unmarshaled.get_tex2_1()).get_m_value_0());
	marshaled.___width_2 = unmarshaled.get_width_2();
	marshaled.___height_3 = unmarshaled.get_height_3();
}
extern "C" void ExternalTextureDesc_t3964848077_marshal_pinvoke_back(const ExternalTextureDesc_t3964848077_marshaled_pinvoke& marshaled, ExternalTextureDesc_t3964848077& unmarshaled)
{
	IntPtr_t unmarshaled_tex1_temp;
	memset(&unmarshaled_tex1_temp, 0, sizeof(unmarshaled_tex1_temp));
	IntPtr_t unmarshaled_tex1_temp_temp;
	unmarshaled_tex1_temp_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___tex1_0));
	unmarshaled_tex1_temp = unmarshaled_tex1_temp_temp;
	unmarshaled.set_tex1_0(unmarshaled_tex1_temp);
	IntPtr_t unmarshaled_tex2_temp;
	memset(&unmarshaled_tex2_temp, 0, sizeof(unmarshaled_tex2_temp));
	IntPtr_t unmarshaled_tex2_temp_temp;
	unmarshaled_tex2_temp_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___tex2_1));
	unmarshaled_tex2_temp = unmarshaled_tex2_temp_temp;
	unmarshaled.set_tex2_1(unmarshaled_tex2_temp);
	int32_t unmarshaled_width_temp = 0;
	unmarshaled_width_temp = marshaled.___width_2;
	unmarshaled.set_width_2(unmarshaled_width_temp);
	int32_t unmarshaled_height_temp = 0;
	unmarshaled_height_temp = marshaled.___height_3;
	unmarshaled.set_height_3(unmarshaled_height_temp);
}
// Conversion method for clean up from marshalling of: VoidAR/ExternalTextureDesc
extern "C" void ExternalTextureDesc_t3964848077_marshal_pinvoke_cleanup(ExternalTextureDesc_t3964848077_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: VoidAR/ExternalTextureDesc
extern "C" void ExternalTextureDesc_t3964848077_marshal_com(const ExternalTextureDesc_t3964848077& unmarshaled, ExternalTextureDesc_t3964848077_marshaled_com& marshaled)
{
	marshaled.___tex1_0 = reinterpret_cast<intptr_t>((unmarshaled.get_tex1_0()).get_m_value_0());
	marshaled.___tex2_1 = reinterpret_cast<intptr_t>((unmarshaled.get_tex2_1()).get_m_value_0());
	marshaled.___width_2 = unmarshaled.get_width_2();
	marshaled.___height_3 = unmarshaled.get_height_3();
}
extern "C" void ExternalTextureDesc_t3964848077_marshal_com_back(const ExternalTextureDesc_t3964848077_marshaled_com& marshaled, ExternalTextureDesc_t3964848077& unmarshaled)
{
	IntPtr_t unmarshaled_tex1_temp;
	memset(&unmarshaled_tex1_temp, 0, sizeof(unmarshaled_tex1_temp));
	IntPtr_t unmarshaled_tex1_temp_temp;
	unmarshaled_tex1_temp_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___tex1_0));
	unmarshaled_tex1_temp = unmarshaled_tex1_temp_temp;
	unmarshaled.set_tex1_0(unmarshaled_tex1_temp);
	IntPtr_t unmarshaled_tex2_temp;
	memset(&unmarshaled_tex2_temp, 0, sizeof(unmarshaled_tex2_temp));
	IntPtr_t unmarshaled_tex2_temp_temp;
	unmarshaled_tex2_temp_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___tex2_1));
	unmarshaled_tex2_temp = unmarshaled_tex2_temp_temp;
	unmarshaled.set_tex2_1(unmarshaled_tex2_temp);
	int32_t unmarshaled_width_temp = 0;
	unmarshaled_width_temp = marshaled.___width_2;
	unmarshaled.set_width_2(unmarshaled_width_temp);
	int32_t unmarshaled_height_temp = 0;
	unmarshaled_height_temp = marshaled.___height_3;
	unmarshaled.set_height_3(unmarshaled_height_temp);
}
// Conversion method for clean up from marshalling of: VoidAR/ExternalTextureDesc
extern "C" void ExternalTextureDesc_t3964848077_marshal_com_cleanup(ExternalTextureDesc_t3964848077_marshaled_com& marshaled)
{
}
// System.Void VoidAR/Image2ImageTarget::.ctor()
extern "C"  void Image2ImageTarget__ctor_m3111905800 (Image2ImageTarget_t791379109 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR/MarkerMap::.ctor()
extern "C"  void MarkerMap__ctor_m3444299521 (MarkerMap_t684226712 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidAR/MatchingResultDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void MatchingResultDelegate__ctor_m1322320478 (MatchingResultDelegate_t688824355 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void VoidAR/MatchingResultDelegate::Invoke(System.Int32)
extern "C"  void MatchingResultDelegate_Invoke_m2642080543 (MatchingResultDelegate_t688824355 * __this, int32_t ___result0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MatchingResultDelegate_Invoke_m2642080543((MatchingResultDelegate_t688824355 *)__this->get_prev_9(),___result0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___result0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___result0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_MatchingResultDelegate_t688824355(Il2CppObject* delegate, int32_t ___result0)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___result0' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___result0);

	// Marshaling cleanup of parameter '___result0' native representation

}
// System.IAsyncResult VoidAR/MatchingResultDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t1448170597_il2cpp_TypeInfo_var;
extern const uint32_t MatchingResultDelegate_BeginInvoke_m2914422188_MetadataUsageId;
extern "C"  Il2CppObject * MatchingResultDelegate_BeginInvoke_m2914422188 (MatchingResultDelegate_t688824355 * __this, int32_t ___result0, AsyncCallback_t889871978 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MatchingResultDelegate_BeginInvoke_m2914422188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &___result0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void VoidAR/MatchingResultDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void MatchingResultDelegate_EndInvoke_m3991656732 (MatchingResultDelegate_t688824355 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void VoidAR/MyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void MyDelegate__ctor_m2463674910 (MyDelegate_t3224270133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void VoidAR/MyDelegate::Invoke(System.String)
extern "C"  void MyDelegate_Invoke_m2465817758 (MyDelegate_t3224270133 * __this, String_t* ___str0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		MyDelegate_Invoke_m2465817758((MyDelegate_t3224270133 *)__this->get_prev_9(),___str0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___str0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___str0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___str0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___str0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___str0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_MyDelegate_t3224270133(Il2CppObject* delegate, String_t* ___str0)
{
	typedef void (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___str0' to native representation
	char* ____str0_marshaled = NULL;
	____str0_marshaled = il2cpp_codegen_marshal_string(___str0);

	// Native function invocation
	_il2cpp_pinvoke_func(____str0_marshaled);

	// Marshaling cleanup of parameter '___str0' native representation
	il2cpp_codegen_marshal_free(____str0_marshaled);
	____str0_marshaled = NULL;

}
// System.IAsyncResult VoidAR/MyDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MyDelegate_BeginInvoke_m1728350909 (MyDelegate_t3224270133 * __this, String_t* ___str0, AsyncCallback_t889871978 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___str0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void VoidAR/MyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void MyDelegate_EndInvoke_m2377538200 (MyDelegate_t3224270133 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: VoidAR/PowerAREvent
extern "C" void PowerAREvent_t1377968902_marshal_pinvoke(const PowerAREvent_t1377968902& unmarshaled, PowerAREvent_t1377968902_marshaled_pinvoke& marshaled)
{
	marshaled.___TargetID_0 = unmarshaled.get_TargetID_0();
}
extern "C" void PowerAREvent_t1377968902_marshal_pinvoke_back(const PowerAREvent_t1377968902_marshaled_pinvoke& marshaled, PowerAREvent_t1377968902& unmarshaled)
{
	int32_t unmarshaled_TargetID_temp = 0;
	unmarshaled_TargetID_temp = marshaled.___TargetID_0;
	unmarshaled.set_TargetID_0(unmarshaled_TargetID_temp);
}
// Conversion method for clean up from marshalling of: VoidAR/PowerAREvent
extern "C" void PowerAREvent_t1377968902_marshal_pinvoke_cleanup(PowerAREvent_t1377968902_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: VoidAR/PowerAREvent
extern "C" void PowerAREvent_t1377968902_marshal_com(const PowerAREvent_t1377968902& unmarshaled, PowerAREvent_t1377968902_marshaled_com& marshaled)
{
	marshaled.___TargetID_0 = unmarshaled.get_TargetID_0();
}
extern "C" void PowerAREvent_t1377968902_marshal_com_back(const PowerAREvent_t1377968902_marshaled_com& marshaled, PowerAREvent_t1377968902& unmarshaled)
{
	int32_t unmarshaled_TargetID_temp = 0;
	unmarshaled_TargetID_temp = marshaled.___TargetID_0;
	unmarshaled.set_TargetID_0(unmarshaled_TargetID_temp);
}
// Conversion method for clean up from marshalling of: VoidAR/PowerAREvent
extern "C" void PowerAREvent_t1377968902_marshal_com_cleanup(PowerAREvent_t1377968902_marshaled_com& marshaled)
{
}
// System.Void VoidARMain::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t VoidARMain__ctor_m560636343_MetadataUsageId;
extern "C"  void VoidARMain__ctor_m560636343 (VoidARMain_t3733354248 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidARMain__ctor_m560636343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_shapeMatchAccuracy_4(5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_accessKey_6(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_secretKey_7(L_1);
		__this->set_tracking_9((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidARMain::Awake()
extern "C"  void VoidARMain_Awake_m2447810196 (VoidARMain_t3733354248 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoidARMain::Start()
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3477385581;
extern Il2CppCodeGenString* _stringLiteral698721667;
extern const uint32_t VoidARMain_Start_m1636554803_MetadataUsageId;
extern "C"  void VoidARMain_Start_m1636554803 (VoidARMain_t3733354248 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidARMain_Start_m1636554803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		VoidAR_t451713137 * L_0 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_CameraIndex_10();
		int32_t L_2 = __this->get_markerType_3();
		bool L_3 = __this->get_UseExtensionTracking_8();
		NullCheck(L_0);
		VoidAR_init_m3437805449(L_0, L_1, ((int32_t)((int32_t)L_2+(int32_t)1)), L_3, /*hidden argument*/NULL);
		Il2CppObject * L_4 = VoidARMain_AddTargets_m3043644900(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_markerType_3();
		if (L_5)
		{
			goto IL_0046;
		}
	}
	{
		VoidAR_t451713137 * L_6 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_shapeMatchAccuracy_4();
		NullCheck(L_6);
		VoidAR_setShapeMatchLevel_m764983593(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0046:
	{
		VoidAR_t451713137 * L_8 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		VoidAR_match_m3342905039(L_8, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_markerType_3();
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_00c6;
		}
	}
	{
		bool L_10 = __this->get_is_use_cloud_5();
		if (!L_10)
		{
			goto IL_00c6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3477385581, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_accessKey_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_11);
		bool L_13 = String_Equals_m2633592423(L_11, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_14 = __this->get_secretKey_7();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_14);
		bool L_16 = String_Equals_m2633592423(L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00aa;
		}
	}

IL_009b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral698721667, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00aa:
	{
		VoidAR_t451713137 * L_17 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_18 = __this->get_accessKey_6();
		String_t* L_19 = __this->get_secretKey_7();
		bool L_20 = __this->get_is_use_cloud_5();
		NullCheck(L_17);
		VoidAR_setCloudUser_m1766345473(L_17, L_18, L_19, L_20, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void VoidARMain::Update()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1448170597_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2780673938;
extern Il2CppCodeGenString* _stringLiteral1363759788;
extern Il2CppCodeGenString* _stringLiteral1258045531;
extern Il2CppCodeGenString* _stringLiteral2494949592;
extern Il2CppCodeGenString* _stringLiteral2536488540;
extern Il2CppCodeGenString* _stringLiteral2514798312;
extern const uint32_t VoidARMain_Update_m3397719590_MetadataUsageId;
extern "C"  void VoidARMain_Update_m3397719590 (VoidARMain_t3733354248 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidARMain_Update_m3397719590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	{
		VoidAR_t451713137 * L_0 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t2805735124 * L_1 = __this->get_ARCamera_2();
		NullCheck(L_0);
		VoidAR_update_m1138037781(L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_is_use_cloud_5();
		if (!L_2)
		{
			goto IL_011a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_3;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_1 = L_4;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_5;
		VoidAR_t451713137 * L_6 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = VoidAR_getCloudInfo_m205723223(L_6, (&V_0), (&V_1), (&V_2), /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_3;
		if ((((int32_t)L_8) <= ((int32_t)(-1))))
		{
			goto IL_006c;
		}
	}
	{
		String_t* L_9 = V_0;
		String_t* L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m612901809(NULL /*static, unused*/, L_9, _stringLiteral2780673938, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_3;
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &L_13);
		String_t* L_15 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1363759788, L_14, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_006c:
	{
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_18 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00d0;
		}
	}
	{
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_19);
		bool L_21 = String_Equals_m2633592423(L_19, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00d0;
		}
	}
	{
		String_t* L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_24 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00d0;
		}
	}
	{
		String_t* L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_25);
		bool L_27 = String_Equals_m2633592423(L_25, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00d0;
		}
	}
	{
		String_t* L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1258045531, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		String_t* L_30 = V_0;
		String_t* L_31 = V_1;
		Il2CppObject * L_32 = VoidARMain_LoadMainGameObject_m2131435887(__this, L_30, L_31, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_32, /*hidden argument*/NULL);
		goto IL_011a;
	}

IL_00d0:
	{
		String_t* L_33 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_35 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_011a;
		}
	}
	{
		String_t* L_36 = Application_get_streamingAssetsPath_m8890645(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2494949592, L_36, _stringLiteral2536488540, /*hidden argument*/NULL);
		V_0 = L_37;
		String_t* L_38 = Application_get_streamingAssetsPath_m8890645(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_39 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2494949592, L_38, _stringLiteral2514798312, /*hidden argument*/NULL);
		V_0 = L_39;
		String_t* L_40 = V_0;
		String_t* L_41 = V_1;
		String_t* L_42 = V_2;
		Il2CppObject * L_43 = VoidARMain_LoadMainGameObjectTest_m4094532099(__this, L_40, L_41, L_42, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_43, /*hidden argument*/NULL);
	}

IL_011a:
	{
		return;
	}
}
// System.Void VoidARMain::OnDestroy()
extern "C"  void VoidARMain_OnDestroy_m2149002830 (VoidARMain_t3733354248 * __this, const MethodInfo* method)
{
	{
		VoidAR_t451713137 * L_0 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VoidAR_unloadResource_m606358455(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoidARMain::OnApplicationPause(System.Boolean)
extern "C"  void VoidARMain_OnApplicationPause_m3652861797 (VoidARMain_t3733354248 * __this, bool ___pause0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = ___pause0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		VoidAR_t451713137 * L_1 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		VoidAR_stopCapture_m1505908938(L_1, /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_0015:
	{
		V_0 = 0;
		VoidAR_t451713137 * L_2 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_CameraIndex_10();
		NullCheck(L_2);
		VoidAR_startCapture_m1392394096(L_2, L_3, (&V_0), /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void VoidARMain::OnApplicationQuit()
extern "C"  void VoidARMain_OnApplicationQuit_m3047144225 (VoidARMain_t3733354248 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator VoidARMain::LoadMainGameObject(System.String,System.String)
extern Il2CppClass* U3CLoadMainGameObjectU3Ec__Iterator1_t888947034_il2cpp_TypeInfo_var;
extern const uint32_t VoidARMain_LoadMainGameObject_m2131435887_MetadataUsageId;
extern "C"  Il2CppObject * VoidARMain_LoadMainGameObject_m2131435887 (VoidARMain_t3733354248 * __this, String_t* ___path0, String_t* ___name1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidARMain_LoadMainGameObject_m2131435887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * V_0 = NULL;
	{
		U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * L_0 = (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 *)il2cpp_codegen_object_new(U3CLoadMainGameObjectU3Ec__Iterator1_t888947034_il2cpp_TypeInfo_var);
		U3CLoadMainGameObjectU3Ec__Iterator1__ctor_m3933975662(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * L_1 = V_0;
		String_t* L_2 = ___path0;
		NullCheck(L_1);
		L_1->set_path_0(L_2);
		U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * L_3 = V_0;
		String_t* L_4 = ___name1;
		NullCheck(L_3);
		L_3->set_name_2(L_4);
		U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * L_5 = V_0;
		String_t* L_6 = ___path0;
		NullCheck(L_5);
		L_5->set_U3CU24U3Epath_7(L_6);
		U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * L_7 = V_0;
		String_t* L_8 = ___name1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Ename_8(L_8);
		U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * L_9 = V_0;
		return L_9;
	}
}
// System.Collections.IEnumerator VoidARMain::LoadMainGameObjectTest(System.String,System.String,System.String)
extern Il2CppClass* U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831_il2cpp_TypeInfo_var;
extern const uint32_t VoidARMain_LoadMainGameObjectTest_m4094532099_MetadataUsageId;
extern "C"  Il2CppObject * VoidARMain_LoadMainGameObjectTest_m4094532099 (VoidARMain_t3733354248 * __this, String_t* ___path0, String_t* ___name1, String_t* ___videoInfoStr2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidARMain_LoadMainGameObjectTest_m4094532099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * V_0 = NULL;
	{
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_0 = (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 *)il2cpp_codegen_object_new(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831_il2cpp_TypeInfo_var);
		U3CLoadMainGameObjectTestU3Ec__Iterator2__ctor_m664999829(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_1 = V_0;
		String_t* L_2 = ___path0;
		NullCheck(L_1);
		L_1->set_path_0(L_2);
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_3 = V_0;
		String_t* L_4 = ___name1;
		NullCheck(L_3);
		L_3->set_name_2(L_4);
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_5 = V_0;
		String_t* L_6 = ___videoInfoStr2;
		NullCheck(L_5);
		L_5->set_videoInfoStr_8(L_6);
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_7 = V_0;
		String_t* L_8 = ___path0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Epath_15(L_8);
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_9 = V_0;
		String_t* L_10 = ___name1;
		NullCheck(L_9);
		L_9->set_U3CU24U3Ename_16(L_10);
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_11 = V_0;
		String_t* L_12 = ___videoInfoStr2;
		NullCheck(L_11);
		L_11->set_U3CU24U3EvideoInfoStr_17(L_12);
		U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * L_13 = V_0;
		return L_13;
	}
}
// System.Collections.IEnumerator VoidARMain::AddTargets()
extern Il2CppClass* U3CAddTargetsU3Ec__Iterator3_t660142821_il2cpp_TypeInfo_var;
extern const uint32_t VoidARMain_AddTargets_m3043644900_MetadataUsageId;
extern "C"  Il2CppObject * VoidARMain_AddTargets_m3043644900 (VoidARMain_t3733354248 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VoidARMain_AddTargets_m3043644900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAddTargetsU3Ec__Iterator3_t660142821 * V_0 = NULL;
	{
		U3CAddTargetsU3Ec__Iterator3_t660142821 * L_0 = (U3CAddTargetsU3Ec__Iterator3_t660142821 *)il2cpp_codegen_object_new(U3CAddTargetsU3Ec__Iterator3_t660142821_il2cpp_TypeInfo_var);
		U3CAddTargetsU3Ec__Iterator3__ctor_m4182318543(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAddTargetsU3Ec__Iterator3_t660142821 * L_1 = V_0;
		return L_1;
	}
}
// System.Void VoidARMain/<AddTargets>c__Iterator3::.ctor()
extern "C"  void U3CAddTargetsU3Ec__Iterator3__ctor_m4182318543 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoidARMain/<AddTargets>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAddTargetsU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2910634357 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object VoidARMain/<AddTargets>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAddTargetsU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3680384733 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean VoidARMain/<AddTargets>c__Iterator3::MoveNext()
extern const Il2CppType* GameObject_t1366199518_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU5D_t2005073387_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4197717748_il2cpp_TypeInfo_var;
extern Il2CppClass* Image2ImageTarget_t791379109_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t3146501818_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3529032890_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2494949592;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral2765707069;
extern const uint32_t U3CAddTargetsU3Ec__Iterator3_MoveNext_m2505530933_MetadataUsageId;
extern "C"  bool U3CAddTargetsU3Ec__Iterator3_MoveNext_m2505530933 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAddTargetsU3Ec__Iterator3_MoveNext_m2505530933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0102;
		}
	}
	{
		goto IL_0190;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(GameObject_t1366199518_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t2631032261* L_3 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_U3CgameObjectsU3E__0_0(((GameObjectU5BU5D_t2005073387*)IsInst(L_3, GameObjectU5BU5D_t2005073387_il2cpp_TypeInfo_var)));
		List_1_t4197717748 * L_4 = (List_1_t4197717748 *)il2cpp_codegen_object_new(List_1_t4197717748_il2cpp_TypeInfo_var);
		List_1__ctor_m3529032890(L_4, /*hidden argument*/List_1__ctor_m3529032890_MethodInfo_var);
		__this->set_U3Cimage2ImageTargetesU3E__1_1(L_4);
		__this->set_U3CiU3E__2_2(0);
		goto IL_0166;
	}

IL_0052:
	{
		GameObjectU5BU5D_t2005073387* L_5 = __this->get_U3CgameObjectsU3E__0_0();
		int32_t L_6 = __this->get_U3CiU3E__2_2();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		Marker_t1240580442 * L_8 = GameObject_GetComponent_TisMarker_t1240580442_m452303857(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), /*hidden argument*/GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var);
		__this->set_U3CmarkerU3E__3_3(L_8);
		Marker_t1240580442 * L_9 = __this->get_U3CmarkerU3E__3_3();
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0158;
		}
	}
	{
		Image2ImageTarget_t791379109 * L_11 = (Image2ImageTarget_t791379109 *)il2cpp_codegen_object_new(Image2ImageTarget_t791379109_il2cpp_TypeInfo_var);
		Image2ImageTarget__ctor_m3111905800(L_11, /*hidden argument*/NULL);
		__this->set_U3CobjU3E__4_4(L_11);
		Image2ImageTarget_t791379109 * L_12 = __this->get_U3CobjU3E__4_4();
		NullCheck(L_12);
		L_12->set_isMarkerLocal_0((bool)1);
		Image2ImageTarget_t791379109 * L_13 = __this->get_U3CobjU3E__4_4();
		GameObjectU5BU5D_t2005073387* L_14 = __this->get_U3CgameObjectsU3E__0_0();
		int32_t L_15 = __this->get_U3CiU3E__2_2();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck(L_13);
		L_13->set_ImageTarget_2(((L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16))));
		Image2ImageTarget_t791379109 * L_17 = __this->get_U3CobjU3E__4_4();
		String_t* L_18 = Application_get_streamingAssetsPath_m8890645(NULL /*static, unused*/, /*hidden argument*/NULL);
		Marker_t1240580442 * L_19 = __this->get_U3CmarkerU3E__3_3();
		NullCheck(L_19);
		String_t* L_20 = L_19->get_imageFilePath_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral2494949592, L_18, _stringLiteral372029315, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_imageUrl_1(L_21);
		Image2ImageTarget_t791379109 * L_22 = __this->get_U3CobjU3E__4_4();
		NullCheck(L_22);
		String_t* L_23 = L_22->get_imageUrl_1();
		WWW_t3146501818 * L_24 = (WWW_t3146501818 *)il2cpp_codegen_object_new(WWW_t3146501818_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_24, L_23, /*hidden argument*/NULL);
		__this->set_U3CfileU3E__5_5(L_24);
		WWW_t3146501818 * L_25 = __this->get_U3CfileU3E__5_5();
		__this->set_U24current_7(L_25);
		__this->set_U24PC_6(1);
		goto IL_0192;
	}

IL_0102:
	{
		WWW_t3146501818 * L_26 = __this->get_U3CfileU3E__5_5();
		NullCheck(L_26);
		String_t* L_27 = WWW_get_error_m3092701216(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0131;
		}
	}
	{
		Image2ImageTarget_t791379109 * L_28 = __this->get_U3CobjU3E__4_4();
		NullCheck(L_28);
		String_t* L_29 = L_28->get_imageUrl_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m2596409543(NULL /*static, unused*/, L_29, _stringLiteral2765707069, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		goto IL_0158;
	}

IL_0131:
	{
		Image2ImageTarget_t791379109 * L_31 = __this->get_U3CobjU3E__4_4();
		WWW_t3146501818 * L_32 = __this->get_U3CfileU3E__5_5();
		NullCheck(L_32);
		ByteU5BU5D_t3835026402* L_33 = WWW_get_bytes_m420718112(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_imagedata_3(L_33);
		List_1_t4197717748 * L_34 = __this->get_U3Cimage2ImageTargetesU3E__1_1();
		Image2ImageTarget_t791379109 * L_35 = __this->get_U3CobjU3E__4_4();
		NullCheck(L_34);
		VirtActionInvoker1< Image2ImageTarget_t791379109 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Add(!0) */, L_34, L_35);
	}

IL_0158:
	{
		int32_t L_36 = __this->get_U3CiU3E__2_2();
		__this->set_U3CiU3E__2_2(((int32_t)((int32_t)L_36+(int32_t)1)));
	}

IL_0166:
	{
		int32_t L_37 = __this->get_U3CiU3E__2_2();
		GameObjectU5BU5D_t2005073387* L_38 = __this->get_U3CgameObjectsU3E__0_0();
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		VoidAR_t451713137 * L_39 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t4197717748 * L_40 = __this->get_U3Cimage2ImageTargetesU3E__1_1();
		NullCheck(L_39);
		VoidAR_addTargets_m1495199696(L_39, L_40, /*hidden argument*/NULL);
		__this->set_U24PC_6((-1));
	}

IL_0190:
	{
		return (bool)0;
	}

IL_0192:
	{
		return (bool)1;
	}
	// Dead block : IL_0194: ldloc.1
}
// System.Void VoidARMain/<AddTargets>c__Iterator3::Dispose()
extern "C"  void U3CAddTargetsU3Ec__Iterator3_Dispose_m964754966 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void VoidARMain/<AddTargets>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CAddTargetsU3Ec__Iterator3_Reset_m2343917316_MetadataUsageId;
extern "C"  void U3CAddTargetsU3Ec__Iterator3_Reset_m2343917316 (U3CAddTargetsU3Ec__Iterator3_t660142821 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAddTargetsU3Ec__Iterator3_Reset_m2343917316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VoidARMain/<LoadMainGameObject>c__Iterator1::.ctor()
extern "C"  void U3CLoadMainGameObjectU3Ec__Iterator1__ctor_m3933975662 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoidARMain/<LoadMainGameObject>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3936489996 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object VoidARMain/<LoadMainGameObject>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m602916020 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Boolean VoidARMain/<LoadMainGameObject>c__Iterator1::MoveNext()
extern Il2CppClass* WWW_t3146501818_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1366199518_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1020075391;
extern const uint32_t U3CLoadMainGameObjectU3Ec__Iterator1_MoveNext_m725974914_MetadataUsageId;
extern "C"  bool U3CLoadMainGameObjectU3Ec__Iterator1_MoveNext_m725974914 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadMainGameObjectU3Ec__Iterator1_MoveNext_m725974914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
		if (L_1 == 2)
		{
			goto IL_00e7;
		}
	}
	{
		goto IL_0115;
	}

IL_0025:
	{
		String_t* L_2 = __this->get_path_0();
		WWW_t3146501818 * L_3 = (WWW_t3146501818 *)il2cpp_codegen_object_new(WWW_t3146501818_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CbundleU3E__0_1(L_3);
		WWW_t3146501818 * L_4 = __this->get_U3CbundleU3E__0_1();
		__this->set_U24current_6(L_4);
		__this->set_U24PC_5(1);
		goto IL_0117;
	}

IL_004e:
	{
		WWW_t3146501818 * L_5 = __this->get_U3CbundleU3E__0_1();
		NullCheck(L_5);
		String_t* L_6 = WWW_get_error_m3092701216(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_7 = __this->get_path_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1020075391, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		goto IL_010e;
	}

IL_0078:
	{
		VoidAR_t451713137 * L_9 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = __this->get_name_2();
		NullCheck(L_9);
		bool L_11 = VoidAR_isMarkerExist_m1590497145(L_9, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_010e;
		}
	}
	{
		WWW_t3146501818 * L_12 = __this->get_U3CbundleU3E__0_1();
		NullCheck(L_12);
		AssetBundle_t945621937 * L_13 = WWW_get_assetBundle_m80031863(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Object_t1181371020 * L_14 = AssetBundle_get_mainAsset_m3897042316(L_13, /*hidden argument*/NULL);
		Object_t1181371020 * L_15 = Object_Instantiate_m2439155489(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->set_U3CmyBundleU3E__1_3(((GameObject_t1366199518 *)CastclassSealed(L_15, GameObject_t1366199518_il2cpp_TypeInfo_var)));
		GameObject_t1366199518 * L_16 = __this->get_U3CmyBundleU3E__1_3();
		NullCheck(L_16);
		Marker_t1240580442 * L_17 = GameObject_GetComponent_TisMarker_t1240580442_m452303857(L_16, /*hidden argument*/GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var);
		__this->set_U3CmU3E__2_4(L_17);
		Marker_t1240580442 * L_18 = __this->get_U3CmU3E__2_4();
		String_t* L_19 = __this->get_name_2();
		NullCheck(L_18);
		L_18->set_imageFilePath_4(L_19);
		GameObject_t1366199518 * L_20 = __this->get_U3CmyBundleU3E__1_3();
		__this->set_U24current_6(L_20);
		__this->set_U24PC_5(2);
		goto IL_0117;
	}

IL_00e7:
	{
		WWW_t3146501818 * L_21 = __this->get_U3CbundleU3E__0_1();
		NullCheck(L_21);
		AssetBundle_t945621937 * L_22 = WWW_get_assetBundle_m80031863(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		AssetBundle_Unload_m167529087(L_22, (bool)0, /*hidden argument*/NULL);
		VoidAR_t451713137 * L_23 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		Marker_t1240580442 * L_24 = __this->get_U3CmU3E__2_4();
		GameObject_t1366199518 * L_25 = __this->get_U3CmyBundleU3E__1_3();
		NullCheck(L_23);
		VoidAR_addCloudTarget_m1660151627(L_23, L_24, L_25, /*hidden argument*/NULL);
	}

IL_010e:
	{
		__this->set_U24PC_5((-1));
	}

IL_0115:
	{
		return (bool)0;
	}

IL_0117:
	{
		return (bool)1;
	}
	// Dead block : IL_0119: ldloc.1
}
// System.Void VoidARMain/<LoadMainGameObject>c__Iterator1::Dispose()
extern "C"  void U3CLoadMainGameObjectU3Ec__Iterator1_Dispose_m3687080867 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void VoidARMain/<LoadMainGameObject>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadMainGameObjectU3Ec__Iterator1_Reset_m2185412017_MetadataUsageId;
extern "C"  void U3CLoadMainGameObjectU3Ec__Iterator1_Reset_m2185412017 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadMainGameObjectU3Ec__Iterator1_Reset_m2185412017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VoidARMain/<LoadMainGameObjectTest>c__Iterator2::.ctor()
extern "C"  void U3CLoadMainGameObjectTestU3Ec__Iterator2__ctor_m664999829 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoidARMain/<LoadMainGameObjectTest>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectTestU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4278228499 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_14();
		return L_0;
	}
}
// System.Object VoidARMain/<LoadMainGameObjectTest>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectTestU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m431667771 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_14();
		return L_0;
	}
}
// System.Boolean VoidARMain/<LoadMainGameObjectTest>c__Iterator2::MoveNext()
extern Il2CppClass* WWW_t3146501818_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t12548584_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1366199518_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1685951112_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisVideoBehaviour_t999888626_m4069762147_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1020075391;
extern Il2CppCodeGenString* _stringLiteral3645118885;
extern const uint32_t U3CLoadMainGameObjectTestU3Ec__Iterator2_MoveNext_m3888402055_MetadataUsageId;
extern "C"  bool U3CLoadMainGameObjectTestU3Ec__Iterator2_MoveNext_m3888402055 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadMainGameObjectTestU3Ec__Iterator2_MoveNext_m3888402055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_13();
		V_0 = L_0;
		__this->set_U24PC_13((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
		if (L_1 == 2)
		{
			goto IL_026c;
		}
	}
	{
		goto IL_029a;
	}

IL_0025:
	{
		String_t* L_2 = __this->get_path_0();
		WWW_t3146501818 * L_3 = (WWW_t3146501818 *)il2cpp_codegen_object_new(WWW_t3146501818_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CbundleU3E__0_1(L_3);
		WWW_t3146501818 * L_4 = __this->get_U3CbundleU3E__0_1();
		__this->set_U24current_14(L_4);
		__this->set_U24PC_13(1);
		goto IL_029c;
	}

IL_004e:
	{
		WWW_t3146501818 * L_5 = __this->get_U3CbundleU3E__0_1();
		NullCheck(L_5);
		String_t* L_6 = WWW_get_error_m3092701216(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0078;
		}
	}
	{
		String_t* L_7 = __this->get_path_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1020075391, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t12548584_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		goto IL_0293;
	}

IL_0078:
	{
		VoidAR_t451713137 * L_9 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = __this->get_name_2();
		NullCheck(L_9);
		bool L_11 = VoidAR_isMarkerExist_m1590497145(L_9, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0293;
		}
	}
	{
		WWW_t3146501818 * L_12 = __this->get_U3CbundleU3E__0_1();
		NullCheck(L_12);
		AssetBundle_t945621937 * L_13 = WWW_get_assetBundle_m80031863(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Object_t1181371020 * L_14 = AssetBundle_get_mainAsset_m3897042316(L_13, /*hidden argument*/NULL);
		Object_t1181371020 * L_15 = Object_Instantiate_m2439155489(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->set_U3CmyBundleU3E__1_3(((GameObject_t1366199518 *)CastclassSealed(L_15, GameObject_t1366199518_il2cpp_TypeInfo_var)));
		GameObject_t1366199518 * L_16 = __this->get_U3CmyBundleU3E__1_3();
		NullCheck(L_16);
		Marker_t1240580442 * L_17 = GameObject_GetComponent_TisMarker_t1240580442_m452303857(L_16, /*hidden argument*/GameObject_GetComponent_TisMarker_t1240580442_m452303857_MethodInfo_var);
		__this->set_U3CmU3E__2_4(L_17);
		Marker_t1240580442 * L_18 = __this->get_U3CmU3E__2_4();
		String_t* L_19 = __this->get_name_2();
		NullCheck(L_18);
		L_18->set_imageFilePath_4(L_19);
		GameObject_t1366199518 * L_20 = __this->get_U3CmyBundleU3E__1_3();
		NullCheck(L_20);
		Transform_t224878301 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t224878301 * L_22 = Transform_FindChild_m2677714886(L_21, _stringLiteral3645118885, /*hidden argument*/NULL);
		NullCheck(L_22);
		VideoBehaviour_t999888626 * L_23 = Component_GetComponent_TisVideoBehaviour_t999888626_m4069762147(L_22, /*hidden argument*/Component_GetComponent_TisVideoBehaviour_t999888626_m4069762147_MethodInfo_var);
		__this->set_U3CvbU3E__3_5(L_23);
		__this->set_U3CmarkerWidthU3E__4_6(((int32_t)640));
		__this->set_U3CmarkerHeightU3E__5_7(((int32_t)480));
		String_t* L_24 = __this->get_videoInfoStr_8();
		CharU5BU5D_t1685951112* L_25 = ((CharU5BU5D_t1685951112*)SZArrayNew(CharU5BU5D_t1685951112_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)124));
		NullCheck(L_24);
		StringU5BU5D_t3764931161* L_26 = String_Split_m3326265864(L_24, L_25, /*hidden argument*/NULL);
		__this->set_U3CurlInfosU3E__6_9(L_26);
		StringU5BU5D_t3764931161* L_27 = __this->get_U3CurlInfosU3E__6_9();
		NullCheck(L_27);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length))))) < ((int32_t)3)))
		{
			goto IL_0155;
		}
	}
	{
		StringU5BU5D_t3764931161* L_28 = __this->get_U3CurlInfosU3E__6_9();
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 1);
		int32_t L_29 = 1;
		int32_t L_30 = Int32_Parse_m3683414232(NULL /*static, unused*/, ((L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29))), /*hidden argument*/NULL);
		__this->set_U3CmarkerWidthU3E__4_6(L_30);
		StringU5BU5D_t3764931161* L_31 = __this->get_U3CurlInfosU3E__6_9();
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 2);
		int32_t L_32 = 2;
		int32_t L_33 = Int32_Parse_m3683414232(NULL /*static, unused*/, ((L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32))), /*hidden argument*/NULL);
		__this->set_U3CmarkerHeightU3E__5_7(L_33);
	}

IL_0155:
	{
		VideoBehaviour_t999888626 * L_34 = __this->get_U3CvbU3E__3_5();
		StringU5BU5D_t3764931161* L_35 = __this->get_U3CurlInfosU3E__6_9();
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 0);
		int32_t L_36 = 0;
		NullCheck(L_34);
		L_34->set_path_2(((L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_36))));
		GameObject_t1366199518 * L_37 = __this->get_U3CmyBundleU3E__1_3();
		NullCheck(L_37);
		Transform_t224878301 * L_38 = GameObject_get_transform_m909382139(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_t224878301 * L_39 = Transform_FindChild_m2677714886(L_38, _stringLiteral3645118885, /*hidden argument*/NULL);
		NullCheck(L_39);
		GameObject_t1366199518 * L_40 = Component_get_gameObject_m3105766835(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_t224878301 * L_41 = GameObject_get_transform_m909382139(L_40, /*hidden argument*/NULL);
		Vector3_t465617797  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector3__ctor_m2638739322(&L_42, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localEulerAngles_m2927195985(L_41, L_42, /*hidden argument*/NULL);
		__this->set_U3CaspU3E__7_10((0.75f));
		int32_t L_43 = __this->get_U3CmarkerHeightU3E__5_7();
		int32_t L_44 = __this->get_U3CmarkerWidthU3E__4_6();
		__this->set_U3CratioU3E__8_11(((float)((float)(((float)((float)L_43)))/(float)(((float)((float)L_44))))));
		GameObject_t1366199518 * L_45 = __this->get_U3CmyBundleU3E__1_3();
		NullCheck(L_45);
		Transform_t224878301 * L_46 = GameObject_get_transform_m909382139(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_t224878301 * L_47 = Transform_FindChild_m2677714886(L_46, _stringLiteral3645118885, /*hidden argument*/NULL);
		NullCheck(L_47);
		GameObject_t1366199518 * L_48 = Component_get_gameObject_m3105766835(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_t224878301 * L_49 = GameObject_get_transform_m909382139(L_48, /*hidden argument*/NULL);
		__this->set_U3CtranU3E__9_12(L_49);
		float L_50 = __this->get_U3CratioU3E__8_11();
		if ((!(((float)L_50) > ((float)(1.0f)))))
		{
			goto IL_022e;
		}
	}
	{
		Transform_t224878301 * L_51 = __this->get_U3CtranU3E__9_12();
		float L_52 = __this->get_U3CratioU3E__8_11();
		float L_53 = __this->get_U3CaspU3E__7_10();
		float L_54 = __this->get_U3CaspU3E__7_10();
		Vector3_t465617797  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector3__ctor_m2638739322(&L_55, ((float)((float)((float)((float)(0.1f)/(float)L_52))*(float)L_53)), (0.1f), ((float)((float)(0.1f)*(float)L_54)), /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_set_localScale_m2325460848(L_51, L_55, /*hidden argument*/NULL);
		goto IL_0254;
	}

IL_022e:
	{
		Transform_t224878301 * L_56 = __this->get_U3CtranU3E__9_12();
		float L_57 = __this->get_U3CratioU3E__8_11();
		Vector3_t465617797  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Vector3__ctor_m2638739322(&L_58, (0.1f), (0.1f), ((float)((float)(0.1f)*(float)L_57)), /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_localScale_m2325460848(L_56, L_58, /*hidden argument*/NULL);
	}

IL_0254:
	{
		GameObject_t1366199518 * L_59 = __this->get_U3CmyBundleU3E__1_3();
		__this->set_U24current_14(L_59);
		__this->set_U24PC_13(2);
		goto IL_029c;
	}

IL_026c:
	{
		WWW_t3146501818 * L_60 = __this->get_U3CbundleU3E__0_1();
		NullCheck(L_60);
		AssetBundle_t945621937 * L_61 = WWW_get_assetBundle_m80031863(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		AssetBundle_Unload_m167529087(L_61, (bool)0, /*hidden argument*/NULL);
		VoidAR_t451713137 * L_62 = VoidAR_GetInstance_m3573393179(NULL /*static, unused*/, /*hidden argument*/NULL);
		Marker_t1240580442 * L_63 = __this->get_U3CmU3E__2_4();
		GameObject_t1366199518 * L_64 = __this->get_U3CmyBundleU3E__1_3();
		NullCheck(L_62);
		VoidAR_addCloudTarget_m1660151627(L_62, L_63, L_64, /*hidden argument*/NULL);
	}

IL_0293:
	{
		__this->set_U24PC_13((-1));
	}

IL_029a:
	{
		return (bool)0;
	}

IL_029c:
	{
		return (bool)1;
	}
	// Dead block : IL_029e: ldloc.1
}
// System.Void VoidARMain/<LoadMainGameObjectTest>c__Iterator2::Dispose()
extern "C"  void U3CLoadMainGameObjectTestU3Ec__Iterator2_Dispose_m2412491932 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_13((-1));
		return;
	}
}
// System.Void VoidARMain/<LoadMainGameObjectTest>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadMainGameObjectTestU3Ec__Iterator2_Reset_m364025674_MetadataUsageId;
extern "C"  void U3CLoadMainGameObjectTestU3Ec__Iterator2_Reset_m364025674 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadMainGameObjectTestU3Ec__Iterator2_Reset_m364025674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
