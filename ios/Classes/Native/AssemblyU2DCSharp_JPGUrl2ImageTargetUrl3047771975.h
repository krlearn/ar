﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JPGUrl2ImageTargetUrl
struct  JPGUrl2ImageTargetUrl_t3047771975  : public Il2CppObject
{
public:
	// System.Boolean JPGUrl2ImageTargetUrl::isLocalImage
	bool ___isLocalImage_0;
	// System.String JPGUrl2ImageTargetUrl::JpgNameURL
	String_t* ___JpgNameURL_1;
	// System.String JPGUrl2ImageTargetUrl::ImageTargetURL
	String_t* ___ImageTargetURL_2;

public:
	inline static int32_t get_offset_of_isLocalImage_0() { return static_cast<int32_t>(offsetof(JPGUrl2ImageTargetUrl_t3047771975, ___isLocalImage_0)); }
	inline bool get_isLocalImage_0() const { return ___isLocalImage_0; }
	inline bool* get_address_of_isLocalImage_0() { return &___isLocalImage_0; }
	inline void set_isLocalImage_0(bool value)
	{
		___isLocalImage_0 = value;
	}

	inline static int32_t get_offset_of_JpgNameURL_1() { return static_cast<int32_t>(offsetof(JPGUrl2ImageTargetUrl_t3047771975, ___JpgNameURL_1)); }
	inline String_t* get_JpgNameURL_1() const { return ___JpgNameURL_1; }
	inline String_t** get_address_of_JpgNameURL_1() { return &___JpgNameURL_1; }
	inline void set_JpgNameURL_1(String_t* value)
	{
		___JpgNameURL_1 = value;
		Il2CppCodeGenWriteBarrier(&___JpgNameURL_1, value);
	}

	inline static int32_t get_offset_of_ImageTargetURL_2() { return static_cast<int32_t>(offsetof(JPGUrl2ImageTargetUrl_t3047771975, ___ImageTargetURL_2)); }
	inline String_t* get_ImageTargetURL_2() const { return ___ImageTargetURL_2; }
	inline String_t** get_address_of_ImageTargetURL_2() { return &___ImageTargetURL_2; }
	inline void set_ImageTargetURL_2(String_t* value)
	{
		___ImageTargetURL_2 = value;
		Il2CppCodeGenWriteBarrier(&___ImageTargetURL_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
