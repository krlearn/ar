﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ExternalTextureDesc_t3964848077;
struct ExternalTextureDesc_t3964848077_marshaled_pinvoke;

extern "C" void ExternalTextureDesc_t3964848077_marshal_pinvoke(const ExternalTextureDesc_t3964848077& unmarshaled, ExternalTextureDesc_t3964848077_marshaled_pinvoke& marshaled);
extern "C" void ExternalTextureDesc_t3964848077_marshal_pinvoke_back(const ExternalTextureDesc_t3964848077_marshaled_pinvoke& marshaled, ExternalTextureDesc_t3964848077& unmarshaled);
extern "C" void ExternalTextureDesc_t3964848077_marshal_pinvoke_cleanup(ExternalTextureDesc_t3964848077_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ExternalTextureDesc_t3964848077;
struct ExternalTextureDesc_t3964848077_marshaled_com;

extern "C" void ExternalTextureDesc_t3964848077_marshal_com(const ExternalTextureDesc_t3964848077& unmarshaled, ExternalTextureDesc_t3964848077_marshaled_com& marshaled);
extern "C" void ExternalTextureDesc_t3964848077_marshal_com_back(const ExternalTextureDesc_t3964848077_marshaled_com& marshaled, ExternalTextureDesc_t3964848077& unmarshaled);
extern "C" void ExternalTextureDesc_t3964848077_marshal_com_cleanup(ExternalTextureDesc_t3964848077_marshaled_com& marshaled);
