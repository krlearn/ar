﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion83606849.h"
#include "UnityEngine_UnityEngine_Matrix4x41261955742.h"
#include "UnityEngine_UnityEngine_Vector3465617797.h"

// UnityEngine.Quaternion MyUtils::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t83606849  MyUtils_GetRotation_m2559326662 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1261955742  ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 MyUtils::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t465617797  MyUtils_GetPosition_m4264985309 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1261955742  ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 MyUtils::GetScale(UnityEngine.Matrix4x4)
extern "C"  Vector3_t465617797  MyUtils_GetScale_m2960603332 (Il2CppObject * __this /* static, unused */, Matrix4x4_t1261955742  ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 MyUtils::getLookAtMatrix(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Matrix4x4_t1261955742  MyUtils_getLookAtMatrix_m4143432437 (Il2CppObject * __this /* static, unused */, Vector3_t465617797  ___pos0, Vector3_t465617797  ___target1, Vector3_t465617797  ___up2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
