﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundle
struct AssetBundle_t945621937;
// UnityEngine.Object
struct Object_t1181371020;
struct Object_t1181371020_marshaled_pinvoke;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Object UnityEngine.AssetBundle::get_mainAsset()
extern "C"  Object_t1181371020 * AssetBundle_get_mainAsset_m3897042316 (AssetBundle_t945621937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern "C"  void AssetBundle_Unload_m167529087 (AssetBundle_t945621937 * __this, bool ___unloadAllLoadedObjects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
