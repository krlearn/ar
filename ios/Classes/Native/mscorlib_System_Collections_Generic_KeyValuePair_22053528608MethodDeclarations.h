﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944347872MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,VoidAR/MarkerMap>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1096269525(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2053528608 *, String_t*, MarkerMap_t684226712 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,VoidAR/MarkerMap>::get_Key()
#define KeyValuePair_2_get_Key_m3991964846(__this, method) ((  String_t* (*) (KeyValuePair_2_t2053528608 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,VoidAR/MarkerMap>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2561794590(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2053528608 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,VoidAR/MarkerMap>::get_Value()
#define KeyValuePair_2_get_Value_m747115171(__this, method) ((  MarkerMap_t684226712 * (*) (KeyValuePair_2_t2053528608 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,VoidAR/MarkerMap>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3046159382(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2053528608 *, MarkerMap_t684226712 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,VoidAR/MarkerMap>::ToString()
#define KeyValuePair_2_ToString_m1120700996(__this, method) ((  String_t* (*) (KeyValuePair_2_t2053528608 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
