﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloudUI
struct CloudUI_t2626580091;
// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"

// System.Void CloudUI::.ctor()
extern "C"  void CloudUI__ctor_m3207867426 (CloudUI_t2626580091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudUI::Start()
extern "C"  void CloudUI_Start_m3010768470 (CloudUI_t2626580091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudUI::OnClick(UnityEngine.GameObject)
extern "C"  void CloudUI_OnClick_m3960742617 (CloudUI_t2626580091 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloudUI::<Start>m__5()
extern "C"  void CloudUI_U3CStartU3Em__5_m3471406432 (CloudUI_t2626580091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
