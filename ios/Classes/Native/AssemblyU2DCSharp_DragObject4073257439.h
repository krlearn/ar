﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIText
struct GUIText_t3960373569;
// UnityEngine.Transform
struct Transform_t224878301;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "UnityEngine_UnityEngine_Vector3465617797.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragObject
struct  DragObject_t4073257439  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GUIText DragObject::message
	GUIText_t3960373569 * ___message_2;
	// UnityEngine.Transform DragObject::pickedObject
	Transform_t224878301 * ___pickedObject_3;
	// UnityEngine.Vector3 DragObject::lastPlanePoint
	Vector3_t465617797  ___lastPlanePoint_4;

public:
	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(DragObject_t4073257439, ___message_2)); }
	inline GUIText_t3960373569 * get_message_2() const { return ___message_2; }
	inline GUIText_t3960373569 ** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(GUIText_t3960373569 * value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier(&___message_2, value);
	}

	inline static int32_t get_offset_of_pickedObject_3() { return static_cast<int32_t>(offsetof(DragObject_t4073257439, ___pickedObject_3)); }
	inline Transform_t224878301 * get_pickedObject_3() const { return ___pickedObject_3; }
	inline Transform_t224878301 ** get_address_of_pickedObject_3() { return &___pickedObject_3; }
	inline void set_pickedObject_3(Transform_t224878301 * value)
	{
		___pickedObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___pickedObject_3, value);
	}

	inline static int32_t get_offset_of_lastPlanePoint_4() { return static_cast<int32_t>(offsetof(DragObject_t4073257439, ___lastPlanePoint_4)); }
	inline Vector3_t465617797  get_lastPlanePoint_4() const { return ___lastPlanePoint_4; }
	inline Vector3_t465617797 * get_address_of_lastPlanePoint_4() { return &___lastPlanePoint_4; }
	inline void set_lastPlanePoint_4(Vector3_t465617797  value)
	{
		___lastPlanePoint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
