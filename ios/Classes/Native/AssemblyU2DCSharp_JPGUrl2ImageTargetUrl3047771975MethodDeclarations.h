﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JPGUrl2ImageTargetUrl
struct JPGUrl2ImageTargetUrl_t3047771975;

#include "codegen/il2cpp-codegen.h"

// System.Void JPGUrl2ImageTargetUrl::.ctor()
extern "C"  void JPGUrl2ImageTargetUrl__ctor_m1425597358 (JPGUrl2ImageTargetUrl_t3047771975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
