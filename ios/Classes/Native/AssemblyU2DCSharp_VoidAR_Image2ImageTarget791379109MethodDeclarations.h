﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidAR/Image2ImageTarget
struct Image2ImageTarget_t791379109;

#include "codegen/il2cpp-codegen.h"

// System.Void VoidAR/Image2ImageTarget::.ctor()
extern "C"  void Image2ImageTarget__ctor_m3111905800 (Image2ImageTarget_t791379109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
