﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImageUI
struct ImageUI_t751656141;
// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"

// System.Void ImageUI::.ctor()
extern "C"  void ImageUI__ctor_m4034870110 (ImageUI_t751656141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUI::Start()
extern "C"  void ImageUI_Start_m2147152894 (ImageUI_t751656141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUI::OnClick(UnityEngine.GameObject)
extern "C"  void ImageUI_OnClick_m884303051 (ImageUI_t751656141 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageUI::<Start>m__9()
extern "C"  void ImageUI_U3CStartU3Em__9_m1005909124 (ImageUI_t751656141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
