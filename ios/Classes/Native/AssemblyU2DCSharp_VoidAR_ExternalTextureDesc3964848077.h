﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4028081426.h"
#include "mscorlib_System_IntPtr3076297692.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidAR/ExternalTextureDesc
struct  ExternalTextureDesc_t3964848077 
{
public:
	// System.IntPtr VoidAR/ExternalTextureDesc::tex1
	IntPtr_t ___tex1_0;
	// System.IntPtr VoidAR/ExternalTextureDesc::tex2
	IntPtr_t ___tex2_1;
	// System.Int32 VoidAR/ExternalTextureDesc::width
	int32_t ___width_2;
	// System.Int32 VoidAR/ExternalTextureDesc::height
	int32_t ___height_3;

public:
	inline static int32_t get_offset_of_tex1_0() { return static_cast<int32_t>(offsetof(ExternalTextureDesc_t3964848077, ___tex1_0)); }
	inline IntPtr_t get_tex1_0() const { return ___tex1_0; }
	inline IntPtr_t* get_address_of_tex1_0() { return &___tex1_0; }
	inline void set_tex1_0(IntPtr_t value)
	{
		___tex1_0 = value;
	}

	inline static int32_t get_offset_of_tex2_1() { return static_cast<int32_t>(offsetof(ExternalTextureDesc_t3964848077, ___tex2_1)); }
	inline IntPtr_t get_tex2_1() const { return ___tex2_1; }
	inline IntPtr_t* get_address_of_tex2_1() { return &___tex2_1; }
	inline void set_tex2_1(IntPtr_t value)
	{
		___tex2_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(ExternalTextureDesc_t3964848077, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(ExternalTextureDesc_t3964848077, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: VoidAR/ExternalTextureDesc
struct ExternalTextureDesc_t3964848077_marshaled_pinvoke
{
	intptr_t ___tex1_0;
	intptr_t ___tex2_1;
	int32_t ___width_2;
	int32_t ___height_3;
};
// Native definition for marshalling of: VoidAR/ExternalTextureDesc
struct ExternalTextureDesc_t3964848077_marshaled_com
{
	intptr_t ___tex1_0;
	intptr_t ___tex2_1;
	int32_t ___width_2;
	int32_t ___height_3;
};
