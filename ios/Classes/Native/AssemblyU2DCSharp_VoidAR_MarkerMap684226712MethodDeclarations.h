﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidAR/MarkerMap
struct MarkerMap_t684226712;

#include "codegen/il2cpp-codegen.h"

// System.Void VoidAR/MarkerMap::.ctor()
extern "C"  void MarkerMap__ctor_m3444299521 (MarkerMap_t684226712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
