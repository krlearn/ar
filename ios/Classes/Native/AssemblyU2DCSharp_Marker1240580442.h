﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "AssemblyU2DCSharp_Marker_EMarkerState3748166026.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Marker
struct  Marker_t1240580442  : public MonoBehaviour_t774292115
{
public:
	// Marker/EMarkerState Marker::markerState
	int32_t ___markerState_2;
	// UnityEngine.GameObject Marker::model
	GameObject_t1366199518 * ___model_3;
	// System.String Marker::imageFilePath
	String_t* ___imageFilePath_4;

public:
	inline static int32_t get_offset_of_markerState_2() { return static_cast<int32_t>(offsetof(Marker_t1240580442, ___markerState_2)); }
	inline int32_t get_markerState_2() const { return ___markerState_2; }
	inline int32_t* get_address_of_markerState_2() { return &___markerState_2; }
	inline void set_markerState_2(int32_t value)
	{
		___markerState_2 = value;
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(Marker_t1240580442, ___model_3)); }
	inline GameObject_t1366199518 * get_model_3() const { return ___model_3; }
	inline GameObject_t1366199518 ** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(GameObject_t1366199518 * value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier(&___model_3, value);
	}

	inline static int32_t get_offset_of_imageFilePath_4() { return static_cast<int32_t>(offsetof(Marker_t1240580442, ___imageFilePath_4)); }
	inline String_t* get_imageFilePath_4() const { return ___imageFilePath_4; }
	inline String_t** get_address_of_imageFilePath_4() { return &___imageFilePath_4; }
	inline void set_imageFilePath_4(String_t* value)
	{
		___imageFilePath_4 = value;
		Il2CppCodeGenWriteBarrier(&___imageFilePath_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
