﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3146501818;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// Marker
struct Marker_t1240580442;
// VideoBehaviour
struct VideoBehaviour_t999888626;
// System.String[]
struct StringU5BU5D_t3764931161;
// UnityEngine.Transform
struct Transform_t224878301;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidARMain/<LoadMainGameObjectTest>c__Iterator2
struct  U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831  : public Il2CppObject
{
public:
	// System.String VoidARMain/<LoadMainGameObjectTest>c__Iterator2::path
	String_t* ___path_0;
	// UnityEngine.WWW VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<bundle>__0
	WWW_t3146501818 * ___U3CbundleU3E__0_1;
	// System.String VoidARMain/<LoadMainGameObjectTest>c__Iterator2::name
	String_t* ___name_2;
	// UnityEngine.GameObject VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<myBundle>__1
	GameObject_t1366199518 * ___U3CmyBundleU3E__1_3;
	// Marker VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<m>__2
	Marker_t1240580442 * ___U3CmU3E__2_4;
	// VideoBehaviour VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<vb>__3
	VideoBehaviour_t999888626 * ___U3CvbU3E__3_5;
	// System.Int32 VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<markerWidth>__4
	int32_t ___U3CmarkerWidthU3E__4_6;
	// System.Int32 VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<markerHeight>__5
	int32_t ___U3CmarkerHeightU3E__5_7;
	// System.String VoidARMain/<LoadMainGameObjectTest>c__Iterator2::videoInfoStr
	String_t* ___videoInfoStr_8;
	// System.String[] VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<urlInfos>__6
	StringU5BU5D_t3764931161* ___U3CurlInfosU3E__6_9;
	// System.Single VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<asp>__7
	float ___U3CaspU3E__7_10;
	// System.Single VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<ratio>__8
	float ___U3CratioU3E__8_11;
	// UnityEngine.Transform VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<tran>__9
	Transform_t224878301 * ___U3CtranU3E__9_12;
	// System.Int32 VoidARMain/<LoadMainGameObjectTest>c__Iterator2::$PC
	int32_t ___U24PC_13;
	// System.Object VoidARMain/<LoadMainGameObjectTest>c__Iterator2::$current
	Il2CppObject * ___U24current_14;
	// System.String VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<$>path
	String_t* ___U3CU24U3Epath_15;
	// System.String VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<$>name
	String_t* ___U3CU24U3Ename_16;
	// System.String VoidARMain/<LoadMainGameObjectTest>c__Iterator2::<$>videoInfoStr
	String_t* ___U3CU24U3EvideoInfoStr_17;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_U3CbundleU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CbundleU3E__0_1)); }
	inline WWW_t3146501818 * get_U3CbundleU3E__0_1() const { return ___U3CbundleU3E__0_1; }
	inline WWW_t3146501818 ** get_address_of_U3CbundleU3E__0_1() { return &___U3CbundleU3E__0_1; }
	inline void set_U3CbundleU3E__0_1(WWW_t3146501818 * value)
	{
		___U3CbundleU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbundleU3E__0_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_U3CmyBundleU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CmyBundleU3E__1_3)); }
	inline GameObject_t1366199518 * get_U3CmyBundleU3E__1_3() const { return ___U3CmyBundleU3E__1_3; }
	inline GameObject_t1366199518 ** get_address_of_U3CmyBundleU3E__1_3() { return &___U3CmyBundleU3E__1_3; }
	inline void set_U3CmyBundleU3E__1_3(GameObject_t1366199518 * value)
	{
		___U3CmyBundleU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmyBundleU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CmU3E__2_4() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CmU3E__2_4)); }
	inline Marker_t1240580442 * get_U3CmU3E__2_4() const { return ___U3CmU3E__2_4; }
	inline Marker_t1240580442 ** get_address_of_U3CmU3E__2_4() { return &___U3CmU3E__2_4; }
	inline void set_U3CmU3E__2_4(Marker_t1240580442 * value)
	{
		___U3CmU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CvbU3E__3_5() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CvbU3E__3_5)); }
	inline VideoBehaviour_t999888626 * get_U3CvbU3E__3_5() const { return ___U3CvbU3E__3_5; }
	inline VideoBehaviour_t999888626 ** get_address_of_U3CvbU3E__3_5() { return &___U3CvbU3E__3_5; }
	inline void set_U3CvbU3E__3_5(VideoBehaviour_t999888626 * value)
	{
		___U3CvbU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvbU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U3CmarkerWidthU3E__4_6() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CmarkerWidthU3E__4_6)); }
	inline int32_t get_U3CmarkerWidthU3E__4_6() const { return ___U3CmarkerWidthU3E__4_6; }
	inline int32_t* get_address_of_U3CmarkerWidthU3E__4_6() { return &___U3CmarkerWidthU3E__4_6; }
	inline void set_U3CmarkerWidthU3E__4_6(int32_t value)
	{
		___U3CmarkerWidthU3E__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CmarkerHeightU3E__5_7() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CmarkerHeightU3E__5_7)); }
	inline int32_t get_U3CmarkerHeightU3E__5_7() const { return ___U3CmarkerHeightU3E__5_7; }
	inline int32_t* get_address_of_U3CmarkerHeightU3E__5_7() { return &___U3CmarkerHeightU3E__5_7; }
	inline void set_U3CmarkerHeightU3E__5_7(int32_t value)
	{
		___U3CmarkerHeightU3E__5_7 = value;
	}

	inline static int32_t get_offset_of_videoInfoStr_8() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___videoInfoStr_8)); }
	inline String_t* get_videoInfoStr_8() const { return ___videoInfoStr_8; }
	inline String_t** get_address_of_videoInfoStr_8() { return &___videoInfoStr_8; }
	inline void set_videoInfoStr_8(String_t* value)
	{
		___videoInfoStr_8 = value;
		Il2CppCodeGenWriteBarrier(&___videoInfoStr_8, value);
	}

	inline static int32_t get_offset_of_U3CurlInfosU3E__6_9() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CurlInfosU3E__6_9)); }
	inline StringU5BU5D_t3764931161* get_U3CurlInfosU3E__6_9() const { return ___U3CurlInfosU3E__6_9; }
	inline StringU5BU5D_t3764931161** get_address_of_U3CurlInfosU3E__6_9() { return &___U3CurlInfosU3E__6_9; }
	inline void set_U3CurlInfosU3E__6_9(StringU5BU5D_t3764931161* value)
	{
		___U3CurlInfosU3E__6_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlInfosU3E__6_9, value);
	}

	inline static int32_t get_offset_of_U3CaspU3E__7_10() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CaspU3E__7_10)); }
	inline float get_U3CaspU3E__7_10() const { return ___U3CaspU3E__7_10; }
	inline float* get_address_of_U3CaspU3E__7_10() { return &___U3CaspU3E__7_10; }
	inline void set_U3CaspU3E__7_10(float value)
	{
		___U3CaspU3E__7_10 = value;
	}

	inline static int32_t get_offset_of_U3CratioU3E__8_11() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CratioU3E__8_11)); }
	inline float get_U3CratioU3E__8_11() const { return ___U3CratioU3E__8_11; }
	inline float* get_address_of_U3CratioU3E__8_11() { return &___U3CratioU3E__8_11; }
	inline void set_U3CratioU3E__8_11(float value)
	{
		___U3CratioU3E__8_11 = value;
	}

	inline static int32_t get_offset_of_U3CtranU3E__9_12() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CtranU3E__9_12)); }
	inline Transform_t224878301 * get_U3CtranU3E__9_12() const { return ___U3CtranU3E__9_12; }
	inline Transform_t224878301 ** get_address_of_U3CtranU3E__9_12() { return &___U3CtranU3E__9_12; }
	inline void set_U3CtranU3E__9_12(Transform_t224878301 * value)
	{
		___U3CtranU3E__9_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtranU3E__9_12, value);
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}

	inline static int32_t get_offset_of_U24current_14() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U24current_14)); }
	inline Il2CppObject * get_U24current_14() const { return ___U24current_14; }
	inline Il2CppObject ** get_address_of_U24current_14() { return &___U24current_14; }
	inline void set_U24current_14(Il2CppObject * value)
	{
		___U24current_14 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_14, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Epath_15() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CU24U3Epath_15)); }
	inline String_t* get_U3CU24U3Epath_15() const { return ___U3CU24U3Epath_15; }
	inline String_t** get_address_of_U3CU24U3Epath_15() { return &___U3CU24U3Epath_15; }
	inline void set_U3CU24U3Epath_15(String_t* value)
	{
		___U3CU24U3Epath_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Epath_15, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ename_16() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CU24U3Ename_16)); }
	inline String_t* get_U3CU24U3Ename_16() const { return ___U3CU24U3Ename_16; }
	inline String_t** get_address_of_U3CU24U3Ename_16() { return &___U3CU24U3Ename_16; }
	inline void set_U3CU24U3Ename_16(String_t* value)
	{
		___U3CU24U3Ename_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ename_16, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EvideoInfoStr_17() { return static_cast<int32_t>(offsetof(U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831, ___U3CU24U3EvideoInfoStr_17)); }
	inline String_t* get_U3CU24U3EvideoInfoStr_17() const { return ___U3CU24U3EvideoInfoStr_17; }
	inline String_t** get_address_of_U3CU24U3EvideoInfoStr_17() { return &___U3CU24U3EvideoInfoStr_17; }
	inline void set_U3CU24U3EvideoInfoStr_17(String_t* value)
	{
		___U3CU24U3EvideoInfoStr_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EvideoInfoStr_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
