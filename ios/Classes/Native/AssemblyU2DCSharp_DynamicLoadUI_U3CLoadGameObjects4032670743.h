﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>
struct List_1_t4197717748;
// JPGUrl2ImageTargetUrl
struct JPGUrl2ImageTargetUrl_t3047771975;
// UnityEngine.WWW
struct WWW_t3146501818;
// VoidAR/Image2ImageTarget
struct Image2ImageTarget_t791379109;
// System.Object
struct Il2CppObject;
// DynamicLoadUI
struct DynamicLoadUI_t1834292937;

#include "mscorlib_System_Object707969140.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2420141438.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicLoadUI/<LoadGameObjects>c__Iterator0
struct  U3CLoadGameObjectsU3Ec__Iterator0_t4032670743  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<VoidAR/Image2ImageTarget> DynamicLoadUI/<LoadGameObjects>c__Iterator0::<image2ImageTargetes>__0
	List_1_t4197717748 * ___U3Cimage2ImageTargetesU3E__0_0;
	// System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl> DynamicLoadUI/<LoadGameObjects>c__Iterator0::<$s_3>__1
	Enumerator_t2420141438  ___U3CU24s_3U3E__1_1;
	// JPGUrl2ImageTargetUrl DynamicLoadUI/<LoadGameObjects>c__Iterator0::<url>__2
	JPGUrl2ImageTargetUrl_t3047771975 * ___U3CurlU3E__2_2;
	// UnityEngine.WWW DynamicLoadUI/<LoadGameObjects>c__Iterator0::<file>__3
	WWW_t3146501818 * ___U3CfileU3E__3_3;
	// UnityEngine.WWW DynamicLoadUI/<LoadGameObjects>c__Iterator0::<bundle>__4
	WWW_t3146501818 * ___U3CbundleU3E__4_4;
	// VoidAR/Image2ImageTarget DynamicLoadUI/<LoadGameObjects>c__Iterator0::<obj>__5
	Image2ImageTarget_t791379109 * ___U3CobjU3E__5_5;
	// System.Int32 DynamicLoadUI/<LoadGameObjects>c__Iterator0::$PC
	int32_t ___U24PC_6;
	// System.Object DynamicLoadUI/<LoadGameObjects>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// DynamicLoadUI DynamicLoadUI/<LoadGameObjects>c__Iterator0::<>f__this
	DynamicLoadUI_t1834292937 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3Cimage2ImageTargetesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U3Cimage2ImageTargetesU3E__0_0)); }
	inline List_1_t4197717748 * get_U3Cimage2ImageTargetesU3E__0_0() const { return ___U3Cimage2ImageTargetesU3E__0_0; }
	inline List_1_t4197717748 ** get_address_of_U3Cimage2ImageTargetesU3E__0_0() { return &___U3Cimage2ImageTargetesU3E__0_0; }
	inline void set_U3Cimage2ImageTargetesU3E__0_0(List_1_t4197717748 * value)
	{
		___U3Cimage2ImageTargetesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cimage2ImageTargetesU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_3U3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U3CU24s_3U3E__1_1)); }
	inline Enumerator_t2420141438  get_U3CU24s_3U3E__1_1() const { return ___U3CU24s_3U3E__1_1; }
	inline Enumerator_t2420141438 * get_address_of_U3CU24s_3U3E__1_1() { return &___U3CU24s_3U3E__1_1; }
	inline void set_U3CU24s_3U3E__1_1(Enumerator_t2420141438  value)
	{
		___U3CU24s_3U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CurlU3E__2_2() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U3CurlU3E__2_2)); }
	inline JPGUrl2ImageTargetUrl_t3047771975 * get_U3CurlU3E__2_2() const { return ___U3CurlU3E__2_2; }
	inline JPGUrl2ImageTargetUrl_t3047771975 ** get_address_of_U3CurlU3E__2_2() { return &___U3CurlU3E__2_2; }
	inline void set_U3CurlU3E__2_2(JPGUrl2ImageTargetUrl_t3047771975 * value)
	{
		___U3CurlU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__3_3() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U3CfileU3E__3_3)); }
	inline WWW_t3146501818 * get_U3CfileU3E__3_3() const { return ___U3CfileU3E__3_3; }
	inline WWW_t3146501818 ** get_address_of_U3CfileU3E__3_3() { return &___U3CfileU3E__3_3; }
	inline void set_U3CfileU3E__3_3(WWW_t3146501818 * value)
	{
		___U3CfileU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfileU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CbundleU3E__4_4() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U3CbundleU3E__4_4)); }
	inline WWW_t3146501818 * get_U3CbundleU3E__4_4() const { return ___U3CbundleU3E__4_4; }
	inline WWW_t3146501818 ** get_address_of_U3CbundleU3E__4_4() { return &___U3CbundleU3E__4_4; }
	inline void set_U3CbundleU3E__4_4(WWW_t3146501818 * value)
	{
		___U3CbundleU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbundleU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__5_5() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U3CobjU3E__5_5)); }
	inline Image2ImageTarget_t791379109 * get_U3CobjU3E__5_5() const { return ___U3CobjU3E__5_5; }
	inline Image2ImageTarget_t791379109 ** get_address_of_U3CobjU3E__5_5() { return &___U3CobjU3E__5_5; }
	inline void set_U3CobjU3E__5_5(Image2ImageTarget_t791379109 * value)
	{
		___U3CobjU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CLoadGameObjectsU3Ec__Iterator0_t4032670743, ___U3CU3Ef__this_8)); }
	inline DynamicLoadUI_t1834292937 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline DynamicLoadUI_t1834292937 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(DynamicLoadUI_t1834292937 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
