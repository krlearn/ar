﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4114307779MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::.ctor()
#define List_1__ctor_m3529032890(__this, method) ((  void (*) (List_1_t4197717748 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::.ctor(System.Int32)
#define List_1__ctor_m3546920394(__this, ___capacity0, method) ((  void (*) (List_1_t4197717748 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::.cctor()
#define List_1__cctor_m156513094(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2651495929(__this, method) ((  Il2CppObject* (*) (List_1_t4197717748 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m423890169(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4197717748 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2411403272(__this, method) ((  Il2CppObject * (*) (List_1_t4197717748 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m4216715865(__this, ___item0, method) ((  int32_t (*) (List_1_t4197717748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1132490169(__this, ___item0, method) ((  bool (*) (List_1_t4197717748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m276927783(__this, ___item0, method) ((  int32_t (*) (List_1_t4197717748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m501305602(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4197717748 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m119793488(__this, ___item0, method) ((  void (*) (List_1_t4197717748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2945583344(__this, method) ((  bool (*) (List_1_t4197717748 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3090725525(__this, method) ((  bool (*) (List_1_t4197717748 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2135764733(__this, method) ((  Il2CppObject * (*) (List_1_t4197717748 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m4266588162(__this, method) ((  bool (*) (List_1_t4197717748 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1238755385(__this, method) ((  bool (*) (List_1_t4197717748 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2709708032(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4197717748 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2490616975(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4197717748 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Add(T)
#define List_1_Add_m3346196670(__this, ___item0, method) ((  void (*) (List_1_t4197717748 *, Image2ImageTarget_t791379109 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2164265321(__this, ___newCount0, method) ((  void (*) (List_1_t4197717748 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1661529249(__this, ___collection0, method) ((  void (*) (List_1_t4197717748 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2993412129(__this, ___enumerable0, method) ((  void (*) (List_1_t4197717748 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m247109800(__this, ___collection0, method) ((  void (*) (List_1_t4197717748 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::AsReadOnly()
#define List_1_AsReadOnly_m3953110745(__this, method) ((  ReadOnlyCollection_1_t3890962581 * (*) (List_1_t4197717748 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Clear()
#define List_1_Clear_m3215960442(__this, method) ((  void (*) (List_1_t4197717748 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Contains(T)
#define List_1_Contains_m2566122500(__this, ___item0, method) ((  bool (*) (List_1_t4197717748 *, Image2ImageTarget_t791379109 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1561069278(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4197717748 *, Image2ImageTargetU5BU5D_t2755005224*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Find(System.Predicate`1<T>)
#define List_1_Find_m273278144(__this, ___match0, method) ((  Image2ImageTarget_t791379109 * (*) (List_1_t4197717748 *, Predicate_1_t4198762421 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2201614121(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4198762421 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m777337650(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4197717748 *, int32_t, int32_t, Predicate_1_t4198762421 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::GetEnumerator()
#define List_1_GetEnumerator_m951101811(__this, method) ((  Enumerator_t163748572  (*) (List_1_t4197717748 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::IndexOf(T)
#define List_1_IndexOf_m3781312488(__this, ___item0, method) ((  int32_t (*) (List_1_t4197717748 *, Image2ImageTarget_t791379109 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1597377429(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4197717748 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2877948730(__this, ___index0, method) ((  void (*) (List_1_t4197717748 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Insert(System.Int32,T)
#define List_1_Insert_m3679710039(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4197717748 *, int32_t, Image2ImageTarget_t791379109 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2797367772(__this, ___collection0, method) ((  void (*) (List_1_t4197717748 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Remove(T)
#define List_1_Remove_m2810997835(__this, ___item0, method) ((  bool (*) (List_1_t4197717748 *, Image2ImageTarget_t791379109 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m233199793(__this, ___match0, method) ((  int32_t (*) (List_1_t4197717748 *, Predicate_1_t4198762421 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1201431491(__this, ___index0, method) ((  void (*) (List_1_t4197717748 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Reverse()
#define List_1_Reverse_m2710127053(__this, method) ((  void (*) (List_1_t4197717748 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Sort()
#define List_1_Sort_m2211258323(__this, method) ((  void (*) (List_1_t4197717748 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2796095118(__this, ___comparison0, method) ((  void (*) (List_1_t4197717748 *, Comparison_1_t627961589 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::ToArray()
#define List_1_ToArray_m1504079828(__this, method) ((  Image2ImageTargetU5BU5D_t2755005224* (*) (List_1_t4197717748 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::TrimExcess()
#define List_1_TrimExcess_m3556603084(__this, method) ((  void (*) (List_1_t4197717748 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::get_Capacity()
#define List_1_get_Capacity_m1058709762(__this, method) ((  int32_t (*) (List_1_t4197717748 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2949358561(__this, ___value0, method) ((  void (*) (List_1_t4197717748 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::get_Count()
#define List_1_get_Count_m3773279321(__this, method) ((  int32_t (*) (List_1_t4197717748 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// T System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::get_Item(System.Int32)
#define List_1_get_Item_m697430623(__this, ___index0, method) ((  Image2ImageTarget_t791379109 * (*) (List_1_t4197717748 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>::set_Item(System.Int32,T)
#define List_1_set_Item_m770036492(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4197717748 *, int32_t, Image2ImageTarget_t791379109 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
