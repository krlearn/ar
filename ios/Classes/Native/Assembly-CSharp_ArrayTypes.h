﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// JPGUrl2ImageTargetUrl
struct JPGUrl2ImageTargetUrl_t3047771975;
// VoidAR/Image2ImageTarget
struct Image2ImageTarget_t791379109;
// Marker
struct Marker_t1240580442;
// VoidAR/MarkerMap
struct MarkerMap_t684226712;

#include "mscorlib_System_Array4136897760.h"
#include "AssemblyU2DCSharp_JPGUrl2ImageTargetUrl3047771975.h"
#include "AssemblyU2DCSharp_VoidAR_Image2ImageTarget791379109.h"
#include "AssemblyU2DCSharp_Marker1240580442.h"
#include "AssemblyU2DCSharp_VoidAR_MarkerMap684226712.h"

#pragma once
// JPGUrl2ImageTargetUrl[]
struct JPGUrl2ImageTargetUrlU5BU5D_t3143090110  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) JPGUrl2ImageTargetUrl_t3047771975 * m_Items[1];

public:
	inline JPGUrl2ImageTargetUrl_t3047771975 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JPGUrl2ImageTargetUrl_t3047771975 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JPGUrl2ImageTargetUrl_t3047771975 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VoidAR/Image2ImageTarget[]
struct Image2ImageTargetU5BU5D_t2755005224  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Image2ImageTarget_t791379109 * m_Items[1];

public:
	inline Image2ImageTarget_t791379109 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Image2ImageTarget_t791379109 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Image2ImageTarget_t791379109 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Marker[]
struct MarkerU5BU5D_t3893320703  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Marker_t1240580442 * m_Items[1];

public:
	inline Marker_t1240580442 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Marker_t1240580442 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Marker_t1240580442 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VoidAR/MarkerMap[]
struct MarkerMapU5BU5D_t608131593  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) MarkerMap_t684226712 * m_Items[1];

public:
	inline MarkerMap_t684226712 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MarkerMap_t684226712 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MarkerMap_t684226712 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
