﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragObject
struct DragObject_t4073257439;

#include "codegen/il2cpp-codegen.h"

// System.Void DragObject::.ctor()
extern "C"  void DragObject__ctor_m2177710912 (DragObject_t4073257439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragObject::Start()
extern "C"  void DragObject_Start_m4294151148 (DragObject_t4073257439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragObject::Update()
extern "C"  void DragObject_Update_m1756000767 (DragObject_t4073257439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
