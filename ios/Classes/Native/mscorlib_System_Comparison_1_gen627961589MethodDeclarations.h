﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen544551620MethodDeclarations.h"

// System.Void System.Comparison`1<VoidAR/Image2ImageTarget>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m91513820(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t627961589 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<VoidAR/Image2ImageTarget>::Invoke(T,T)
#define Comparison_1_Invoke_m542404524(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t627961589 *, Image2ImageTarget_t791379109 *, Image2ImageTarget_t791379109 *, const MethodInfo*))Comparison_1_Invoke_m2798106261_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<VoidAR/Image2ImageTarget>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m3238511481(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t627961589 *, Image2ImageTarget_t791379109 *, Image2ImageTarget_t791379109 *, AsyncCallback_t889871978 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1817828810_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<VoidAR/Image2ImageTarget>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m289241146(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t627961589 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1056665895_gshared)(__this, ___result0, method)
