﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato80338603MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m241099790(__this, ___l0, method) ((  void (*) (Enumerator_t2420141438 *, List_1_t2159143318 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2043264108(__this, method) ((  void (*) (Enumerator_t2420141438 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2999648232(__this, method) ((  Il2CppObject * (*) (Enumerator_t2420141438 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl>::Dispose()
#define Enumerator_Dispose_m3731682427(__this, method) ((  void (*) (Enumerator_t2420141438 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl>::VerifyState()
#define Enumerator_VerifyState_m4054864(__this, method) ((  void (*) (Enumerator_t2420141438 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl>::MoveNext()
#define Enumerator_MoveNext_m860455637(__this, method) ((  bool (*) (Enumerator_t2420141438 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<JPGUrl2ImageTargetUrl>::get_Current()
#define Enumerator_get_Current_m1520675709(__this, method) ((  JPGUrl2ImageTargetUrl_t3047771975 * (*) (Enumerator_t2420141438 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
