﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidAR
struct VoidAR_t451713137;
// System.String
struct String_t;
// System.Double[]
struct DoubleU5BU5D_t2839599125;
// System.Byte[]
struct ByteU5BU5D_t3835026402;
// System.Int32[]
struct Int32U5BU5D_t3315407976;
// Marker
struct Marker_t1240580442;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// UnityEngine.Camera
struct Camera_t2805735124;
// VoidAR/Image2ImageTarget
struct Image2ImageTarget_t791379109;
// System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>
struct List_1_t4197717748;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String1967731336.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "AssemblyU2DCSharp_Marker1240580442.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"
#include "UnityEngine_UnityEngine_Camera2805735124.h"
#include "UnityEngine_UnityEngine_Matrix4x41261955742.h"
#include "AssemblyU2DCSharp_VoidAR_Image2ImageTarget791379109.h"

// System.Void VoidAR::.ctor()
extern "C"  void VoidAR__ctor_m729901080 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::CallBackFunction(System.String)
extern "C"  void VoidAR_CallBackFunction_m2677318649 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::ShapeMatchingCallBack(System.Int32)
extern "C"  void VoidAR_ShapeMatchingCallBack_m2416343084 (Il2CppObject * __this /* static, unused */, int32_t ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_setDebugLog(System.IntPtr)
extern "C"  void VoidAR__setDebugLog_m1237218656 (Il2CppObject * __this /* static, unused */, IntPtr_t ___fp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_setMatchingResult(System.IntPtr)
extern "C"  void VoidAR__setMatchingResult_m3843032621 (Il2CppObject * __this /* static, unused */, IntPtr_t ___fp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_initBGTexture(System.Int32&,System.Int32&,System.Int32&)
extern "C"  void VoidAR__initBGTexture_m585106992 (Il2CppObject * __this /* static, unused */, int32_t* ___textureId0, int32_t* ___width1, int32_t* ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_initAndroidBGTexutre(System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern "C"  void VoidAR__initAndroidBGTexutre_m108464840 (Il2CppObject * __this /* static, unused */, int32_t* ___t10, int32_t* ___t21, int32_t* ___width2, int32_t* ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_setScreenSize(System.Int32&,System.Int32&)
extern "C"  void VoidAR__setScreenSize_m914176366 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_initLibrary(System.Int32)
extern "C"  void VoidAR__initLibrary_m4058222619 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_buildGLProjectionMatrix(System.Int32,System.Int32,System.Double[])
extern "C"  void VoidAR__buildGLProjectionMatrix_m2182184962 (Il2CppObject * __this /* static, unused */, int32_t ___screenWidth0, int32_t ___sceenHeight1, DoubleU5BU5D_t2839599125* ___matrix442, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_startCapture(System.Int32,System.Int32&)
extern "C"  void VoidAR__startCapture_m3291993969 (Il2CppObject * __this /* static, unused */, int32_t ___cameraIndex0, int32_t* ___opened1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_stopCapture()
extern "C"  void VoidAR__stopCapture_m719693631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_getMatchResult(System.IntPtr,System.Int32&,System.Int32&,System.Int32&,System.Double[],System.Byte[],System.Int32&,System.Int32[])
extern "C"  void VoidAR__getMatchResult_m819854597 (Il2CppObject * __this /* static, unused */, IntPtr_t ___frame0, int32_t* ___width1, int32_t* ___height2, int32_t* ___flip3, DoubleU5BU5D_t2839599125* ___matrix4, ByteU5BU5D_t3835026402* ___markers5, int32_t* ___mainMarker6, Int32U5BU5D_t3315407976* ___scores7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_unloadResource()
extern "C"  void VoidAR__unloadResource_m1465702158 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_match()
extern "C"  void VoidAR__match_m1170501128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_getTrackingTime(System.Double[])
extern "C"  void VoidAR__getTrackingTime_m1283378439 (Il2CppObject * __this /* static, unused */, DoubleU5BU5D_t2839599125* ___times0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_reset()
extern "C"  void VoidAR__reset_m2806132926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_getTrackStatus(System.Int32[])
extern "C"  void VoidAR__getTrackStatus_m2897874513 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3315407976* ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_setDebugDraw(System.Int32)
extern "C"  void VoidAR__setDebugDraw_m1360007373 (Il2CppObject * __this /* static, unused */, int32_t ___debugDraw0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_analyzing(System.String,System.IntPtr)
extern "C"  void VoidAR__analyzing_m2999421260 (Il2CppObject * __this /* static, unused */, String_t* ___path0, IntPtr_t ___fp1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_addCurrentShapeImageTarget(System.Int32&,System.Byte[],System.Int32)
extern "C"  void VoidAR__addCurrentShapeImageTarget_m144643407 (Il2CppObject * __this /* static, unused */, int32_t* ___success0, ByteU5BU5D_t3835026402* ___name1, int32_t ___tracking2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_getNewCloudTarget(System.Int32&,System.Byte[],System.Byte[],System.Byte[])
extern "C"  void VoidAR__getNewCloudTarget_m2546856289 (Il2CppObject * __this /* static, unused */, int32_t* ___newTarget0, ByteU5BU5D_t3835026402* ___urls1, ByteU5BU5D_t3835026402* ___names2, ByteU5BU5D_t3835026402* ___matadata3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_setCloudUser(System.String,System.String,System.Int32)
extern "C"  void VoidAR__setCloudUser_m456182358 (Il2CppObject * __this /* static, unused */, String_t* ___accessKey0, String_t* ___secretKey1, int32_t ___useCloud2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::_setShapeMatchLevel(System.Int32)
extern "C"  void VoidAR__setShapeMatchLevel_m1236546128 (Il2CppObject * __this /* static, unused */, int32_t ___lvl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::_checkNet()
extern "C"  bool VoidAR__checkNet_m2630442504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::_useExtensionTracking(System.Boolean)
extern "C"  bool VoidAR__useExtensionTracking_m2889953485 (Il2CppObject * __this /* static, unused */, bool ___use0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::_addTarget(System.String,System.Byte[],System.Int32)
extern "C"  bool VoidAR__addTarget_m1219800199 (Il2CppObject * __this /* static, unused */, String_t* ___path0, ByteU5BU5D_t3835026402* ___data1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::_finishAddTarget()
extern "C"  bool VoidAR__finishAddTarget_m3800497644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::_removeImageTarget(System.String)
extern "C"  bool VoidAR__removeImageTarget_m3284403673 (Il2CppObject * __this /* static, unused */, String_t* ___targetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoidAR VoidAR::GetInstance()
extern "C"  VoidAR_t451713137 * VoidAR_GetInstance_m3573393179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::setShapeMatchLevel(System.Int32)
extern "C"  void VoidAR_setShapeMatchLevel_m764983593 (VoidAR_t451713137 * __this, int32_t ___lvl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::init(System.Int32,System.Int32,System.Boolean)
extern "C"  void VoidAR_init_m3437805449 (VoidAR_t451713137 * __this, int32_t ___cameraIndex0, int32_t ___markerType1, bool ___extensionTracking2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::initBGTexture()
extern "C"  void VoidAR_initBGTexture_m1773369504 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::setScreenSize(System.Int32&,System.Int32&)
extern "C"  void VoidAR_setScreenSize_m2831642495 (VoidAR_t451713137 * __this, int32_t* ___width0, int32_t* ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::isMarkerExist(System.String)
extern "C"  bool VoidAR_isMarkerExist_m1590497145 (VoidAR_t451713137 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::addCloudTarget(Marker,UnityEngine.GameObject)
extern "C"  void VoidAR_addCloudTarget_m1660151627 (VoidAR_t451713137 * __this, Marker_t1240580442 * ___m0, GameObject_t1366199518 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::initShapeLibrary(System.String,System.Int32)
extern "C"  bool VoidAR_initShapeLibrary_m2925494533 (VoidAR_t451713137 * __this, String_t* ___path0, int32_t ___markerType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::buildGLProjectionMatrix(System.Int32,System.Int32)
extern "C"  void VoidAR_buildGLProjectionMatrix_m2599995543 (VoidAR_t451713137 * __this, int32_t ___screenWidth0, int32_t ___sceenHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::startCapture(System.Int32,System.Int32&)
extern "C"  void VoidAR_startCapture_m1392394096 (VoidAR_t451713137 * __this, int32_t ___cameraIndex0, int32_t* ___opened1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::checkNet()
extern "C"  bool VoidAR_checkNet_m1936777097 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::stopCapture()
extern "C"  void VoidAR_stopCapture_m1505908938 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::getMatchResult(System.IntPtr,System.Int32&,System.Int32&,System.Int32&,System.Double[],System.Byte[],System.Int32&,System.Int32[])
extern "C"  void VoidAR_getMatchResult_m624574000 (VoidAR_t451713137 * __this, IntPtr_t ___frame0, int32_t* ___width1, int32_t* ___height2, int32_t* ___flip3, DoubleU5BU5D_t2839599125* ___matrix4, ByteU5BU5D_t3835026402* ___markers5, int32_t* ___mainMarker6, Int32U5BU5D_t3315407976* ___scores7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::unloadResource()
extern "C"  void VoidAR_unloadResource_m606358455 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoidAR::update(UnityEngine.Camera)
extern "C"  int32_t VoidAR_update_m1138037781 (VoidAR_t451713137 * __this, Camera_t2805735124 * ___arCamera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::match()
extern "C"  void VoidAR_match_m3342905039 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::getTrackingTime(System.Double[])
extern "C"  void VoidAR_getTrackingTime_m1491785298 (VoidAR_t451713137 * __this, DoubleU5BU5D_t2839599125* ___times0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::reset()
extern "C"  void VoidAR_reset_m3948994529 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoidAR::getTrackStatus()
extern "C"  int32_t VoidAR_getTrackStatus_m415318551 (VoidAR_t451713137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::addCurrentShapeImageTarget(System.Int32&,System.Byte[],System.Int32)
extern "C"  void VoidAR_addCurrentShapeImageTarget_m2500774288 (VoidAR_t451713137 * __this, int32_t* ___success0, ByteU5BU5D_t3835026402* ___name1, int32_t ___tracking2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::setDebugDraw(System.Int32)
extern "C"  void VoidAR_setDebugDraw_m1312727622 (VoidAR_t451713137 * __this, int32_t ___debugDraw0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 VoidAR::PerspectiveOffCenter(System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  Matrix4x4_t1261955742  VoidAR_PerspectiveOffCenter_m1977762971 (Il2CppObject * __this /* static, unused */, float ___fx0, float ___fy1, float ___cx2, float ___cy3, int32_t ___screenWidth4, int32_t ___screenHeight5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidAR::getMatrix(System.Int32,System.Double[],UnityEngine.Matrix4x4&)
extern "C"  bool VoidAR_getMatrix_m1862633888 (VoidAR_t451713137 * __this, int32_t ___index0, DoubleU5BU5D_t2839599125* ___inMatrix1, Matrix4x4_t1261955742 * ___matrix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::updateTracking(UnityEngine.Camera)
extern "C"  void VoidAR_updateTracking_m3752549890 (VoidAR_t451713137 * __this, Camera_t2805735124 * ___arCamera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoidAR::getCloudInfo(System.String&,System.String&,System.String&)
extern "C"  int32_t VoidAR_getCloudInfo_m205723223 (VoidAR_t451713137 * __this, String_t** ___url0, String_t** ___name1, String_t** ___matadata2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::setCloudUser(System.String,System.String,System.Boolean)
extern "C"  void VoidAR_setCloudUser_m1766345473 (VoidAR_t451713137 * __this, String_t* ___accessKey0, String_t* ___sercetKey1, bool ___useCloud2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::useExentsionTracking(System.Boolean)
extern "C"  void VoidAR_useExentsionTracking_m1015617692 (VoidAR_t451713137 * __this, bool ___use0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::addTarget(VoidAR/Image2ImageTarget)
extern "C"  void VoidAR_addTarget_m1335584111 (VoidAR_t451713137 * __this, Image2ImageTarget_t791379109 * ___image2ImageTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::addTargets(System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>)
extern "C"  void VoidAR_addTargets_m1495199696 (VoidAR_t451713137 * __this, List_1_t4197717748 * ___image2ImageTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR::removeTarget(System.String)
extern "C"  void VoidAR_removeTarget_m2605434269 (VoidAR_t451713137 * __this, String_t* ___targetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
