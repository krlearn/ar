﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidAR/MyDelegate
struct MyDelegate_t3224270133;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1538479585;
// System.AsyncCallback
struct AsyncCallback_t889871978;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object707969140.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "mscorlib_System_String1967731336.h"
#include "mscorlib_System_AsyncCallback889871978.h"

// System.Void VoidAR/MyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void MyDelegate__ctor_m2463674910 (MyDelegate_t3224270133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR/MyDelegate::Invoke(System.String)
extern "C"  void MyDelegate_Invoke_m2465817758 (MyDelegate_t3224270133 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_MyDelegate_t3224270133(Il2CppObject* delegate, String_t* ___str0);
// System.IAsyncResult VoidAR/MyDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MyDelegate_BeginInvoke_m1728350909 (MyDelegate_t3224270133 * __this, String_t* ___str0, AsyncCallback_t889871978 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR/MyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void MyDelegate_EndInvoke_m2377538200 (MyDelegate_t3224270133 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
