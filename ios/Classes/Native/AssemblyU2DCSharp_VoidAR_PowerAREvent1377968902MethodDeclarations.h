﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PowerAREvent_t1377968902;
struct PowerAREvent_t1377968902_marshaled_pinvoke;

extern "C" void PowerAREvent_t1377968902_marshal_pinvoke(const PowerAREvent_t1377968902& unmarshaled, PowerAREvent_t1377968902_marshaled_pinvoke& marshaled);
extern "C" void PowerAREvent_t1377968902_marshal_pinvoke_back(const PowerAREvent_t1377968902_marshaled_pinvoke& marshaled, PowerAREvent_t1377968902& unmarshaled);
extern "C" void PowerAREvent_t1377968902_marshal_pinvoke_cleanup(PowerAREvent_t1377968902_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PowerAREvent_t1377968902;
struct PowerAREvent_t1377968902_marshaled_com;

extern "C" void PowerAREvent_t1377968902_marshal_com(const PowerAREvent_t1377968902& unmarshaled, PowerAREvent_t1377968902_marshaled_com& marshaled);
extern "C" void PowerAREvent_t1377968902_marshal_com_back(const PowerAREvent_t1377968902_marshaled_com& marshaled, PowerAREvent_t1377968902& unmarshaled);
extern "C" void PowerAREvent_t1377968902_marshal_com_cleanup(PowerAREvent_t1377968902_marshaled_com& marshaled);
