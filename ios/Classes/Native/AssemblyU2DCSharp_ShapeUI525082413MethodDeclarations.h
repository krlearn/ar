﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShapeUI
struct ShapeUI_t525082413;
// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"

// System.Void ShapeUI::.ctor()
extern "C"  void ShapeUI__ctor_m946005802 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::Start()
extern "C"  void ShapeUI_Start_m3839721906 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::OnGUI()
extern "C"  void ShapeUI_OnGUI_m176602914 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::OnClick(UnityEngine.GameObject)
extern "C"  void ShapeUI_OnClick_m2498337499 (ShapeUI_t525082413 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::Update()
extern "C"  void ShapeUI_Update_m2661239157 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::OnPreRender()
extern "C"  void ShapeUI_OnPreRender_m4062510828 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::OnPostRender()
extern "C"  void ShapeUI_OnPostRender_m3204645719 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::<Start>m__A()
extern "C"  void ShapeUI_U3CStartU3Em__A_m1976121500 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::<Start>m__B()
extern "C"  void ShapeUI_U3CStartU3Em__B_m147915533 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShapeUI::<Start>m__C()
extern "C"  void ShapeUI_U3CStartU3Em__C_m1693796498 (ShapeUI_t525082413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
