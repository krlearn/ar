﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidARMain
struct VoidARMain_t3733354248;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String1967731336.h"

// System.Void VoidARMain::.ctor()
extern "C"  void VoidARMain__ctor_m560636343 (VoidARMain_t3733354248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain::Awake()
extern "C"  void VoidARMain_Awake_m2447810196 (VoidARMain_t3733354248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain::Start()
extern "C"  void VoidARMain_Start_m1636554803 (VoidARMain_t3733354248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain::Update()
extern "C"  void VoidARMain_Update_m3397719590 (VoidARMain_t3733354248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain::OnDestroy()
extern "C"  void VoidARMain_OnDestroy_m2149002830 (VoidARMain_t3733354248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain::OnApplicationPause(System.Boolean)
extern "C"  void VoidARMain_OnApplicationPause_m3652861797 (VoidARMain_t3733354248 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain::OnApplicationQuit()
extern "C"  void VoidARMain_OnApplicationQuit_m3047144225 (VoidARMain_t3733354248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoidARMain::LoadMainGameObject(System.String,System.String)
extern "C"  Il2CppObject * VoidARMain_LoadMainGameObject_m2131435887 (VoidARMain_t3733354248 * __this, String_t* ___path0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoidARMain::LoadMainGameObjectTest(System.String,System.String,System.String)
extern "C"  Il2CppObject * VoidARMain_LoadMainGameObjectTest_m4094532099 (VoidARMain_t3733354248 * __this, String_t* ___path0, String_t* ___name1, String_t* ___videoInfoStr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoidARMain::AddTargets()
extern "C"  Il2CppObject * VoidARMain_AddTargets_m3043644900 (VoidARMain_t3733354248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
