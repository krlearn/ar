﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4028081426.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidAR/PowerAREvent
struct  PowerAREvent_t1377968902 
{
public:
	// System.Int32 VoidAR/PowerAREvent::TargetID
	int32_t ___TargetID_0;

public:
	inline static int32_t get_offset_of_TargetID_0() { return static_cast<int32_t>(offsetof(PowerAREvent_t1377968902, ___TargetID_0)); }
	inline int32_t get_TargetID_0() const { return ___TargetID_0; }
	inline int32_t* get_address_of_TargetID_0() { return &___TargetID_0; }
	inline void set_TargetID_0(int32_t value)
	{
		___TargetID_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: VoidAR/PowerAREvent
struct PowerAREvent_t1377968902_marshaled_pinvoke
{
	int32_t ___TargetID_0;
};
// Native definition for marshalling of: VoidAR/PowerAREvent
struct PowerAREvent_t1377968902_marshaled_com
{
	int32_t ___TargetID_0;
};
