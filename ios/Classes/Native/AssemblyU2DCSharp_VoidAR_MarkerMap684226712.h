﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;
// Marker
struct Marker_t1240580442;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidAR/MarkerMap
struct  MarkerMap_t684226712  : public Il2CppObject
{
public:
	// UnityEngine.GameObject VoidAR/MarkerMap::gameObject
	GameObject_t1366199518 * ___gameObject_0;
	// Marker VoidAR/MarkerMap::marker
	Marker_t1240580442 * ___marker_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(MarkerMap_t684226712, ___gameObject_0)); }
	inline GameObject_t1366199518 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t1366199518 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t1366199518 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_0, value);
	}

	inline static int32_t get_offset_of_marker_1() { return static_cast<int32_t>(offsetof(MarkerMap_t684226712, ___marker_1)); }
	inline Marker_t1240580442 * get_marker_1() const { return ___marker_1; }
	inline Marker_t1240580442 ** get_address_of_marker_1() { return &___marker_1; }
	inline void set_marker_1(Marker_t1240580442 * value)
	{
		___marker_1 = value;
		Il2CppCodeGenWriteBarrier(&___marker_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
