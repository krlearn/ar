﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidARMain/<LoadMainGameObjectTest>c__Iterator2
struct U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VoidARMain/<LoadMainGameObjectTest>c__Iterator2::.ctor()
extern "C"  void U3CLoadMainGameObjectTestU3Ec__Iterator2__ctor_m664999829 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoidARMain/<LoadMainGameObjectTest>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectTestU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4278228499 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoidARMain/<LoadMainGameObjectTest>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectTestU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m431667771 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidARMain/<LoadMainGameObjectTest>c__Iterator2::MoveNext()
extern "C"  bool U3CLoadMainGameObjectTestU3Ec__Iterator2_MoveNext_m3888402055 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain/<LoadMainGameObjectTest>c__Iterator2::Dispose()
extern "C"  void U3CLoadMainGameObjectTestU3Ec__Iterator2_Dispose_m2412491932 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain/<LoadMainGameObjectTest>c__Iterator2::Reset()
extern "C"  void U3CLoadMainGameObjectTestU3Ec__Iterator2_Reset_m364025674 (U3CLoadMainGameObjectTestU3Ec__Iterator2_t703256831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
