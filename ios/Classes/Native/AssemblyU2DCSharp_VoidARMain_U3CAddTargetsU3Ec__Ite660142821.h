﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2005073387;
// System.Collections.Generic.List`1<VoidAR/Image2ImageTarget>
struct List_1_t4197717748;
// Marker
struct Marker_t1240580442;
// VoidAR/Image2ImageTarget
struct Image2ImageTarget_t791379109;
// UnityEngine.WWW
struct WWW_t3146501818;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidARMain/<AddTargets>c__Iterator3
struct  U3CAddTargetsU3Ec__Iterator3_t660142821  : public Il2CppObject
{
public:
	// UnityEngine.GameObject[] VoidARMain/<AddTargets>c__Iterator3::<gameObjects>__0
	GameObjectU5BU5D_t2005073387* ___U3CgameObjectsU3E__0_0;
	// System.Collections.Generic.List`1<VoidAR/Image2ImageTarget> VoidARMain/<AddTargets>c__Iterator3::<image2ImageTargetes>__1
	List_1_t4197717748 * ___U3Cimage2ImageTargetesU3E__1_1;
	// System.Int32 VoidARMain/<AddTargets>c__Iterator3::<i>__2
	int32_t ___U3CiU3E__2_2;
	// Marker VoidARMain/<AddTargets>c__Iterator3::<marker>__3
	Marker_t1240580442 * ___U3CmarkerU3E__3_3;
	// VoidAR/Image2ImageTarget VoidARMain/<AddTargets>c__Iterator3::<obj>__4
	Image2ImageTarget_t791379109 * ___U3CobjU3E__4_4;
	// UnityEngine.WWW VoidARMain/<AddTargets>c__Iterator3::<file>__5
	WWW_t3146501818 * ___U3CfileU3E__5_5;
	// System.Int32 VoidARMain/<AddTargets>c__Iterator3::$PC
	int32_t ___U24PC_6;
	// System.Object VoidARMain/<AddTargets>c__Iterator3::$current
	Il2CppObject * ___U24current_7;

public:
	inline static int32_t get_offset_of_U3CgameObjectsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U3CgameObjectsU3E__0_0)); }
	inline GameObjectU5BU5D_t2005073387* get_U3CgameObjectsU3E__0_0() const { return ___U3CgameObjectsU3E__0_0; }
	inline GameObjectU5BU5D_t2005073387** get_address_of_U3CgameObjectsU3E__0_0() { return &___U3CgameObjectsU3E__0_0; }
	inline void set_U3CgameObjectsU3E__0_0(GameObjectU5BU5D_t2005073387* value)
	{
		___U3CgameObjectsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgameObjectsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3Cimage2ImageTargetesU3E__1_1() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U3Cimage2ImageTargetesU3E__1_1)); }
	inline List_1_t4197717748 * get_U3Cimage2ImageTargetesU3E__1_1() const { return ___U3Cimage2ImageTargetesU3E__1_1; }
	inline List_1_t4197717748 ** get_address_of_U3Cimage2ImageTargetesU3E__1_1() { return &___U3Cimage2ImageTargetesU3E__1_1; }
	inline void set_U3Cimage2ImageTargetesU3E__1_1(List_1_t4197717748 * value)
	{
		___U3Cimage2ImageTargetesU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3Cimage2ImageTargetesU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_2() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U3CiU3E__2_2)); }
	inline int32_t get_U3CiU3E__2_2() const { return ___U3CiU3E__2_2; }
	inline int32_t* get_address_of_U3CiU3E__2_2() { return &___U3CiU3E__2_2; }
	inline void set_U3CiU3E__2_2(int32_t value)
	{
		___U3CiU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CmarkerU3E__3_3() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U3CmarkerU3E__3_3)); }
	inline Marker_t1240580442 * get_U3CmarkerU3E__3_3() const { return ___U3CmarkerU3E__3_3; }
	inline Marker_t1240580442 ** get_address_of_U3CmarkerU3E__3_3() { return &___U3CmarkerU3E__3_3; }
	inline void set_U3CmarkerU3E__3_3(Marker_t1240580442 * value)
	{
		___U3CmarkerU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmarkerU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__4_4() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U3CobjU3E__4_4)); }
	inline Image2ImageTarget_t791379109 * get_U3CobjU3E__4_4() const { return ___U3CobjU3E__4_4; }
	inline Image2ImageTarget_t791379109 ** get_address_of_U3CobjU3E__4_4() { return &___U3CobjU3E__4_4; }
	inline void set_U3CobjU3E__4_4(Image2ImageTarget_t791379109 * value)
	{
		___U3CobjU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobjU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CfileU3E__5_5() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U3CfileU3E__5_5)); }
	inline WWW_t3146501818 * get_U3CfileU3E__5_5() const { return ___U3CfileU3E__5_5; }
	inline WWW_t3146501818 ** get_address_of_U3CfileU3E__5_5() { return &___U3CfileU3E__5_5; }
	inline void set_U3CfileU3E__5_5(WWW_t3146501818 * value)
	{
		___U3CfileU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfileU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAddTargetsU3Ec__Iterator3_t660142821, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
