﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidAR/MatchingResultDelegate
struct MatchingResultDelegate_t688824355;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1538479585;
// System.AsyncCallback
struct AsyncCallback_t889871978;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object707969140.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "mscorlib_System_AsyncCallback889871978.h"

// System.Void VoidAR/MatchingResultDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void MatchingResultDelegate__ctor_m1322320478 (MatchingResultDelegate_t688824355 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR/MatchingResultDelegate::Invoke(System.Int32)
extern "C"  void MatchingResultDelegate_Invoke_m2642080543 (MatchingResultDelegate_t688824355 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_MatchingResultDelegate_t688824355(Il2CppObject* delegate, int32_t ___result0);
// System.IAsyncResult VoidAR/MatchingResultDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MatchingResultDelegate_BeginInvoke_m2914422188 (MatchingResultDelegate_t688824355 * __this, int32_t ___result0, AsyncCallback_t889871978 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidAR/MatchingResultDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void MatchingResultDelegate_EndInvoke_m3991656732 (MatchingResultDelegate_t688824355 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
