﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DynamicLoadUI
struct DynamicLoadUI_t1834292937;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"

// System.Void DynamicLoadUI::.ctor()
extern "C"  void DynamicLoadUI__ctor_m1247451118 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::initConfig()
extern "C"  void DynamicLoadUI_initConfig_m503738412 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::Start()
extern "C"  void DynamicLoadUI_Start_m439724174 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::OnGUI()
extern "C"  void DynamicLoadUI_OnGUI_m259973830 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::OnClick(UnityEngine.GameObject)
extern "C"  void DynamicLoadUI_OnClick_m1489096455 (DynamicLoadUI_t1834292937 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::Update()
extern "C"  void DynamicLoadUI_Update_m2189177905 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::OnPreRender()
extern "C"  void DynamicLoadUI_OnPreRender_m1813738144 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::OnPostRender()
extern "C"  void DynamicLoadUI_OnPostRender_m2167730739 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DynamicLoadUI::LoadGameObjects()
extern "C"  Il2CppObject * DynamicLoadUI_LoadGameObjects_m1267458712 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::<Start>m__6()
extern "C"  void DynamicLoadUI_U3CStartU3Em__6_m3672923077 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::<Start>m__7()
extern "C"  void DynamicLoadUI_U3CStartU3Em__7_m3814085578 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI::<Start>m__8()
extern "C"  void DynamicLoadUI_U3CStartU3Em__8_m1548837347 (DynamicLoadUI_t1834292937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
