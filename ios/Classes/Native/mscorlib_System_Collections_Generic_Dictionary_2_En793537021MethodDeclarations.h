﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En684356285MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m753113839(__this, ___dictionary0, method) ((  void (*) (Enumerator_t793537021 *, Dictionary_2_t3496208296 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4055336142(__this, method) ((  Il2CppObject * (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4289845808(__this, method) ((  void (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3704401837(__this, method) ((  DictionaryEntry_t1445113794  (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2830797568(__this, method) ((  Il2CppObject * (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4045101362(__this, method) ((  Il2CppObject * (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::MoveNext()
#define Enumerator_MoveNext_m3306365281(__this, method) ((  bool (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::get_Current()
#define Enumerator_get_Current_m1048306582(__this, method) ((  KeyValuePair_2_t2053528608  (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m700920019(__this, method) ((  String_t* (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1325371443(__this, method) ((  MarkerMap_t684226712 * (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::Reset()
#define Enumerator_Reset_m1302378957(__this, method) ((  void (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::VerifyState()
#define Enumerator_VerifyState_m2621779608(__this, method) ((  void (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m863193656(__this, method) ((  void (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoidAR/MarkerMap>::Dispose()
#define Enumerator_Dispose_m989223531(__this, method) ((  void (*) (Enumerator_t793537021 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
