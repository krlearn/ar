﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DynamicLoadUI/<LoadGameObjects>c__Iterator0
struct U3CLoadGameObjectsU3Ec__Iterator0_t4032670743;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DynamicLoadUI/<LoadGameObjects>c__Iterator0::.ctor()
extern "C"  void U3CLoadGameObjectsU3Ec__Iterator0__ctor_m750538106 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DynamicLoadUI/<LoadGameObjects>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadGameObjectsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1857533900 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DynamicLoadUI/<LoadGameObjects>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadGameObjectsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3175333924 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DynamicLoadUI/<LoadGameObjects>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadGameObjectsU3Ec__Iterator0_MoveNext_m2895991842 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI/<LoadGameObjects>c__Iterator0::Dispose()
extern "C"  void U3CLoadGameObjectsU3Ec__Iterator0_Dispose_m3648127845 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicLoadUI/<LoadGameObjects>c__Iterator0::Reset()
extern "C"  void U3CLoadGameObjectsU3Ec__Iterator0_Reset_m2294091015 (U3CLoadGameObjectsU3Ec__Iterator0_t4032670743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
