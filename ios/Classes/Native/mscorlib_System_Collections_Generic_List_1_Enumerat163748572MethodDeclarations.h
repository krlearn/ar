﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerato80338603MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<VoidAR/Image2ImageTarget>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2840438388(__this, ___l0, method) ((  void (*) (Enumerator_t163748572 *, List_1_t4197717748 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VoidAR/Image2ImageTarget>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1414659366(__this, method) ((  void (*) (Enumerator_t163748572 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<VoidAR/Image2ImageTarget>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1501611224(__this, method) ((  Il2CppObject * (*) (Enumerator_t163748572 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VoidAR/Image2ImageTarget>::Dispose()
#define Enumerator_Dispose_m457248269(__this, method) ((  void (*) (Enumerator_t163748572 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VoidAR/Image2ImageTarget>::VerifyState()
#define Enumerator_VerifyState_m3171147554(__this, method) ((  void (*) (Enumerator_t163748572 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<VoidAR/Image2ImageTarget>::MoveNext()
#define Enumerator_MoveNext_m2807011299(__this, method) ((  bool (*) (Enumerator_t163748572 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<VoidAR/Image2ImageTarget>::get_Current()
#define Enumerator_get_Current_m4042599567(__this, method) ((  Image2ImageTarget_t791379109 * (*) (Enumerator_t163748572 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
