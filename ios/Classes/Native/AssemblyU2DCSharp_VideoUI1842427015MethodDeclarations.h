﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoUI
struct VideoUI_t1842427015;
// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"

// System.Void VideoUI::.ctor()
extern "C"  void VideoUI__ctor_m2018193328 (VideoUI_t1842427015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoUI::Start()
extern "C"  void VideoUI_Start_m1606344332 (VideoUI_t1842427015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoUI::OnClick(UnityEngine.GameObject)
extern "C"  void VideoUI_OnClick_m4151328389 (VideoUI_t1842427015 * __this, GameObject_t1366199518 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoUI::<Start>m__D()
extern "C"  void VideoUI_U3CStartU3Em__D_m1701572033 (VideoUI_t1842427015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
