﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2805735124;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "AssemblyU2DCSharp_VoidARMain_EMarkerType3099013493.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoidARMain
struct  VoidARMain_t3733354248  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.Camera VoidARMain::ARCamera
	Camera_t2805735124 * ___ARCamera_2;
	// VoidARMain/EMarkerType VoidARMain::markerType
	int32_t ___markerType_3;
	// System.Int32 VoidARMain::shapeMatchAccuracy
	int32_t ___shapeMatchAccuracy_4;
	// System.Boolean VoidARMain::is_use_cloud
	bool ___is_use_cloud_5;
	// System.String VoidARMain::accessKey
	String_t* ___accessKey_6;
	// System.String VoidARMain::secretKey
	String_t* ___secretKey_7;
	// System.Boolean VoidARMain::UseExtensionTracking
	bool ___UseExtensionTracking_8;
	// System.Boolean VoidARMain::tracking
	bool ___tracking_9;
	// System.Int32 VoidARMain::CameraIndex
	int32_t ___CameraIndex_10;

public:
	inline static int32_t get_offset_of_ARCamera_2() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___ARCamera_2)); }
	inline Camera_t2805735124 * get_ARCamera_2() const { return ___ARCamera_2; }
	inline Camera_t2805735124 ** get_address_of_ARCamera_2() { return &___ARCamera_2; }
	inline void set_ARCamera_2(Camera_t2805735124 * value)
	{
		___ARCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___ARCamera_2, value);
	}

	inline static int32_t get_offset_of_markerType_3() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___markerType_3)); }
	inline int32_t get_markerType_3() const { return ___markerType_3; }
	inline int32_t* get_address_of_markerType_3() { return &___markerType_3; }
	inline void set_markerType_3(int32_t value)
	{
		___markerType_3 = value;
	}

	inline static int32_t get_offset_of_shapeMatchAccuracy_4() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___shapeMatchAccuracy_4)); }
	inline int32_t get_shapeMatchAccuracy_4() const { return ___shapeMatchAccuracy_4; }
	inline int32_t* get_address_of_shapeMatchAccuracy_4() { return &___shapeMatchAccuracy_4; }
	inline void set_shapeMatchAccuracy_4(int32_t value)
	{
		___shapeMatchAccuracy_4 = value;
	}

	inline static int32_t get_offset_of_is_use_cloud_5() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___is_use_cloud_5)); }
	inline bool get_is_use_cloud_5() const { return ___is_use_cloud_5; }
	inline bool* get_address_of_is_use_cloud_5() { return &___is_use_cloud_5; }
	inline void set_is_use_cloud_5(bool value)
	{
		___is_use_cloud_5 = value;
	}

	inline static int32_t get_offset_of_accessKey_6() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___accessKey_6)); }
	inline String_t* get_accessKey_6() const { return ___accessKey_6; }
	inline String_t** get_address_of_accessKey_6() { return &___accessKey_6; }
	inline void set_accessKey_6(String_t* value)
	{
		___accessKey_6 = value;
		Il2CppCodeGenWriteBarrier(&___accessKey_6, value);
	}

	inline static int32_t get_offset_of_secretKey_7() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___secretKey_7)); }
	inline String_t* get_secretKey_7() const { return ___secretKey_7; }
	inline String_t** get_address_of_secretKey_7() { return &___secretKey_7; }
	inline void set_secretKey_7(String_t* value)
	{
		___secretKey_7 = value;
		Il2CppCodeGenWriteBarrier(&___secretKey_7, value);
	}

	inline static int32_t get_offset_of_UseExtensionTracking_8() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___UseExtensionTracking_8)); }
	inline bool get_UseExtensionTracking_8() const { return ___UseExtensionTracking_8; }
	inline bool* get_address_of_UseExtensionTracking_8() { return &___UseExtensionTracking_8; }
	inline void set_UseExtensionTracking_8(bool value)
	{
		___UseExtensionTracking_8 = value;
	}

	inline static int32_t get_offset_of_tracking_9() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___tracking_9)); }
	inline bool get_tracking_9() const { return ___tracking_9; }
	inline bool* get_address_of_tracking_9() { return &___tracking_9; }
	inline void set_tracking_9(bool value)
	{
		___tracking_9 = value;
	}

	inline static int32_t get_offset_of_CameraIndex_10() { return static_cast<int32_t>(offsetof(VoidARMain_t3733354248, ___CameraIndex_10)); }
	inline int32_t get_CameraIndex_10() const { return ___CameraIndex_10; }
	inline int32_t* get_address_of_CameraIndex_10() { return &___CameraIndex_10; }
	inline void set_CameraIndex_10(int32_t value)
	{
		___CameraIndex_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
