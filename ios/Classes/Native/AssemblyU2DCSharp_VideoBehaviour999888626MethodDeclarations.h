﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoBehaviour
struct VideoBehaviour_t999888626;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "mscorlib_System_String1967731336.h"

// System.Void VideoBehaviour::.ctor()
extern "C"  void VideoBehaviour__ctor_m1696050063 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::setVideoURL(System.IntPtr,System.String)
extern "C"  void VideoBehaviour_setVideoURL_m517507651 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, String_t* ___videoURL1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VideoBehaviour::updateVideoTexture(System.IntPtr,System.Int32)
extern "C"  int32_t VideoBehaviour_updateVideoTexture_m2281003269 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, int32_t ___textureID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::play(System.IntPtr)
extern "C"  void VideoBehaviour_play_m3310994277 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::pauseVideo(System.IntPtr)
extern "C"  void VideoBehaviour_pauseVideo_m4095381734 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::onExit(System.IntPtr)
extern "C"  void VideoBehaviour_onExit_m2467534378 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VideoBehaviour::seekTo(System.IntPtr,System.Single)
extern "C"  int32_t VideoBehaviour_seekTo_m3643336045 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, float ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr VideoBehaviour::initVideoPlayer()
extern "C"  IntPtr_t VideoBehaviour_initVideoPlayer_m916844338 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VideoBehaviour::getState(System.IntPtr)
extern "C"  int32_t VideoBehaviour_getState_m3474545224 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VideoBehaviour::getWidth(System.IntPtr)
extern "C"  int32_t VideoBehaviour_getWidth_m2218897931 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VideoBehaviour::getHeight(System.IntPtr)
extern "C"  int32_t VideoBehaviour_getHeight_m2717968170 (Il2CppObject * __this /* static, unused */, IntPtr_t ___videoPlayerPtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::Start()
extern "C"  void VideoBehaviour_Start_m3135072211 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::updateShaderProperties(System.Int32,System.Int32)
extern "C"  void VideoBehaviour_updateShaderProperties_m4109932260 (VideoBehaviour_t999888626 * __this, int32_t ___texWidth0, int32_t ___texHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::updateVideoPath()
extern "C"  void VideoBehaviour_updateVideoPath_m2549856422 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::OnDisable()
extern "C"  void VideoBehaviour_OnDisable_m4130485718 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::createTexture(System.Int32,System.Int32)
extern "C"  void VideoBehaviour_createTexture_m1175852098 (VideoBehaviour_t999888626 * __this, int32_t ___videoWidth0, int32_t ___videoHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::OnRenderObject()
extern "C"  void VideoBehaviour_OnRenderObject_m170918469 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr VideoBehaviour::getVideoPlayer()
extern "C"  IntPtr_t VideoBehaviour_getVideoPlayer_m2899693470 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::Update()
extern "C"  void VideoBehaviour_Update_m2848589696 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::OnDestroy()
extern "C"  void VideoBehaviour_OnDestroy_m41376480 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::OnApplicationPause(System.Boolean)
extern "C"  void VideoBehaviour_OnApplicationPause_m2429793449 (VideoBehaviour_t999888626 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::OnFind()
extern "C"  void VideoBehaviour_OnFind_m537416423 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::OnLost()
extern "C"  void VideoBehaviour_OnLost_m1504587150 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoBehaviour::LostOther()
extern "C"  void VideoBehaviour_LostOther_m352837323 (VideoBehaviour_t999888626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
