﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoidARMain/<LoadMainGameObject>c__Iterator1
struct U3CLoadMainGameObjectU3Ec__Iterator1_t888947034;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VoidARMain/<LoadMainGameObject>c__Iterator1::.ctor()
extern "C"  void U3CLoadMainGameObjectU3Ec__Iterator1__ctor_m3933975662 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoidARMain/<LoadMainGameObject>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3936489996 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoidARMain/<LoadMainGameObject>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadMainGameObjectU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m602916020 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoidARMain/<LoadMainGameObject>c__Iterator1::MoveNext()
extern "C"  bool U3CLoadMainGameObjectU3Ec__Iterator1_MoveNext_m725974914 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain/<LoadMainGameObject>c__Iterator1::Dispose()
extern "C"  void U3CLoadMainGameObjectU3Ec__Iterator1_Dispose_m3687080867 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoidARMain/<LoadMainGameObject>c__Iterator1::Reset()
extern "C"  void U3CLoadMainGameObjectU3Ec__Iterator1_Reset_m2185412017 (U3CLoadMainGameObjectU3Ec__Iterator1_t888947034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
